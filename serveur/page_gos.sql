drop table if exists acte CASCADE;
drop table if exists agent_device CASCADE;
drop table if exists agent_sector CASCADE;
drop table if exists agent_activity CASCADE;
drop table if exists alerts_documents CASCADE;
drop table if exists alerts CASCADE;
drop table if exists gps_coordinates CASCADE;
drop table if exists documents CASCADE;
drop table if exists site CASCADE;
drop table if exists sector CASCADE;
drop table if exists district CASCADE;
drop table if exists device CASCADE;
drop table if exists doc_type CASCADE;
drop table if exists agent CASCADE;
drop table if exists access_right_employee_group CASCADE;
drop table if exists employee_group_employee CASCADE;
drop table if exists employee CASCADE;
drop table if exists employee_group CASCADE;
drop table if exists access_right CASCADE;
drop table if exists rules Cascade;
drop table if exists district_rules CASCADE;

CREATE TABLE employee
   (
    employee_id serial NOT NULL  ,
    employee_name varchar(255) NOT NULL  ,
    employee_surname varchar(255) ,
    employee_email varchar(255) UNIQUE,
    employee_password varchar(255) ,
    employee_phone_number varchar(255) UNIQUE,
    employee_matricule varchar(255) UNIQUE,
    employee_login varchar(255) UNIQUE,
    employee_birthdate timestamp ,
    employee_place_of_birth varchar(255) ,
    employee_nationality varchar(255) ,
    employee_adress varchar(255) ,
    last_connection_date timestamp,
    token varchar(255) ,
    is_first_connection boolean not null default true check (is_first_connection in (true, false)), 
    employee_gender varchar(64) not null default 'M' check (employee_gender in ('M','F')) ,
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,
    CONSTRAINT pk_employee PRIMARY KEY (employee_id)
   );
   CREATE TABLE access_right
   (
    access_right_id serial NOT NULL  ,
    access_right_name varchar(255) NOT NULL  UNIQUE,
    access_right_status int2 not null default 0 check (access_right_status in (0, 1, 2, 3, 4, 5)),  
    CONSTRAINT PK_access_right PRIMARY KEY (access_right_id)
   );
    CREATE TABLE employee_group
   (
    employee_group_id serial NOT NULL  ,
    group_name varchar(255) NOT NULL UNIQUE ,
    group_status int2 not null default 0 check (group_status in (0, 1, 2, 3, 4, 5)),  
    CONSTRAINT PK_employee_group PRIMARY KEY (employee_group_id)
   );
CREATE TABLE employee_group_employee
   (
    employee_group_id int8 NOT NULL  ,
    employee_id int8 NOT NULL  ,
    from_date timestamp NOT NULL default NOW() ,
    end_date timestamp NULL,  
   CONSTRAINT PK_employee_group_employee PRIMARY KEY (employee_group_id, employee_id, from_date)
   );


CREATE  INDEX I_FK_employee_group_employee_employee_group
     ON employee_group_employee (employee_group_id)
    ;

CREATE  INDEX I_FK_employee_group_employee_employee
     ON employee_group_employee (employee_id)
    ;
	
ALTER TABLE employee_group_employee ADD 
     CONSTRAINT FK_employee_group_employee_employee_group
          FOREIGN KEY (employee_group_id)
               REFERENCES employee_group (employee_group_id)
			   on update cascade on delete cascade;

ALTER TABLE employee_group_employee ADD 
     CONSTRAINT FK_employee_group_employee_employee
          FOREIGN KEY (employee_id)
               REFERENCES employee (employee_id)
			   on update cascade on delete cascade;

CREATE TABLE access_right_employee_group
   (
    employee_group_id int8 NOT NULL  ,
    access_right_id int8 NOT NULL  ,
    from_date timestamp NOT NULL  DEFAULT NOW(),
    end_date timestamp NULL,  
   CONSTRAINT PK_access_right_employee_group PRIMARY KEY (employee_group_id, access_right_id, from_date)
   );


CREATE  INDEX I_FK_access_right_employee_group_USE
     ON access_right_employee_group (employee_group_id)
    ;

CREATE  INDEX I_FK_access_right_employee_group_ACC
     ON access_right_employee_group (access_right_id)
    ;
	
ALTER TABLE access_right_employee_group ADD 
     CONSTRAINT FK_access_right_employee_group_USERGRO
          FOREIGN KEY (employee_group_id)
               REFERENCES employee_group (employee_group_id)
			   on update cascade on delete cascade;

ALTER TABLE access_right_employee_group ADD 
     CONSTRAINT FK_access_right_employee_group_ACCESSR
          FOREIGN KEY (access_right_id)
               REFERENCES access_right (access_right_id)
			   on update cascade on delete cascade;
			   
CREATE TABLE sector
   (
    sector_id serial NOT NULL  ,
    sector_name varchar(255) NOT NULL UNIQUE,
    description varchar(255) ,
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,
    CONSTRAINT pk_sector PRIMARY KEY (sector_id)
   );
 CREATE TABLE rules
   (
    rules_id serial NOT NULL  ,
    rules_name varchar(255) NOT NULL UNIQUE,
    description varchar(255) ,
	sous_zone varchar(255) ,
	code varchar(255) ,
	localisation varchar(255) ,
	superficie_constructible varchar(255) ,
	hauteur_maximale varchar(255) ,
	coefficient_occupation_sol varchar(255) ,
	coefficient_emprise_sol varchar(255) ,
	recul_rapport_aux_voies varchar(255) ,
	implantation_limites_separatives varchar(255) ,
	distance_entre_constructions varchar(255) ,
	espace_libre varchar(255) ,
	espace_plante varchar(255) ,
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,
    CONSTRAINT pk_rules PRIMARY KEY (rules_id)
   );
   
CREATE TABLE district
   (
    district_id serial NOT NULL  ,
	sector_id int8,
    district_name varchar(255) NOT NULL UNIQUE,
	commune varchar(255) not null,
	standing varchar(255) not null,
	population varchar(255) not null,
	densite varchar(255) not null,
    description varchar(255) ,
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,
    CONSTRAINT pk_district PRIMARY KEY (district_id)
   );
  
   CREATE TABLE district_rules
   (
    district_id int8 not null,
    rules_id int8 not null,
    create_date timestamp NOT NULL default NOW()  ,
    end_date timestamp NULL,
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,  
    CONSTRAINT pk_district_rules PRIMARY KEY (district_id, rules_id,create_date)
   );
create index i_fk_district_rules_district
	on district_rules (district_id);
create index i_fk_district_rules_rules
	on district_rules (rules_id);
alter table district_rules add 
	constraint fk_district_rules_district
		foreign key (district_id)
			references district (district_id)
				on update cascade on delete cascade;
alter table district_rules add 
	constraint fk_district_rules_rules
		foreign key (rules_id)
			references rules (rules_id)
				on update cascade on delete cascade;   

CREATE TABLE device
   (
    device_id serial NOT NULL,
    device_name varchar(255) NOT NULL UNIQUE,
    device_imei varchar(255) NOT NULL UNIQUE,
    description varchar(255),
    createdDate timestamp DEFAULT NOW(),
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,		
    CONSTRAINT pk_device PRIMARY KEY (device_id)
   );

CREATE TABLE doc_type
   (
    doc_type_id serial NOT NULL  ,
    doc_type_name varchar(255) not null default 'TEXT' check (doc_type_name in ('TEXT', 'IMAGE', 'AUDIO', 'VIDEO')) ,
    description varchar(255),  
    CONSTRAINT pk_doc_type PRIMARY KEY (doc_type_id)
   );

CREATE TABLE agent
   (
    agent_id serial NOT NULL  ,
    agent_name varchar(255) NOT NULL UNIQUE,
    agent_login varchar(255) NOT NULL UNIQUE,
    agent_password varchar(255) NOT NULL,
    is_first_connection boolean not null default true check (is_first_connection in (true, false)),
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,
    agent_phone_number varchar(255),
    last_connection_date timestamp,
    token varchar(255) ,  
   CONSTRAINT pk_agent PRIMARY KEY (agent_id)
   );
 
CREATE TABLE agent_sector
   (
    agent_id int8 not null,
    sector_id int8 not null,
    begin_date timestamp NOT NULL default NOW()  ,
    end_date timestamp NULL,  
    CONSTRAINT pk_agent_sector PRIMARY KEY (agent_id, sector_id, begin_date)
   );
create index i_fk_agent_sector_agent
	on agent_sector (agent_id);
create index i_fk_agent_sector_sector
	on agent_sector (sector_id);
alter table agent_sector add 
	constraint fk_agent_sector_agent
		foreign key (agent_id)
			references agent (agent_id)
				on update cascade on delete cascade;
alter table agent_sector add 
	constraint fk_agent_sector_sector
		foreign key (sector_id)
			references sector (sector_id)
				on update cascade on delete cascade;				
CREATE TABLE agent_device
   (
    agent_id int8 not null,
    device_id int8 not null,
    begin_date timestamp NOT NULL default NOW() ,
    end_date timestamp,
   CONSTRAINT pk_agent_device PRIMARY KEY (agent_id, device_id, begin_date)
   ); 
create index i_fk_agent_device_agent
	on agent_device (agent_id);
create index i_fk_agent_device_device
	on agent_device (device_id);
alter table agent_device add 
	constraint fk_agent_device_agent
		foreign key (agent_id)
			references agent (agent_id)
				on update cascade on delete cascade;
alter table agent_device add 
	constraint fk_agent_device_device
		foreign key (device_id)
			references device (device_id)
				on update cascade on delete cascade;
CREATE TABLE site
   (
    site_id serial NOT NULL  ,
    district_id int8 not null ,
    ref_site varchar(255) NOT NULL,
    site_name varchar(255) NOT NULL,
    owner_name varchar(255) ,
    owner_phone varchar(255),
    owner_cni varchar(255),
    description varchar(255),
	date_to_stop timestamp, --Date d'arrêt des travaux
	date_to_finish timestamp, -- Date de fin du chantier dans le cas où le chantier a pu se dérouler jusqu'o la fin
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,
    CONSTRAINT pk_site PRIMARY KEY (site_id)
   );
 create index i_fk_site_district
	on site (district_id); 
alter table site add 
	constraint fk_site_district
		foreign key (district_id)
			references district (district_id)
				on update cascade on delete cascade;
 CREATE TABLE documents
   (
    document_id serial NOT NULL  ,
    site_id int8 null,
    doc_type_id int8 not null,
    document_name varchar(255) NOT NULL,
	document_code varchar(255),
	document_number varchar(255),
    document_path varchar(255),
    document_extension varchar(255),
    secret_key varchar(255),
    created_date varchar(255),
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,  
   CONSTRAINT pk_documents PRIMARY KEY (document_id)
   );
create index i_fk_documents_site
	on documents (site_id);
		
create index i_fk_documents_doc_type
	on documents (doc_type_id);
	
alter table documents add 
	constraint fk_documents_site
		foreign key (site_id)
			references site (site_id)
				on update cascade on delete cascade;
alter table documents add 
	constraint fk_documents_doc_type
		foreign key (doc_type_id)
			references doc_type (doc_type_id)
				on update cascade on delete cascade;

CREATE TABLE gps_coordinates
   (
    gps_coord_id serial not null,
    site_id int8 not null,
    point_name varchar(255) NOT NULL,
    x_coord real,
    y_coord real, 
    CONSTRAINT pk_gps_coordonnees PRIMARY KEY (gps_coord_id)
   );
 create index i_fk_gps_coordonnees_site
	on gps_coordinates (site_id);
	
alter table gps_coordinates add 
	constraint fk_gps_coordonnees_site
		foreign key (site_id)
			references site (site_id)
				on update cascade on delete cascade;

CREATE TABLE acte
	(
	acte_id serial not null,
	intitule varchar(255),
	acte_date timestamp not null default now(),
	CONSTRAINT pk_acte PRIMARY KEY (acte_id)
	);
				
CREATE TABLE agent_activity
   (
    agent_activity_id serial not null,
    agent_id int8 not null,
	document_id int8,
    agent_action varchar(255),
    status int2 not null default 0 check (status in (0, 1)) ,
    agent_activity_date timestamp NOT NULL default NOW(),
    CONSTRAINT pk_agent_activity PRIMARY KEY (agent_activity_id)
   );
 create index i_fk_agent_activity_agent
	on agent_activity (agent_id);
	
alter table agent_activity add 
	constraint fk_agent_activity_agent
		foreign key (agent_id)
			references agent (agent_id)
				on update cascade on delete cascade;

create index i_fk_agent_activity_documents
	on agent_activity (document_id);
	
alter table agent_activity add 
	constraint fk_agent_activity_documents
		foreign key (document_id)
			references documents (document_id)
				on update cascade on delete cascade;
CREATE TABLE alerts
   (
    alerts_id serial not null,
    alerts_message varchar(255) not null,
	alerts_description varchar(255),
    person_name varchar(255) not null,
    person_phone varchar(255) not null,
    alerts_district varchar(255) not null,
    alerts_date timestamp NOT NULL default NOW(),
	status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,
    CONSTRAINT pk_alerts PRIMARY KEY (alerts_id)
   );
   CREATE TABLE alerts_documents
   (
    document_id serial NOT NULL,
    alerts_id int8 not null,
    document_name varchar(255),
    document_path varchar(255),
    document_extension varchar(255),
    doc_type varchar(255),
    secret_key varchar(255),
    created_date varchar(255),
    status int2 not null default 0 check (status in (0, 1, 2, 3, 4, 5)) ,  
   CONSTRAINT pk_alerts_documents PRIMARY KEY (document_id)
   );
create index i_fk_alerts_documents_alerts
	on alerts_documents (alerts_id);
	
alter table alerts_documents add 
	constraint fk_alerts_documents_alerts
		foreign key (alerts_id)
			references alerts (alerts_id)
				on update cascade on delete cascade;

insert into employee (employee_name, employee_surname, employee_email, employee_password, employee_phone_number, employee_matricule, employee_login, employee_birthdate, employee_place_of_birth, employee_nationality, employee_adress, is_first_connection)
values
	('The priviledge', 'Administrator', 'admin@gmail.com', '6HVfqk184-)-DsmWBcVpxgdQb-@b-@', '654789654', 'M65472C', 'admin', '1990-04-23', 'Yaoundé', 'Camerounais', 'Bastos-Yaoundé', false);

insert into agent (agent_name, agent_login, agent_password, is_first_connection, agent_phone_number)
values
	('Dongo Christian', 'christian', 'G2g/m0GNwP1C37r9/0wntgb-@b-@', false, '695874520'),
	('Onana paul', 'paul', 'yU7WGNhdfWrmZi16EpqmXQb-@b-@', false, '678951009'),
	('Zang Christophe', 'christophe', '7iVg7mMU2egL-)-nVSpPczNQb-@b-@', false, '654128795'),
	('Ateba Jean', 'jean', 'qe2Uqy86gpfNjwejHLZv2gb-@b-@', false, '679654120');

insert into district
	(district_name, commune, standing, population, densite, description, status)
values
	('Cite Verte', 'Yaounde 2', 'Moyen Standing', 0, 0, ' ' ,0),
	('Madagascar', 'Yaounde 2', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Mokolo', 'Yaounde 2', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Grand-Messa', 'Yaounde 2', 'Moyen Standing', 0, 0, ' ' ,0),
	('Ekoudou', 'Yaounde 2', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Nsimeyong 1', 'Yaounde 3', 'Moyen Standing', 0, 0, ' ' ,0),
	('Mballa 2 ', 'Yaounde 1', 'Haut Standing', 0, 0, ' ' ,0),
	('Tsinga', 'Yaounde 2', 'Moyen Standing', 0, 0, ' ' ,0),
	('Nkom-Kana ', 'Yaounde 2', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Messa Carriere', 'Yaounde 2', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Mballa 3', 'Yaounde 1', 'Moyen Standing', 0, 0, ' ' ,0),
	('Oliga ', 'Yaounde 2', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Obili ', 'Yaounde 3', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Etetak ', 'Yaounde 7', 'Periurbain', 0, 0, ' ' ,0),
	('Oyom-Abang ', 'Yaounde 3', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Melen 8B et C ', 'Yaounde 6', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Etoug-Ebe 2 ', 'Yaounde 6', 'Periurbain', 0, 0, ' ' ,0),
	('Mvog-betsi ', 'Yaounde 6', 'Periurbain', 0, 0, ' ' ,0),
	('Nkolbisson ', 'Yaounde 7', 'Periurbain', 0, 0, ' ' ,0),
	('Mendong 2 ', 'Yaounde 6', 'Periurbain', 0, 0, ' ' ,0),
	('Ahala 1 ', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Ngoa-Ekele 1 ', 'Yaounde 3', 'Spontane', 0, 0, ' ' ,0),
	('Efoulan ', 'Yaounde 3', 'Spontane', 0, 0, ' ' ,0),
	('Manguier ', 'Yaounde 1', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Nlong Mvolye ', 'Yaounde 3', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Obobogo ', 'Yaounde 3', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Biyem-Assi ', 'Yaounde 6', 'Moyen Standing', 0, 0, ' ' ,0),
	('Nsam ', 'Yaounde 3', 'Spontane', 0, 0, ' ' ,0),
	('Mvan-Nord ', 'Yaounde 4', 'Haut Standing (peu dense)', 0, 0, ' ' ,0),
	('Tongolo ', 'Yaounde 1', 'Haut Standing', 0, 0, ' ' ,0),
	('Mballa 1', 'Yaounde 1', 'Haut Standing', 0, 0, ' ' ,0),
	('Ndamvout ', 'Yaounde 4', 'Haut Standing', 0, 0, ' ' ,0),
	('Messame-Ndongo ', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Odza ', 'Yaounde 4', 'Haut Standing (peu dense)', 0, 0, ' ' ,0),
	('Mvog-Mbi ', 'Yaounde 4', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Ekoumdoum ', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Awae ', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Nkolondom ', 'Yaounde 1', 'Periurbain', 0, 0, ' ' ,0),
	('Nkomo ', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Quartier Fouda ', 'Yaounde 5', 'Moyen Standing', 0, 0, ' ' ,0),
	('Ekounou ', 'Yaounde 4', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Elig-Essono ', 'Yaounde 1', 'Moyen Standing', 0, 0, ' ' ,0),
	('Ngousso 2 ', 'Yaounde 5', 'Moyen Standing', 0, 0, ' ' ,0),
	('Biteng ', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Etoa-Meki 1', 'Yaounde 1', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Kondengui 1 ', 'Yaounde 4', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Elig-Edzoa ', 'Yaounde 1', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Mimboman 1', 'Yaounde 4', 'Moyen Standing', 0, 0, ' ' ,0),
	('Etam-Bafia', 'Yaounde 4', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Nkol-Ndongo 2 ', 'Yaounde 4', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Melen 2 - Centre Administratif ', 'Yaounde 3', 'Haut Standing', 0, 0, ' ' ,0),
	('Emana', 'Yaounde 1', 'Periurbain', 0, 0, ' ' ,0),
	('Mvog-Ada ', 'Yaounde 5', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Essos ', 'Yaounde 5', 'Moyen Standing', 0, 0, ' ' ,0),
	('Nkol-Messeng ', 'Yaounde 5', 'Periurbain', 0, 0, ' ' ,0),
	('Nkol-Ebogo', 'Yaounde 5', 'Moyen Standing', 0, 0, ' ' ,0),
	('Ecole de Police', 'Yaounde 2', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Abom ', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Ngousso 1', 'Yaounde 5', 'Haut Standing', 0, 0, ' ' ,0),
	('Nkolmesseng 1', 'Yaounde 5', 'Periurbain', 0, 0, ' ' ,0),
	('Afanoya 1 ', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Afanoya 3 ', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Afanoya 4 ', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Eleveur ', 'Yaounde 5', 'Periurbain', 0, 0, ' ' ,0),
	('Centre Commercial', 'Yaounde 1', 'Haut Standing', 0, 0, ' ' ,0),
	('Nkoleton', 'Yaounde 1', 'Periurbain', 0, 0, ' ' ,0),
	('Nlongkak ', 'Yaounde 1', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Afanoya 2 ', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Nkolfon ', 'Yaounde 2', 'Periurbain', 0, 0, ' ' ,0),
	('Febe', 'Yaounde 2', 'Periurbain', 0, 0, ' ' ,0),
	('Ntoungou', 'Yaounde 2', 'Periurbain', 0, 0, ' ' ,0),
	('Mekoumbou 2 ', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Ntouessong', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Mekoumbou 1', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Mebandan', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Etoa', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Mvan-Sud', 'Yaounde 4', 'Haut Standing (peu dense)', 0, 0, ' ' ,0),
	('Ekie ', 'Yaounde 4', 'Spontane', 0, 0, ' ' ,0),
	('Ahala 2', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Nsimeyong 3', 'Yaounde 3', 'Periurbain', 0, 0, ' ' ,0),
	('Mfandena 1 ', 'Yaounde 5', 'Haut Standing', 0, 0, ' ' ,0),
	('Etoudi', 'Yaounde 1', 'Periurbain', 0, 0, ' ' ,0),
	('Messassi', 'Yaounde 1', 'Periurbain', 0, 0, ' ' ,0),
	('Okolo', 'Yaounde 1', 'Periurbain', 0, 0, ' ' ,0),
	('Olembe', 'Yaounde 1', 'Periurbain', 0, 0, ' ' ,0),
	('Nyom', 'Yaounde 1', 'Periurbain', 0, 0, ' ' ,0),
	('Nkolafeme', 'Yaounde 7', 'Periurbain', 0, 0, ' ' ,0),
	('Melen 8', 'Yaounde 6', 'Spontane', 0, 0, ' ' ,0),
	('Emombo', 'Yaounde 4', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Simbock', 'Yaounde 6', 'Periurbain', 0, 0, ' ' ,0),
	('Olezoa', 'Yaounde 3', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Dakar', 'Yaounde 3', 'Spontane', 0, 0, ' ' ,0),
	('Kondengui 3', 'Yaounde 4', 'Moyen Standing', 0, 0, ' ' ,0),
	('Minkoameyos', 'Yaounde 7', 'Periurbain', 0, 0, ' ' ,0),
	('Kondengui 2', 'Yaounde 4', 'Moyen Standing', 0, 0, ' ' ,0),
	('Etoug-Ebe 1', 'Yaounde 6', 'Spontane', 0, 0, ' ' ,0),
	('Melen', 'Yaounde 6', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Elig-Effa', 'Yaounde 6', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Nkolbikok', 'Yaounde 6', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Nkol-Ndongo 1', 'Yaounde 4', 'Moyen Standing', 0, 0, ' ' ,0),
	('Mimboman 3', 'Yaounde 4', 'Moyen Standing', 0, 0, ' ' ,0),
	('Ntui-Essong', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Nkolo', 'Yaounde 4', 'Periurbain', 0, 0, ' ' ,0),
	('Etoa-Meki 2', 'Yaounde 1', 'Moyen Standing', 0, 0, ' ' ,0),
	('Mendong 1', 'Yaounde 6', 'Moyen Standing', 0, 0, ' ' ,0),
	('Mfandena 2', 'Yaounde 5', 'Spontane', 0, 0, ' ' ,0),
	('Nkolso', 'Yaounde 7', 'Moyen Standing', 0, 0, ' ' ,0),
	('Ngousso-Ntem', 'Yaounde 5', 'Periurbain', 0, 0, ' ' ,0),	
	('Ngoulmekong', 'Yaounde 5', 'Periurbain', 0, 0, ' ' ,0),
	('Nsimeyong 2', 'Yaounde 3', 'Moyen Standing', 0, 0, ' ' ,0),
	('Ngoa-Ekele 2', 'Yaounde 3', 'Spontane a structurer', 0, 0, ' ' ,0),
	('Simbock Ecole de guerre', 'Yaounde 6', 'Periurbain', 0, 0, ' ' ,0);
	
insert into sector
	(sector_name, description, status)
values
	('Secteur 1', 'ce secteur est composé des quartiers des arrondissements de Yaoundé 1 et 2 ',0),
	('Secteur 2', 'ce secteur est composé des quartiers des arrondissements de Yaoundé 3 et 4',0),
	('Secteur 3', 'ce secteur est composé des quartiers des arrondissements de Yaoundé 5 et 6',0);
insert into agent_sector
	(agent_id, sector_id)
values
	(1, 1),
	(2, 2),
	(3, 3);

insert into doc_type
	(doc_type_name, description)
values
	('TEXT', 'Fichiers PDF ou Word'),
	('IMAGE', 'Fichiers images'),
	('AUDIO', 'Fichiers audios'),
	('VIDEO', 'Fichiers audios');

insert into device
	(device_name, device_imei,status)
values
	('Samsung Galaxy S3', '458963257',3);
	
insert into device
	(device_name, device_imei)
values
	('Tecno Phantom 8', '896523457'),
	('Htc one', '325697412'),
	('Samsung s7 ege', '325698412'),
	('Sony Xperia Z5', '425698753');

insert into agent_device
	(agent_id, device_id)
values
	(1, 1),
	(2, 2),
	(3, 3),
	(4, 4);
	
insert into access_right
	(access_right_name)
values
	('All'),
	('administrator_right'),
	('employee_right');
	
insert into employee_group
	(group_name)
values
	('Admin'),
	('Employee');
	
insert into employee_group_employee
	(employee_group_id, employee_id)
values
	(1, 1);
	
insert into access_right_employee_group
	(employee_group_id, access_right_id)
values
	(1, 2),
	(2, 3);
INSERT INTO site (district_id, ref_site,site_name,owner_name,owner_phone,owner_cni) 
			VALUES (89,'Tef12','SECEL_SARL','OWONA','654588842','2753689');
			
INSERT INTO site (district_id, ref_site,site_name,owner_name,owner_phone,owner_cni) 
			VALUES (6,'Cha12','Chefferie_Tamtam','NJI Junior','659588842','1853689');
			
INSERT INTO site (district_id, ref_site,site_name,owner_name,owner_phone,owner_cni) 
			VALUES (20,'med102','Kamthe Hotel','Ateba Jean Paul','679588842','5853689');
			
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (1,'A1',11.545682698488235,3.8627272239588555);
INSERT INTO gps_coordinates (site_id,point_name,x_coord,y_coord) 
			VALUES (1,'A2',11.545739024877548,3.862812190633734);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (1,'A3',11.545652523636818,3.8628697271174044);
INSERT INTO gps_coordinates (site_id,point_name,x_coord,y_coord) 
			VALUES (1,'A4',11.545591503381727,3.8627887746218232);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (1,'A5',11.545682698488235,3.8627272239588555);
			
INSERT INTO gps_coordinates (site_id,point_name,x_coord,y_coord) 
			VALUES (2,'A1', 11.488045379519463,3.830546322031619);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (2,'A2',11.488129198551178,3.8306393205653184);
INSERT INTO gps_coordinates (site_id,point_name,x_coord,y_coord) 
			VALUES (2,'A3',11.48808494210243,3.830681470976549);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (2,'A4',11.488095000386236,3.830692844896684);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (2,'A5',11.48807220160961,3.83071090935777);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (2,'A6',11.48806281387806,3.8306975282755262);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (2,'A7',11.488026604056358,3.830730980980747);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (2,'A8',11.487944796681404,3.830641996781976);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (2,'A9',11.488045379519463,3.830546322031619);
			
INSERT INTO gps_coordinates (site_id,point_name,x_coord,y_coord) 
			VALUES (3,'A1',11.469820439815521,3.831163858868357);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (3,'A2',11.46997332572937,3.831169211298364);
INSERT INTO gps_coordinates (site_id,point_name,x_coord,y_coord) 
			VALUES (3,'A3',11.46996796131134, 3.8312494977444347);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (3,'A4',11.46998941898346,3.8312521739591583);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (3,'A5',11.46998941898346,3.8313431652553196);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (3,'A6',11.469876766204834,3.8313458414697634);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (3,'A7',11.469876766204834, 3.831332460397443);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (3,'A8',11.469817757606506, 3.831332460397443);
INSERT INTO gps_coordinates ( site_id,point_name,x_coord,y_coord) 
			VALUES (3,'A9',11.469820439815521,3.831163858868357);
