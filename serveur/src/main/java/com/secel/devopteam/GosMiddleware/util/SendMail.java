package com.secel.devopteam.GosMiddleware.util;

import java.util.ArrayList;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ResourceBundle;

public class SendMail {

	//private static final ResourceBundle smtpBundle = ResourceBundle.getBundle("ressources.smtp");
	
	
	public static Properties buildProperties () {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		//props.put("mail.smtp.host", "mail.pagegos.com ");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		//	props.put("mail.smtp.host", smtpBundle.getString("mail.smtp.host"));
		//	props.put("mail.smtp.socketFactory.port", smtpBundle.getString("mail.smtp.socketFactory.port"));
		//	props.put("mail.smtp.socketFactory.class", smtpBundle.getString("mail.smtp.socketFactory.class"));
		//	props.put("mail.smtp.auth", smtpBundle.getString("mail.smtp.auth"));
		//	props.put("mail.smtp.port", smtpBundle.getString("mail.smtp.port"));
		return props;
	}
	
	/**
     * Send mail to a single user
     *
     * @author Garrik Brel JAGHO
     * @param receiverAdress 
     * @param subject
     * @param content
     * @return
     */
    public static void sendMailToSingleReceiver (String receiverAdress, String subject, String content) throws AddressException, MessagingException {
		Properties props = buildProperties();
        Session session = Session.getDefaultInstance(props,
	                new javax.mail.Authenticator() {
	                    @Override
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication("pagegos48@gmail.com", "devsecel1234");
	                    }
	                }
	            );
        
		Message message = new MimeMessage(session);
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiverAdress));
		message.setSubject(subject);
		message.setContent(content, "text/html; charset=ISO-8859-1");
		Transport transport = session.getTransport("smtp");
	            transport.connect("smtp.gmail.com", "pagegos48@gmail.com", "devsecel1234");
	            transport.sendMessage(message, message.getAllRecipients());
	            transport.close();
    }
    
    /**
     * Send mail to a multiple users
     *
     * @author Garrik Brel JAGHO
     * @param DestList 
     * @param subject
     * @param content
     * @return
     */
//    public static void sendToManyReceivers (ArrayList<String> DestList, String subject, String content ) throws AddressException, MessagingException {
//    	Properties props = buildProperties();
//    	Session session = Session.getDefaultInstance(props,
//                new javax.mail.Authenticator() {
//                    @Override
//                    protected PasswordAuthentication getPasswordAuthentication() {
//                        return new PasswordAuthentication(smtpBundle.getString("mail.session.user"), smtpBundle.getString("mail.session.pass"));
//                    }
//                }
//        );
//		
//		Message message = new MimeMessage(session);
//		for (String s:DestList) {
//			message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(s));
//		}
//		
//		message.setSubject(subject);
//		message.setContent(content, "text/html; charset=ISO-8859-1"); 
//		
//		Transport transport = session.getTransport("smtp");
//        transport.connect(smtpBundle.getString("mail.smtp.host"), smtpBundle.getString("mail.session.user"), smtpBundle.getString("mail.session.pass"));
//        transport.sendMessage(message, message.getAllRecipients());
//        transport.close();
//	}
}
