package com.secel.devopteam.GosMiddleware.api;

import java.util.List;


import com.secel.devopteam.GosMiddleware.beans.DocType;
import com.secel.devopteam.GosMiddleware.dao.DocTypeDao;

public class DocTypeController {
	
	public static DocType createDocType (DocType docType, DocTypeDao docTypeDao) throws Exception {
		return docTypeDao.saveOrUpdateDocType(docType);
	}
	public static List<DocType> getAllDocTypes (DocTypeDao docTypeDao) throws Exception {
		return docTypeDao.getAllDocType();
	}
	public static DocType getDocTypeById (DocType docType, DocTypeDao docTypeDao) throws Exception {
		return docTypeDao.findDocTypeById(docType.getDocTypeId());
	}
	public static DocType updateDocType (DocType docType, DocTypeDao docTypeDao) throws Exception {
		return docTypeDao.saveOrUpdateDocType(docType);
	}

}
