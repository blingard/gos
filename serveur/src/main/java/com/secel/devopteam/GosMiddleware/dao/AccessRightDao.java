package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.AccessRight;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class AccessRightDao extends AbstractDAO<AccessRight> {

	public AccessRightDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public AccessRight saveOrUpdateAccessRight(@Valid AccessRight accessRight) {
		return persist(accessRight);
	}
	
	public AccessRight findById(int id) {
        return get(id);
    }
	
	public List<AccessRight> getAllAccessRights() {
		return this.list(this.query("SELECT a FROM AccessRight a WHERE a.accessRightStatus = :accessRightStatus").setShort("accessRightStatus", GosMiddlewareConstants.STATE_ACTIVATED));
	}
	
	public AccessRight getAccessRightByName(String accessRightName) {
		return this.uniqueResult(this.query("SELECT a FROM AccessRight a WHERE a.accessRightName= : accessRightName").setString("accessRightName", accessRightName));
    }

}
