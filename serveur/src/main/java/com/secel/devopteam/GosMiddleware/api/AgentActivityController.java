package com.secel.devopteam.GosMiddleware.api;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentActivity;
import com.secel.devopteam.GosMiddleware.beans.Documents;
import com.secel.devopteam.GosMiddleware.beans.Site;
import com.secel.devopteam.GosMiddleware.dao.AgentActivityDao;
import com.secel.devopteam.GosMiddleware.model.AlertSystem;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

public class AgentActivityController {
	
	public static AgentActivity createAgentActivity (AgentActivity agentActivity, AgentActivityDao agentActivityDao) throws Exception {
		return agentActivityDao.saveOrUpdateAgentActivity(agentActivity);
	}
	public static List<AgentActivity> getAllAgentActivities (AgentActivityDao agentActivityDao) throws Exception {
		return agentActivityDao.getAllAgentActivitys();
	}
	public static AgentActivity getAgentActivityById (AgentActivity agentActivity, AgentActivityDao agentActivityDao) throws Exception {
		return agentActivityDao.findAgentActivityById(agentActivity.getAgentActivityId());
	}
	public static AgentActivity updateAgentActivity (AgentActivity agentActivity, AgentActivityDao agentActivityDao) throws Exception {
		return agentActivityDao.saveOrUpdateAgentActivity(agentActivity);
	}
	public static List<AgentActivity> getAllActivityForAgent(Agent agent, AgentActivityDao agentActivityDao){
		return agentActivityDao.getAllActivitysByAgent(agent);
				
	}
	public static List<AlertSystem> getAgentActivitiesConvocation(AgentActivityDao agentActivityDao) {
		List<AgentActivity> agentActivitiesConvocation = agentActivityDao.getAgentActivitiesConvocation();
		ArrayList<AlertSystem> alertSystems = new ArrayList<AlertSystem>();
		if(agentActivitiesConvocation.size() == 0) {
			alertSystems = null;
		}else {
			ArrayList<AlertSystem> alerystems = new ArrayList<AlertSystem>();
			for(AgentActivity agentActivity : agentActivitiesConvocation) {
				Documents doc = agentActivity.getDocumentId();
				Site site = doc.getSiteId();
				ArrayList<AgentActivity> agentActivities = new ArrayList<AgentActivity>();
				int index = 0 ;
				for(AgentActivity agentActivit : agentActivitiesConvocation) {
					Documents doc1 = agentActivit.getDocumentId();
					Site site1 = doc1.getSiteId();
					if(site.getSiteId() == site1.getSiteId()) {
						if(agentActivit.getAgentActivityDate().before(agentActivity.getAgentActivityDate()) || agentActivit.getAgentActivityDate().equals(agentActivity.getAgentActivityDate()))
							agentActivities.add(agentActivit);				
					}
					index = index + 1 ;
				}
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(GosMiddlewareConstants.DATE_FORMAT, Locale.FRENCH);
				String toDay = simpleDateFormat.format(new Date());
				final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(GosMiddlewareConstants.DATE_FORMAT, Locale.FRENCH);
				int i = agentActivities.size();
				Documents d = agentActivities.get(i-1).getDocumentId();
			    String datedoc = d.getCreatedDate();
			    LocalDate nowDate = LocalDate.parse(toDay, formatter);
			    LocalDate docDate = LocalDate.parse(datedoc, formatter);
			    long days = ChronoUnit.DAYS.between(docDate, nowDate);
			    final AlertSystem alertSystem = new AlertSystem(agentActivities.get(i-1), days);
			    alerystems.add(alertSystem);
			}
			for(AlertSystem alerstems : alerystems) {
				
				if(alertSystems.size() == 0 ) {
					alertSystems.add(alerstems);
				}else {
					int in = 0 ;
					for(AlertSystem alerstem : alertSystems) {
						if(alerstem.getAgentActivitiesConvocation().getDocumentId().getSiteId().getSiteId()==alerstems.getAgentActivitiesConvocation().getDocumentId().getSiteId().getSiteId()) {
							in = in + 1 ;
						}					
					}
					if(in == 0 ) {
						if(alerstems.getAgentActivitiesConvocation().getStatus()==0) {
							alertSystems.add(alerstems);
						}else{
						}						
					}
				}
				
			}
		}
		return alertSystems;
	}
	public static List<AlertSystem> getAgentActivitiesConvocationKO(AgentActivityDao agentActivityDao) {
		List<AgentActivity> agentActivitiesConvocation = agentActivityDao.getAgentActivitiesConvocationOk();
		ArrayList<AlertSystem> alertSystems = new ArrayList<AlertSystem>();
		if(agentActivitiesConvocation.size() == 0) {
			alertSystems = null;
		}else {
			ArrayList<AlertSystem> alerystems = new ArrayList<AlertSystem>();
			for(AgentActivity agentActivity : agentActivitiesConvocation) {
				Documents doc = agentActivity.getDocumentId();
				Site site = doc.getSiteId();
				ArrayList<AgentActivity> agentActivities = new ArrayList<AgentActivity>();
				int index = 0 ;
				for(AgentActivity agentActivit : agentActivitiesConvocation) {
					Documents doc1 = agentActivit.getDocumentId();
					Site site1 = doc1.getSiteId();
					if(site.getSiteId() == site1.getSiteId()) {
						if(agentActivit.getAgentActivityDate().before(agentActivity.getAgentActivityDate()) || agentActivit.getAgentActivityDate().equals(agentActivity.getAgentActivityDate()))
							agentActivities.add(agentActivit);				
					}
					index = index + 1 ;
				}
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(GosMiddlewareConstants.DATE_FORMAT, Locale.FRENCH);
				String toDay = simpleDateFormat.format(new Date());
				final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(GosMiddlewareConstants.DATE_FORMAT, Locale.FRENCH);
				int i = agentActivities.size();
				Documents d = agentActivities.get(i-1).getDocumentId();
			    String datedoc = d.getCreatedDate();
			    LocalDate nowDate = LocalDate.parse(toDay, formatter);
			    LocalDate docDate = LocalDate.parse(datedoc, formatter);
			    long days = ChronoUnit.DAYS.between(docDate, nowDate);
			    final AlertSystem alertSystem = new AlertSystem(agentActivities.get(i-1), days);
			    alerystems.add(alertSystem);
			}
			for(AlertSystem alerstems : alerystems) {
				
				if(alertSystems.size() == 0 ) {
					alertSystems.add(alerstems);
				}else {
					int in = 0 ;
					for(AlertSystem alerstem : alertSystems) {
						if(alerstem.getAgentActivitiesConvocation().getDocumentId().getSiteId().getSiteId()==alerstems.getAgentActivitiesConvocation().getDocumentId().getSiteId().getSiteId()) {
							in = in + 1 ;
						}					
					}
					if(in == 0 ) {
						if(alerstems.getAgentActivitiesConvocation().getStatus()==0) {
							alertSystems.add(alerstems);
						}else{
						}						
					}
				}
				
			}
		}
		return alertSystems;
	}
	public static AlertSystem setAgentActivitiesConvocation(AlertSystem alertSystem,AgentActivityDao agentActivityDao ) {
		AgentActivity agentActivity = alertSystem.getAgentActivitiesConvocation();
		agentActivity.setStatus((short) 1);
		agentActivity =  agentActivityDao.saveOrUpdateAgentActivity(agentActivity);
		final AlertSystem alertSystems = new AlertSystem(agentActivity, 0);
		return alertSystems;
		
		
		
	}

}
