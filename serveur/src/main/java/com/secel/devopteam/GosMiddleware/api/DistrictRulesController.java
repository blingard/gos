package com.secel.devopteam.GosMiddleware.api;

import java.util.ArrayList;
import java.util.List;

import com.secel.devopteam.GosMiddleware.beans.District;
import com.secel.devopteam.GosMiddleware.beans.DistrictRules;
import com.secel.devopteam.GosMiddleware.beans.Rules;
import com.secel.devopteam.GosMiddleware.dao.DistrictRulesDao;

public class DistrictRulesController {
	public static DistrictRules saveUpdate(DistrictRules districtRule, DistrictRulesDao districtRulesDao) {
		return districtRulesDao.saveUpdateDistrictRules(districtRule);
	}
	
	public static List<District> getAllDistrictForRules(Rules rule, DistrictRulesDao districtRulesDao){
		List<DistrictRules> districtRulesList = districtRulesDao.getAllDistrictForRules(rule);
		if(districtRulesList.isEmpty()) {
			return null;
		}else {
			List<District> districtList = null;
			for(DistrictRules districtRulesListes : districtRulesList) {
				districtList.add(districtRulesListes.getDistrict());
			}
			return districtList;
		}
	}
	
	public static List<Rules> getAllRulesForDistrict(District district, DistrictRulesDao districtRulesDao){
		List<DistrictRules> districtRulesList = districtRulesDao.getAllRulesForDistrict(district);
		if(districtRulesList.isEmpty()) {
			return null;
		}else {
			List<Rules> rulesList = null;
			for(DistrictRules districtRulesListes : districtRulesList) {
				rulesList.add(districtRulesListes.getRules());
			}
			return rulesList;
		}
	}

}
