package com.secel.devopteam.GosMiddleware.dao;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.District;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class DistrictDao extends AbstractDAO<District>{

	public DistrictDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public District saveOrUpdateDistrict(@Valid District district) {
		return persist(district);
	}
	
	public District findDistrictById(int id) {
        return get(id);
    }

	public List<District> getAllDistricts() {
		return this.list(this.query("SELECT s FROM District s")); 
	}
	
	/*public List<District> getAllFreeDistricts() {
		return this.list(this.query("SELECT s FROM District s WHERE s.districtId = :districtId").setParameter("districtId", 0)); 
	}*/
	
	public List<District> getAllFreeDistricts() {
		return this.list(this.query("SELECT s FROM District s WHERE s.status = :status").setShort("status", GosMiddlewareConstants.STATE_ACTIVATED)); 
	}
	public List<District> getAllDistrictOfSector(Sector sector) {
		return this.list(this.query("SELECT d FROM District d WHERE d.sectorId.sectorId = :sectorId").setParameter("sectorId", sector.getSectorId())); 
	}
}
