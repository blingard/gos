/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Blingard
 */
@Embeddable
public class DistrictRulesPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "district_id")
    private long districtId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rules_id")
    private long rulesId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    public DistrictRulesPK() {
    }

    public DistrictRulesPK(long districtId, long rulesId, Date createDate) {
        this.districtId = districtId;
        this.rulesId = rulesId;
        this.createDate = createDate;
    }

    public long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(long districtId) {
        this.districtId = districtId;
    }

    public long getRulesId() {
        return rulesId;
    }

    public void setRulesId(long rulesId) {
        this.rulesId = rulesId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) districtId;
        hash += (int) rulesId;
        hash += (createDate != null ? createDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DistrictRulesPK)) {
            return false;
        }
        DistrictRulesPK other = (DistrictRulesPK) object;
        if (this.districtId != other.districtId) {
            return false;
        }
        if (this.rulesId != other.rulesId) {
            return false;
        }
        if ((this.createDate == null && other.createDate != null) || (this.createDate != null && !this.createDate.equals(other.createDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.secel.devopteam.GosMiddleware.beans.DistrictRulesPK[ districtId=" + districtId + ", rulesId=" + rulesId + ", createDate=" + createDate + " ]";
    }
    
}
