/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ken
 */
@Entity
@Table(name = "agent_activity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AgentActivity.findAll", query = "SELECT a FROM AgentActivity a")
    , @NamedQuery(name = "AgentActivity.findByAgentActivityId", query = "SELECT a FROM AgentActivity a WHERE a.agentActivityId = :agentActivityId")
    , @NamedQuery(name = "AgentActivity.findByAgentAction", query = "SELECT a FROM AgentActivity a WHERE a.agentAction = :agentAction")
    , @NamedQuery(name = "AgentActivity.findByAgentActivityDate", query = "SELECT a FROM AgentActivity a WHERE a.agentActivityDate = :agentActivityDate")})
public class AgentActivity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "agent_activity_id")
    private Integer agentActivityId;
    @Column(name = "agent_action")
    private String agentAction;
    @Column(name = "status")
    private short status;
    @Column(name = "agent_activity_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date agentActivityDate;
    @JoinColumn(name = "agent_id", referencedColumnName = "agent_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Agent agentId;
    @JoinColumn(name = "document_id", referencedColumnName = "document_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Documents documentId;

    public AgentActivity() {
    }

    public AgentActivity(Integer agentActivityId,short status) {
        this.agentActivityId = agentActivityId;
        this.status = status;
    }
    
    

    public Integer getAgentActivityId() {
        return agentActivityId;
    }

    public void setAgentActivityId(Integer agentActivityId) {
        this.agentActivityId = agentActivityId;
    }

    public String getAgentAction() {
        return agentAction;
    }

    public void setAgentAction(String agentAction) {
        this.agentAction = agentAction;
    }
    
    

    public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public Date getAgentActivityDate() {
        return agentActivityDate;
    }

    public void setAgentActivityDate(Date agentActivityDate) {
        this.agentActivityDate = agentActivityDate;
    }

    public Agent getAgentId() {
        return agentId;
    }

    public void setAgentId(Agent agentId) {
        this.agentId = agentId;
    }

    public Documents getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Documents documentId) {
        this.documentId = documentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agentActivityId != null ? agentActivityId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgentActivity)) {
            return false;
        }
        AgentActivity other = (AgentActivity) object;
        if ((this.agentActivityId == null && other.agentActivityId != null) || (this.agentActivityId != null && !this.agentActivityId.equals(other.agentActivityId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.AgentActivity[ agentActivityId=" + agentActivityId + " ]";
    }
    
}
