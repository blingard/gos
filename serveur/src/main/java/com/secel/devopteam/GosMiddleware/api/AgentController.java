package com.secel.devopteam.GosMiddleware.api;

import java.util.ArrayList;
import java.util.List;



import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentDevice;
import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.Device;
import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroupEmployee;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.dao.AgentDao;
import com.secel.devopteam.GosMiddleware.dao.AgentDeviceDao;
import com.secel.devopteam.GosMiddleware.dao.AgentSectorDao;
import com.secel.devopteam.GosMiddleware.dao.DeviceDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeGroupEmployeeDao;
import com.secel.devopteam.GosMiddleware.util.CryptographyException;
import com.secel.devopteam.GosMiddleware.util.Encryptor;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;
import com.secel.devopteam.GosMiddleware.util.SendMail;


/**
 * 
 * @author JAGHO Brel
 *
 */

public class AgentController {
	
	public static Agent createAgent (Agent agent, AgentDao agentDao) throws Exception {
		try{
			String passTest = "paul";
			String nonCryptedPassword = GosMiddlewareConstants.DEFAULT_AGENT_PASSWORD;
			String cryptedPassword = Encryptor.encrypt(nonCryptedPassword);
			agent.setAgentPassword(cryptedPassword);
			agent.setIsFirstConnection(true);
			return agentDao.saveOrUpdateAgent(agent);
		}
		catch (CryptographyException ex ) {
			ex.printStackTrace();
			throw new CryptographyException("Impossible de crypter le mot de passe");
		}
	}
	
	public static List<Agent> getAllAgents (AgentDao agentDao) throws Exception {
		return agentDao.getAllAgents();
	}
	
	public static Agent connectAgentWithLoginAndPass (Agent agent, AgentDao agentDao) throws Exception {
		try{
			String nonCryptedPassword = agent.getAgentPassword();
			String cryptedPassword = Encryptor.encrypt(nonCryptedPassword);
			agent.setAgentPassword(cryptedPassword);
			return agentDao.connectAgentWithLoginAndPass(agent);
		}
		catch (CryptographyException ex ) {
			ex.printStackTrace();
			throw new CryptographyException("Impossible de crypter le mot de passe");
		}
	}
	
	public static Agent getAgentById (Agent agent, AgentDao agentDao) throws Exception {
		return agentDao.findAgentById(agent.getAgentId());
	}
	
	public static List<AgentSector> getAllAgentSectors (Agent agent, AgentSectorDao agentsectorDao) throws Exception {
		return agentsectorDao.getAgentsSectorsByAgent(agent);
	}
	
	public static Sector getAgentsSectorsByAgentForMobile (Agent agent, AgentSectorDao agentsectorDao) throws Exception {
		List<AgentSector> agentSectorList = agentsectorDao.getAgentsSectorsByAgentForMobile(agent);
		Sector sect = null;
		for(AgentSector agentSector : agentSectorList) {
			if(agentSector.getEndDate()==null) {				
				sect = agentSector.getSector();
			}				
		}		
		return sect;
	}
	
	public static Agent updateAgent (Agent agent, AgentDao agentDao) throws Exception {
		return agentDao.saveOrUpdateAgent(agent); 
	}
	
	public static AgentDevice affectDeviceToAgent (AgentDevice agtDevice, AgentDeviceDao agentDeviceDao, DeviceDao deviceDao) throws Exception {
		AgentDevice agentDeviceSave=agentDeviceDao.assignDeviceToAgent(agtDevice, deviceDao);
		return agentDeviceSave;
	}
	
	public static Agent defineNewIdentifiers (Agent agent, AgentDao agentDao) throws Exception {
		try{
			String nonCryptedPassword = agent.getAgentPassword();
			String cryptedPassword = Encryptor.encrypt(nonCryptedPassword);
			agent.setAgentPassword(cryptedPassword);
			return agentDao.saveOrUpdateAgent(agent);
		}
		catch (CryptographyException ex ) {
			ex.printStackTrace();
			throw new CryptographyException("Impossible de crypter le mot de passe");
		} 
	}
	public static AgentDevice updateAgentDeviceEndDate (AgentDevice agDevice, AgentDeviceDao agentDeviceDao) throws Exception {
		return agentDeviceDao.assignDevicesToAgent(agDevice);
	}
	public static AgentSector updateAgentSectorEndDate (AgentSector agSector, AgentSectorDao agentSectorDao) throws Exception {
		return agentSectorDao.updateAgentSectorToAgent(agSector);
	}
	
	public static List<Agent> getAgentsByLogin (Agent agent, AgentDao agentDao) throws Exception {
		return agentDao.getAgentsByLogin(agent);
	}
	public static Agent changeAgentPassword(Agent agent, String oldPassword ,String newPassword, AgentDao agentDao) throws Exception {
		Agent agentBD=agentDao.findAgentById(agent.getAgentId());
		try {
			String cryptedPassword = agentBD.getAgentPassword();
			String decriptedPassword= Encryptor.decrypt(cryptedPassword);
			if(!decriptedPassword.equals(oldPassword)) {
				return null;
			}
			String cryptedNewPassword = Encryptor.encrypt(newPassword);
			agentBD.setIsFirstConnection(false);
			agentBD.setAgentPassword(cryptedNewPassword);
			agent= agentDao.saveOrUpdateAgent(agentBD);
			return agent;	
		}catch (CryptographyException ex ) {
				ex.printStackTrace();
				throw new CryptographyException("Impossible de crypter le mot de passe");
			}
	}
	
	public static List<AgentDevice> getAllAgentDevices (AgentDeviceDao agentDeviceDao) throws Exception {
		List<AgentDevice>l1=agentDeviceDao.getAllAgentDevice();
		List<AgentDevice>l2=new ArrayList<AgentDevice>();
		if(!l1.isEmpty()) {
			for(AgentDevice agDevice: l1) {
				if(agDevice.getEndDate()==null) {
					l2.add(agDevice);
				}
			}
		}
		
		return l2;
	}
	public static List<AgentDevice> getAllAgentDevicesByAgent (AgentDeviceDao agentDeviceDao) throws Exception {
		List<AgentDevice>l1=agentDeviceDao.getAllAgentDevices();
		List<AgentDevice>l2=new ArrayList<AgentDevice>();
		if(!l1.isEmpty()) {
			for(AgentDevice agDevice: l1) {
				if(agDevice.getEndDate()==null) {
					l2.add(agDevice);
				}
			}
		}
		
		return l2;
	}
	public static List<AgentSector> getAllAgentSectors (AgentSectorDao agentSectorDao) throws Exception {
		List<AgentSector>l1=agentSectorDao.getAllAgentsSectors();
		List<AgentSector>l2=new ArrayList<AgentSector>();
		if(!l1.isEmpty()) {
			for(AgentSector agSector: l1) {
				if(agSector.getEndDate()==null) {
					l2.add(agSector);
				}
			}
		}
		
		return l2;
	}
	
	public static Agent rememberAgentPassword (Agent agent, AgentDao agentDao) throws Exception {
		List<Agent> agtList = agentDao.getAgentsByLogin(agent);
		Agent agt = new Agent();
		try{
			if (!agtList.isEmpty()) {
				agt = agtList.get(0);
				String password = Encryptor.decrypt(agt.getAgentPassword());
				SendMail.sendMailToSingleReceiver(agent.getAgentPhoneNumber(), "Renvoi de mot de passe", "Votre mot de passe est: "+password);
			}
			return agt;
		}
		catch (Exception ex ) {
			ex.printStackTrace();
			throw new CryptographyException("Impossible de crypter le mot de passe");
		} 
	}
}
