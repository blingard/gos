package com.secel.devopteam.GosMiddleware.api;

import java.util.ArrayList;
import java.util.List;

import com.secel.devopteam.GosMiddleware.beans.AccessRightEmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroupEmployee;
import com.secel.devopteam.GosMiddleware.dao.AccessRightEmployeeGroupDao;


public class AccessRightEmployeeGroupController {
	public static AccessRightEmployeeGroup createAccessRightEmployeeGroup (AccessRightEmployeeGroup accessRightEmployeeGroup, AccessRightEmployeeGroupDao accessRightEmployeeGroupDao) throws Exception {
		return accessRightEmployeeGroupDao.saveOrUpdateAccessRightEmployeeGroup(accessRightEmployeeGroup);
	}
	public static List<AccessRightEmployeeGroup> getAllAccessRightGroups (AccessRightEmployeeGroupDao accessRightEmployeeGroupDao) throws Exception {
		List<AccessRightEmployeeGroup>l1=accessRightEmployeeGroupDao.getAllAccessRightEmployeeGroups();
		List<AccessRightEmployeeGroup>l2=new ArrayList<AccessRightEmployeeGroup>();
		if(!l1.isEmpty()) {
			for(AccessRightEmployeeGroup arempGroup: l1) {
				if(arempGroup.getEndDate()==null) {
					l2.add(arempGroup);
				}
			}
		}
		
		return l2;
	}
	
	public static List<AccessRightEmployeeGroup> getAccessRightsByGroup (EmployeeGroup employeeGroup, AccessRightEmployeeGroupDao accessRightEmployeeGroupDao) throws Exception {
		return accessRightEmployeeGroupDao.getAccessRightByEmployeeGroup(employeeGroup);
	}
	
	public static AccessRightEmployeeGroup updateAccessRightEmployeeGroup (AccessRightEmployeeGroup accessRightEmployeeGroup, AccessRightEmployeeGroupDao accessRightEmployeeGroupDao) throws Exception {
		return accessRightEmployeeGroupDao.saveOrUpdateAccessRightEmployeeGroup(accessRightEmployeeGroup); 
	}
	
	public static AccessRightEmployeeGroup affectAccessRightToGroup (AccessRightEmployeeGroup accessRightEmployeeGroup, AccessRightEmployeeGroupDao accessRightEmployeeGroupDao) throws Exception {
		return accessRightEmployeeGroupDao.assignAccessRightToGroup(accessRightEmployeeGroup);
	}

}
