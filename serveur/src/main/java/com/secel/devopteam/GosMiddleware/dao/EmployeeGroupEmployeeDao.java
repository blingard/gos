package com.secel.devopteam.GosMiddleware.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;


import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroupEmployee;

import io.dropwizard.hibernate.AbstractDAO;

public class EmployeeGroupEmployeeDao extends AbstractDAO<EmployeeGroupEmployee> {

	public EmployeeGroupEmployeeDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public EmployeeGroupEmployee saveOrUpdateEmployeeGroupEmployee(@Valid EmployeeGroupEmployee employeeGroupEmployee) {
		return persist(employeeGroupEmployee);
	}
	
	public EmployeeGroupEmployee findById(int id) {
        return get(id);
    }
	
	public List<EmployeeGroupEmployee> getAllEmployeeGroupEmployees() {
		return this.list(this.query("SELECT a FROM EmployeeGroupEmployee a"));
	}
	public List<EmployeeGroupEmployee> getAllGroupsForEmployee(Employee employee) {
		return this.list(this.query("SELECT a FROM EmployeeGroupEmployee a WHERE a.employeeId.employeeId= :employeeId").setInteger("employeeId", employee.getEmployeeId()));
	}
	public List<EmployeeGroupEmployee> getAllGroupsForEmployeeWhereEndDate(Employee employee) {
		List<EmployeeGroupEmployee> l1=this.list(this.query("SELECT a FROM EmployeeGroupEmployee a WHERE a.employeeGroupEmployeePK.employeeId= :employeeId").setInteger("employeeId", employee.getEmployeeId()));
		for(EmployeeGroupEmployee empGemp: l1) {
			if(empGemp.getEndDate()!=null) {
				l1.remove(empGemp);
			}
			
		}
		return l1;
	}
	public EmployeeGroupEmployee assignEmployeeToGoup(@Valid EmployeeGroupEmployee EmpGroupEmployee) {
		return this.persist(EmpGroupEmployee); 
	}

}
