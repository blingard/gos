package com.secel.devopteam.GosMiddleware.api;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.Device;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.dao.AgentSectorDao;
import com.secel.devopteam.GosMiddleware.dao.DistrictDao;
import com.secel.devopteam.GosMiddleware.dao.SectorDao;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;


/**
 * 
 * @author TEV Lion
 *
 */
public class AgentSectorController {
	
	public static AgentSector createAgentSector (AgentSector agentSector, AgentSectorDao agentSectorDao, SectorDao sectorDao) throws Exception {
		AgentSector agsectorSave= agentSectorDao.assignSectorToAgent(agentSector, sectorDao);
		return agsectorSave;
	}
	
	public static List<AgentSector> getAllAgentsSectors (AgentSectorDao agentSectorDao) throws Exception {
		return agentSectorDao.getAllAgentsSectors();
	}
	public static List<AgentSector> getAgentsSectorsActives (AgentSectorDao agentSectorDao) throws Exception {
		List<AgentSector>agSectorList= agentSectorDao.getAllAgentsSectors();
		List<AgentSector>l2=new ArrayList<AgentSector>();
		if(!agSectorList.isEmpty()) {
			for(AgentSector agSector: agSectorList) {
				if(agSector.getEndDate()==null) {
					l2.add(agSector);
				}
			}
		}
		return l2;
	}
	
	public static List<AgentSector> getAgentsSectorsByAgent (Agent agent, AgentSectorDao agentSectorDao) throws Exception {
		return  agentSectorDao.getAgentsSectorsByAgent(agent);
		
	}
	public static List<AgentSector> getAgentsSectorsByAgentEndDate (Agent agent, AgentSectorDao agentSectorDao) throws Exception {
		List<AgentSector>l1= agentSectorDao.getAgentsSectorsByAgent(agent);
		List<AgentSector>l2=new ArrayList<AgentSector>();
		if(!l1.isEmpty()) {
			for(AgentSector agSector: l1) {
				if(agSector.getEndDate()==null) {
					l2.add(agSector);
				}
			}
		}
		
		return l2;
	}
	public static List<AgentSector> getAgentsSectorsBySector (Sector sector, AgentSectorDao agentSectorDao) throws Exception {
		return agentSectorDao.getAgentsSectorsBySector(sector);
	}
	
	public static AgentSector updateAgentSector (AgentSector agentSector, AgentSectorDao agentSectorDao) throws Exception {
		return agentSectorDao.saveOrUpdateAgentSector(agentSector); 
	}

	public static AgentSector getCurrentAgentSector (Agent agent, AgentSectorDao agentSectorDao) throws Exception {
		AgentSector currentAgtSctr = null;
		for( AgentSector agtSctr : AgentSectorController.getAgentsSectorsByAgent(agent, agentSectorDao)){
			if (agtSctr.getEndDate() == null) {
				currentAgtSctr = agtSctr;
			}
		}
		return currentAgtSctr;
	}

}
