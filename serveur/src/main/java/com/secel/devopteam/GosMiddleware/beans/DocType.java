/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author JAGHO Brel
 */
@Entity
@Table(name = "doc_type")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "DocType.findAll", query = "SELECT d FROM DocType d")
        , @NamedQuery(name = "DocType.findByDocTypeId", query = "SELECT d FROM DocType d WHERE d.docTypeId = :docTypeId")
        , @NamedQuery(name = "DocType.findByDocTypeName", query = "SELECT d FROM DocType d WHERE d.docTypeName = :docTypeName")
        , @NamedQuery(name = "DocType.findByDescription", query = "SELECT d FROM DocType d WHERE d.description = :description")})
public class DocType implements Serializable {

        private static final long serialVersionUID = 1L;
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Basic(optional = false)
        @Column(name = "doc_type_id")
        private Integer docTypeId;
        @Basic(optional = false)
        @Column(name = "doc_type_name")
        private String docTypeName;
        @Column(name = "description")
        private String description;
        @OneToMany(cascade = CascadeType.ALL, mappedBy = "docTypeId", fetch = FetchType.LAZY)
        private List<Documents> documentsList;

        public DocType() {
        }

        public DocType(Integer docTypeId) {
                this.docTypeId = docTypeId;
        }

        public DocType(Integer docTypeId, String docTypeName) {
                this.docTypeId = docTypeId;
                this.docTypeName = docTypeName;
        }

        public Integer getDocTypeId() {
                return docTypeId;
        }

        public void setDocTypeId(Integer docTypeId) {
                this.docTypeId = docTypeId;
        }

        public String getDocTypeName() {
                return docTypeName;
        }

        public void setDocTypeName(String docTypeName) {
                this.docTypeName = docTypeName;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        @XmlTransient
        public List<Documents> getDocumentsList() {
                return documentsList;
        }

        public void setDocumentsList(List<Documents> documentsList) {
                this.documentsList = documentsList;
        }

        @Override
        public int hashCode() {
                int hash = 0;
                hash += (docTypeId != null ? docTypeId.hashCode() : 0);
                return hash;
        }

        @Override
        public boolean equals(Object object) {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof DocType)) {
                        return false;
                }
                DocType other = (DocType) object;
                if ((this.docTypeId == null && other.docTypeId != null) || (this.docTypeId != null && !this.docTypeId.equals(other.docTypeId))) {
                        return false;
                }
                return true;
        }

        @Override
        public String toString() {
                return "generatebeans.page_gos.DocType[ docTypeId=" + docTypeId + " ]";
        }
        
}
