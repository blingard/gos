package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.GpsCoordinates;
import com.secel.devopteam.GosMiddleware.beans.Site;

import io.dropwizard.hibernate.AbstractDAO;

public class GpsCoordinatesDao extends AbstractDAO<GpsCoordinates>{

	public GpsCoordinatesDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public GpsCoordinates saveOrUpdateGpsCoordinates(@Valid GpsCoordinates gps) {
		return persist(gps);
	}
	
	public GpsCoordinates findGpsCoordinatesById(int id) {
        return get(id);
    }
	
	public List<GpsCoordinates> getAllGpsCoordinates() {
		return this.list(this.query("SELECT g FROM GpsCoordinates g"));
	}
	public List<GpsCoordinates> getAllGpsCoordinatesForSite(Site site) {
		return this.list(this.query("SELECT g FROM GpsCoordinates g WHERE g.siteId.siteId= :siteId").setInteger("siteId", site.getSiteId()));
	}

}
