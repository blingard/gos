/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "agent")
@NamedQueries({
    @NamedQuery(name = "Agent.findAll", query = "SELECT a FROM Agent a"),
    @NamedQuery(name = "Agent.findByAgentId", query = "SELECT a FROM Agent a WHERE a.agentId = :agentId"),
    @NamedQuery(name = "Agent.findByAgentName", query = "SELECT a FROM Agent a WHERE a.agentName = :agentName"),
    @NamedQuery(name = "Agent.findByAgentLogin", query = "SELECT a FROM Agent a WHERE a.agentLogin = :agentLogin"),
    @NamedQuery(name = "Agent.findByAgentPassword", query = "SELECT a FROM Agent a WHERE a.agentPassword = :agentPassword"),
    @NamedQuery(name = "Agent.findByIsFirstConnection", query = "SELECT a FROM Agent a WHERE a.isFirstConnection = :isFirstConnection"),
    @NamedQuery(name = "Agent.findByStatus", query = "SELECT a FROM Agent a WHERE a.status = :status"),
    @NamedQuery(name = "Agent.findByAgentPhoneNumber", query = "SELECT a FROM Agent a WHERE a.agentPhoneNumber = :agentPhoneNumber"),
    @NamedQuery(name = "Agent.findByLastConnectionDate", query = "SELECT a FROM Agent a WHERE a.lastConnectionDate = :lastConnectionDate"),
    @NamedQuery(name = "Agent.findByToken", query = "SELECT a FROM Agent a WHERE a.token = :token")})
public class Agent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "agent_id")
    private Integer agentId;
    @Column(name = "agent_name")
    private String agentName;
    @Column(name = "agent_login")
    private String agentLogin;
    @Column(name = "agent_password")
    private String agentPassword;
    @Basic(optional = false)
    @Column(name = "is_first_connection")
    private boolean isFirstConnection;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @Column(name = "agent_phone_number")
    private String agentPhoneNumber;
    @Column(name = "last_connection_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastConnectionDate;
    @Column(name = "token")
    private String token;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "agent", fetch = FetchType.LAZY)
    private List<AgentDevice> agentDeviceList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "agent", fetch = FetchType.LAZY)
    private List<AgentSector> agentSectorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "agentId", fetch = FetchType.LAZY)
    private List<AgentActivity> agentActivityList;

    public Agent() {
    }

    public Agent(Integer agentId) {
        this.agentId = agentId;
    }

    public Agent(Integer agentId, boolean isFirstConnection, short status) {
        this.agentId = agentId;
        this.isFirstConnection = isFirstConnection;
        this.status = status;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentLogin() {
        return agentLogin;
    }

    public void setAgentLogin(String agentLogin) {
        this.agentLogin = agentLogin;
    }

    public String getAgentPassword() {
        return agentPassword;
    }

    public void setAgentPassword(String agentPassword) {
        this.agentPassword = agentPassword;
    }

    public boolean getIsFirstConnection() {
        return isFirstConnection;
    }

    public void setIsFirstConnection(boolean isFirstConnection) {
        this.isFirstConnection = isFirstConnection;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public String getAgentPhoneNumber() {
        return agentPhoneNumber;
    }

    public void setAgentPhoneNumber(String agentPhoneNumber) {
        this.agentPhoneNumber = agentPhoneNumber;
    }

    public Date getLastConnectionDate() {
        return lastConnectionDate;
    }

    public void setLastConnectionDate(Date lastConnectionDate) {
        this.lastConnectionDate = lastConnectionDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<AgentDevice> getAgentDeviceList() {
        return agentDeviceList;
    }

    public void setAgentDeviceList(List<AgentDevice> agentDeviceList) {
        this.agentDeviceList = agentDeviceList;
    }

    public List<AgentSector> getAgentSectorList() {
        return agentSectorList;
    }

    public void setAgentSectorList(List<AgentSector> agentSectorList) {
        this.agentSectorList = agentSectorList;
    }

    public List<AgentActivity> getAgentActivityList() {
        return agentActivityList;
    }

    public void setAgentActivityList(List<AgentActivity> agentActivityList) {
        this.agentActivityList = agentActivityList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agentId != null ? agentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agent)) {
            return false;
        }
        Agent other = (Agent) object;
        if ((this.agentId == null && other.agentId != null) || (this.agentId != null && !this.agentId.equals(other.agentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.generatedbeans.Agent[ agentId=" + agentId + " ]";
    }
    
}
