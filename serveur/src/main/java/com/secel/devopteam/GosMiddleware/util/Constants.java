package com.secel.devopteam.GosMiddleware.util;

public enum Constants {
	EMPLOYEE_ACCESS("public"),
	ADMIN_ACCESS("administrator"),
	EMPLOYEE_ACCESS_RIGHT("employee_right"),
	ADMIN_ACCESS_RIGHT("administrator_right"),
	DATE_FORMAT("d MMM yyyy HH:mm:ss"),
	ROOT_RIGHT("All");
	 
	    private String right;
	 
	    Constants(String envUrl) {
	        this.right = envUrl;
	    }
	 
	    public String getUrl() {
	        return right;
	    }

}
