package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

/**
*
* @author JAGHO Brel
*/
public class AgentDao extends AbstractDAO<Agent>{
	
	public AgentDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	public Agent saveOrUpdateAgent(@Valid Agent agent) {
		return persist(agent);
	}
	
	public Agent findAgentById(int id) {
        return get(id);
    }
	
	public List<Agent> getAllAgents() {
		return this.list(this.query("SELECT a FROM Agent a"));
	}
	
	public Agent connectAgentWithLoginAndPass(Agent agent) {
        return this.uniqueResult(this.query("SELECT a FROM Agent a WHERE a.agentLogin= :agentLogin AND a.agentPassword= :agentPassword AND a.status= :status")
        		.setString("agentLogin", agent.getAgentLogin()).setString("agentPassword", agent.getAgentPassword()).setShort("status", GosMiddlewareConstants.STATE_ACTIVATED));
    }
	
	public List<Agent> getAgentsByLogin(Agent agent) {
        return this.list(this.query("SELECT a FROM Agent a WHERE a.agentLogin= :agentLogin")
        		.setString("agentLogin", agent.getAgentLogin()));
    }
}
