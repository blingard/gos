package com.secel.devopteam.GosMiddleware.api;

import java.util.List;


import com.secel.devopteam.GosMiddleware.beans.GpsCoordinates;
import com.secel.devopteam.GosMiddleware.beans.Site;
import com.secel.devopteam.GosMiddleware.dao.GpsCoordinatesDao;

public class GpsCoordinatesController {
	
	public static GpsCoordinates createGpsCoordinates(GpsCoordinates gpsCoordinates, GpsCoordinatesDao gpsCoordinatesDao) throws Exception {
		return gpsCoordinatesDao.saveOrUpdateGpsCoordinates(gpsCoordinates);
	}
	public static GpsCoordinates getGpsCoordinatesById (GpsCoordinates gpsCoordinates, GpsCoordinatesDao gpsCoordinatesDao) throws Exception {
		return gpsCoordinatesDao.findGpsCoordinatesById(gpsCoordinates.getGpsCoordId());
	}
	
	public static List<GpsCoordinates> getAllGpsCoordinatesForSite (Site site, GpsCoordinatesDao gpsCoordinatesDao) throws Exception {
		System.out.println("getAllGpsCoordinatesForSite");
		return gpsCoordinatesDao.getAllGpsCoordinatesForSite(site);
	}
	public static GpsCoordinates updateGpsCoordinates (GpsCoordinates gpsCoordinates, GpsCoordinatesDao gpsCoordinatesDao) throws Exception {
		return gpsCoordinatesDao.saveOrUpdateGpsCoordinates(gpsCoordinates);
	}
}
