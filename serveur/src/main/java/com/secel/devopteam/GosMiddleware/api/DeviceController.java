package com.secel.devopteam.GosMiddleware.api;


import java.util.*;

import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentDevice;
import com.secel.devopteam.GosMiddleware.beans.Device;
import com.secel.devopteam.GosMiddleware.dao.AgentDeviceDao;
import com.secel.devopteam.GosMiddleware.dao.DeviceDao;

public class DeviceController {
	public static Device createDevice (Device device, DeviceDao deviceDao) throws Exception {
		Date date = new Date();
		device.setCreateddate(date);
		return deviceDao.saveOrUpdateDevice(device);
	}
	public static List<Device> getAllDevices (DeviceDao deviceDao) throws Exception {
		return deviceDao.getAllDevices();
	}
	
	public static List<Device> getAllFreeDevices (DeviceDao deviceDao) throws Exception {
		return deviceDao.getAllFreeDevices();
	}
	
	public static Device getDeviceById (Device device, DeviceDao deviceDao) throws Exception {
		return deviceDao.findDeviceById(device.getDeviceId());
	}
	public static List<AgentDevice> getAllDeviceForAgentEnDate(Agent agent, AgentDeviceDao agentdeviceDao){
		List<AgentDevice>l1=agentdeviceDao.getAllDeviceForAgent(agent);
		List<AgentDevice>l2=new ArrayList<AgentDevice>();
		if(!l1.isEmpty()) {
			for(AgentDevice agDevice: l1) {
				if(agDevice.getEndDate()==null) {
					l2.add(agDevice);
				}
			}
		}
		return l2;		
	}
	public static List<AgentDevice> getAllDeviceForAgent(Agent agent, AgentDeviceDao agentdeviceDao){
		return agentdeviceDao.getAllDeviceForAgent(agent);
				
	}
	public static Device updateDevice (Device device, DeviceDao deviceDao) throws Exception {
		return deviceDao.saveOrUpdateDevice(device);
	}

	public static List<AgentDevice> getAllAgentDevice(AgentDeviceDao agentdeviceDao){
		return agentdeviceDao.getAllAgentDevice();		
	}
	
}
