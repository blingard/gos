package com.secel.devopteam.GosMiddleware.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.AccessRightEmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentDevice;
import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroupEmployee;

import io.dropwizard.hibernate.AbstractDAO;

public class AccessRightEmployeeGroupDao extends AbstractDAO<AccessRightEmployeeGroup>{

	public AccessRightEmployeeGroupDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public AccessRightEmployeeGroup saveOrUpdateAccessRightEmployeeGroup(@Valid AccessRightEmployeeGroup accessRightEmployeeGroup) {
		return persist(accessRightEmployeeGroup);
	}
		
	public AccessRightEmployeeGroup findById(int id) {
        return get(id);
    }
	
	public List<AccessRightEmployeeGroup> getAllAccessRightEmployeeGroups() {
		return this.list(this.query("SELECT a FROM AccessRightEmployeeGroup a"));
	}
	public AccessRightEmployeeGroup assignAccessRightToGroup(@Valid AccessRightEmployeeGroup accessRightEmployeeGroup) {
		 List<AccessRightEmployeeGroup> lastAccessList = this.list(this.query("SELECT a FROM AccessRightEmployeeGroup a WHERE a.employeeGroup.employeeGroupId = :employeeGroupId")
				 .setInteger("employeeGroupId", accessRightEmployeeGroup.getEmployeeGroup().getEmployeeGroupId()));
		 for (AccessRightEmployeeGroup aRightEmployeeGroup : lastAccessList) {
			if(aRightEmployeeGroup.getEndDate() == null) {
				aRightEmployeeGroup.setEndDate(accessRightEmployeeGroup.getAccessRightEmployeeGroupPK().getFromDate());
				this.persist(accessRightEmployeeGroup);
			}
		}
		return this.persist(accessRightEmployeeGroup); 
	}
	public List<AccessRightEmployeeGroup> getAllAccessRightForEmployeeGroup(EmployeeGroup employeeGroup) {
		return this.list(this.query("SELECT a FROM AccessRightEmployeeGroup a WHERE a.employeeGroupId.employeeGroupId= :employeeGroupId").setInteger("employeeGroupId", employeeGroup.getEmployeeGroupId()));
	}
	
	public List<AccessRightEmployeeGroup> getAllAccessRightForEmployeeGroupWhereEndDate(EmployeeGroup employeeGroup) {
		List<AccessRightEmployeeGroup> l1 = this.list(this.query("SELECT a FROM AccessRightEmployeeGroup a WHERE a.accessRightEmployeeGroupPK.employeeGroupId= :employeeGroupId").setInteger("employeeGroupId", employeeGroup.getEmployeeGroupId()));
		if (!l1.isEmpty()) {
			List<AccessRightEmployeeGroup> listToRemove = new ArrayList<AccessRightEmployeeGroup>();
			for(AccessRightEmployeeGroup accRightG : l1) {
				if(accRightG.getEndDate()!=null) {
					listToRemove.add(accRightG);
					//l1.remove(accRightG);
				}
				
			}
			l1.removeAll(listToRemove);
		}
		return l1;
	}

	public List<AccessRightEmployeeGroup> getAccessRightByEmployeeGroup(EmployeeGroup employeeGroup) {
		return this.list( this.query("SELECT a FROM AccessRightEmployeeGroup a WHERE a.EmployeeGroup.employeeGroupId = :employeeGroupId")
											.setParameter("employeeGroupId", employeeGroup.getEmployeeGroupId()) ); 
	}
	public AccessRightEmployeeGroup updateEndDateAccessRightEmployeeGroup(@Valid AccessRightEmployeeGroup accessRightEmployeeGroup) {
		return this.persist(accessRightEmployeeGroup); 
	}
}
