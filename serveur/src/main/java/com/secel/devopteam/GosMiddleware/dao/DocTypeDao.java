package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.DocType;

import io.dropwizard.hibernate.AbstractDAO;

public class DocTypeDao extends AbstractDAO<DocType>{

	public DocTypeDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public DocType saveOrUpdateDocType(@Valid DocType docType) {
		return persist(docType);
	}
	
	public DocType findDocTypeById(int id) {
        return get(id);
    }
	
	public List<DocType> getAllDocType() {
		return this.list(this.query("SELECT d FROM DocType d"));
	}
	

}
