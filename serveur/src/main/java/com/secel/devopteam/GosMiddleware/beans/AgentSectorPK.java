/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author JAGHO Brel
 */
@Embeddable
public class AgentSectorPK implements Serializable {

        @Basic(optional = false)
        @Column(name = "agent_id")
        private long agentId;
        @Basic(optional = false)
        @Column(name = "sector_id")
        private long sectorId;
        @Basic(optional = false)
        @Column(name = "begin_date")
        @Temporal(TemporalType.TIMESTAMP)
        private Date beginDate;

        public AgentSectorPK() {
        }

        public AgentSectorPK(long agentId, long sectorId, Date beginDate) {
                this.agentId = agentId;
                this.sectorId = sectorId;
                this.beginDate = beginDate;
        }

        public long getAgentId() {
                return agentId;
        }

        public void setAgentId(long agentId) {
                this.agentId = agentId;
        }

        public long getSectorId() {
                return sectorId;
        }

        public void setSectorId(long sectorId) {
                this.sectorId = sectorId;
        }

        public Date getBeginDate() {
                return beginDate;
        }

        public void setBeginDate(Date beginDate) {
                this.beginDate = beginDate;
        }

        @Override
        public int hashCode() {
                int hash = 0;
                hash += (int) agentId;
                hash += (int) sectorId;
                hash += (beginDate != null ? beginDate.hashCode() : 0);
                return hash;
        }

        @Override
        public boolean equals(Object object) {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof AgentSectorPK)) {
                        return false;
                }
                AgentSectorPK other = (AgentSectorPK) object;
                if (this.agentId != other.agentId) {
                        return false;
                }
                if (this.sectorId != other.sectorId) {
                        return false;
                }
                if ((this.beginDate == null && other.beginDate != null) || (this.beginDate != null && !this.beginDate.equals(other.beginDate))) {
                        return false;
                }
                return true;
        }

        @Override
        public String toString() {
                return "generatebeans.page_gos.AgentSectorPK[ agentId=" + agentId + ", sectorId=" + sectorId + ", beginDate=" + beginDate + " ]";
        }
        
}
