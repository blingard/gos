package com.secel.devopteam.GosMiddleware.dao;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentDevice;
import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class AgentSectorDao extends AbstractDAO<AgentSector>{

	public AgentSectorDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	
	/*
	public AgentSector saveOrUpdateAgentSector(@Valid AgentSector agentSector) {
		return persist(agentSector);
	}*/
	
	
	public AgentSector saveOrUpdateAgentSector(@Valid AgentSector agentSector) {
		List<AgentSector> lastDevice = this.list(this.query("SELECT a FROM AgentSector a WHERE a.agent.agentId = :agentId")
				 																.setParameter("agentId", agentSector.getAgent().getAgentId()));
		 for (AgentSector agtSector : lastDevice) {
			if(agtSector.getEndDate() == null) {
				agtSector.setEndDate(agentSector.getAgentSectorPK().getBeginDate());
				this.persist(agtSector);
			}
		}
		return this.persist(agentSector);
	}
	
	public AgentSector assignSectorToAgent(@Valid AgentSector agtSector, SectorDao secDao) {
		 List<AgentSector> agentSectorList = this.list(this.query("SELECT a FROM AgentSector a WHERE a.agent.agentId = :agentId")
				 .setInteger("agentId", agtSector.getAgent().getAgentId()));
		 boolean existingValue = false;
		 if (agentSectorList.isEmpty()) {
			 Sector sec = secDao.findSectorById(agtSector.getSector().getSectorId());
			 agtSector.setSector(sec);
			 agtSector.getSector().setStatus(GosMiddlewareConstants.STATE_USED_DEVICE);
			 return persist(agtSector);
		 }
		 else {
			 for (AgentSector agentSector : agentSectorList) {
				if(agentSector.getEndDate() == null) {
					agentSector.setEndDate(agtSector.getAgentSectorPK().getBeginDate());
					agentSector.getSector().setStatus(GosMiddlewareConstants.STATE_ACTIVATED);
					persist(agentSector);
				}
				else {
					if (agtSector.getSector().getSectorId() == agentSector.getSector().getSectorId()) {
						existingValue = true;
						agtSector.setSector(agentSector.getSector());
					}
				}
			}
			 if (!existingValue) {
				 Sector sec = secDao.findSectorById(agtSector.getSector().getSectorId());
				 agtSector.setSector(sec);
			 }
			 agtSector.getSector().setStatus(GosMiddlewareConstants.STATE_USED_DEVICE);
			return persist(agtSector);
		}
	}

	public List<AgentSector> getAllAgentsSectors() {
		return this.list( this.query("SELECT a FROM AgentSector a") ); 
	}
	
	public List<AgentSector> getAgentsSectorsByAgent(Agent agent) {
		return this.list( this.query("SELECT a FROM AgentSector a WHERE a.agent.agentId = :agentId")
											.setParameter("agentId", agent.getAgentId()) ); 
	}
	
	public  List<AgentSector> getAgentsSectorsByAgentForMobile(Agent agent) {
		return this.list(this.query("SELECT a FROM AgentSector a WHERE a.agent.agentId = :agentId")
											.setParameter("agentId", agent.getAgentId())); 
	}

	public List<AgentSector> getAgentsSectorsBySector(Sector sector) {
		return this.list( this.query("SELECT a FROM AgentSector a WHERE a.sector.sectorId = :sectorId")
											.setParameter("sectorId", sector.getSectorId()) ); 
	}
	public AgentSector updateAgentSectorToAgent(@Valid AgentSector AgSector) {
		return this.persist(AgSector); 
	}
	
}
