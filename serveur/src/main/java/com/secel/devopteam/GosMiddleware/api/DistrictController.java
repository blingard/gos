package com.secel.devopteam.GosMiddleware.api;

import java.util.ArrayList;
import java.util.List;

import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.District;
import com.secel.devopteam.GosMiddleware.beans.Documents;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.beans.Site;
import com.secel.devopteam.GosMiddleware.dao.DocumentsDao;
import com.secel.devopteam.GosMiddleware.dao.DistrictDao;
import com.secel.devopteam.GosMiddleware.dao.SiteDao;

public class DistrictController {
	
	public static District createDistrict (District district, DistrictDao districtDao) throws Exception {
		return districtDao.saveOrUpdateDistrict(district);
	}
	public static List<District> getAllDistricts (DistrictDao districtDao) throws Exception {
		return districtDao.getAllDistricts();
	}
	
	public static List<District> getAllFreeDistricts (DistrictDao districtDao) throws Exception {
		return districtDao.getAllFreeDistricts();
	}
	
	public static District getDistrictById (District district, DistrictDao districtDao) throws Exception {
		return districtDao.findDistrictById(district.getDistrictId());
	}
	
	public static District updateDistrict (District district, DistrictDao districtDao) throws Exception {
		return districtDao.saveOrUpdateDistrict(district);    
	}
	
	public static List<District> getAllDistrictOfSector(Sector sector, SiteDao sitedao, DistrictDao districtdao) {
		List<District> districts = districtdao.getAllDistrictOfSector(sector);
		return districts;
	}

}
 