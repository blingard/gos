package com.secel.devopteam.GosMiddleware;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.*;
//import javax.validation.constraints.NotEmpty;
import javax.validation.Valid;
import io.dropwizard.db.DataSourceFactory;

/**
 * 
 * @author JAGHO Brel
 *
 */
public class GosMiddlewareConfiguration extends Configuration {
    // TODO: implement service configuration
		//@NotEmpty
		@JsonProperty
		private String defaultName = "gosMiddleWare";
		
		@NotNull
	    private String tokenkey;

		public String getDefaultName() {
			return defaultName;
		}

		@NotNull
	    private String login;
	    @NotNull
	    private String password;
	    
	    @JsonProperty
		public String getLogin() {
			return login;
		}
		@JsonProperty
		public String getPassword() {
			return password;
		}
		
		@Valid
		@NotNull
		private DataSourceFactory database = new DataSourceFactory();

		@JsonProperty("database")
		public DataSourceFactory getDataSourceFactory() {
			return database;
		}
		
		@JsonProperty()
		public String getTokenkey() {
			return tokenkey;
		}
		
		public void setDataSourceFactory(DataSourceFactory f) {
			 database = f;
		}
}
