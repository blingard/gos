package com.secel.devopteam.GosMiddleware.util;

import java.io.Serializable;

public class EmployeeAccessRights implements Serializable{
	private static final long serialVersionUID = 1L;
	private int employeeId;
	private int groupId;
	private int accessRightId;
	private String accessRightName;
	
	public EmployeeAccessRights () {
		
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getAccessRightId() {
		return accessRightId;
	}

	public void setAccessRightId(int accessRightId) {
		this.accessRightId = accessRightId;
	}

	public String getAccessRightName() {
		return accessRightName;
	}

	public void setAccessRightName(String accessRightName) {
		this.accessRightName = accessRightName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accessRightName == null) ? 0 : accessRightName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeAccessRights other = (EmployeeAccessRights) obj;
		if (accessRightName == null) {
			if (other.accessRightName != null)
				return false;
		} else if (!accessRightName.equals(other.accessRightName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EmployeeAccessRights [groupId=" + groupId + ", employeeId=" + employeeId + "]";
	}
}
