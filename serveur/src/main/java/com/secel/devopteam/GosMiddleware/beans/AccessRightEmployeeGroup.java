/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ken
 */
@Entity
@Table(name = "access_right_employee_group")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccessRightEmployeeGroup.findAll", query = "SELECT a FROM AccessRightEmployeeGroup a")
    , @NamedQuery(name = "AccessRightEmployeeGroup.findByEmployeeGroupId", query = "SELECT a FROM AccessRightEmployeeGroup a WHERE a.accessRightEmployeeGroupPK.employeeGroupId = :employeeGroupId")
    , @NamedQuery(name = "AccessRightEmployeeGroup.findByAccessRightId", query = "SELECT a FROM AccessRightEmployeeGroup a WHERE a.accessRightEmployeeGroupPK.accessRightId = :accessRightId")
    , @NamedQuery(name = "AccessRightEmployeeGroup.findByFromDate", query = "SELECT a FROM AccessRightEmployeeGroup a WHERE a.accessRightEmployeeGroupPK.fromDate = :fromDate")
    , @NamedQuery(name = "AccessRightEmployeeGroup.findByEndDate", query = "SELECT a FROM AccessRightEmployeeGroup a WHERE a.endDate = :endDate")})
public class AccessRightEmployeeGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AccessRightEmployeeGroupPK accessRightEmployeeGroupPK;
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @JoinColumn(name = "access_right_id", referencedColumnName = "access_right_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private AccessRight accessRight;
    @JoinColumn(name = "employee_group_id", referencedColumnName = "employee_group_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private EmployeeGroup employeeGroup;

    public AccessRightEmployeeGroup() {
    }

    public AccessRightEmployeeGroup(AccessRightEmployeeGroupPK accessRightEmployeeGroupPK) {
        this.accessRightEmployeeGroupPK = accessRightEmployeeGroupPK;
    }

    public AccessRightEmployeeGroup(long employeeGroupId, long accessRightId, Date fromDate) {
        this.accessRightEmployeeGroupPK = new AccessRightEmployeeGroupPK(employeeGroupId, accessRightId, fromDate);
    }

    public AccessRightEmployeeGroupPK getAccessRightEmployeeGroupPK() {
        return accessRightEmployeeGroupPK;
    }

    public void setAccessRightEmployeeGroupPK(AccessRightEmployeeGroupPK accessRightEmployeeGroupPK) {
        this.accessRightEmployeeGroupPK = accessRightEmployeeGroupPK;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public AccessRight getAccessRight() {
        return accessRight;
    }

    public void setAccessRight(AccessRight accessRight) {
        this.accessRight = accessRight;
    }

    public EmployeeGroup getEmployeeGroup() {
        return employeeGroup;
    }

    public void setEmployeeGroup(EmployeeGroup employeeGroup) {
        this.employeeGroup = employeeGroup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accessRightEmployeeGroupPK != null ? accessRightEmployeeGroupPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccessRightEmployeeGroup)) {
            return false;
        }
        AccessRightEmployeeGroup other = (AccessRightEmployeeGroup) object;
        if ((this.accessRightEmployeeGroupPK == null && other.accessRightEmployeeGroupPK != null) || (this.accessRightEmployeeGroupPK != null && !this.accessRightEmployeeGroupPK.equals(other.accessRightEmployeeGroupPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.AccessRightEmployeeGroup[ accessRightEmployeeGroupPK=" + accessRightEmployeeGroupPK + " ]";
    }
    
}
