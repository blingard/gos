/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ken
 */
@Entity
@Table(name = "agent_alerts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AgentAlerts.findAll", query = "SELECT a FROM AgentAlerts a")
    , @NamedQuery(name = "AgentAlerts.findByAgentAlertsId", query = "SELECT a FROM AgentAlerts a WHERE a.agentAlertsId = :agentAlertsId")
    , @NamedQuery(name = "AgentAlerts.findByAlertsPurpose", query = "SELECT a FROM AgentAlerts a WHERE a.alertsPurpose = :alertsPurpose")
    , @NamedQuery(name = "AgentAlerts.findByAgentResponse", query = "SELECT a FROM AgentAlerts a WHERE a.agentResponse = :agentResponse")
    , @NamedQuery(name = "AgentAlerts.findByStatus", query = "SELECT a FROM AgentAlerts a WHERE a.status = :status")})
public class AgentAlerts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "agent_alerts_id")
    private Integer agentAlertsId;
    @Column(name = "alerts_purpose")
    private String alertsPurpose;
    @Column(name = "agent_response")
    private String agentResponse;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "agent_id", referencedColumnName = "agent_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Agent agentId;

    public AgentAlerts() {
    }

    public AgentAlerts(Integer agentAlertsId) {
        this.agentAlertsId = agentAlertsId;
    }

    public AgentAlerts(Integer agentAlertsId, short status) {
        this.agentAlertsId = agentAlertsId;
        this.status = status;
    }

    public Integer getAgentAlertsId() {
        return agentAlertsId;
    }

    public void setAgentAlertsId(Integer agentAlertsId) {
        this.agentAlertsId = agentAlertsId;
    }

    public String getAlertsPurpose() {
        return alertsPurpose;
    }

    public void setAlertsPurpose(String alertsPurpose) {
        this.alertsPurpose = alertsPurpose;
    }

    public String getAgentResponse() {
        return agentResponse;
    }

    public void setAgentResponse(String agentResponse) {
        this.agentResponse = agentResponse;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Agent getAgentId() {
        return agentId;
    }

    public void setAgentId(Agent agentId) {
        this.agentId = agentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agentAlertsId != null ? agentAlertsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgentAlerts)) {
            return false;
        }
        AgentAlerts other = (AgentAlerts) object;
        if ((this.agentAlertsId == null && other.agentAlertsId != null) || (this.agentAlertsId != null && !this.agentAlertsId.equals(other.agentAlertsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.AgentAlerts[ agentAlertsId=" + agentAlertsId + " ]";
    }
    
}
