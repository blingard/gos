package com.secel.devopteam.GosMiddleware.dao;

import org.hibernate.SessionFactory;
import java.util.List;

import javax.validation.Valid;

import com.secel.devopteam.GosMiddleware.beans.Rules;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class RulesDao  extends AbstractDAO<Rules>{

	public RulesDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	
	public Rules saveUpdateRules(@Valid Rules rules) {
		return persist(rules);
	}
	
	public List<Rules> getAllActiveRules(){
		return this.list(this.query("SELECT r FROM Rules r WHERE r.status = :status").setShort("status", GosMiddlewareConstants.STATE_ACTIVATED));
	}
	
	public List<Rules> getAllRules(){
		return this.list(this.query("SELECT r FROM Rules r"));
	}

}
