package com.secel.devopteam.GosMiddleware;

import io.dropwizard.Application;

import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.swagger.jaxrs.config.BeanConfig;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.db.DataSourceFactory;
import java.text.SimpleDateFormat;
import javax.servlet.FilterRegistration;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.servlet.DispatcherType;
import java.util.EnumSet;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.swagger.jaxrs.listing.ApiListingResource;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.secel.devopteam.GosMiddleware.beans.*;
import com.secel.devopteam.GosMiddleware.dao.*;
import com.secel.devopteam.GosMiddleware.api.MainController;
import com.secel.devopteam.GosMiddleware.api.UploadController;

public class GosMiddlewareApplication extends Application<GosMiddlewareConfiguration> {
	private static final Logger LOGGER = LoggerFactory.getLogger(GosMiddlewareApplication.class);

    public static void main(final String[] args) throws Exception {
        new GosMiddlewareApplication().run(args);
    }
    
    private final HibernateBundle<GosMiddlewareConfiguration> hibernate = new HibernateBundle<GosMiddlewareConfiguration>(
    		Agent.class, AgentDevice.class, AgentDevicePK.class, AgentSector.class,
    		AgentSectorPK.class, Device.class, DocType.class, Documents.class, GpsCoordinates.class,
			Sector.class, Site.class, Employee.class, AccessRight.class, AccessRightEmployeeGroup.class, AccessRightEmployeeGroupPK.class,
			EmployeeGroup.class, EmployeeGroupEmployee.class, EmployeeGroupEmployeePK.class, AgentActivity.class,
			Alerts.class, AlertsDocuments.class, District.class, Rules.class,DistrictRules.class,DistrictRulesPK.class) {
		@Override
		public DataSourceFactory getDataSourceFactory(GosMiddlewareConfiguration configuration) {
			return configuration.getDataSourceFactory();
		}

		@Override
		protected Hibernate5Module createHibernate5Module() {
			Hibernate5Module module = super.createHibernate5Module();
			module.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);
			module.disable(Hibernate5Module.Feature.FORCE_LAZY_LOADING);
			return module;
		}
		
	};

    @Override
    public String getName() {
        return "GosMiddleware";
    }

    @Override
    public void initialize(final Bootstrap<GosMiddlewareConfiguration> bootstrap) {
        // TODO: application initialization
    	bootstrap.addBundle(hibernate);
    	bootstrap.getObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));
    }

    @Override
    public void run(final GosMiddlewareConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
    	LOGGER.info("Registering REST resources");
    	// init Swagger resources
    	initSwagger(configuration, environment);
    	// Enable CORS headers
	    final FilterRegistration.Dynamic cors =
	    environment.servlets().addFilter("CORS", CrossOriginFilter.class);
	    cors.setInitParameter("allowedOrigins", "*");
	    cors.setInitParameter("allowedHeaders", "*");
	   // cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
	    cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");
	    cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        environment.jersey().register(new JsonProcessingExceptionMapper(true));
        environment.jersey().register(MultiPartFeature.class);
        
        final DeviceDao devicedao = new DeviceDao(hibernate.getSessionFactory());
        final AgentDao agentdao = new AgentDao(hibernate.getSessionFactory());
        final DocTypeDao docTypedao = new DocTypeDao(hibernate.getSessionFactory());
        final DocumentsDao documentsdao = new DocumentsDao(hibernate.getSessionFactory());
        final GpsCoordinatesDao gpscoordinatesdao = new GpsCoordinatesDao(hibernate.getSessionFactory());
        final DistrictDao districtdao = new DistrictDao(hibernate.getSessionFactory());
        final SiteDao sitedao= new SiteDao(hibernate.getSessionFactory());
        final AgentDeviceDao agentdevicedao= new AgentDeviceDao(hibernate.getSessionFactory());
        final AgentSectorDao agentsectordao= new AgentSectorDao(hibernate.getSessionFactory());
        final EmployeeDao employeedao= new EmployeeDao(hibernate.getSessionFactory());
        final EmployeeGroupDao employeeGroupDao= new EmployeeGroupDao(hibernate.getSessionFactory());
        final AccessRightDao accessrightdao= new AccessRightDao(hibernate.getSessionFactory());
        final EmployeeGroupEmployeeDao employeeGroupEmployeeDao= new EmployeeGroupEmployeeDao(hibernate.getSessionFactory());
        final AccessRightEmployeeGroupDao accessRightEmployeeGroupDao= new AccessRightEmployeeGroupDao(hibernate.getSessionFactory());
        final AgentActivityDao agentActivityDao= new AgentActivityDao(hibernate.getSessionFactory());
        final AlertsDao alertsDao= new AlertsDao(hibernate.getSessionFactory());
        final AlertsDocumentsDao alertsDocumentsDao= new AlertsDocumentsDao(hibernate.getSessionFactory());
        final SectorDao sectorDao = new SectorDao(hibernate.getSessionFactory());
        final DistrictRulesDao districtRulesdao = new DistrictRulesDao(hibernate.getSessionFactory());
    	final RulesDao rulesdao = new RulesDao(hibernate.getSessionFactory());
        
        environment.jersey().register(new MainController(configuration.getTokenkey(),devicedao, agentdao, docTypedao, documentsdao, gpscoordinatesdao, districtdao, sitedao, agentdevicedao, agentsectordao, employeedao, employeeGroupDao,accessrightdao, employeeGroupEmployeeDao, accessRightEmployeeGroupDao, agentActivityDao,alertsDocumentsDao, alertsDao,sectorDao,rulesdao,districtRulesdao));
        environment.jersey().register(new UploadController());
    }
    
    private void initSwagger(GosMiddlewareConfiguration configuration, Environment environment) {
		environment.jersey().register(new ApiListingResource());
		environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);

		BeanConfig config = new BeanConfig();
		config.setTitle("gos app");
		config.setVersion("1.0.0");
		config.setSchemes(new String[]{"http"});
        //config.setHost("localhost:8002");
        config.setBasePath("/api");
		config.setResourcePackage("com.secel.devopteam.GosMiddleware.api");
		config.setScan(true);
	}

}
