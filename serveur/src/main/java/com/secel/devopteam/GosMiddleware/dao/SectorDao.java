package com.secel.devopteam.GosMiddleware.dao;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.District;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class SectorDao extends AbstractDAO<Sector>{

	public SectorDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public Sector saveOrUpdateSector(@Valid Sector sector) {
		return persist(sector);
	}
	
	public boolean delete(Sector sector) {
		
		try {
			currentSession().delete(sector);
			return true;
		}catch (Exception e) {
			return false;
		}
	}
	
	public Sector findSectorById(int id) {
        return get(id);
    }
	
	public List<Sector> findAllSectorById(List<AgentSector> agentSectorList) {
		List<Sector> sectorList = new ArrayList<Sector>();
		for (AgentSector agtSect : agentSectorList) {
			Sector sect = agtSect.getSector();
			sectorList.add(sect);
		}
		return sectorList;
    }
	public List<Sector> getAllSectors() {
		return this.list(this.query("SELECT s FROM Sector s WHERE s.status = :status OR s.status = :statusOccupated").setShort("status", GosMiddlewareConstants.STATE_ACTIVATED).setShort("statusOccupated", GosMiddlewareConstants.STATE_USED_DEVICE)); 
	}
	
	public List<Sector> getAllFreeSectors() {
		return this.list(this.query("SELECT s FROM Sector s WHERE s.status = :status").setShort("status", GosMiddlewareConstants.STATE_ACTIVATED)); 
	}
 

}
