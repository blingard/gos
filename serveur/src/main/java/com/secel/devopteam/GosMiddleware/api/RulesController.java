package com.secel.devopteam.GosMiddleware.api;

import java.util.List;

import com.secel.devopteam.GosMiddleware.beans.Rules;
import com.secel.devopteam.GosMiddleware.dao.RulesDao;

public class RulesController {
	public static List<Rules> getAllRules(RulesDao ruleDao){
		return ruleDao.getAllActiveRules();
	}
	
	public static Rules saveUpdate(RulesDao ruleDao, Rules rule) {
		return ruleDao.saveUpdateRules(rule);	
	}

}
