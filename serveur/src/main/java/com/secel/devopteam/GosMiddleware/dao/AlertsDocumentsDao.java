package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.Alerts;
import com.secel.devopteam.GosMiddleware.beans.AlertsDocuments;

import io.dropwizard.hibernate.AbstractDAO;

public class AlertsDocumentsDao  extends AbstractDAO<AlertsDocuments>{

	public AlertsDocumentsDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	
	public AlertsDocuments saveOrUpdateAlertsDocuments(@Valid AlertsDocuments doc) {
		return persist(doc);
	}
	
	public AlertsDocuments findAlertsDocumentsById(int id) {
        return get(id);
    }
	
	public List<AlertsDocuments> getAllAlertsDocuments() {
		return this.list(this.query("SELECT d FROM AlertsDocuments d"));
	}
	public List<AlertsDocuments> getAllDocumentsForAlerts(Alerts alerts) {
		return this.list(this.query("SELECT d FROM AlertsDocuments d WHERE d.alertsId.alertsId= :alertsId").setInteger("alertsId", alerts.getAlertsId()));
	}

}
