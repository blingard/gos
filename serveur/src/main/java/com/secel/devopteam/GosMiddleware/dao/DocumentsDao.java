package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.Documents;
import com.secel.devopteam.GosMiddleware.beans.GpsCoordinates;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.beans.Site;

import io.dropwizard.hibernate.AbstractDAO;

public class DocumentsDao extends AbstractDAO<Documents>{

	public DocumentsDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public Documents saveOrUpdateDocuments(@Valid Documents doc) {
		return persist(doc);
	}
	
	public Documents findDocumentsById(int id) {
        return get(id);
    }
	
	public List<Documents> getAllDocuments() {
		return this.list(this.query("SELECT d FROM Documents d"));
	}
	public List<Documents> getAllDocumentsForSite(Site site) {
		return this.list(this.query("SELECT d FROM Documents d WHERE d.siteId.siteId= :siteId").setInteger("siteId", site.getSiteId()));
	}

}
