package com.secel.devopteam.GosMiddleware.api;


import java.io.File;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentActivity;
import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.District;
import com.secel.devopteam.GosMiddleware.beans.DocType;
import com.secel.devopteam.GosMiddleware.beans.Documents;
import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.beans.GpsCoordinates;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.beans.Site;
import com.secel.devopteam.GosMiddleware.dao.AgentActivityDao;
import com.secel.devopteam.GosMiddleware.dao.AgentSectorDao;
import com.secel.devopteam.GosMiddleware.dao.DocumentsDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeDao;
import com.secel.devopteam.GosMiddleware.dao.GpsCoordinatesDao;
import com.secel.devopteam.GosMiddleware.dao.DistrictDao;
import com.secel.devopteam.GosMiddleware.dao.SiteDao;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

public class SiteController {
	public static Site createSite (Site site, AgentActivity agentActivity, SiteDao siteDao, GpsCoordinatesDao gpscoordinatesDao, DocumentsDao documentsdao, AgentActivityDao agentActivityDao) throws Exception {
		 List<GpsCoordinates> listgps = site.getGpsCoordinatesList();
		 List<Documents> listDocs = site.getDocumentsList();
		 List<GpsCoordinates> coordList = new ArrayList<GpsCoordinates>();
		 List<Documents> docsList = new ArrayList<Documents>();
		 site.setGpsCoordinatesList(coordList);
		 site.setDocumentsList(docsList);
		 Site siteCreated = siteDao.saveOrUpdateSite(site);
		 if (siteCreated!=null) {
			 for(GpsCoordinates gpscoordinates: listgps) {
				 gpscoordinates.setSiteId(siteCreated);
				 gpscoordinatesDao.saveOrUpdateGpsCoordinates(gpscoordinates) ;
			 }
			 Documents docToSave = listDocs.get(0);
			 docToSave.setDocTypeId(new DocType(2));
			 docToSave.setSiteId(siteCreated);
			 docToSave.setDocumentName(listDocs.get(0).getDocumentPath().substring(listDocs.get(0).getDocumentPath().lastIndexOf(File.separator)+1, listDocs.get(0).getDocumentPath().lastIndexOf(".")));
			 docToSave.setDocumentExtension(listDocs.get(0).getDocumentPath().substring(listDocs.get(0).getDocumentPath().lastIndexOf(".")+1, listDocs.get(0).getDocumentPath().length()));
			 documentsdao.saveOrUpdateDocuments(docToSave);
		 }
		
		 agentActivity.setAgentAction(GosMiddlewareConstants.ACTION_CREATE_SITE);
		 agentActivity.setDocumentId(listDocs.get(0));
		 agentActivity.setAgentActivityDate(new Date());
		 agentActivityDao.saveOrUpdateAgentActivity(agentActivity);
		 return siteCreated;
	}
	
	public static Site createSiteFlutter (Site site, Agent agentId, SiteDao siteDao, GpsCoordinatesDao gpscoordinatesDao, DocumentsDao documentsdao, AgentActivityDao agentActivityDao) throws Exception {
		List<GpsCoordinates> listgps = site.getGpsCoordinatesList();
		List<Documents> listDocs = site.getDocumentsList();
		List<GpsCoordinates> coordList = new ArrayList<GpsCoordinates>();
		List<Documents> docsList = new ArrayList<Documents>();
		site.setGpsCoordinatesList(coordList);
		site.setDocumentsList(docsList);
		Site siteCreated = siteDao.saveOrUpdateSite(site);
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(GosMiddlewareConstants.DATE_FORMAT, Locale.FRENCH);
		if (siteCreated!=null) {
			for(GpsCoordinates gpscoordinates: listgps) {
				gpscoordinates.setSiteId(siteCreated);
				gpscoordinatesDao.saveOrUpdateGpsCoordinates(gpscoordinates) ;
			}
			Documents docToSave = listDocs.get(0);
			docToSave.setDocTypeId(listDocs.get(0).getDocTypeId());
			docToSave.setSiteId(siteCreated);			
			docToSave.setCreatedDate(simpleDateFormat.format(new Date()));
			docToSave.setDocumentExtension(listDocs.get(0).getDocumentPath().substring(listDocs.get(0).getDocumentPath().lastIndexOf(".")+1, listDocs.get(0).getDocumentPath().length()));
			documentsdao.saveOrUpdateDocuments(docToSave);
		}
		AgentActivity agentActivity = new AgentActivity();
		agentActivity.setAgentId(agentId);
		agentActivity.setAgentAction(GosMiddlewareConstants.ACTION_CREATE_SITE);
		agentActivity.setAgentActivityDate(date);
		agentActivity.setDocumentId(listDocs.get(0));
		agentActivityDao.saveOrUpdateAgentActivity(agentActivity);
		return siteCreated;
	}
	
	public static List<Site> getAllSites (SiteDao siteDao) throws Exception {
		return siteDao.getAllSites();
	}
	
	public static List<Site> getAllSitesWithoutConditions (SiteDao siteDao) throws Exception {
		return siteDao.getAllSitesWithoutConditions();
	}
	
	public static List<Site> getAllSiteByDistrict (District district, SiteDao siteDao) throws Exception {
		return siteDao.getAllSiteByDistrict(district);
	}
	
	public static Site getSiteById (Site site, SiteDao siteDao) throws Exception {
		return siteDao.findSiteById(site.getSiteId());
	}
	public static Site updateSite (Site site, Agent connectedAgent, AgentActivity agentActivity, SiteDao siteDao, GpsCoordinatesDao gpscoordinatesDao, DocumentsDao documentsdao,  AgentActivityDao agentActivityDao) throws Exception {
		 List<GpsCoordinates> listgps = site.getGpsCoordinatesList();
		 List<Documents> listDocs = site.getDocumentsList();
		 List<GpsCoordinates> coordList = new ArrayList<GpsCoordinates>();
		 List<Documents> docsList = new ArrayList<Documents>();
		 site.setGpsCoordinatesList(coordList);
		 site.setDocumentsList(docsList);
		 Site siteUpdated = siteDao.saveOrUpdateSite(site);
		 
		 if (siteUpdated!=null) {
			 for(GpsCoordinates gpscoordinates: listgps) {
				 if(gpscoordinates.getSiteId()== null) {
					 gpscoordinates.setSiteId(siteUpdated); 
				 }
				 gpscoordinatesDao.saveOrUpdateGpsCoordinates(gpscoordinates) ;
			 }
			 Documents docToSave = listDocs.get(0);
			 docToSave.setDocTypeId(new DocType(2));
			 docToSave.setSiteId(siteUpdated);
			 docToSave.setDocumentName(listDocs.get(0).getDocumentPath().substring(listDocs.get(0).getDocumentPath().lastIndexOf(File.separator)+1, listDocs.get(0).getDocumentPath().lastIndexOf(".")));
			 docToSave.setDocumentExtension(listDocs.get(0).getDocumentPath().substring(listDocs.get(0).getDocumentPath().lastIndexOf(".")+1, listDocs.get(0).getDocumentPath().length()));
			 documentsdao.saveOrUpdateDocuments(docToSave);
		 }
		 if(connectedAgent!=null) {
			 agentActivity.setAgentAction(GosMiddlewareConstants.ACTION_UPDATE_SITE);
			 agentActivity.setDocumentId(listDocs.get(0));
			 agentActivityDao.saveOrUpdateAgentActivity(agentActivity);
		 }
		return siteUpdated;
	}
	public static Site interruptWorkOfSite(Site site, SiteDao siteDao) {
		site.setStatus(GosMiddlewareConstants.STATE_INTERRUPT_WORK);
		Site SiteToUpdateDateToStop=siteDao.saveOrUpdateSite(site);
		return SiteToUpdateDateToStop;
	}
	public static Site relanceWorkOfSite(Site site, SiteDao siteDao) {
		site.setStatus(GosMiddlewareConstants.STATE_ACTIVATED);
		Site SiteToRelance=siteDao.saveOrUpdateSite(site);
		return SiteToRelance;
	}
	
	public static Site finishWorkOfSite(Site site, SiteDao siteDao) {
		site.setStatus(GosMiddlewareConstants.STATE_FINISH_WORK);
		Site SiteToUpdateDateToFinish=siteDao.saveOrUpdateSite(site);
		return SiteToUpdateDateToFinish;
	}
	public static List<Site> getAllSectorSitesForAgent (Agent agent, AgentSectorDao agentsectordao, SiteDao siteDao) throws Exception {
		List<AgentSector> sectorOfAgentList=agentsectordao.getAgentsSectorsByAgent(agent);
		List<Site>siteOfAgentList=new ArrayList<Site>();
		if(!sectorOfAgentList.isEmpty()) {
			for(AgentSector agSector:sectorOfAgentList ) {
				if(agSector.getEndDate()==null) {
					siteOfAgentList.addAll(siteDao.getAllSiteForSector(agSector.getSector()));
					/*siteOfAgentList= siteDao.getAllSiteForSector(agSector.getSector());
					for(Site sit:siteOfAgentList) {
						if(sit.getStatus()!=GosMiddlewareConstants.STATE_ACTIVATED) {
							siteOfAgentList.remove(sit);
						}
					}
					return siteOfAgentList;*/
				}
			}
			List<Site>sitesToRemove = new ArrayList<Site>();
			for(Site sit:siteOfAgentList) {
				if(sit.getStatus()!=GosMiddlewareConstants.STATE_ACTIVATED) {
					sitesToRemove.add(sit);
					//siteOfAgentList.remove(sit);
				}
			}
			if (!sitesToRemove.isEmpty()) {
				siteOfAgentList.removeAll(sitesToRemove);
			}
		}
		return siteOfAgentList;
	}
	public static List<Site> getSiteByRef (Site site, SiteDao siteDao) throws Exception {
		return siteDao.getSitesByRef(site);
	}

}
