/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "sector")
@NamedQueries({
    @NamedQuery(name = "Sector.findAll", query = "SELECT s FROM Sector s"),
    @NamedQuery(name = "Sector.findBySectorId", query = "SELECT s FROM Sector s WHERE s.sectorId = :sectorId"),
    @NamedQuery(name = "Sector.findBySectorName", query = "SELECT s FROM Sector s WHERE s.sectorName = :sectorName"),
    @NamedQuery(name = "Sector.findByDescription", query = "SELECT s FROM Sector s WHERE s.description = :description"),
    @NamedQuery(name = "Sector.findByStatus", query = "SELECT s FROM Sector s WHERE s.status = :status")})
public class Sector implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sector_id")
    private Integer sectorId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "sector_name")
    private String sectorName;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private short status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sector", fetch = FetchType.LAZY)
    private List<AgentSector> agentSectorList;
    @OneToMany(mappedBy = "sectorId", fetch = FetchType.LAZY)
    private List<District> districtList;

    
    public Sector() {
    }

    public Sector(Integer sectorId) {
        this.sectorId = sectorId;
    }

    public Sector(Integer sectorId, String sectorName, short status) {
        this.sectorId = sectorId;
        this.sectorName = sectorName;
        this.status = status;
    }

    public Integer getSectorId() {
        return sectorId;
    }

    public void setSectorId(Integer sectorId) {
        this.sectorId = sectorId;
    }

    public String getSectorName() {
        return sectorName;
    }

    public void setSectorName(String sectorName) {
        this.sectorName = sectorName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public List<AgentSector> getAgentSectorList() {
        return agentSectorList;
    }

    public void setAgentSectorList(List<AgentSector> agentSectorList) {
        this.agentSectorList = agentSectorList;
    }   

    public List<District> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<District> districtList) {
        this.districtList = districtList;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sectorId != null ? sectorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sector)) {
            return false;
        }
        Sector other = (Sector) object;
        if ((this.sectorId == null && other.sectorId != null) || (this.sectorId != null && !this.sectorId.equals(other.sectorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.secel.devopteam.GosMiddleware.beans.Sector[ sectorId=" + sectorId + " ]";
    }    
}
