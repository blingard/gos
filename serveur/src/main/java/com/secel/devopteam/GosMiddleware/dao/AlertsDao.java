package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.Alerts;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class AlertsDao extends AbstractDAO<Alerts>{
	
	public AlertsDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	public Alerts saveOrUpdateAlerts(@Valid Alerts alerts) {
		return persist(alerts);
	}
	public Alerts findAlertsById(int id) {
        return get(id);
    }
	public List<Alerts> getAllAlerts() {
		return this.list(this.query("SELECT d FROM Alerts d "));
	}
	public List<Alerts> getAllActivateAlerts() {
		return this.list(this.query("SELECT a FROM Alerts a WHERE a.status = :status").setShort("status", GosMiddlewareConstants.STATE_ACTIVATED));
	}
}
