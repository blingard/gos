/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JAGHO Brel
 */
@Entity
@Table(name = "gps_coordinates")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "GpsCoordinates.findAll", query = "SELECT g FROM GpsCoordinates g")
        , @NamedQuery(name = "GpsCoordinates.findByGpsCoordId", query = "SELECT g FROM GpsCoordinates g WHERE g.gpsCoordId = :gpsCoordId")
        , @NamedQuery(name = "GpsCoordinates.findByPointName", query = "SELECT g FROM GpsCoordinates g WHERE g.pointName = :pointName")
        , @NamedQuery(name = "GpsCoordinates.findByXCoord", query = "SELECT g FROM GpsCoordinates g WHERE g.xCoord = :xCoord")
        , @NamedQuery(name = "GpsCoordinates.findByYCoord", query = "SELECT g FROM GpsCoordinates g WHERE g.yCoord = :yCoord")})
public class GpsCoordinates implements Serializable {

        private static final long serialVersionUID = 1L;
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Basic(optional = false)
        @Column(name = "gps_coord_id")
        private Integer gpsCoordId;
        @Column(name = "point_name")
        private String pointName;
        // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
        @Column(name = "x_coord")
        private Float xCoord;
        @Column(name = "y_coord")
        private Float yCoord;
        @JoinColumn(name = "site_id", referencedColumnName = "site_id")
        @ManyToOne(optional = false, fetch = FetchType.EAGER)
        private Site siteId;

        public GpsCoordinates() {
        }

        public GpsCoordinates(Integer gpsCoordId) {
                this.gpsCoordId = gpsCoordId;
        }

        public Integer getGpsCoordId() {
                return gpsCoordId;
        }

        public void setGpsCoordId(Integer gpsCoordId) {
                this.gpsCoordId = gpsCoordId;
        }

        public String getPointName() {
                return pointName;
        }

        public void setPointName(String pointName) {
                this.pointName = pointName;
        }

        public Float getXCoord() {
                return xCoord;
        }

        public void setXCoord(Float xCoord) {
                this.xCoord = xCoord;
        }

        public Float getYCoord() {
                return yCoord;
        }

        public void setYCoord(Float yCoord) {
                this.yCoord = yCoord;
        }

        public Site getSiteId() {
                return siteId;
        }

        public void setSiteId(Site siteId) {
                this.siteId = siteId;
        }

        @Override
        public int hashCode() {
                int hash = 0;
                hash += (gpsCoordId != null ? gpsCoordId.hashCode() : 0);
                return hash;
        }

        @Override
        public boolean equals(Object object) {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof GpsCoordinates)) {
                        return false;
                }
                GpsCoordinates other = (GpsCoordinates) object;
                if ((this.gpsCoordId == null && other.gpsCoordId != null) || (this.gpsCoordId != null && !this.gpsCoordId.equals(other.gpsCoordId))) {
                        return false;
                }
                return true;
        }

        @Override
        public String toString() {
                return "generatebeans.page_gos.GpsCoordinates[ gpsCoordId=" + gpsCoordId + " ]";
        }
        
}
