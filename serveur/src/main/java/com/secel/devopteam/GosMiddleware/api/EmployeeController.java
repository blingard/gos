package com.secel.devopteam.GosMiddleware.api;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.NonUniqueObjectException;
import org.joda.time.DateTime;

import com.secel.devopteam.GosMiddleware.beans.AccessRight;
import com.secel.devopteam.GosMiddleware.beans.AccessRightEmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroupEmployee;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroupEmployeePK;
import com.secel.devopteam.GosMiddleware.dao.AccessRightDao;
import com.secel.devopteam.GosMiddleware.dao.AccessRightEmployeeGroupDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeGroupDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeGroupEmployeeDao;
import com.secel.devopteam.GosMiddleware.util.CryptographyException;
import com.secel.devopteam.GosMiddleware.util.EmployeeAccessRights;
import com.secel.devopteam.GosMiddleware.util.Encryptor;
import com.secel.devopteam.GosMiddleware.util.GlobalEmployee;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;
import com.secel.devopteam.GosMiddleware.util.SendMail;

public class EmployeeController {
	public static Employee createEmployee (Employee employee, EmployeeDao employeeDao, EmployeeGroupEmployeeDao employeeGroupEmployeedao,EmployeeGroupDao groupdao) throws Exception {
		try{
			String nonCryptedPassword = GosMiddlewareConstants.DEFAULT_EMPLOYEE_PASSWORD;
			String cryptedPassword = Encryptor.encrypt(nonCryptedPassword);
			employee.setEmployeePassword(cryptedPassword);
			employee.setIsFirstConnection(true);
			Employee employeReturn= employeeDao.saveOrUpdateEmployee(employee);
			
			EmployeeGroup group=groupdao.getEmployeeGroupByName("Employee");
			
			EmployeeGroupEmployeePK empGroupprimaryKey = new EmployeeGroupEmployeePK();
			empGroupprimaryKey.setEmployeeGroupId(group.getEmployeeGroupId());
			empGroupprimaryKey.setEmployeeId(employeReturn.getEmployeeId());
			empGroupprimaryKey.setFromDate(new Timestamp(System.currentTimeMillis()));
			
			EmployeeGroupEmployee empGroupEmployee = new EmployeeGroupEmployee();
			empGroupEmployee.setEmployee(employeReturn);
			empGroupEmployee.setEmployeeGroup(group);
			empGroupEmployee.setEmployeeGroupEmployeePK(empGroupprimaryKey);
			employeeGroupEmployeedao.assignEmployeeToGoup(empGroupEmployee);
			
			return employeReturn;
		}
		catch (CryptographyException ex ) {
			throw new CryptographyException("Impossible de crypter le mot de passe");
		}
	}
	
	public static List<Employee> getAllEmployees (EmployeeDao employeeDao) throws Exception {
		return employeeDao.getAllEmployees();
	}
	
	public static GlobalEmployee connectEmployeeWithLoginAndPass (Employee employee,EmployeeGroupEmployeeDao empGroupEmpDao, AccessRightEmployeeGroupDao arEmpGroupDao, EmployeeDao employeeDao) throws Exception {
		try{

			GlobalEmployee globalEmpToReturn = new GlobalEmployee();
			String nonCryptedPassword = employee.getEmployeePassword();
			String cryptedPassword = Encryptor.encrypt(nonCryptedPassword);
			employee.setEmployeePassword(cryptedPassword);
			Employee connectedEmployee=employeeDao.connectEmployeeWithLoginAndPass(employee);
			
			List<EmployeeGroupEmployee> LempGemp = empGroupEmpDao.getAllGroupsForEmployeeWhereEndDate(connectedEmployee);
			List<AccessRightEmployeeGroup> LaREmpG;

			List<EmployeeAccessRights> acessRightsList = new ArrayList<EmployeeAccessRights>();
			if(!LempGemp.isEmpty()) {
				for(EmployeeGroupEmployee empgemp : LempGemp){
					if(empgemp!=null) {
						LaREmpG = arEmpGroupDao.getAllAccessRightForEmployeeGroupWhereEndDate(empgemp.getEmployeeGroup());
						empgemp.getEmployeeGroup().setAccessRightEmployeeGroupList(LaREmpG);
						if (!LaREmpG.isEmpty()) {
							for (AccessRightEmployeeGroup aremg:LaREmpG) {
								EmployeeAccessRights accessRight = new EmployeeAccessRights();
								accessRight.setAccessRightId(aremg.getAccessRight().getAccessRightId());
								accessRight.setAccessRightName(aremg.getAccessRight().getAccessRightName());
								accessRight.setGroupId(aremg.getEmployeeGroup().getEmployeeGroupId());
								accessRight.setEmployeeId(connectedEmployee.getEmployeeId());
								acessRightsList.add(accessRight);
							}
						}
					}
				}
			}
			
			//connectedEmployee.setEmployeeGroupEmployeeList(LempGemp);	
			//connectedEmployee.setAccessRightList(acessRightsList);
			globalEmpToReturn.setEmployee(connectedEmployee);
			globalEmpToReturn.setRightsList(acessRightsList);
			return globalEmpToReturn;
			
		}
		catch (CryptographyException ex ) {
			ex.printStackTrace();
			throw new CryptographyException("Impossible de crypter le mot de passe");
		}
		
	}
	
	public static Employee updateEmployee (Employee employee, EmployeeDao employeeDao) throws Exception {
		System.err.println("updateEmployee");
		return employeeDao.saveOrUpdateEmployee(employee);
	}
	
	
	public static Employee getEmployeeById (Employee employee, EmployeeDao employeeDao) throws Exception {
		String CryptedPassword = employee.getEmployeePassword();
		String noncryptedPassword = Encryptor.decrypt(CryptedPassword);
		employee.setEmployeePassword(noncryptedPassword);
		return employeeDao.findEmployeeById(employee.getEmployeeId());
	}
	public static GlobalEmployee defineNewIdentifiers (Employee employee, EmployeeDao employeeDao,EmployeeGroupEmployeeDao empGroupEmpDao, AccessRightEmployeeGroupDao arEmpGroupDao) throws Exception {
		try{
			GlobalEmployee globalEmpToReturn = new GlobalEmployee();
			String nonCryptedPassword = employee.getEmployeePassword();
			String cryptedPassword = Encryptor.encrypt(nonCryptedPassword);
			employee.setEmployeePassword(cryptedPassword);
			Employee newEmployee = employeeDao.saveOrUpdateEmployee(employee);		
			
			List<EmployeeGroupEmployee> LempGemp = empGroupEmpDao.getAllGroupsForEmployeeWhereEndDate(newEmployee);
			List<AccessRightEmployeeGroup> LaREmpG;
			
			List<EmployeeAccessRights> acessRightsList = new ArrayList<EmployeeAccessRights>();
			if(!LempGemp.isEmpty()) {
				for(EmployeeGroupEmployee empgemp : LempGemp){
					if(empgemp!=null) {
						LaREmpG = arEmpGroupDao.getAllAccessRightForEmployeeGroupWhereEndDate(empgemp.getEmployeeGroup());
						empgemp.getEmployeeGroup().setAccessRightEmployeeGroupList(LaREmpG);
						if (!LaREmpG.isEmpty()) {
							for (AccessRightEmployeeGroup aremg:LaREmpG) {
								EmployeeAccessRights accessRight = new EmployeeAccessRights();
								accessRight.setAccessRightId(aremg.getAccessRight().getAccessRightId());
								accessRight.setAccessRightName(aremg.getAccessRight().getAccessRightName());
								accessRight.setGroupId(aremg.getEmployeeGroup().getEmployeeGroupId());
								accessRight.setEmployeeId(newEmployee.getEmployeeId());
								acessRightsList.add(accessRight);
							}
						}
					}
				}
			}
			
			globalEmpToReturn.setEmployee(newEmployee);
			globalEmpToReturn.setRightsList(acessRightsList);
			return globalEmpToReturn;
		}
		catch (CryptographyException ex ) {
			ex.printStackTrace();
			throw new CryptographyException("Impossible de crypter le mot de passe");
		} 
	}
	
	public static Employee rememberEmployeePassword (Employee employee, EmployeeDao employeeDao) throws Exception {
		List<Employee> empList = employeeDao.getEmployeesByLogin(employee);
		Employee empl = new Employee();
		try{
			if (!empList.isEmpty()) {
				empl = empList.get(0);
				String password = Encryptor.decrypt(empl.getEmployeePassword());
				SendMail.sendMailToSingleReceiver(employee.getEmployeeEmail(), "Renvoi de mot de passe", "Votre mot de passe est: "+password);
			}
			return empl;
		}
		catch (Exception ex ) {
			ex.printStackTrace();
			throw new CryptographyException("Impossible de crypter le mot de passe");
		} 
	}
	public static AccessRight createAccessRight(AccessRight accessRight, AccessRightDao accessrightdao) {
		return accessrightdao.saveOrUpdateAccessRight(accessRight);
	}
	
	public static EmployeeGroup createEmployeeGroup(EmployeeGroup userGroup, EmployeeGroupDao groupdao) {
		return groupdao.saveOrUpdateEmployeeGroup(userGroup);
		
	}
	
	public static EmployeeGroup findGroupById(int  EmployeeGroupId, EmployeeGroupDao employeeGroupdao) {
		return employeeGroupdao.findById(EmployeeGroupId);
	}
	
	public static List<AccessRight> getAllAccessRights (AccessRightDao accessRightDao) throws Exception {
		return accessRightDao.getAllAccessRights();
	}
	
	public static List<EmployeeGroup> getAllEmployeeGroups (EmployeeGroupDao employeeGroupDao) throws Exception {
		return employeeGroupDao.getAllGroups();
	}
	
	public static AccessRight updateAccessRight (AccessRight accessRight, AccessRightDao accessRightDao) throws Exception {
		return accessRightDao.saveOrUpdateAccessRight(accessRight);
	}
	
	public static EmployeeGroup updateEmployeeGroup (EmployeeGroup employeeGroup, EmployeeGroupDao employeeGroupDao) throws Exception {
		return employeeGroupDao.saveOrUpdateEmployeeGroup(employeeGroup);
	}
	
	public static EmployeeGroupEmployee affectEmployeeToGroup (EmployeeGroupEmployee employeeGroupEmployee, EmployeeGroupEmployeeDao employeeGroupEmployeeDao) throws Exception {
		return employeeGroupEmployeeDao.assignEmployeeToGoup(employeeGroupEmployee);
	}
	
	public static EmployeeGroupEmployee createEmployeeGroupEmployee(EmployeeGroupEmployee employeeGroupEmployee, EmployeeGroupEmployeeDao employeeGroupEmployeedao) {
		return employeeGroupEmployeedao.saveOrUpdateEmployeeGroupEmployee(employeeGroupEmployee);
		
	}
	
	public static List<EmployeeGroupEmployee> getAllEmployeeGroupEmployees (EmployeeGroupEmployeeDao employeeGroupEmployeeDao) throws Exception {
		List<EmployeeGroupEmployee>l1=employeeGroupEmployeeDao.getAllEmployeeGroupEmployees();
		List<EmployeeGroupEmployee>l2=new ArrayList<EmployeeGroupEmployee>();
		if(!l1.isEmpty()) {
			for(EmployeeGroupEmployee empGemp: l1) {
				if(empGemp.getEndDate()==null) {
					l2.add(empGemp);
				}
			}
		}
		
		return l2;
	}
	
	public static Employee changeEmployeePassword(Employee employee, String newPassword, EmployeeDao employeeDao) throws Exception {
			Employee employeeBD=employeeDao.findEmployeeById(employee.getEmployeeId());
			try {
				String cryptedPassword = employeeBD.getEmployeePassword();
				
				String decriptedPassword= Encryptor.decrypt(cryptedPassword);
				/*if(!decriptedPassword.equals(employee.getEmployeePassword())) {
					return null;
				}*/
				if(!decriptedPassword.equals(employee.getEmployeePassword())) {
					return null;
				}
				String cryptedNewPassword = Encryptor.encrypt(newPassword);
				employeeBD.setEmployeePassword(cryptedNewPassword);
				employee= employeeDao.saveOrUpdateEmployee(employeeBD);
				return employee;	
			}catch (CryptographyException ex ) {
					ex.printStackTrace();
					throw new CryptographyException("Impossible de crypter le mot de passe");
				}
	}
	public static EmployeeGroupEmployee updateEmployeeGroupEmployeeWhereEndDate (EmployeeGroupEmployee empGemp, EmployeeGroupEmployeeDao employeeGroupEmployeeDao) throws Exception {
		return employeeGroupEmployeeDao.assignEmployeeToGoup(empGemp);
	}
	public static EmployeeGroupEmployee updateEmployeeGroupEmployee (EmployeeGroupEmployee empGemp, EmployeeGroupEmployeeDao employeeGroupEmployeeDao) throws Exception {
		return employeeGroupEmployeeDao.saveOrUpdateEmployeeGroupEmployee(empGemp);
	}
	
	public static AccessRightEmployeeGroup updateAccessRightEmployeeGroupEndDate (AccessRightEmployeeGroup arempGroup, AccessRightEmployeeGroupDao accessRightEmployeeGroupDao) throws Exception {
		return accessRightEmployeeGroupDao.updateEndDateAccessRightEmployeeGroup(arempGroup);
	}
	public static AccessRightEmployeeGroup updateAccessRightEmployeeGroup (AccessRightEmployeeGroup arempGroup, AccessRightEmployeeGroupDao accessRightEmployeeGroupDao) throws Exception {
		return accessRightEmployeeGroupDao.saveOrUpdateAccessRightEmployeeGroup(arempGroup);
	}

	
	public static List<Employee> getEmployeesByLogin (Employee employee, EmployeeDao employeeDao) throws Exception {
		return employeeDao.getEmployeesByLogin(employee);
	}
	public static List<Employee> getEmployeesByMail (Employee employee, EmployeeDao employeeDao) throws Exception {
		return employeeDao.getEmployeesByMail(employee);
	}
	public static List<Employee> getEmployeesByTel (Employee employee, EmployeeDao employeeDao) throws Exception {
		return employeeDao.getEmployeesByTel(employee);
	}
	public static List<Employee> getEmployeesByMatric (Employee employee, EmployeeDao employeeDao) throws Exception {
		return employeeDao.getEmployeesByMatric(employee);
	}
	
	
}
