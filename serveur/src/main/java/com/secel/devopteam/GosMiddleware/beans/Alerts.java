/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "alerts")
@NamedQueries({
    @NamedQuery(name = "Alerts.findAll", query = "SELECT a FROM Alerts a"),
    @NamedQuery(name = "Alerts.findByAlertsId", query = "SELECT a FROM Alerts a WHERE a.alertsId = :alertsId"),
    @NamedQuery(name = "Alerts.findByAlertsMessage", query = "SELECT a FROM Alerts a WHERE a.alertsMessage = :alertsMessage"),
    @NamedQuery(name = "Alerts.findByAlertsDescription", query = "SELECT a FROM Alerts a WHERE a.alertsDescription = :alertsDescription"),
    @NamedQuery(name = "Alerts.findByPersonName", query = "SELECT a FROM Alerts a WHERE a.personName = :personName"),
    @NamedQuery(name = "Alerts.findByPersonPhone", query = "SELECT a FROM Alerts a WHERE a.personPhone = :personPhone"),
    @NamedQuery(name = "Alerts.findByAlertsDistrict", query = "SELECT a FROM Alerts a WHERE a.alertsDistrict = :alertsDistrict"),
    @NamedQuery(name = "Alerts.findByAlertsDate", query = "SELECT a FROM Alerts a WHERE a.alertsDate = :alertsDate"),
    @NamedQuery(name = "Alerts.findByStatus", query = "SELECT a FROM Alerts a WHERE a.status = :status")})
public class Alerts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "alerts_id")
    private Integer alertsId;
    @Column(name = "alerts_message")
    private String alertsMessage;
    @Column(name = "alerts_description")
    private String alertsDescription;
    @Column(name = "person_name")
    private String personName;
    @Column(name = "person_phone")
    private String personPhone;
    @Column(name = "alerts_district")
    private String alertsDistrict;
    @Basic(optional = false)
    @Column(name = "alerts_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date alertsDate;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "alertsId", fetch = FetchType.LAZY)
    private List<AlertsDocuments> alertsDocumentsList;

    public Alerts() {
    }

    public Alerts(Integer alertsId) {
        this.alertsId = alertsId;
    }

    public Alerts(Integer alertsId, Date alertsDate, short status) {
        this.alertsId = alertsId;
        this.alertsDate = alertsDate;
        this.status = status;
    }

    public Integer getAlertsId() {
        return alertsId;
    }

    public void setAlertsId(Integer alertsId) {
        this.alertsId = alertsId;
    }

    public String getAlertsMessage() {
        return alertsMessage;
    }

    public void setAlertsMessage(String alertsMessage) {
        this.alertsMessage = alertsMessage;
    }

    public String getAlertsDescription() {
        return alertsDescription;
    }

    public void setAlertsDescription(String alertsDescription) {
        this.alertsDescription = alertsDescription;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonPhone() {
        return personPhone;
    }

    public void setPersonPhone(String personPhone) {
        this.personPhone = personPhone;
    }

    public String getAlertsDistrict() {
        return alertsDistrict;
    }

    public void setAlertsDistrict(String alertsDistrict) {
        this.alertsDistrict = alertsDistrict;
    }

    public Date getAlertsDate() {
        return alertsDate;
    }

    public void setAlertsDate(Date alertsDate) {
        this.alertsDate = alertsDate;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public List<AlertsDocuments> getAlertsDocumentsList() {
        return alertsDocumentsList;
    }

    public void setAlertsDocumentsList(List<AlertsDocuments> alertsDocumentsList) {
        this.alertsDocumentsList = alertsDocumentsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (alertsId != null ? alertsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alerts)) {
            return false;
        }
        Alerts other = (Alerts) object;
        if ((this.alertsId == null && other.alertsId != null) || (this.alertsId != null && !this.alertsId.equals(other.alertsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.generatedbeans.Alerts[ alertsId=" + alertsId + " ]";
    }
    
}
