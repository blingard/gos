/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ken
 */
@Embeddable
public class EmployeeGroupEmployeePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "employee_group_id")
    private long employeeGroupId;
    @Basic(optional = false)
    @Column(name = "employee_id")
    private long employeeId;
    @Basic(optional = false)
    @Column(name = "from_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;

    public EmployeeGroupEmployeePK() {
    }

    public EmployeeGroupEmployeePK(long employeeGroupId, long employeeId, Date fromDate) {
        this.employeeGroupId = employeeGroupId;
        this.employeeId = employeeId;
        this.fromDate = fromDate;
    }

    public long getEmployeeGroupId() {
        return employeeGroupId;
    }

    public void setEmployeeGroupId(long employeeGroupId) {
        this.employeeGroupId = employeeGroupId;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) employeeGroupId;
        hash += (int) employeeId;
        hash += (fromDate != null ? fromDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeGroupEmployeePK)) {
            return false;
        }
        EmployeeGroupEmployeePK other = (EmployeeGroupEmployeePK) object;
        if (this.employeeGroupId != other.employeeGroupId) {
            return false;
        }
        if (this.employeeId != other.employeeId) {
            return false;
        }
        if ((this.fromDate == null && other.fromDate != null) || (this.fromDate != null && !this.fromDate.equals(other.fromDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.EmployeeGroupEmployeePK[ employeeGroupId=" + employeeGroupId + ", employeeId=" + employeeId + ", fromDate=" + fromDate + " ]";
    }
    
}
