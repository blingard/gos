/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Blingard
 */
@Entity
@Table(name = "district_rules")
@NamedQueries({
    @NamedQuery(name = "DistrictRules.findAll", query = "SELECT d FROM DistrictRules d")})
public class DistrictRules implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DistrictRulesPK districtRulesPK;
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "district_id", referencedColumnName = "district_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private District district;
    @JoinColumn(name = "rules_id", referencedColumnName = "rules_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Rules rules;

    public DistrictRules() {
    }

    public DistrictRules(DistrictRulesPK districtRulesPK) {
        this.districtRulesPK = districtRulesPK;
    }

    public DistrictRules(DistrictRulesPK districtRulesPK, short status) {
        this.districtRulesPK = districtRulesPK;
        this.status = status;
    }

    public DistrictRules(long districtId, long rulesId, Date createDate) {
        this.districtRulesPK = new DistrictRulesPK(districtId, rulesId, createDate);
    }

    public DistrictRulesPK getDistrictRulesPK() {
        return districtRulesPK;
    }

    public void setDistrictRulesPK(DistrictRulesPK districtRulesPK) {
        this.districtRulesPK = districtRulesPK;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Rules getRules() {
        return rules;
    }

    public void setRules(Rules rules) {
        this.rules = rules;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (districtRulesPK != null ? districtRulesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DistrictRules)) {
            return false;
        }
        DistrictRules other = (DistrictRules) object;
        if ((this.districtRulesPK == null && other.districtRulesPK != null) || (this.districtRulesPK != null && !this.districtRulesPK.equals(other.districtRulesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.secel.devopteam.GosMiddleware.beans.DistrictRules[ districtRulesPK=" + districtRulesPK + " ]";
    }
    
}
