package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.District;
import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.beans.Site;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class SiteDao extends AbstractDAO<Site> {

	public SiteDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public Site saveOrUpdateSite(@Valid Site site) {
		return persist(site);
	}
	
	public Site findSiteById(int id) {
        return get(id);
    }
	
	public List<Site> getAllSites() {
		return this.list(this.query("SELECT s FROM Site s WHERE s.status = :status").setShort("status", GosMiddlewareConstants.STATE_ACTIVATED));
	}
	
	public List<Site> getAllSiteForDistrict(District district) {
		return this.list(this.query("SELECT s FROM Site s WHERE s.districtId.districtId = :districtId").setInteger("districtId", district.getDistrictId()));
	}
	
	public List<Site> getAllSitesWithoutConditions() {
		return this.list(this.query("SELECT s FROM Site s"));
	}
	
	public List<Site> getSitesByRef(Site site) {
        return this.list(this.query("SELECT e FROM Site e WHERE e.refSite= :refSite")
        		.setString("refSite", site.getRefSite()));
    }
	
	public List<Site> getAllSiteForSector(Sector sector) {
		return this.list(this.query("SELECT s FROM Site s WHERE s.sectorId.sectorId = :sectorId").setInteger("sectorId", sector.getSectorId()));
	}
	public List<Site> getAllSiteByDistrict(District district) {
		return this.list(this.query("SELECT s FROM Site s WHERE s.districtId.districtId = :districtId").setInteger("districtId", district.getDistrictId()));
	}

}

	
	
	
