package com.secel.devopteam.GosMiddleware.api;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.secel.devopteam.GosMiddleware.beans.AgentActivity;
import com.secel.devopteam.GosMiddleware.beans.DocType;
import com.secel.devopteam.GosMiddleware.beans.Documents;
import com.secel.devopteam.GosMiddleware.beans.Site;
import com.secel.devopteam.GosMiddleware.dao.AgentActivityDao;
import com.secel.devopteam.GosMiddleware.dao.DocTypeDao;
import com.secel.devopteam.GosMiddleware.dao.DocumentsDao;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;
public class DocumentsController {
	
	public static Documents createDocuments (Documents documents,AgentActivity agentActivity, DocumentsDao documentsDao, AgentActivityDao agentActivityDao) throws Exception {
		documents.setDocumentExtension(documents.getDocumentPath().substring(documents.getDocumentPath().lastIndexOf(".")+1, documents.getDocumentPath().length()));
		agentActivity.setAgentAction(GosMiddlewareConstants.ACTION_DOC_SITE);
		agentActivity.setDocumentId(documents);
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(GosMiddlewareConstants.DATE_FORMAT, Locale.FRENCH);
		agentActivity.setAgentActivityDate(date);
		documents.setCreatedDate(simpleDateFormat.format(new Date()));
		agentActivityDao.saveOrUpdateAgentActivity(agentActivity);
		return documentsDao.saveOrUpdateDocuments(documents);
	}
	public static Documents createDocumentDesktop (Documents documents, DocumentsDao documentsDao) throws Exception {
		documents.setStatus(GosMiddlewareConstants.STATE_VERIF_DOC);
		documents.setDocumentExtension(documents.getDocumentPath().substring(documents.getDocumentPath().lastIndexOf(".")+1, documents.getDocumentPath().length()));
		return documentsDao.saveOrUpdateDocuments(documents);
	}
	public static Documents updateDocument (Documents documents, DocumentsDao documentsDao) throws Exception {
		return documentsDao.saveOrUpdateDocuments(documents);
	}
	public static List<Documents> getAllDocuments (DocumentsDao documentsDao) throws Exception {
		return documentsDao.getAllDocuments();
	}
	public static Documents getDocumentsById (Documents documents, DocumentsDao documentsDao) throws Exception {
		return documentsDao.findDocumentsById(documents.getDocumentId());
	}
	public static List<Documents> getAllSiteDocuments (Site site, DocumentsDao documentsDao) throws Exception {
		return documentsDao.getAllDocumentsForSite(site);
	}
	
	public static List<DocType> getAllDocTypes (DocTypeDao docTypeDao) throws Exception {
		return docTypeDao.getAllDocType();
	}
	public static Documents createDocumentsFlutter(Documents documents, AgentActivity agentActivity,
		DocumentsDao documentsdao, AgentActivityDao agentActivityDao, String acte) {
		documents.setDocumentExtension(documents.getDocumentPath().substring(documents.getDocumentPath().lastIndexOf(".")+1, documents.getDocumentPath().length()));
		//if(!documents.getDocumentNumber().isEmpty() && !documents.getDocumentCode().equals("CO")) {
			//documents.setStatus(GosMiddlewareConstants.STATE_VERIF_DOC);
		//}
		agentActivity.setAgentAction(acte);
		agentActivity.setDocumentId(documents);
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(GosMiddlewareConstants.DATE_FORMAT, Locale.FRENCH);
		agentActivity.setAgentActivityDate(date);
		documents.setCreatedDate(simpleDateFormat.format(new Date()));
		agentActivityDao.saveOrUpdateAgentActivity(agentActivity);
		return documentsdao.saveOrUpdateDocuments(documents);
	}

}
