package com.secel.devopteam.GosMiddleware.api;

import java.util.List;

import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.dao.SectorDao;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

public class SectorController {
	public static Sector createSector (Sector sector, SectorDao sectorDao) throws Exception {
		return sectorDao.saveOrUpdateSector(sector);
	}
	public static List<Sector> getAllSectors (SectorDao sectorDao) throws Exception {
		return sectorDao.getAllSectors();
	}
	
	public static List<Sector> getAllFreeSectors (SectorDao sectorDao) throws Exception {
		return sectorDao.getAllFreeSectors();
	}
	
	public static Sector getSectorById (Sector sector, SectorDao sectorDao) throws Exception {
		return sectorDao.findSectorById(sector.getSectorId());
	}
	
	public static List<Sector> getAllSectorByAgentId (List<AgentSector> agtsector, SectorDao sectorDao) throws Exception {
		return sectorDao.findAllSectorById(agtsector);
	}
	
	public static Sector updateSector (Sector sector, SectorDao sectorDao) throws Exception {
		sector.setStatus(GosMiddlewareConstants.STATE_ACTIVATED);
		return sectorDao.saveOrUpdateSector(sector);    
	}
	public static boolean deleteSector (Sector sector, SectorDao sectorDao) throws Exception {
		 return sectorDao.delete(sector);  
	}
	/*public static List<Site> getAllSiteOfSector (Sector sector, SiteDao siteDao) throws Exception {
		return siteDao.getAllSiteForSector(sector);
	}*/

}
