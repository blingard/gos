package com.secel.devopteam.GosMiddleware.api;

import java.util.List;

import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.Alerts;
import com.secel.devopteam.GosMiddleware.dao.AgentDao;
import com.secel.devopteam.GosMiddleware.dao.AlertsDao;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

public class AlertsController {
	
	public static Alerts createAlerts (Alerts alerts, AlertsDao alertsDao) throws Exception {
		alerts.setStatus(GosMiddlewareConstants.STATE_ACTIVATED);
		return alertsDao.saveOrUpdateAlerts(alerts);
	}
	public static List<Alerts> getAllAlerts (AlertsDao alertsDao) throws Exception {
		return alertsDao.getAllAlerts();
	}
	public static List<Alerts> getAllActivateAlerts (AlertsDao alertsDao) throws Exception {
		return alertsDao.getAllActivateAlerts();
	}
	public static Alerts getAlertsById (Alerts alerts, AlertsDao alertsDao) throws Exception {
		return alertsDao.findAlertsById(alerts.getAlertsId());
	}
	public static Alerts updateAlerts (Alerts alert, AlertsDao alertDao) throws Exception {
		return alertDao.saveOrUpdateAlerts(alert); 
	}

}
