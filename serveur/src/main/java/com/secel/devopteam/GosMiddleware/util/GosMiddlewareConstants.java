package com.secel.devopteam.GosMiddleware.util;

/**
 * 
 * @author JAGHO Brel
 *
 */
public class GosMiddlewareConstants {
	public static String TEST="administrator_right";
	
	//Durée maximale d'inactivité (30 min)
	public static final int
	TIMEOUT_VALUE=30;
	
	//Codes d'erreurs
	public static final int
	USER_NON_AUTHENTICATED = 401,
	CONNECTION_TIMEOUT = 403,
	ERROR_PAGE_NOT_FOUND = 404,
	SERVER_ERROR = 500,
	SERVER_DENY_RESPONSE = 504;
	
	public static final String 
	USER_PORT = "8051",
	DEFAULT_EMPLOYEE_PASSWORD = "employe2020",
	DEFAULT_AGENT_PASSWORD = "agent2020",
	ACTION_CREATE_SITE = "Création complete du site",
	ACTION_DOC_SITE = "Ajout d'un document sur le site",
	ACTION_UPDATE_SITE = "Téléversement d'un document",
	ACTION_RELANCEMENT_SITE = "Relancement",
	CONVOCATION="Convocation",
	ACTION_DOC_TYPE="IMAGE";
	/*
	 * si un document existe metre le status a 0 , si il n'existe pas mettre le status a 1, en attente de verification mettre a 4, creation des permis metre a 5
	 * 
	 * 
	*/
	
	public static final short
	STATE_ACTIVATED=0,
	STATE_DELETED=2,
	STATE_ARCHIVE=1,
	STATE_USED_DEVICE=3,
	STATE_INTERRUPT_WORK=4,
	STATE_NOTIFICATION_READ=1,
	STATE_NOTIFICATION_UNREAD=0,
	STATE_VERIF_DOC=5,
	STATE_WAIT_VERIF_DOC=4,
	STATE_FINISH_WORK=5; 
	
	
	//AccessRights management
	public static final String
	EMPLOYEE_ACCESS = "public",
	ADMIN_ACCESS = "administrator",
	
	EMPLOYEE_ACCESS_RIGHT = "employee_right",
	ADMIN_ACCESS_RIGHT = "administrator_right",
	DATE_FORMAT = "d MMM yyyy HH:mm:ss",
	ROOT_RIGHT = "All";
	
	

}
