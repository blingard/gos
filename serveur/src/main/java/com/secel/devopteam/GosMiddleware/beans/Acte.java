/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "acte")
@NamedQueries({
    @NamedQuery(name = "Acte.findAll", query = "SELECT a FROM Acte a"),
    @NamedQuery(name = "Acte.findByActeId", query = "SELECT a FROM Acte a WHERE a.acteId = :acteId"),
    @NamedQuery(name = "Acte.findByIntitule", query = "SELECT a FROM Acte a WHERE a.intitule = :intitule"),
    @NamedQuery(name = "Acte.findByActeDate", query = "SELECT a FROM Acte a WHERE a.acteDate = :acteDate")})
public class Acte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "acte_id")
    private Integer acteId;
    @Column(name = "intitule")
    private String intitule;
    @Basic(optional = false)
    @Column(name = "acte_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date acteDate;

    public Acte() {
    }

    public Acte(Integer acteId) {
        this.acteId = acteId;
    }

    public Acte(Integer acteId, Date acteDate) {
        this.acteId = acteId;
        this.acteDate = acteDate;
    }

    public Integer getActeId() {
        return acteId;
    }

    public void setActeId(Integer acteId) {
        this.acteId = acteId;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Date getActeDate() {
        return acteDate;
    }

    public void setActeDate(Date acteDate) {
        this.acteDate = acteDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (acteId != null ? acteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acte)) {
            return false;
        }
        Acte other = (Acte) object;
        if ((this.acteId == null && other.acteId != null) || (this.acteId != null && !this.acteId.equals(other.acteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.generatedbeans.Acte[ acteId=" + acteId + " ]";
    }
    
}
