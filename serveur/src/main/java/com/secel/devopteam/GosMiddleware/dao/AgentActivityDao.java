package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentActivity;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class AgentActivityDao extends AbstractDAO<AgentActivity>{
	
	public AgentActivityDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	public AgentActivity saveOrUpdateAgentActivity(@Valid AgentActivity agentActivity) {
		return persist(agentActivity);
	}
	public AgentActivity findAgentActivityById(int id) {
        return get(id);
    }
	
	public List<AgentActivity> getAllAgentActivitys() {
		return this.list(this.query("SELECT a FROM AgentActivity a"));
	}
	public List<AgentActivity> getAllActivitysByAgent(Agent agent) {
        return this.list(this.query("SELECT a FROM AgentActivity a WHERE a.agentId= :agentId")
        		.setInteger("agentId", agent.getAgentId()));
    }
	public List<AgentActivity> getAgentActivitiesConvocation() {
		return this.list(this.query("SELECT a FROM AgentActivity a WHERE a.agentAction= :agentAction")
        		.setParameter("agentAction", GosMiddlewareConstants.CONVOCATION));
	}
	public List<AgentActivity> getAgentActivitiesConvocationOk() {
		return this.list(this.query("SELECT a FROM AgentActivity a WHERE a.agentAction= :agentAction AND a.status= :status")
        		.setParameter("agentAction", GosMiddlewareConstants.CONVOCATION).setParameter("status", GosMiddlewareConstants.STATE_ACTIVATED));
	}

}
