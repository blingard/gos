/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JAGHO Brel
 */
@Entity
@Table(name = "agent_sector")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "AgentSector.findAll", query = "SELECT a FROM AgentSector a")
        , @NamedQuery(name = "AgentSector.findByAgentId", query = "SELECT a FROM AgentSector a WHERE a.agentSectorPK.agentId = :agentId")
        , @NamedQuery(name = "AgentSector.findBySectorId", query = "SELECT a FROM AgentSector a WHERE a.agentSectorPK.sectorId = :sectorId")
        , @NamedQuery(name = "AgentSector.findByBeginDate", query = "SELECT a FROM AgentSector a WHERE a.agentSectorPK.beginDate = :beginDate")
        , @NamedQuery(name = "AgentSector.findByEndDate", query = "SELECT a FROM AgentSector a WHERE a.endDate = :endDate")})
public class AgentSector implements Serializable {

        private static final long serialVersionUID = 1L;
        @EmbeddedId
        protected AgentSectorPK agentSectorPK;
        @Column(name = "end_date")
        @Temporal(TemporalType.TIMESTAMP)
        private Date endDate;
        @JoinColumn(name = "agent_id", referencedColumnName = "agent_id", insertable = false, updatable = false)
        @ManyToOne(optional = false, fetch = FetchType.EAGER)
        private Agent agent;
        @JoinColumn(name = "sector_id", referencedColumnName = "sector_id", insertable = false, updatable = false)
        @ManyToOne(optional = false, fetch = FetchType.EAGER)
        private Sector sector;

        public AgentSector() {
        } 

        public AgentSector(AgentSectorPK agentSectorPK) {
                this.agentSectorPK = agentSectorPK;
        }

        public AgentSector(long agentId, long sectorId, Date beginDate) {
                this.agentSectorPK = new AgentSectorPK(agentId, sectorId, beginDate);
        }

        public AgentSectorPK getAgentSectorPK() {
                return agentSectorPK;
        }

        public void setAgentSectorPK(AgentSectorPK agentSectorPK) {
                this.agentSectorPK = agentSectorPK;
        }

        public Date getEndDate() {
                return endDate;
        }

        public void setEndDate(Date endDate) {
                this.endDate = endDate;
        }

        public Agent getAgent() {
                return agent;
        }

        public void setAgent(Agent agent) {
                this.agent = agent;
        }

        public Sector getSector() {
                return sector;
        }

        public void setSector(Sector sector) {
                this.sector = sector;
        }

        @Override
        public int hashCode() {
                int hash = 0;
                hash += (agentSectorPK != null ? agentSectorPK.hashCode() : 0);
                return hash;
        }

        @Override
        public boolean equals(Object object) {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof AgentSector)) {
                        return false;
                }
                AgentSector other = (AgentSector) object;
                if ((this.agentSectorPK == null && other.agentSectorPK != null) || (this.agentSectorPK != null && !this.agentSectorPK.equals(other.agentSectorPK))) {
                        return false;
                }
                return true;
        }

		@Override
		public String toString() {
			return "AgentSector [agentSectorPK=" + agentSectorPK + ", endDate=" + endDate + ", agent=" + agent
					+ ", sector=" + sector + "]";
		}

        
        
}
