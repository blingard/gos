/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ken
 */
@Entity
@Table(name = "access_right")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccessRight.findAll", query = "SELECT a FROM AccessRight a")
    , @NamedQuery(name = "AccessRight.findByAccessRightId", query = "SELECT a FROM AccessRight a WHERE a.accessRightId = :accessRightId")
    , @NamedQuery(name = "AccessRight.findByAccessRightName", query = "SELECT a FROM AccessRight a WHERE a.accessRightName = :accessRightName")
    , @NamedQuery(name = "AccessRight.findByAccessRightStatus", query = "SELECT a FROM AccessRight a WHERE a.accessRightStatus = :accessRightStatus")})
public class AccessRight implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "access_right_id")
    private Integer accessRightId;
    @Column(name = "access_right_name")
    private String accessRightName;
    @Basic(optional = false)
    @Column(name = "access_right_status")
    private short accessRightStatus;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accessRight", fetch = FetchType.LAZY)
    private List<AccessRightEmployeeGroup> accessRightEmployeeGroupList;

    public AccessRight() {
    }

    public AccessRight(Integer accessRightId) {
        this.accessRightId = accessRightId;
    }

    public AccessRight(Integer accessRightId, short accessRightStatus) {
        this.accessRightId = accessRightId;
        this.accessRightStatus = accessRightStatus;
    }

    public Integer getAccessRightId() {
        return accessRightId;
    }

    public void setAccessRightId(Integer accessRightId) {
        this.accessRightId = accessRightId;
    }

    public String getAccessRightName() {
        return accessRightName;
    }

    public void setAccessRightName(String accessRightName) {
        this.accessRightName = accessRightName;
    }

    public short getAccessRightStatus() {
        return accessRightStatus;
    }

    public void setAccessRightStatus(short accessRightStatus) {
        this.accessRightStatus = accessRightStatus;
    }

    @XmlTransient
    public List<AccessRightEmployeeGroup> getAccessRightEmployeeGroupList() {
        return accessRightEmployeeGroupList;
    }

    public void setAccessRightEmployeeGroupList(List<AccessRightEmployeeGroup> accessRightEmployeeGroupList) {
        this.accessRightEmployeeGroupList = accessRightEmployeeGroupList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accessRightId != null ? accessRightId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccessRight)) {
            return false;
        }
        AccessRight other = (AccessRight) object;
        if ((this.accessRightId == null && other.accessRightId != null) || (this.accessRightId != null && !this.accessRightId.equals(other.accessRightId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.AccessRight[ accessRightId=" + accessRightId + " ]";
    }
    
}
