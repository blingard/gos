package com.secel.devopteam.GosMiddleware.dao;


import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.Device;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class DeviceDao extends AbstractDAO<Device>{
	
	public DeviceDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	
	public Device saveOrUpdateDevice(@Valid Device device) {
		return persist(device);
	}
	
	public Device findDeviceById(int id) {
        return get(id);
    }
	
	public List<Device> getAllDevices() {
		return this.list(this.query("SELECT d FROM Device d Where d.status= :status OR d.status = :statusOccupated").setShort("status", GosMiddlewareConstants.STATE_ACTIVATED).setShort("statusOccupated", GosMiddlewareConstants.STATE_USED_DEVICE));
	}
	
	public List<Device> getAllFreeDevices() {
		return this.list(this.query("SELECT d FROM Device d Where d.status= :status").setShort("status", GosMiddlewareConstants.STATE_ACTIVATED));
	}
}
