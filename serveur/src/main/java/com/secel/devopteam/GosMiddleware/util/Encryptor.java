package com.secel.devopteam.GosMiddleware.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Classe utilitaire pour le cryptage et le décryptage des mots de passe.
 * 
 * @author JAGHO Brel
 * @version 1.0
 */

public class Encryptor {
	/**
	 * Forme courte de l'algorithme de cryptage par défaut
	 */
	public static final String DEFAULT_ENCRYPTION_ALGO_SHORT = "AES";
	
	/**
	 * Forme longue de l'algorithme de cryptage par défaut
	 */
	public static final String DEFAULT_ENCRYPTION_ALGO_LONG = "AES/CBC/PKCS5Padding";
	
	/**
	 * Clé privée de cryptage (<i>n'utiliser qu'en cas de besoin réel</i>)
	 */
	public static final String DEFAULT_ENCRYPTION_SECRET_KEY = "#1_%$M-(>=*/@]{+";
	
	/**
	 * Vecteur d'initialisation de cryptage (<i>ne utiliser qu'en cas de besoin r�el</i>) 
	 */
	public static final String DEFAULT_ENCRYPTION_INIT_VECTOR = "<~r2&-I|g.q[}9s:";

	public static String encrypt(String message) throws CryptographyException{

		try {
			SecretKeySpec secretKeySpec = new SecretKeySpec(DEFAULT_ENCRYPTION_SECRET_KEY.getBytes(), DEFAULT_ENCRYPTION_ALGO_SHORT);
			IvParameterSpec ivParamSpec = new IvParameterSpec(DEFAULT_ENCRYPTION_INIT_VECTOR.getBytes());
			/*Random rand = new SecureRandom();
			byte[] bytes = new byte[16];
			rand.nextBytes(bytes);
			IvParameterSpec ivParamSpec = new IvParameterSpec(bytes);*/
	 
			Cipher cipher = Cipher.getInstance(DEFAULT_ENCRYPTION_ALGO_LONG);
			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParamSpec);
	 
			byte[] messageBytes = message.getBytes("UTF8");
			
			byte[] encryptedMessageBytes = cipher.doFinal(messageBytes);
			BASE64Encoder base64Encoder = new BASE64Encoder();
			
			return base64Encoder.encode(encryptedMessageBytes).replace("=", "b-@").replace("+", "-)-");
			
		} catch (IllegalBlockSizeException e) {
			throw new CryptographyException(e);
		} catch (BadPaddingException e) {
			throw new CryptographyException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new CryptographyException(e);
		} catch (NoSuchPaddingException e) {
			throw new CryptographyException(e);
		} catch (InvalidKeyException e) {
			throw new CryptographyException(e);
		} catch (InvalidAlgorithmParameterException e) {
			throw new CryptographyException(e);
		} catch (UnsupportedEncodingException e) {
			throw new CryptographyException(e);
		}
	}
	
	public static String decrypt(String encryptedMessage) throws CryptographyException{

		try {
			encryptedMessage = encryptedMessage.replace("b-@", "=").replace("-)-", "+");
			SecretKeySpec secretKeySpec = new SecretKeySpec(DEFAULT_ENCRYPTION_SECRET_KEY.getBytes(), DEFAULT_ENCRYPTION_ALGO_SHORT);
			IvParameterSpec ivParamSpec = new IvParameterSpec(DEFAULT_ENCRYPTION_INIT_VECTOR.getBytes());
	 
			Cipher cipher = Cipher.getInstance(DEFAULT_ENCRYPTION_ALGO_LONG);
			cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParamSpec);
			
			BASE64Decoder base64Decoder = new BASE64Decoder();
			byte[] encryptedMessageBytes = base64Decoder.decodeBuffer(encryptedMessage);
			byte[] messageBytes = cipher.doFinal(encryptedMessageBytes);
			
			return new String(messageBytes, "UTF8");
		} catch (IllegalBlockSizeException e) {
			throw new CryptographyException(e);
		} catch (BadPaddingException e) {
			throw new CryptographyException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new CryptographyException(e);
		} catch (NoSuchPaddingException e) {
			throw new CryptographyException(e);
		} catch (InvalidKeyException e) {
			throw new CryptographyException(e);
		} catch (InvalidAlgorithmParameterException e) {
			throw new CryptographyException(e);
		} catch (UnsupportedEncodingException e) {
			throw new CryptographyException(e);
		} catch (IOException e) {
			throw new CryptographyException(e);
		}
	}
}
