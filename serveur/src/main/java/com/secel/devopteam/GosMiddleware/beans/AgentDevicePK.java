/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author JAGHO Brel
 */
@Embeddable
public class AgentDevicePK implements Serializable {

        @Basic(optional = false)
        @Column(name = "agent_id")
        private long agentId;
        @Basic(optional = false)
        @Column(name = "device_id")
        private long deviceId;
        @Basic(optional = false)
        @Column(name = "begin_date")
        @Temporal(TemporalType.TIMESTAMP)
        private Date beginDate;

        public AgentDevicePK() {
        }

        public AgentDevicePK(long agentId, long deviceId, Date beginDate) {
                this.agentId = agentId;
                this.deviceId = deviceId;
                this.beginDate = beginDate;
        }

        public long getAgentId() {
                return agentId;
        }

        public void setAgentId(long agentId) {
                this.agentId = agentId;
        }

        public long getDeviceId() {
                return deviceId;
        }

        public void setDeviceId(long deviceId) {
                this.deviceId = deviceId;
        }

        public Date getBeginDate() {
                return beginDate;
        }

        public void setBeginDate(Date beginDate) {
                this.beginDate = beginDate;
        }

        @Override
        public int hashCode() {
                int hash = 0;
                hash += (int) agentId;
                hash += (int) deviceId;
                hash += (beginDate != null ? beginDate.hashCode() : 0);
                return hash;
        }

        @Override
        public boolean equals(Object object) {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof AgentDevicePK)) {
                        return false;
                }
                AgentDevicePK other = (AgentDevicePK) object;
                if (this.agentId != other.agentId) {
                        return false;
                }
                if (this.deviceId != other.deviceId) {
                        return false;
                }
                if ((this.beginDate == null && other.beginDate != null) || (this.beginDate != null && !this.beginDate.equals(other.beginDate))) {
                        return false;
                }
                return true;
        }

        @Override
        public String toString() {
                return "generatebeans.page_gos.AgentDevicePK[ agentId=" + agentId + ", deviceId=" + deviceId + ", beginDate=" + beginDate + " ]";
        }
        
}
