/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JAGHO Brel
 */
@Entity
@Table(name = "agent_device")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "AgentDevice.findAll", query = "SELECT a FROM AgentDevice a")
        , @NamedQuery(name = "AgentDevice.findByAgentId", query = "SELECT a FROM AgentDevice a WHERE a.agentDevicePK.agentId = :agentId")
        , @NamedQuery(name = "AgentDevice.findByDeviceId", query = "SELECT a FROM AgentDevice a WHERE a.agentDevicePK.deviceId = :deviceId")
        , @NamedQuery(name = "AgentDevice.findByBeginDate", query = "SELECT a FROM AgentDevice a WHERE a.agentDevicePK.beginDate = :beginDate")
        , @NamedQuery(name = "AgentDevice.findByEndDate", query = "SELECT a FROM AgentDevice a WHERE a.endDate = :endDate")})
public class AgentDevice implements Serializable {

        private static final long serialVersionUID = 1L;
        @EmbeddedId
        protected AgentDevicePK agentDevicePK;
        @Column(name = "end_date")
        @Temporal(TemporalType.TIMESTAMP)
        private Date endDate;
        @JoinColumn(name = "agent_id", referencedColumnName = "agent_id", insertable = false, updatable = false)
        @ManyToOne(optional = false, fetch = FetchType.EAGER)
        private Agent agent;
        @JoinColumn(name = "device_id", referencedColumnName = "device_id", insertable = false, updatable = false)
        @ManyToOne(optional = false, fetch = FetchType.EAGER)
        private Device device;

        public AgentDevice() {
        }

        public AgentDevice(AgentDevicePK agentDevicePK) {
                this.agentDevicePK = agentDevicePK;
        }

        public AgentDevice(long agentId, long deviceId, Date beginDate) {
                this.agentDevicePK = new AgentDevicePK(agentId, deviceId, beginDate);
        }

        public AgentDevicePK getAgentDevicePK() {
                return agentDevicePK;
        }

        public void setAgentDevicePK(AgentDevicePK agentDevicePK) {
                this.agentDevicePK = agentDevicePK;
        }

        public Date getEndDate() {
                return endDate;
        }

        public void setEndDate(Date endDate) {
                this.endDate = endDate;
        }

        public Agent getAgent() {
                return agent;
        }

        public void setAgent(Agent agent) {
                this.agent = agent;
        }

        public Device getDevice() {
                return device;
        }

        public void setDevice(Device device) {
                this.device = device;
        }

        @Override
        public int hashCode() {
                int hash = 0;
                hash += (agentDevicePK != null ? agentDevicePK.hashCode() : 0);
                return hash;
        }

        @Override
        public boolean equals(Object object) {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof AgentDevice)) {
                        return false;
                }
                AgentDevice other = (AgentDevice) object;
                if ((this.agentDevicePK == null && other.agentDevicePK != null) || (this.agentDevicePK != null && !this.agentDevicePK.equals(other.agentDevicePK))) {
                        return false;
                }
                return true;
        }

        @Override
        public String toString() {
                return "generatebeans.page_gos.AgentDevice[ agentDevicePK=" + agentDevicePK + " ]";
        }
        
}
