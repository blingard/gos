package com.secel.devopteam.GosMiddleware.model;

import com.secel.devopteam.GosMiddleware.beans.Alerts;
import com.secel.devopteam.GosMiddleware.beans.AlertsDocuments;

public class ViewsAlertsDocuments {
	private Alerts alerts;
	private AlertsDocuments alertsDocuments;
	
	public ViewsAlertsDocuments(Alerts alerts, AlertsDocuments alertsDocuments) {
		super();
		this.alerts = alerts;
		this.alertsDocuments = alertsDocuments;
	}

	public ViewsAlertsDocuments() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Alerts getAlerts() {
		return alerts;
	}

	public void setAlerts(Alerts alerts) {
		this.alerts = alerts;
	}

	public AlertsDocuments getAlertsDocuments() {
		return alertsDocuments;
	}

	public void setAlertsDocuments(AlertsDocuments alertsDocuments) {
		this.alertsDocuments = alertsDocuments;
	}
	
	
}
