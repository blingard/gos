package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;


import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class EmployeeDao extends AbstractDAO<Employee> {
	
	public EmployeeDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public Employee saveOrUpdateEmployee(Employee employee) {
		return persist(employee);
	}
	public Employee connectEmployee(int id) {
		
		return get(id);
	}
	public List<Employee> getAllEmployees() {
		return this.list(this.query("SELECT a FROM Employee a WHERE a.status = :status").setShort("status", GosMiddlewareConstants.STATE_ACTIVATED));
	}
	
	public Employee connectEmployeeWithLoginAndPass(Employee employee) {
		return this.uniqueResult(this.query("SELECT e FROM Employee e WHERE e.employeeLogin= :employeeLogin AND e.employeePassword= :employeePassword AND e.status= :status")
        		.setString("employeeLogin", employee.getEmployeeLogin()).setString("employeePassword", employee.getEmployeePassword()).setShort("status", GosMiddlewareConstants.STATE_ACTIVATED));
    }
	public Employee findEmployeeById(int id) {
        return get(id);
    }
	
	public List<Employee> getEmployeesByLogin(Employee employee) {
        return this.list(this.query("SELECT e FROM Employee e WHERE e.employeeLogin= :employeeLogin")
        		.setString("employeeLogin", employee.getEmployeeLogin()));
    }
	
	public List<Employee> getEmployeesByMail(Employee employee) {
        return this.list(this.query("SELECT e FROM Employee e WHERE e.employeeEmail= :employeeEmail")
        		.setString("employeeEmail", employee.getEmployeeEmail()));
    }
	
	public Employee verifEmployeeByLoginAndMail(Employee employee) {
        return this.uniqueResult(this.query("SELECT e FROM Employee e WHERE e.employeeLogin= :employeeLogin AND e.employeeEmail= :employeeEmail")
        		.setString("employeeEmail", employee.getEmployeeEmail()).setString("employeeLogin", employee.getEmployeeLogin()));
    }
	
	public List<Employee> getEmployeesByTel(Employee employee) {
        return this.list(this.query("SELECT e FROM Employee e WHERE e.employeePhoneNumber= :employeePhoneNumber")
        		.setString("employeePhoneNumber", employee.getEmployeePhoneNumber()));
    }
	
	public List<Employee> getEmployeesByMatric(Employee employee) {
        return this.list(this.query("SELECT e FROM Employee e WHERE e.employeeMatricule= :employeeMatricule")
        		.setString("employeeMatricule", employee.getEmployeeMatricule()));
    }
	
}
