package com.secel.devopteam.GosMiddleware.model;



import com.secel.devopteam.GosMiddleware.beans.AgentActivity;

public class AlertSystem {
	private AgentActivity agentActivitiesConvocation;
	private long days;
	public AlertSystem() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AlertSystem(AgentActivity agentActivitiesConvocation, long days) {
		super();
		this.agentActivitiesConvocation = agentActivitiesConvocation;
		this.days = days;
	}
	public AgentActivity getAgentActivitiesConvocation() {
		return agentActivitiesConvocation;
	}
	public void setAgentActivitiesConvocation(AgentActivity agentActivitiesConvocation) {
		this.agentActivitiesConvocation = agentActivitiesConvocation;
	}
	public long getDays() {
		return days;
	}
	public void setDays(long days) {
		this.days = days;
	}
	
	
	

}
