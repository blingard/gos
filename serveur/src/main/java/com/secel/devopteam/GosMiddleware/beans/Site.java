/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "site")
@NamedQueries({
    @NamedQuery(name = "Site.findAll", query = "SELECT s FROM Site s"),
    @NamedQuery(name = "Site.findBySiteId", query = "SELECT s FROM Site s WHERE s.siteId = :siteId"),
    @NamedQuery(name = "Site.findByRefSite", query = "SELECT s FROM Site s WHERE s.refSite = :refSite"),
    @NamedQuery(name = "Site.findBySiteName", query = "SELECT s FROM Site s WHERE s.siteName = :siteName"),
    @NamedQuery(name = "Site.findByOwnerName", query = "SELECT s FROM Site s WHERE s.ownerName = :ownerName"),
    @NamedQuery(name = "Site.findByOwnerPhone", query = "SELECT s FROM Site s WHERE s.ownerPhone = :ownerPhone"),
    @NamedQuery(name = "Site.findByOwnerCni", query = "SELECT s FROM Site s WHERE s.ownerCni = :ownerCni"),
    @NamedQuery(name = "Site.findByDescription", query = "SELECT s FROM Site s WHERE s.description = :description"),
    @NamedQuery(name = "Site.findByDateToStop", query = "SELECT s FROM Site s WHERE s.dateToStop = :dateToStop"),
    @NamedQuery(name = "Site.findByDateToFinish", query = "SELECT s FROM Site s WHERE s.dateToFinish = :dateToFinish"),
    @NamedQuery(name = "Site.findByStatus", query = "SELECT s FROM Site s WHERE s.status = :status")})
public class Site implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "site_id")
    private Integer siteId;
    @Column(name = "ref_site")
    private String refSite;
    @Column(name = "site_name")
    private String siteName;
    @Basic(optional = false)
    @Column(name = "owner_name")
    private String ownerName;
    @Column(name = "owner_phone")
    private String ownerPhone;
    @Column(name = "owner_cni")
    private String ownerCni;
    @Column(name = "description")
    private String description;
    @Column(name = "date_to_stop")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateToStop;
    @Column(name = "date_to_finish")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateToFinish;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @OneToMany(mappedBy = "siteId", fetch = FetchType.LAZY)
    private List<Documents> documentsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "siteId", fetch = FetchType.LAZY)
    private List<GpsCoordinates> gpsCoordinatesList;
    @JoinColumn(name = "district_id", referencedColumnName = "district_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private District districtId;

    public Site() {
    }

    public Site(Integer siteId) {
        this.siteId = siteId;
    }

    public Site(Integer siteId, String ownerName, short status) {
        this.siteId = siteId;
        this.ownerName = ownerName;
        this.status = status;
    }

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    public String getRefSite() {
        return refSite;
    }

    public void setRefSite(String refSite) {
        this.refSite = refSite;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    public String getOwnerCni() {
        return ownerCni;
    }

    public void setOwnerCni(String ownerCni) {
        this.ownerCni = ownerCni;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateToStop() {
        return dateToStop;
    }

    public void setDateToStop(Date dateToStop) {
        this.dateToStop = dateToStop;
    }

    public Date getDateToFinish() {
        return dateToFinish;
    }

    public void setDateToFinish(Date dateToFinish) {
        this.dateToFinish = dateToFinish;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }


    public List<Documents> getDocumentsList() {
        return documentsList;
    }

    public void setDocumentsList(List<Documents> documentsList) {
        this.documentsList = documentsList;
    }

    public List<GpsCoordinates> getGpsCoordinatesList() {
        return gpsCoordinatesList;
    }

    public void setGpsCoordinatesList(List<GpsCoordinates> gpsCoordinatesList) {
        this.gpsCoordinatesList = gpsCoordinatesList;
    }

    public District getDistrictId() {
        return districtId;
    }

    public void setDistrictId(District districtId) {
        this.districtId = districtId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (siteId != null ? siteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Site)) {
            return false;
        }
        Site other = (Site) object;
        if ((this.siteId == null && other.siteId != null) || (this.siteId != null && !this.siteId.equals(other.siteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.generatedbeans.Site[ siteId=" + siteId + " ]";
    }
    
}
