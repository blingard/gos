package com.secel.devopteam.GosMiddleware.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentDevice;
import com.secel.devopteam.GosMiddleware.beans.Device;
import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroupEmployee;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class AgentDeviceDao extends AbstractDAO<AgentDevice>{

	public AgentDeviceDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	
	public List<AgentDevice> getAllDeviceForAgent(Agent agent) {
		return this.list(this.query("SELECT a FROM AgentDevice a WHERE a.agent.agentId= :agentId").setInteger("agentId", agent.getAgentId()));
	}
	
	/*public List<AgentDevice> assignDeviceToAgent(Agent agent) {
		return this.list(this.query("SELECT a FROM AgentDevice a WHERE a.endDate= :null"));
	}*/
	
	public List<AgentDevice> getAllAgentDevice() {
		return this.list(this.query("SELECT a FROM AgentDevice a "));
	}
	
	public AgentDevice assignDeviceToAgent(@Valid AgentDevice agtDevice, DeviceDao deviceDao) {
		 List<AgentDevice> listDevices = this.list(this.query("SELECT a FROM AgentDevice a WHERE a.agent.agentId = :agentId")
				 .setInteger("agentId", agtDevice.getAgent().getAgentId()));
		 
		 boolean existingValue = false;
		 if (listDevices.isEmpty()) {
			 Device dev = deviceDao.findDeviceById(agtDevice.getDevice().getDeviceId());
			 agtDevice.setDevice(dev);
			 agtDevice.getDevice().setStatus(GosMiddlewareConstants.STATE_USED_DEVICE);
			 return persist(agtDevice);
		 }
		 else {
			 for (AgentDevice agentDevice : listDevices) {
				if(agentDevice.getEndDate() == null) {
					agentDevice.setEndDate(agtDevice.getAgentDevicePK().getBeginDate());
					agentDevice.getDevice().setStatus(GosMiddlewareConstants.STATE_ACTIVATED);
					this.persist(agentDevice);
				}
				else {
					if (agentDevice.getDevice().getDeviceId() == agtDevice.getDevice().getDeviceId()) {
						existingValue = true;
						agtDevice.setDevice(agentDevice.getDevice());
					}
				}
			}
			 if (!existingValue) {
				 Device dev = deviceDao.findDeviceById(agtDevice.getDevice().getDeviceId());
				 agtDevice.setDevice(dev);
			 }
			 agtDevice.getDevice().setStatus(GosMiddlewareConstants.STATE_USED_DEVICE);
			return this.persist(agtDevice);
		}
	}
	public AgentDevice assignDevicesToAgent(@Valid AgentDevice AgDevice) {
		return this.persist(AgDevice); 
	}
	public List<AgentDevice> getAllAgentDeviceEndDate(Agent agent) {
		List<AgentDevice> l1=this.list(this.query("SELECT a FROM AgentDevice a WHERE a.AgentDevicePK.agentId= :agentId").setInteger("agentId", agent.getAgentId()));
		for(AgentDevice agDevice: l1) {
			if(agDevice.getEndDate()!=null) {
				l1.remove(agDevice);
			}
			
		}
		return l1;
	}
	public List<AgentDevice> getAllAgentDevices() {
		return this.list(this.query("SELECT a FROM AgentDevice a "));
	}
	

}
