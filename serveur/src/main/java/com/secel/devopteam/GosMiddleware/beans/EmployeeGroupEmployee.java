/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ken
 */
@Entity
@Table(name = "employee_group_employee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmployeeGroupEmployee.findAll", query = "SELECT e FROM EmployeeGroupEmployee e")
    , @NamedQuery(name = "EmployeeGroupEmployee.findByEmployeeGroupId", query = "SELECT e FROM EmployeeGroupEmployee e WHERE e.employeeGroupEmployeePK.employeeGroupId = :employeeGroupId")
    , @NamedQuery(name = "EmployeeGroupEmployee.findByEmployeeId", query = "SELECT e FROM EmployeeGroupEmployee e WHERE e.employeeGroupEmployeePK.employeeId = :employeeId")
    , @NamedQuery(name = "EmployeeGroupEmployee.findByFromDate", query = "SELECT e FROM EmployeeGroupEmployee e WHERE e.employeeGroupEmployeePK.fromDate = :fromDate")
    , @NamedQuery(name = "EmployeeGroupEmployee.findByEndDate", query = "SELECT e FROM EmployeeGroupEmployee e WHERE e.endDate = :endDate")})
public class EmployeeGroupEmployee implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EmployeeGroupEmployeePK employeeGroupEmployeePK;
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Employee employee;
    @JoinColumn(name = "employee_group_id", referencedColumnName = "employee_group_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private EmployeeGroup employeeGroup;

    public EmployeeGroupEmployee() {
    }

    public EmployeeGroupEmployee(EmployeeGroupEmployeePK employeeGroupEmployeePK) {
        this.employeeGroupEmployeePK = employeeGroupEmployeePK;
    }

    public EmployeeGroupEmployee(long employeeGroupId, long employeeId, Date fromDate) {
        this.employeeGroupEmployeePK = new EmployeeGroupEmployeePK(employeeGroupId, employeeId, fromDate);
    }

    public EmployeeGroupEmployeePK getEmployeeGroupEmployeePK() {
        return employeeGroupEmployeePK;
    }

    public void setEmployeeGroupEmployeePK(EmployeeGroupEmployeePK employeeGroupEmployeePK) {
        this.employeeGroupEmployeePK = employeeGroupEmployeePK;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public EmployeeGroup getEmployeeGroup() {
        return employeeGroup;
    }

    public void setEmployeeGroup(EmployeeGroup employeeGroup) {
        this.employeeGroup = employeeGroup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeeGroupEmployeePK != null ? employeeGroupEmployeePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeGroupEmployee)) {
            return false;
        }
        EmployeeGroupEmployee other = (EmployeeGroupEmployee) object;
        if ((this.employeeGroupEmployeePK == null && other.employeeGroupEmployeePK != null) || (this.employeeGroupEmployeePK != null && !this.employeeGroupEmployeePK.equals(other.employeeGroupEmployeePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.EmployeeGroupEmployee[ employeeGroupEmployeePK=" + employeeGroupEmployeePK + " ]";
    }
    
}
