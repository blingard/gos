package com.secel.devopteam.GosMiddleware.api;

import java.io.File;
import javax.validation.Validator;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.http.HttpStatus;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.joda.time.DateTime;

import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import com.secel.devopteam.GosMiddleware.util.ErrorObject;
import com.secel.devopteam.GosMiddleware.util.UploadResponse;
import com.secel.devopteam.GosMiddleware.util.Util;

/**
 * 
 * @author JAGHO Brel
 *
 */

@Path("/gosfile")
@Api(value = "/gosfile")
@Produces(MediaType.APPLICATION_JSON)
public class UploadController {
	//private UserDao userDao;
	//private final Validator validator;

	public UploadController() {
		//this.userDao = userDao;
	}
	
	@POST
	@Path("/uploadvideo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Proprieté invalide") })
	@UnitOfWork
    public Response uploadVideoFile(@Context HttpHeaders httpHeaders,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {
		if(Util.isRequestValid(httpHeaders) /*&& Util.isTokenValid(httpHeaders, userDao)*/){
			UploadResponse upload = processUpload(Util.LOCAL_VIDEO_STORAGE, uploadedInputStream, fileDetail);
	        return Response.status(HttpStatus.OK_200).entity(upload).build();
		}else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
    }
	
	@POST
	@Path("/uploadaudio")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Proprieté invalide") })
	@UnitOfWork
    public Response uploadAudioFile(@Context HttpHeaders httpHeaders,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {
		if(Util.isRequestValid(httpHeaders) /*&& Util.isTokenValid(httpHeaders, userDao)*/){
			UploadResponse upload = processUpload(Util.LOCAL_AUDIO_STORAGE, uploadedInputStream, fileDetail);
	        return Response.status(HttpStatus.OK_200).entity(upload).build();
		}else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
    }
	
	@POST
	@Path("/uploadimage")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Proprieté invalide") })
	//@UnitOfWork
    public Response uploadImageFile(@Context HttpHeaders httpHeaders,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {
		if(Util.isRequestValid(httpHeaders) /*&& Util.isTokenValid(httpHeaders, userDao)*/){
			UploadResponse upload = processUpload(Util.LOCAL_IMAGE_STORAGE, uploadedInputStream, fileDetail);
	        return Response.status(HttpStatus.OK_200).entity(upload).build();
		}else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
    }
	
	@POST
	@Path("/uploadtext")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Proprieté invalide") })
	//@UnitOfWork
    public Response uploadTextFile(@Context HttpHeaders httpHeaders,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {
		if(Util.isRequestValid(httpHeaders) /*&& Util.isTokenValid(httpHeaders, userDao)*/){
			UploadResponse upload = processUpload(Util.LOCAL_TEXT_STORAGE, uploadedInputStream, fileDetail);
	        return Response.status(HttpStatus.OK_200).entity(upload).build();
		}else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
    }
	
	@POST
	@Path("/deletefile")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Proprieté invalide") })
	@UnitOfWork
	public Response deleteFile(@Context HttpHeaders httpHeaders, String fileKey) {
		if(Util.isRequestValid(httpHeaders) /*&& Util.isTokenValid(httpHeaders, userDao)*/){
			File file = new File(fileKey);
			if(file.exists() && file.delete())
				return Response.status(HttpStatus.OK_200).build();
			else{
				ErrorObject errorObject = new ErrorObject(500, "Could not process the deletion");
				return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).entity(errorObject).build();
			}
		}else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}

	private UploadResponse processUpload(String storagePath, InputStream uploadedInputStream, FormDataContentDisposition fileDetail) throws IOException {
		Util.createFolderIfNotExists(storagePath);
		String path = (new File(storagePath)).getAbsolutePath();
		path = path.replace("\\", "/");
		if(!path.endsWith("/"))
			path += "/";
        String uploadedFileLocation = DateTime.now().getMillis() + "-" + fileDetail.getFileName();
		String fileKey = storagePath + "/" + uploadedFileLocation;
		uploadedFileLocation = path + uploadedFileLocation;
        writeToFile(uploadedInputStream, uploadedFileLocation);
        UploadResponse upload = new UploadResponse(fileKey);
        return upload;
	}
    
    private void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) throws IOException {
        int read;
        final int BUFFER_LENGTH = 1024;
        final byte[] buffer = new byte[BUFFER_LENGTH];
        OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
        while ((read = uploadedInputStream.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        out.flush();
        out.close();
    }
    
    @GET
    @Path("/download")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiResponses(value = { @ApiResponse(code = 400, message = "Proprieté invalide") })
	//@UnitOfWork
    public Response downloadFile(@QueryParam(value = "fileKey") String fileKey) {
        File fileDownload = new File(fileKey);
        String [] parts = fileKey.split("/");
        return Response.ok((Object) fileDownload).header("Content-Disposition", "attachment;filename=" + parts[parts.length - 1]).build();
    }
}
