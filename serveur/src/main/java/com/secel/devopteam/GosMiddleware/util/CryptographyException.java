package com.secel.devopteam.GosMiddleware.util;

/**
 * Cette exception doit être levée pour des erreurs de cryptographie.
 * 
 * @author JAGHO Brel
 * @version 1.0
 */
public class CryptographyException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public CryptographyException(String message) {
        super( message );
    }
    public CryptographyException(String message, Throwable cause) {
        super( message, cause );
    }
    public CryptographyException(Throwable cause) {
        super( cause );
    }
}
