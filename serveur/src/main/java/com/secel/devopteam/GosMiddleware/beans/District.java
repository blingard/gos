/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "district")
@NamedQueries({
    @NamedQuery(name = "District.findAll", query = "SELECT d FROM District d"),
    @NamedQuery(name = "District.findByDistrictId", query = "SELECT d FROM District d WHERE d.districtId = :districtId"),
    @NamedQuery(name = "District.findByDistrictName", query = "SELECT d FROM District d WHERE d.districtName = :districtName"),
    @NamedQuery(name = "District.findByCommune", query = "SELECT d FROM District d WHERE d.commune = :commune"),
    @NamedQuery(name = "District.findByStanding", query = "SELECT d FROM District d WHERE d.standing = :standing"),
    @NamedQuery(name = "District.findByPopulation", query = "SELECT d FROM District d WHERE d.population = :population"),
    @NamedQuery(name = "District.findByDensite", query = "SELECT d FROM District d WHERE d.densite = :densite"),
    @NamedQuery(name = "District.findByDescription", query = "SELECT d FROM District d WHERE d.description = :description"),
    @NamedQuery(name = "District.findByStatus", query = "SELECT d FROM District d WHERE d.status = :status")})
public class District implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "district_id")
    private Integer districtId;
    @Basic(optional = false)
    @Column(name = "district_name")
    private String districtName;
    @Basic(optional = false)
    @Column(name = "commune")
    private String commune;
    @Basic(optional = false)
    @Column(name = "standing")
    private String standing;
    @Basic(optional = false)
    @Column(name = "population")
    private String population;
    @Basic(optional = false)
    @Column(name = "densite")
    private String densite;
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "districtId", fetch = FetchType.LAZY)
    private List<Site> siteList;
   // @OneToMany(cascade = CascadeType.ALL, mappedBy = "district", fetch = FetchType.LAZY)
   // private List<DistrictUrbanRules> districtUrbanRulesList;
    @JoinColumn(name = "sector_id", referencedColumnName = "sector_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Sector sectorId;

    public District() {
    }

    public District(Integer districtId) {
        this.districtId = districtId;
    }

    public District(Integer districtId, String districtName, String commune, String standing, String population, String densite, short status) {
        this.districtId = districtId;
        this.districtName = districtName;
        this.commune = commune;
        this.standing = standing;
        this.population = population;
        this.densite = densite;
        this.status = status;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getStanding() {
        return standing;
    }

    public void setStanding(String standing) {
        this.standing = standing;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getDensite() {
        return densite;
    }

    public void setDensite(String densite) {
        this.densite = densite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public List<Site> getSiteList() {
        return siteList;
    }

    public void setSiteList(List<Site> siteList) {
        this.siteList = siteList;
    }
    
   /* public List<DistrictUrbanRules> getDistrictUrbanRulesList() {
        return districtUrbanRulesList;
    }

    public void setDistrictUrbanRulesList(List<DistrictUrbanRules> districtUrbanRulesList) {
        this.districtUrbanRulesList = districtUrbanRulesList;
    }*/

    public Sector getSectorId() {
        return sectorId;
    }

    public void setSectorId(Sector sectorId) {
        this.sectorId = sectorId;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (districtId != null ? districtId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof District)) {
            return false;
        }
        District other = (District) object;
        if ((this.districtId == null && other.districtId != null) || (this.districtId != null && !this.districtId.equals(other.districtId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.generatedbeans.District[ districtId=" + districtId + " ]";
    }
    
}
