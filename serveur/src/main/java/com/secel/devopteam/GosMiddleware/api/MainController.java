package com.secel.devopteam.GosMiddleware.api;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.GET;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import org.eclipse.jetty.http.HttpStatus;
import org.joda.time.DateTime;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import sun.management.resources.agent;

import com.google.gson.Gson;
import com.secel.devopteam.GosMiddleware.beans.AccessRight;
import com.secel.devopteam.GosMiddleware.beans.AccessRightEmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.Agent;
import com.secel.devopteam.GosMiddleware.beans.AgentActivity;
import com.secel.devopteam.GosMiddleware.beans.AgentSector;
import com.secel.devopteam.GosMiddleware.beans.Alerts;
import com.secel.devopteam.GosMiddleware.beans.AlertsDocuments;
import com.secel.devopteam.GosMiddleware.beans.AgentDevice;
import com.secel.devopteam.GosMiddleware.beans.Device;
import com.secel.devopteam.GosMiddleware.beans.District;
import com.secel.devopteam.GosMiddleware.beans.DistrictRules;
import com.secel.devopteam.GosMiddleware.beans.DocType;
import com.secel.devopteam.GosMiddleware.beans.Documents;
import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroupEmployee;
import com.secel.devopteam.GosMiddleware.beans.GpsCoordinates;
import com.secel.devopteam.GosMiddleware.beans.Rules;
import com.secel.devopteam.GosMiddleware.beans.Sector;
import com.secel.devopteam.GosMiddleware.beans.Site;
import com.secel.devopteam.GosMiddleware.dao.AccessRightDao;
import com.secel.devopteam.GosMiddleware.dao.AccessRightEmployeeGroupDao;
import com.secel.devopteam.GosMiddleware.dao.AgentActivityDao;
import com.secel.devopteam.GosMiddleware.dao.AgentDao;
import com.secel.devopteam.GosMiddleware.dao.AgentDeviceDao;
import com.secel.devopteam.GosMiddleware.dao.AgentSectorDao;
import com.secel.devopteam.GosMiddleware.dao.AlertsDao;
import com.secel.devopteam.GosMiddleware.dao.AlertsDocumentsDao;
import com.secel.devopteam.GosMiddleware.dao.DeviceDao;
import com.secel.devopteam.GosMiddleware.dao.DocTypeDao;
import com.secel.devopteam.GosMiddleware.dao.DocumentsDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeGroupDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeGroupEmployeeDao;
import com.secel.devopteam.GosMiddleware.dao.GpsCoordinatesDao;
import com.secel.devopteam.GosMiddleware.dao.RulesDao;
import com.secel.devopteam.GosMiddleware.dao.SectorDao;
import com.secel.devopteam.GosMiddleware.dao.DistrictDao;
import com.secel.devopteam.GosMiddleware.dao.DistrictRulesDao;
import com.secel.devopteam.GosMiddleware.dao.SiteDao;
import com.secel.devopteam.GosMiddleware.model.AlertSystem;
import com.secel.devopteam.GosMiddleware.model.ViewsAlertsDocuments;
import com.secel.devopteam.GosMiddleware.util.CryptographyException;
import com.secel.devopteam.GosMiddleware.util.EmployeeAccessRights;
import com.secel.devopteam.GosMiddleware.util.Encryptor;
import com.secel.devopteam.GosMiddleware.util.ErrorObject;
import com.secel.devopteam.GosMiddleware.util.GlobalEmployee;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;
import com.secel.devopteam.GosMiddleware.util.Util;
import com.sun.mail.imap.protocol.BODY;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.sun.org.apache.xml.internal.serializer.utils.SystemIDResolver;

import antlr.Utils;
import ch.qos.logback.core.net.SyslogOutputStream;
import io.dropwizard.hibernate.UnitOfWork;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import io.swagger.annotations.Api;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

 /**
 * 
 *
 */
@Path("/maincontroller")
@Api(value = "/maincontroller")
@Produces(MediaType.APPLICATION_JSON)
public class MainController {
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	String audienceAuThentification = "pagegosAudienceAuThentificationToken";
	String subjectAuThentification = "pagegosSubjectAuThentificationToken";
	String audienceAuThorization = "pagegosAudienceAuThorizationToken";
	String subjectAuThorization = "pagegosSubjectAuThorizationToken";
	String tokenKeyAuThorization;
	String tokenKeyAuThentification;
	DeviceDao devicedao;
	AgentDao agentdao;
	DocTypeDao docTypedao;
	DocumentsDao documentsdao;
	GpsCoordinatesDao gpscoordinatesdao;
	DistrictDao districtdao;
	SiteDao sitedao;
	AgentSectorDao  agentsectordao;
	AgentDeviceDao agentdevicedao;
	EmployeeDao employeedao;
	EmployeeGroupDao employeeGroupDao;
	AccessRightDao accessrightdao;
	EmployeeGroupEmployeeDao employeeGroupEmployeeDao;
	AccessRightEmployeeGroupDao accessRightEmployeeGroupDao;
	AgentActivityDao agentActivityDao;
	AlertsDao alertsDao;
	AlertsDocumentsDao alertsDocumentsDao;
	SectorDao sectordao;
	RulesDao rulesDao;	
	DistrictRulesDao districtRulesDao;
	public MainController(String tokenKye, DeviceDao devicedao, AgentDao agentdao, DocTypeDao docTypedao, DocumentsDao documentsdao
		, GpsCoordinatesDao gpscoordinatesdao, DistrictDao districtdao, SiteDao sitedao, AgentDeviceDao agentdevicedao
		,AgentSectorDao agentsectordao, EmployeeDao employeedao, EmployeeGroupDao employeeGroupDao,
		AccessRightDao accessrightdao, EmployeeGroupEmployeeDao employeeGroupEmployeeDao,
		AccessRightEmployeeGroupDao accessRightEmployeeGroupDao, AgentActivityDao agentActivityDao, AlertsDocumentsDao alertsDocumentsDao, AlertsDao alertsDao, 
		SectorDao sectordao, RulesDao rulesdao, DistrictRulesDao districtRulesDao) {
		this.tokenKeyAuThentification = "TcWlVp4QhnscuHn182TFXHeX7ph8fqwm8IHNOxyEQtzTRs4udppKpGC7HOXljVek";
		this.tokenKeyAuThorization = "TcWlVp4QhnscuHn182TFXHeX7ph8fqwQAZsedddNOxyEQtzTRs4udppKpGC7HOXljVek";
		this.devicedao = devicedao;
		this.agentdao = agentdao;
		this.docTypedao=docTypedao;
		this.documentsdao=documentsdao;
		this.gpscoordinatesdao=gpscoordinatesdao;
		this.districtdao=districtdao;
		this.sitedao=sitedao;
		this.agentsectordao=agentsectordao;
		this.agentdevicedao=agentdevicedao;
		this.agentsectordao=agentsectordao;
		this.employeedao=employeedao;
		this.employeeGroupDao=employeeGroupDao;
		this.accessrightdao=accessrightdao;
		this.employeeGroupEmployeeDao=employeeGroupEmployeeDao;
		this.accessRightEmployeeGroupDao=accessRightEmployeeGroupDao;
		this.agentActivityDao=agentActivityDao;
		this.alertsDao=alertsDao;
		this.alertsDocumentsDao= alertsDocumentsDao;
		this.sectordao=sectordao;
		this.rulesDao = rulesdao;
		this.districtRulesDao = districtRulesDao;
	}
	
	/************** Begin Agents management ***********/
	@POST
    @Path("agents/rememberAgentPassword")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response rememberAgentPassword (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Agent agentMailed = AgentController.rememberAgentPassword(agent, agentdao);
				return Response.status(HttpStatus.OK_200).entity(agentMailed).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while sending mail or getting user by this login.");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("agents/createAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createAgent (@Context HttpHeaders httpHeaders, Agent agent) {
		System.err.println(httpHeaders.getHeaderString("Authorization"));
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					Agent agentCreated = AgentController.createAgent(agent, agentdao);
					return Response.status(HttpStatus.OK_200).entity(agentCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating an agent");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("agents/getAllAgents")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAgents (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Agent> agentList = AgentController.getAllAgents(agentdao);
				return Response.status(HttpStatus.OK_200).entity(agentList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("agents/getAllAgentsSectors")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAgentsSectors (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AgentSector> agenSectortList = AgentController.getAllAgentSectors(agent, agentsectordao);
				List<Sector> SectortList = SectorController.getAllSectorByAgentId(agenSectortList, sectordao);
				return Response.status(HttpStatus.OK_200).entity(SectortList).build(); 
			}  
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agents sectors");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build(); 
		}
	}
	
	@POST
    @Path("agents/getAllAgentsSectorsForMobile")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAgentsSectorsForMobile (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Sector sector = AgentController.getAgentsSectorsByAgentForMobile(agent, agentsectordao);
				if(sector==null) {
					return Response.status(HttpStatus.FORBIDDEN_403).entity(sector).build();
				}else {			
					return Response.status(HttpStatus.OK_200).entity(sector).build(); 		
				}				
			}  
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agents sectors");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build(); 
		}
	}
/*
	@POST
    @Path("agentsSectors/getNonAffectedSectorsToAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getNonAffectedSectorsToAgent (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Sector> allSectors = SectorController.getAllFreeSectors(sectordao);
				
				AgentSector currentAgentSectorOfAgent = AgentSectorController.getCurrentAgentSector(agent, agentsectordao);
				if( currentAgentSectorOfAgent != null ) {
					Sector currentSectorOfAgent = currentAgentSectorOfAgent.getSector();
					boolean removed = allSectors.remove(currentSectorOfAgent);
				}
				
				return Response.status(HttpStatus.OK_200).entity(allSectors).build(); 
			}
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agents sectors");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build(); 
		}
	}*/
	
	@POST
    @Path("agentsSectors/getNonAffectedSectorsToAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getNonAffectedSectorsToAgent (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Sector> allSectors = SectorController.getAllFreeSectors(sectordao);
				
				AgentSector currentAgentSectorOfAgent = AgentSectorController.getCurrentAgentSector(agent, agentsectordao);
				if( currentAgentSectorOfAgent != null ) {
					Sector currentSectorOfAgent = currentAgentSectorOfAgent.getSector();
					boolean removed = allSectors.remove(currentSectorOfAgent);
				}
				
				return Response.status(HttpStatus.OK_200).entity(allSectors).build(); 
			}
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agents sectors");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build(); 
		}
	}
	
	@GET
    @Path("districtSectors/getNonAffectedDistrictToSector")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getNonAffectedDistrictToSector (@Context HttpHeaders httpHeaders, Sector sector) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<District> allDistrictss = DistrictController.getAllFreeDistricts(districtdao);
				return Response.status(HttpStatus.OK_200).entity(allDistrictss).build(); 
			}
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agents sectors");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build(); 
		}
	}
	
	@GET
    @Path("agentsSectors/getAllAgentsSectors")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAgentsSectors (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AgentSector> agentsSectorstList = AgentSectorController.getAllAgentsSectors(agentsectordao);
				return Response.status(HttpStatus.OK_200).entity(agentsSectorstList).build(); 
			}  
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agents sectors");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build(); 
		}
	}

	@POST
    @Path("agentsSectors/getAgentsSectorsByAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAgentsSectorsByAgent (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AgentSector> agentsSectorsListByAgent = AgentSectorController.getAgentsSectorsByAgent(agent, agentsectordao);
				System.err.println(agentsSectorsListByAgent);
				return Response.status(HttpStatus.OK_200).entity( agentsSectorsListByAgent ).build(); 
			}  
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agents sectors by agent");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build(); 
		}
	}
	
	@POST
    @Path("agentsSectors/getAgentsSectorsBySector")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAgentsSectorsBySector (@Context HttpHeaders httpHeaders, Sector sector) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AgentSector> agentsSectorsListBySector = AgentSectorController.getAgentsSectorsBySector(sector, agentsectordao);
				return Response.status(HttpStatus.OK_200).entity( agentsSectorsListBySector ).build(); 
			}  
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agents sectors by sector");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build(); 
		}
	}
	
	@POST
    @Path("agents/connectAgentWithLoginAndPass")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {@ApiResponse(code = 400, message = "Proprieté invalide")})
	@UnitOfWork
	public Response connectAgentWithLoginAndPass (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Agent agentConnected = AgentController.connectAgentWithLoginAndPass(agent, agentdao);
				return Response.status(HttpStatus.OK_200).entity(agentConnected).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while connecting an agent");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("agents/affectSectorToAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response affectSectorToAgent (@Context HttpHeaders httpHeaders, AgentSector agentSector) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					AgentSector agentSectorCreated = AgentSectorController.createAgentSector(agentSector, agentsectordao, sectordao);
					return Response.status(HttpStatus.OK_200).entity(agentSectorCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while assign the sector to the agent");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("agents/getAgentById")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAgentById (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Agent agentConnected = AgentController.getAgentById(agent, agentdao);
				return Response.status(HttpStatus.OK_200).entity(agentConnected).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an agent");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("agents/updateAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateAgent (@Context HttpHeaders httpHeaders, Agent agent) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					Agent agentUpdated =AgentController.updateAgent(agent, agentdao);
					return Response.status(HttpStatus.OK_200).entity(agentUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating agent");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("agents/updateAgentMobileInfos")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateAgentMobileInfos (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Agent agentUpdated =AgentController.updateAgent(agent, agentdao);
				return Response.status(HttpStatus.OK_200).entity(agentUpdated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating agent");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("agents/affectDeviceToAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response affectDeviceToAgent (@Context HttpHeaders httpHeaders, AgentDevice agentDevice) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					AgentDevice agentDEviceCreated = AgentController.affectDeviceToAgent(agentDevice, agentdevicedao, devicedao);
					return Response.status(HttpStatus.OK_200).entity(agentDEviceCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while assign Device to an agent");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("agents/defineNewIdentifiers")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response defineNewAgentIdentifiers (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Agent agentUpdated = AgentController.defineNewIdentifiers(agent, agentdao);
				return Response.status(HttpStatus.OK_200).entity(agentUpdated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating employee");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("agents/verifyLogin")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response verifyAgentLogin (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Agent> agentList = AgentController.getAgentsByLogin(agent, agentdao);
				return Response.status(HttpStatus.OK_200).entity(agentList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an employee");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	/************** End Agents management ***********/
	
	/************** Begin Site management ***********/
	@POST
    @Path("site/updateSite")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateSite (@Context HttpHeaders httpHeaders, Site site, @QueryParam("incommingAgent") String incommingAgent, @QueryParam("incommingAgentActivity") String incommingAgentActivity) {
		Agent connectedAgent = (Agent)Util.getObjectFromJson(incommingAgent, Agent.class);
		AgentActivity agentActivity = (AgentActivity)Util.getObjectFromJson(incommingAgentActivity, AgentActivity.class);
		if(Util.isRequestValid(httpHeaders)){
			try {
				Site siteUpdated = SiteController.updateSite(site, connectedAgent, agentActivity, sitedao, gpscoordinatesdao, documentsdao, agentActivityDao);
				return Response.status(HttpStatus.OK_200).entity(siteUpdated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating site");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("site/createSiteFlutter")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createSiteFlutter (@Context HttpHeaders httpHeaders, Site site, @QueryParam("agentId") String agentId) {
		Agent agent = new Agent();
		agent.setAgentId(Integer.parseInt(agentId));
		if(Util.isRequestValid(httpHeaders)){
			try {
				Site siteCreated = SiteController.createSiteFlutter(site, agent, sitedao, gpscoordinatesdao, documentsdao, agentActivityDao);
				return Response.status(HttpStatus.OK_200).entity(siteCreated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating site");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	@GET
    @Path("site/getAllSites")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllSites (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Site> siteList = SiteController.getAllSites(sitedao);
				return Response.status(HttpStatus.OK_200).entity(siteList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of sites");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("site/getAllSitesWithoutConditions")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllSitesWithoutConditions (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Site> siteList = SiteController.getAllSitesWithoutConditions(sitedao);
				return Response.status(HttpStatus.OK_200).entity(siteList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of sites");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("site/getSiteById")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getSiteById (@Context HttpHeaders httpHeaders, Site site) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Site siteReturn = SiteController.getSiteById(site, sitedao);
				return Response.status(HttpStatus.OK_200).entity(siteReturn).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an site");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	/************** End Site management ***********/
	
/************** begin sector management ***********/
	@POST
    @Path("sector/createSector")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createSector (@Context HttpHeaders httpHeaders, Sector sector) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					Sector sectorCreated = SectorController.createSector(sector, sectordao);
					return Response.status(HttpStatus.OK_200).entity(sectorCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating sector");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("sector/updateSector")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateSector (@Context HttpHeaders httpHeaders, Sector sector) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					Sector sectorUpdated = SectorController.updateSector(sector, sectordao);
					return Response.status(HttpStatus.OK_200).entity(true).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating sector");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("sector/deleteSector")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response deleteSector (@Context HttpHeaders httpHeaders, Sector sector) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					List<District> districtsList = DistrictController.getAllDistrictOfSector(sector, sitedao, districtdao);
					for(District district : districtsList) {
						district.setStatus(GosMiddlewareConstants.STATE_ACTIVATED);
						district.setSectorId(null);
						DistrictController.updateDistrict(district, districtdao);					
					}
					districtsList = DistrictController.getAllDistrictOfSector(sector, sitedao, districtdao);
					boolean i = SectorController.deleteSector(sector, sectordao);
					if(i) {
						return Response.status(HttpStatus.OK_200).entity(i).build();
					}else {
						return Response.status(HttpStatus.FORBIDDEN_403).entity(i).build();
					}
					
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating sector");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("sector/getAllSector")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllSectors (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Sector> sectorList = SectorController.getAllSectors(sectordao);
				return Response.status(HttpStatus.OK_200).entity(sectorList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of sectors");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	/************** end Sector management ***********/
	
	/************** Begin district management ***********/
	@POST
    @Path("district/createDistrict")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createDistrict (@Context HttpHeaders httpHeaders, District district) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					District districtCreated = DistrictController.createDistrict(district, districtdao);
					return Response.status(HttpStatus.OK_200).entity(districtCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating sector");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("district/updateDistrict")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateDistrict (@Context HttpHeaders httpHeaders, List<District> districts) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					for(District district : districts) {
						DistrictController.updateDistrict(district, districtdao);
					}
					boolean verif = true;
					return Response.status(HttpStatus.OK_200).entity(verif).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating sector");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("sector/getAllDistricts")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllDistricts (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<District> districtList = DistrictController.getAllDistricts(districtdao);
				return Response.status(HttpStatus.OK_200).entity(districtList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of sectors");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("sector/getSectorById")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getDistrictById (@Context HttpHeaders httpHeaders, District district) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				District districtReturn = DistrictController.getDistrictById(district, districtdao);
				return Response.status(HttpStatus.OK_200).entity(districtReturn).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an sector");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("sector/getAllDistrictOfSector")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllDistrictOfSector (@Context HttpHeaders httpHeaders, Sector sector) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<District> districtsList = DistrictController.getAllDistrictOfSector(sector, sitedao, districtdao);
				return Response.status(HttpStatus.OK_200).entity(districtsList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an sites");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	@POST
    @Path("sector/getAllSiteOfDistrict")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllSiteOfDistrict (@Context HttpHeaders httpHeaders, District district) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Site> sitesList = SiteController.getAllSiteByDistrict(district, sitedao);
				return Response.status(HttpStatus.OK_200).entity(sitesList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an sites");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	/************** End Sector management ***********/
	/************** Begin documents management ***********/
	@POST
    @Path("documents/createDocumentsFlutter")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createDocumentsFlutter (@Context HttpHeaders httpHeaders, Documents documents,@QueryParam("agentId") String agentId,@QueryParam("acte") String acte) {
		Agent agent = new Agent(Integer.parseInt(agentId));
		AgentActivity agentActivity = new AgentActivity();
		agentActivity.setAgentId(agent);
		if(Util.isRequestValid(httpHeaders)){
			try {
				Documents documentsCreated = DocumentsController.createDocumentsFlutter(documents,agentActivity, documentsdao, agentActivityDao, acte);
				return Response.status(HttpStatus.OK_200).entity(documentsCreated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("documents/createDocuments")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createDocument (@Context HttpHeaders httpHeaders, Documents documents,@QueryParam("incommingAgentActivity") String incommingAgentActivity) {
		AgentActivity agentActivity = (AgentActivity)Util.getObjectFromJson(incommingAgentActivity, AgentActivity.class);
		if(Util.isRequestValid(httpHeaders)){
			try {
				Documents documentsCreated = DocumentsController.createDocuments(documents,agentActivity, documentsdao, agentActivityDao);
				return Response.status(HttpStatus.OK_200).entity(documentsCreated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}

	@POST
    @Path("documents/createDocumentDesktop")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createDocumentDesktop (@Context HttpHeaders httpHeaders, Documents documents) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Documents documentsCreated = DocumentsController.createDocumentDesktop(documents, documentsdao);
				return Response.status(HttpStatus.OK_200).entity(documentsCreated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("documents/updateDocument")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateDocument (@Context HttpHeaders httpHeaders, Documents documents) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Documents documentsCreated = DocumentsController.updateDocument(documents, documentsdao);
				return Response.status(HttpStatus.OK_200).entity(documentsCreated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("documents/getAllDocuments")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllDocuments (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Documents> documentsList = DocumentsController.getAllDocuments(documentsdao);
				return Response.status(HttpStatus.OK_200).entity(documentsList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("documents/getAllDocTypes")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllDocTypes (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<DocType> docTypeList = DocumentsController.getAllDocTypes(docTypedao);
				return Response.status(HttpStatus.OK_200).entity(docTypeList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting thye list of documents types list");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("documents/getDocumentsById")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getDocumentsById (@Context HttpHeaders httpHeaders, Documents documents) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Documents documentsReturn = DocumentsController.getDocumentsById(documents, documentsdao);
				return Response.status(HttpStatus.OK_200).entity(documentsReturn).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("documents/getAllSiteDocuments")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllSiteDocuments (@Context HttpHeaders httpHeaders, Site site) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Documents> documentsList = DocumentsController.getAllSiteDocuments(site, documentsdao);
				return Response.status(HttpStatus.OK_200).entity(documentsList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}

	
	/************** End documents management ***********/
	
	/************** begin gpscoordinates management ***********/
	@POST
    @Path("gpsCoordinates/createGpsCoordinates")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createGpsCoordinates (@Context HttpHeaders httpHeaders, GpsCoordinates gpsCoordinates) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				GpsCoordinates gpsCoordinatesCreated = GpsCoordinatesController.createGpsCoordinates(gpsCoordinates, gpscoordinatesdao);
				return Response.status(HttpStatus.OK_200).entity(gpsCoordinatesCreated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating gpsCoordinates");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("gpsCoordinates/getAllCoordinatesSite")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllCoordinatesSite (@Context HttpHeaders httpHeaders, Site site) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<GpsCoordinates> gpsCoordinatesList = GpsCoordinatesController.getAllGpsCoordinatesForSite(site, gpscoordinatesdao);
				
				return Response.status(HttpStatus.OK_200).entity(gpsCoordinatesList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting gpsCoordinates");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("gpsCoordinates/updateGpsCoordinates")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateGpsCoordinates (@Context HttpHeaders httpHeaders, GpsCoordinates gpsCoordinates) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				GpsCoordinates gpsCoordinatesUpdated =GpsCoordinatesController.updateGpsCoordinates(gpsCoordinates, gpscoordinatesdao);
				return Response.status(HttpStatus.OK_200).entity(gpsCoordinatesUpdated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating gpsCoordinates");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	

	/************** End gpscoordinates management ***********/
	
	/************** begin device management ***********/
	@POST
    @Path("device/createdDevice")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createDevice (@Context HttpHeaders httpHeaders, Device device) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					Device devicecreated = DeviceController.createDevice(device, devicedao);
					return Response.status(HttpStatus.OK_200).entity(devicecreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating device");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("device/getAllDevices")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllDevices (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Device> deviceList = DeviceController.getAllDevices(devicedao);
				return Response.status(HttpStatus.OK_200).entity(deviceList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of Devices");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("device/getAllFreeDevices")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllFreeDevices (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Device> deviceList = DeviceController.getAllFreeDevices(devicedao);
				return Response.status(HttpStatus.OK_200).entity(deviceList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of Devices");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}	
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	
	@POST
    @Path("device/getAllDeviceForAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllDeviceForAgent (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AgentDevice> agentDeviceListforagent = DeviceController.getAllDeviceForAgent(agent, agentdevicedao);
				return Response.status(HttpStatus.OK_200).entity(agentDeviceListforagent).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agent Device");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("device/updateDevice")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateDevice (@Context HttpHeaders httpHeaders, Device device) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					Device deviceUpdated =DeviceController.updateDevice(device, devicedao);
					return Response.status(HttpStatus.OK_200).entity(deviceUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating device");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("device/getAllAgentDevice")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAgentDevice (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AgentDevice> agtDeviceList = AgentController.getAllAgentDevices(agentdevicedao);
				return Response.status(HttpStatus.OK_200).entity(agtDeviceList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting thye list of Devices");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	/************** End device management ***********/
	/************** Begin docType management ***********/
	@POST
    @Path("docType/updateDocType")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateDocType (@Context HttpHeaders httpHeaders, DocType docType) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				DocType docTypeUpdated =DocTypeController.updateDocType(docType, docTypedao);
				return Response.status(HttpStatus.OK_200).entity(docTypeUpdated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating docType");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	

/************** end docType management ***********/
/************** begin employees management ***********/
	@POST
	@Path("employees/connectEmployeeWithLoginAndPass")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {@ApiResponse(code = 400, message = "Proprieté invalide")})
	@UnitOfWork
	public Response connectEmployeeWithLoginAndPass (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Employee employee = new Employee();
				employee.setEmployeeLogin(httpHeaders.getHeaderString("login"));
				employee.setEmployeePassword(httpHeaders.getHeaderString("pwd"));
				GlobalEmployee employeeConnected = EmployeeController.connectEmployeeWithLoginAndPass(employee, employeeGroupEmployeeDao,accessRightEmployeeGroupDao, employeedao);
				String tokenAuthentificationGen = generateAuthentificationToken(employeeConnected.getEmployee());
				String tokenAuthorizationGen = generateAuthorizationToken(employeeConnected.getRightsList());
				Map<String, Object> token = new HashMap<>();
			    token.put("authentification", tokenAuthentificationGen);
			    token.put("accessToken", tokenAuthorizationGen);
				return Response.status(HttpStatus.OK_200).entity(token).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Login / password not found");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("employees/createEmployee")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createEmployee (@Context HttpHeaders httpHeaders, Employee employeeToCreate) {
			if(Util.isRequestValid(httpHeaders)){
				try {	
					try {
						if(isAdmin(httpHeaders.getHeaderString("Authorization"))) {
							if(EmployeeController.getEmployeesByLogin(employeeToCreate, employeedao).size()!=0) {
								ErrorObject errorObject = new ErrorObject(401, "Login is Already use");
								return Response.status(HttpStatus.NO_CONTENT_204).entity(errorObject).build();
							}
							if(EmployeeController.getEmployeesByMail(employeeToCreate, employeedao).size()!=0) {
								ErrorObject errorObject = new ErrorObject(402, "Email is Already use");
								return Response.status(HttpStatus.NO_CONTENT_204).entity(errorObject).build();
							}
							if(EmployeeController.getEmployeesByMatric(employeeToCreate, employeedao).size()!=0) {
								ErrorObject errorObject = new ErrorObject(403, "Matricule is Already use");
								return Response.status(HttpStatus.NO_CONTENT_204).entity(errorObject).build();
							}
							if(EmployeeController.getEmployeesByTel(employeeToCreate, employeedao).size()!=0) {
								ErrorObject errorObject = new ErrorObject(404, "Phone number is Already use");
								return Response.status(HttpStatus.NO_CONTENT_204).entity(errorObject).build();
							}					
							Employee employeeCreated = EmployeeController.createEmployee(employeeToCreate, employeedao, employeeGroupEmployeeDao, employeeGroupDao);
							return Response.status(HttpStatus.OK_200).entity(employeeCreated).build();							
						}else {

							ErrorObject errorObject = new ErrorObject(405, "You dont have this right");
							return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
						}
					} catch (Exception e) {

						ErrorObject errorObject = new ErrorObject(406, "Token error");
						return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
					}					
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(407, "Errors occured while creating an employee");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(408, "Header not Allow to connect");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
	}
	
	@GET
    @Path("employees/getAllEmployees")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllEmployees (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Employee> employeeList = EmployeeController.getAllEmployees(employeedao);
				return Response.status(HttpStatus.OK_200).entity(employeeList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of employees");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("employees/updateEmployee")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateEmployee (@Context HttpHeaders httpHeaders, Employee employee) {
		if(Util.isRequestValid(httpHeaders)){
			try {	
				try {
					if(isAdmin(httpHeaders.getHeaderString("Authorization"))) {				
						Employee employeeUpdated =EmployeeController.updateEmployee(employee, employeedao);
						return Response.status(HttpStatus.OK_200).entity(employeeUpdated).build();							
					}else if(getIdEmployee(httpHeaders.getHeaderString("Authorization"))==employee.getEmployeeId()){
						Employee employeeUpdated =EmployeeController.updateEmployee(employee, employeedao);
						return Response.status(HttpStatus.OK_200).entity(employeeUpdated).build();	
					}else {
						ErrorObject errorObject = new ErrorObject(405, "You dont have this right");
						return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
					}
				} catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(406, "Token error");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}					
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(407, "Errors occured while creating an employee");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(408, "Header not Allow to connect");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}	
	}
	
	private String makeRandom() {
		int length = 10;
        String symbol = "-&*_@=+"; 
        String cap_letter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
        String small_letter = "abcdefghijklmnopqrstuvwxyz"; 
        String numbers = "0123456789"; 


        String finalString = cap_letter + small_letter + 
                numbers + symbol; 

        Random random = new Random(); 

       // char[] password = new char[length]; 
        String password = "";

        for (int i = 0; i < length; i++) 
        { 
            password = password +
                    finalString.charAt(random.nextInt(finalString.length())); 

        } 
        return password;		
	}
		
	@POST
    @Path("employees/getEmployeeById")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getEmployeeById (@Context HttpHeaders httpHeaders, Employee employee) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Employee employeeReturn = EmployeeController.getEmployeeById(employee, employeedao);
				return Response.status(HttpStatus.OK_200).entity(employeeReturn).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an employee");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	
	@POST
    @Path("employees/verifyLogin")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response verifyEmployeeLogin (@Context HttpHeaders httpHeaders, Employee employee) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Employee> employeeList = EmployeeController.getEmployeesByLogin(employee, employeedao);
				return Response.status(HttpStatus.OK_200).entity(employeeList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Login Already use");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	@POST
    @Path("employees/verifyMail")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response verifyEmployeeMail (@Context HttpHeaders httpHeaders, Employee employee) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Employee> employeeList = EmployeeController.getEmployeesByMail(employee, employeedao);
				return Response.status(HttpStatus.OK_200).entity(employeeList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Email Already use");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("employees/verifyTel")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response verifyEmployeePhoneNumber (@Context HttpHeaders httpHeaders, Employee employee) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Employee> employeeList = EmployeeController.getEmployeesByTel(employee, employeedao);
				return Response.status(HttpStatus.OK_200).entity(employeeList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Phone number Already use");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("employees/verifyMatricule")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response verifyEmployeeMatric (@Context HttpHeaders httpHeaders, Employee employee) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Employee> employeeList = EmployeeController.getEmployeesByMatric(employee, employeedao);
				return Response.status(HttpStatus.OK_200).entity(employeeList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Matricule Already use");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	
	@POST
    @Path("site/verifySiteRef")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response verifySiteRef (@Context HttpHeaders httpHeaders, Site site) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Site> siteList = SiteController.getSiteByRef(site, sitedao);
				return Response.status(HttpStatus.OK_200).entity(siteList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Site Ref Already use");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("employees/defineNewIdentifiers")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response defineNewEmployeeIdentifiers (@Context HttpHeaders httpHeaders, Employee employee) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				GlobalEmployee employeeUpdated = EmployeeController.defineNewIdentifiers(employee, employeedao, employeeGroupEmployeeDao,accessRightEmployeeGroupDao);
				String tokenAuthentificationGen = generateAuthentificationToken(employeeUpdated.getEmployee());
				String tokenAuthorizationGen = generateAuthorizationToken(employeeUpdated.getRightsList());
				Map<String, Object> token = new HashMap<>();
			    token.put("authentification", tokenAuthentificationGen);
			    token.put("accessToken", tokenAuthorizationGen);
				return Response.status(HttpStatus.OK_200).entity(token).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating employee");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("employees/rememberEmployeePassword")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response rememberEmployeePassword (@Context HttpHeaders httpHeaders, Employee employee) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Employee employeeMailed = EmployeeController.rememberEmployeePassword(employee, employeedao);
				return Response.status(HttpStatus.OK_200).entity(employeeMailed).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while sending mail or getting user by this login.");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("employees/createAccessRight")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createAccessRight (@Context HttpHeaders httpHeaders, AccessRight accessRight) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					AccessRight accessRightCreated = EmployeeController.createAccessRight(accessRight, accessrightdao);
					return Response.status(HttpStatus.OK_200).entity(accessRightCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating an accessright");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("employees/createEmployeeGroup")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createEmployeeGroup (@Context HttpHeaders httpHeaders, EmployeeGroup employeeGroup) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					EmployeeGroup employeeGroupCreated = EmployeeController.createEmployeeGroup(employeeGroup, employeeGroupDao);
					return Response.status(HttpStatus.OK_200).entity(employeeGroupCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating an employeeGroup");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@GET
    @Path("employees/getAllEmployeeGroups")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllEmployeeGroups (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<EmployeeGroup> employeeGroupList = EmployeeController.getAllEmployeeGroups(employeeGroupDao);
				return Response.status(HttpStatus.OK_200).entity(employeeGroupList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of employee Group");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@GET
    @Path("employees/getAllAccessRights")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAccessRights (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AccessRight> AccessRightList = EmployeeController.getAllAccessRights(accessrightdao);
				return Response.status(HttpStatus.OK_200).entity(AccessRightList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of access right");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@GET
    @Path("employees/getAllEmployeeGroupEmployees")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllEmployeeGroupEmployees (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<EmployeeGroupEmployee> EmployeeGroupEmployeeList = EmployeeController.getAllEmployeeGroupEmployees(employeeGroupEmployeeDao);
				
				return Response.status(HttpStatus.OK_200).entity(EmployeeGroupEmployeeList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of Group Employee");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@GET
    @Path("accessRights/getAllAccessRightGroups")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAccessRightGroups (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AccessRightEmployeeGroup> accessRightEmployeeGroupList = AccessRightEmployeeGroupController.getAllAccessRightGroups(accessRightEmployeeGroupDao);
				return Response.status(HttpStatus.OK_200).entity(accessRightEmployeeGroupList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of AccessRight Group");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("employees/updateAccessRight")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateAccessRight (@Context HttpHeaders httpHeaders, AccessRight accessRight) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					AccessRight accessRightUpdated =EmployeeController.updateAccessRight(accessRight, accessrightdao);
					return Response.status(HttpStatus.OK_200).entity(accessRightUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating accessRight");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("employees/updateEmployeeGroup")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateEmployeeGroup (@Context HttpHeaders httpHeaders, EmployeeGroup employeeGroup) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					EmployeeGroup employeeGroupUpdated =EmployeeController.updateEmployeeGroup(employeeGroup, employeeGroupDao);
					return Response.status(HttpStatus.OK_200).entity(employeeGroupUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating employee Group");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("employees/updateEmployeeGroupEmployee")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateEmployeeGroupEmployee (@Context HttpHeaders httpHeaders, EmployeeGroupEmployee employeeGroupEmployee, @QueryParam("incommingEmployeeGroupEmp") String incommingEmployeeGroupEmp) {
		EmployeeGroupEmployee lastAffectationEmployeeGroupEmp = (EmployeeGroupEmployee)Util.getObjectFromJson(incommingEmployeeGroupEmp, EmployeeGroupEmployee.class);
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					EmployeeController.updateEmployeeGroupEmployee(lastAffectationEmployeeGroupEmp, employeeGroupEmployeeDao);
					EmployeeGroupEmployee empGroupEmpUpdated =EmployeeController.updateEmployeeGroupEmployee(employeeGroupEmployee, employeeGroupEmployeeDao);
					return Response.status(HttpStatus.OK_200).entity(empGroupEmpUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating employee affection");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("employees/updateAccessRightEmployeeGroup")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateAccessRightEmployeeGroup1 (@Context HttpHeaders httpHeaders, AccessRightEmployeeGroup accessRightEmployeeGroup, @QueryParam("incommingarempGroup") String incommingarempGroup) {
		AccessRightEmployeeGroup lastAffectationarEmployeeGroup = (AccessRightEmployeeGroup)Util.getObjectFromJson(incommingarempGroup, AccessRightEmployeeGroup.class);
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					EmployeeController.updateAccessRightEmployeeGroup(lastAffectationarEmployeeGroup, accessRightEmployeeGroupDao);
					AccessRightEmployeeGroup arempGroupUpdated =EmployeeController.updateAccessRightEmployeeGroup(accessRightEmployeeGroup, accessRightEmployeeGroupDao);
					return Response.status(HttpStatus.OK_200).entity(arempGroupUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating access right affection");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	/************** end employees management ***********/
	/************** begin access right management ***********/
	@POST
    @Path("accessRightEmployeeGroup/updateAccessRightEmployeeGroup")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateAccessRightEmployeeGroup (@Context HttpHeaders httpHeaders, AccessRightEmployeeGroup accessRightEmployeeGroup) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					AccessRightEmployeeGroup accessRightEmployeeGroupUpdated =AccessRightEmployeeGroupController.updateAccessRightEmployeeGroup(accessRightEmployeeGroup, accessRightEmployeeGroupDao);
					return Response.status(HttpStatus.OK_200).entity(accessRightEmployeeGroupUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while updating access right ");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("accessRightEmployeeGroup/getAccessRightsByGroup")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAccessRightsByGroup (@Context HttpHeaders httpHeaders, EmployeeGroup employeeGroup) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AccessRightEmployeeGroup> AccessRightListByGroup = AccessRightEmployeeGroupController.getAccessRightsByGroup(employeeGroup, accessRightEmployeeGroupDao);
				return Response.status(HttpStatus.OK_200).entity( AccessRightListByGroup ).build(); 
			}  
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of access right by group employee");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build(); 
		}
	}
	
	@POST
    @Path("accessRightEmployeeGroup/createAccessRightEmployeeGroup")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createAccessRightEmployeeGroup (@Context HttpHeaders httpHeaders, AccessRightEmployeeGroup accessRightEmployeeGroup) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					AccessRightEmployeeGroup accessRightEmployeeGroupCreated = AccessRightEmployeeGroupController.createAccessRightEmployeeGroup(accessRightEmployeeGroup, accessRightEmployeeGroupDao);
					return Response.status(HttpStatus.OK_200).entity(accessRightEmployeeGroupCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating an accessRight employee Group");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("accessRightEmployeeGroup/affectAccessRightToGroup")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response affectAccessRightToGroup (@Context HttpHeaders httpHeaders, AccessRightEmployeeGroup accessRightEmployeeGroup) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					AccessRightEmployeeGroup accessRightEmployeeGroupCreated = AccessRightEmployeeGroupController.affectAccessRightToGroup(accessRightEmployeeGroup, accessRightEmployeeGroupDao);
					return Response.status(HttpStatus.OK_200).entity(accessRightEmployeeGroupCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while assign access right  to an group");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("employee/affectEmployeeToGroup")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response affectEmployeeToGroup (@Context HttpHeaders httpHeaders, EmployeeGroupEmployee employeeGroupEmployee) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					EmployeeGroupEmployee employeeGroupEmployeeCreated = EmployeeController.affectEmployeeToGroup(employeeGroupEmployee, employeeGroupEmployeeDao);
					return Response.status(HttpStatus.OK_200).entity(employeeGroupEmployeeCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while assign employee  to an group");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	@POST
    @Path("employees/createEmployeeGroupEmployee")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createEmployeeGroupEmployee (@Context HttpHeaders httpHeaders, EmployeeGroupEmployee employeeGroupEmployee) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					EmployeeGroupEmployee employeeGroupEmployeeCreated = EmployeeController.createEmployeeGroupEmployee(employeeGroupEmployee, employeeGroupEmployeeDao);
					return Response.status(HttpStatus.OK_200).entity(employeeGroupEmployeeCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating an employee Group Employee");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
					
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	@POST
    @Path("password/changeEmployeePassword")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response changeEmployeePassword (@Context HttpHeaders httpHeaders, Employee employee, @QueryParam("newPassword") String newPassword) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Employee employeeUpdatePassword =EmployeeController.changeEmployeePassword(employee, newPassword, employeedao);
				if (employeeUpdatePassword != null) {
					return Response.status(HttpStatus.OK_200).entity(employeeUpdatePassword).build();
				}
				else {
					ErrorObject errorObject = new ErrorObject(404, "Le mot de passe saisi n'est pas correct.");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(404, "Errors occured while updated password");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(404, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("password/changeAgentPassword")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response changeAgentPassword (@Context HttpHeaders httpHeaders, Agent agent, @QueryParam("oldPassword") String oldPassword ,@QueryParam("newPassword") String newPassword) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				Agent agentUpdatePassword =AgentController.changeAgentPassword(agent, oldPassword ,newPassword, agentdao);
				if (agentUpdatePassword != null) {
					return Response.status(HttpStatus.OK_200).entity(agentUpdatePassword).build();
				}
				else {
					ErrorObject errorObject = new ErrorObject(404, "Le mot de passe saisi n'est pas correct.");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(404, "Errors occured while updated password");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(404, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	
	
	@POST
    @Path("employees/updateEmployeeGroupEmployeeWhereEndDate")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateEmployeeGroupEmployeeWhereEndDate (@Context HttpHeaders httpHeaders, EmployeeGroupEmployee empGemp) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {	
			if(Util.isRequestValid(httpHeaders)){
				try {
					EmployeeGroupEmployee empGempUpdated =EmployeeController.updateEmployeeGroupEmployeeWhereEndDate(empGemp, employeeGroupEmployeeDao);
					return Response.status(HttpStatus.OK_200).entity(empGempUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while deleted");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
						
				}
			
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	
	}
	
	@POST
    @Path("agent/updateAgentDeviceEndDate")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateAgentDeviceEndDate (@Context HttpHeaders httpHeaders, AgentDevice agDevice) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {	
			if(Util.isRequestValid(httpHeaders)){
				try {
					AgentDevice agDeviceUpdated =AgentController.updateAgentDeviceEndDate(agDevice, agentdevicedao);
					return Response.status(HttpStatus.OK_200).entity(agDeviceUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while deleted");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
						
				}
			
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	
	}
	@POST
    @Path("agent/updateAgentSectorEndDate")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateAgentSectorEndDate (@Context HttpHeaders httpHeaders, AgentSector agSector) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {	
			if(Util.isRequestValid(httpHeaders)){
				try {
					AgentSector agSectorUpdated =AgentController.updateAgentSectorEndDate(agSector,agentsectordao);
					return Response.status(HttpStatus.OK_200).entity(agSectorUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while deleted");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}						
			}			
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	
	}
	
	@POST
    @Path("employees/updateAccessRightEmployeeGroupEndDate")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateAccessRightEmployeeGroupEndDate (@Context HttpHeaders httpHeaders, AccessRightEmployeeGroup arempGroup) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {	
			if(Util.isRequestValid(httpHeaders)){
				try {
					AccessRightEmployeeGroup arempGroupUpdated =EmployeeController.updateAccessRightEmployeeGroupEndDate(arempGroup, accessRightEmployeeGroupDao);
					return Response.status(HttpStatus.OK_200).entity(arempGroupUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while deleted");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
						
				}
			
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	
	}
	
	@POST
    @Path("site/interruptWorkOfSite")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response interruptWorkOfSite (@Context HttpHeaders httpHeaders, Site site) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {	
			if(Util.isRequestValid(httpHeaders)){
				try {
					Site siteToUpdateDateToStop =SiteController.interruptWorkOfSite(site, sitedao);
					return Response.status(HttpStatus.OK_200).entity(siteToUpdateDateToStop).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while interrupt work");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
						
				}
			
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	
	}
	
	
	@POST
    @Path("site/relancementWorkOfSite")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response relancementWorkOfSite (@Context HttpHeaders httpHeaders, Site site) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {	
			if(Util.isRequestValid(httpHeaders)){
				try {
					Site siteToUpdateDateToStop =SiteController.relanceWorkOfSite(site, sitedao);
					return Response.status(HttpStatus.OK_200).entity(siteToUpdateDateToStop).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while interrupt work");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}					
			}			
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}	
	
	@POST
    @Path("site/finishWorkOfSite")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response finishWorkOfSite (@Context HttpHeaders httpHeaders, Site site) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {	
			if(Util.isRequestValid(httpHeaders)){
				try {
					Site siteToUpdateDateToFinsh =SiteController.finishWorkOfSite(site, sitedao);
					return Response.status(HttpStatus.OK_200).entity(siteToUpdateDateToFinsh).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while interrupt work");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
						
			}			
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	
	}
	
	/************** end access right management ***********/
	
	/************** begin agent activities management ***********/
	
	@GET
    @Path("agentActivities/getAllAgentActivities")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAgentActivities (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AgentActivity> agentActivitiesList = AgentActivityController.getAllAgentActivities(agentActivityDao);
				return Response.status(HttpStatus.OK_200).entity(agentActivitiesList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agent activities");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("agentActivities/getAllAlerts")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAlerts (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				//List<AlertSystem> alertSystem = AgentActivityController.getAgentActivitiesConvocation(agentActivityDao);
				List<AlertSystem> alertSystem = AgentActivityController.getAgentActivitiesConvocationKO(agentActivityDao);
				if(alertSystem.size() == 0 ) {
					return Response.status(HttpStatus.FORBIDDEN_403).entity(alertSystem).build();
				}else {
					return Response.status(HttpStatus.OK_200).entity(alertSystem).build();
				}
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agent activities");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("agentActivities/setAllAlerts")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response setAllAlerts (@Context HttpHeaders httpHeaders, AlertSystem alertSystem) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				AlertSystem alertsystem = AgentActivityController.setAgentActivitiesConvocation(alertSystem, agentActivityDao);
				return Response.status(HttpStatus.OK_200).entity(alertSystem).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agent activities");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}	
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	/************** end agent activities management ***********/
	@GET
    @Path("agents/getAllAgentDevicesByAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAgentDevicesByAgent (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AgentDevice> agentDeviceList = AgentController.getAllAgentDevicesByAgent(agentdevicedao);
				return Response.status(HttpStatus.OK_200).entity(agentDeviceList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agent device");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("site/getAllSectorSitesForAgent")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllSectorSitesForAgent (@Context HttpHeaders httpHeaders, Agent agent) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Site> SectorSitesForAgentList = SiteController.getAllSectorSitesForAgent(agent, agentsectordao, sitedao);
				return Response.status(HttpStatus.OK_200).entity(SectorSitesForAgentList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of site for agent");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("agents/getAgentsSectorsActives")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAgentsSectorsActives (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AgentSector> agentSectorList = AgentSectorController.getAgentsSectorsActives(agentsectordao);
				return Response.status(HttpStatus.OK_200).entity(agentSectorList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of agent device");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	/************** begin alerts management ***********/
	@POST
    @Path("alerts/createAlerts")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createAlerts (@Context HttpHeaders httpHeaders,  ViewsAlertsDocuments views) {
			if(Util.isRequestValid(httpHeaders)){
				try {
					AlertsDocuments alertsDoc=views.getAlertsDocuments();
					Alerts alertsCreated = AlertsController.createAlerts(views.getAlerts(), alertsDao);
					alertsDoc.setAlertsId(alertsCreated);
					AlertsDocuments alertsDocuments=AlertsDocumentsController.createAlertsDocuments(views.getAlertsDocuments(), alertsDocumentsDao);
					return Response.status(HttpStatus.OK_200).entity(alertsCreated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating alerts");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
			}
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
	}
	@GET
    @Path("alerts/getAllActivateAlerts")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllActivateAlerts (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<Alerts> alertsList = AlertsController.getAllActivateAlerts(alertsDao);
				return Response.status(HttpStatus.OK_200).entity(alertsList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of alerts");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("alerts/updateAlerts")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response updateAlerts (@Context HttpHeaders httpHeaders, Alerts alerts) {
		if (this.isAdmin(httpHeaders.getHeaderString("Authorization"))) {	
			if(Util.isRequestValid(httpHeaders)){
				try {
					Alerts alertsUpdated =AlertsController.updateAlerts(alerts,alertsDao);
					return Response.status(HttpStatus.OK_200).entity(alertsUpdated).build();
				} 
				catch (Exception e) {
					ErrorObject errorObject = new ErrorObject(408, "Errors occured while deleted");
					return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
				}
						
				}
			
			else{
				ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
		}
		else {
			ErrorObject errorObject = new ErrorObject(403, "Vous n'avez pas les droits requis pour effectuer cette action.");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	
	}
	
	@POST
    @Path("alerts/getAllDocumentsforAlerts")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllDocumentsforAlerts (@Context HttpHeaders httpHeaders, Alerts alerts) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AlertsDocuments> docForAlertsList =AlertsDocumentsController.getAllDocumentsOfAlerts(alerts,alertsDocumentsDao);
				return Response.status(HttpStatus.OK_200).entity(docForAlertsList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of document for alerts");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	
	/************** end alerts management ***********/
	/************** Begin documents alerts management ***********/

	@POST
    @Path("alertsDocuments/createAlertsDocuments")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response createAlertsDocuments (@Context HttpHeaders httpHeaders, AlertsDocuments alertsDocuments) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				AlertsDocuments alertsDocumentsCreated = AlertsDocumentsController.createAlertsDocuments(alertsDocuments, alertsDocumentsDao);
				return Response.status(HttpStatus.OK_200).entity(alertsDocumentsCreated).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while creating documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@GET
    @Path("alertsDocuments/getAllAlertsDocuments")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllAlertsDocuments (@Context HttpHeaders httpHeaders) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AlertsDocuments> alertsDocumentsList = AlertsDocumentsController.getAllAlertsDocuments(alertsDocumentsDao);
				return Response.status(HttpStatus.OK_200).entity(alertsDocumentsList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting the list of documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("alertsDocuments/getAlertsDocumentsById")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAlertsDocumentsById (@Context HttpHeaders httpHeaders, AlertsDocuments alertsDocuments) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				AlertsDocuments alertsDocumentsReturn = AlertsDocumentsController.getAlertsDocumentsById(alertsDocuments, alertsDocumentsDao) ;
				return Response.status(HttpStatus.OK_200).entity(alertsDocumentsReturn).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	@POST
    @Path("alertsDocuments/getAllDocumentsOfAlerts")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnitOfWork
	public Response getAllDocumentsOfAlerts (@Context HttpHeaders httpHeaders, Alerts alerts) {
		if(Util.isRequestValid(httpHeaders)){
			try {
				List<AlertsDocuments> alertsDocumentsList = AlertsDocumentsController.getAllDocumentsOfAlerts(alerts, alertsDocumentsDao);
				return Response.status(HttpStatus.OK_200).entity(alertsDocumentsList).build();
			} 
			catch (Exception e) {
				ErrorObject errorObject = new ErrorObject(408, "Errors occured while getting an documents");
				return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
			}
				
		}
		else{
			ErrorObject errorObject = new ErrorObject(403, "You are not allowed to access this service");
			return Response.status(HttpStatus.FORBIDDEN_403).entity(errorObject).build();
		}
	}
	
	/************** End documents alerts management ***********/

	
	private String generateAuthorizationToken(List<EmployeeAccessRights> employeeAccessRights) {		
	    Map<String, Object> claims = new HashMap<>();
	    List<Object> rightList = new ArrayList();
	    for(EmployeeAccessRights employeeAccessRight : employeeAccessRights) {
	    	Map<String, Object> employeeRight = new HashMap<>();
	    	employeeRight.clear();
	    	employeeRight.put("employeeId", employeeAccessRight.getEmployeeId());
	    	employeeRight.put("groupId", employeeAccessRight.getGroupId());
	    	employeeRight.put("accessRightId", null);
	    	employeeRight.put("accessRightName", employeeAccessRight.getAccessRightName());
	    	rightList.add(employeeRight);	    	
	    }
	    claims.put("accessRight", rightList);
		return doGenerateAuThorization(claims);
	}
	
	
	
	
	private String generateAuthentificationToken(Employee employee) {		
	    Map<String, Object> claims = new HashMap<>();
	    /*
	     * Ajout de l'user
	     * */
	    Map<String, Object> user = new HashMap<>();
	    user.put("employeeAdress", employee.getEmployeeAdress());
	    user.put("employeeBirthdate", employee.getEmployeeBirthdate());
	    user.put("employeeEmail", employee.getEmployeeEmail());
	    user.put("employeeGender", employee.getEmployeeGender());
	    user.put("employeeGroupEmployeeList", null);
	    user.put("employeeId",employee.getEmployeeId());
	    user.put("employeeLogin", employee.getEmployeeLogin());
	    user.put("employeeMatricule", employee.getEmployeeMatricule());
	    user.put("employeeName", employee.getEmployeeName());
	    user.put("employeeNationality", employee.getEmployeeNationality());
	    user.put("employeePassword", null);
	    user.put("employeePhoneNumber", employee.getEmployeePhoneNumber());
	    user.put("employeePlaceOfBirth", employee.getEmployeePlaceOfBirth());
	    user.put("employeeSurname", employee.getEmployeeSurname());
	    user.put("isFirstConnection", employee.getIsFirstConnection());
	    user.put("status", employee.getStatus());
	    claims.put("user", user);
		return doGenerateAuThentification(claims);
	}
	
	private String doGenerateAuThentification(Map<String, Object> claims) {
		Instant now = Instant.now();
	    Instant add = now.plus(30, ChronoUnit.DAYS);		
		final Date createdDate = new Date();
		String az= Jwts.builder()
				.setHeaderParam("typ", "jwt")
				.setAudience(this.audienceAuThentification)
				.claim("employee", claims.get("user"))
				.claim("rightsList", claims.get("accessRight"))
				.setSubject(this.subjectAuThentification)
				.setIssuedAt(Date.from(now))
				.setExpiration(Date.from(add))
				.signWith(Keys.hmacShaKeyFor(Base64.decode(this.tokenKeyAuThentification)))
				.compact();
		return az;		
	}
	
	private String doGenerateAuThorization(Map<String, Object> claims) {
		Instant now = Instant.now();
	    Instant add = now.plus(30, ChronoUnit.DAYS);		
		final Date createdDate = new Date();
		String az= Jwts.builder()
				.setHeaderParam("typ", "jwt")
				.setAudience(this.audienceAuThorization)
				.claim("rightsList", claims.get("accessRight"))
				.setSubject(this.subjectAuThorization)
				.setIssuedAt(Date.from(now))
				.setExpiration(Date.from(add))
				.signWith(Keys.hmacShaKeyFor(Base64.decode(this.tokenKeyAuThorization)))
				.compact();
		return az;		
	}
	
	private Claims getAllClaimsFromToken(String token, boolean tokenType){
		if(tokenType) {//AuThorization token
			byte[] secret = Base64.decode(this.tokenKeyAuThorization);
			return  Jwts.parser()
					.requireAudience(this.audienceAuThorization)
					.setAllowedClockSkewSeconds(62)
					.setSigningKey(Keys.hmacShaKeyFor(secret))
					.parseClaimsJws(token).getBody();
		}else {//AuThentification token
			byte[] secret = Base64.decode(this.tokenKeyAuThentification);
			return  Jwts.parser()
					.requireAudience(this.audienceAuThentification)
					.setAllowedClockSkewSeconds(62)
					.setSigningKey(Keys.hmacShaKeyFor(secret))
					.parseClaimsJws(token).getBody();
		}
		
	}
		
	private Boolean validateToken(String token, boolean tokenType) {
		try {
			getAllClaimsFromToken(token,tokenType);
			return !isTokenExpired(token,tokenType);
		} 
		catch (Exception e) {
			return false;
		}
	}
	
	private Date getExpirationDateFromToken(String token, boolean tokenType) {
		return getAllClaimsFromToken(token,tokenType).getExpiration();
	}
	
	private Boolean isTokenExpired(String token, boolean tokenType) {
		final Date expiration = getExpirationDateFromToken(token,tokenType);
		return expiration.before(new Date());
	}
	
	private boolean isAdmin(String token) {
	    boolean isAdmin = false;
		try {
			Claims claims = getAllClaimsFromToken(token,true);
		    List<Object> rightList = (List<Object>) claims.get("rightsList");
		    Map<String, Object> data;
			for(int i = 0; i<rightList.size(); i++) {
				data = new HashMap<>();
				data = (Map<String, Object>) rightList.get(i);
				if(data.get("accessRightName").equals(GosMiddlewareConstants.ADMIN_ACCESS_RIGHT)){
					isAdmin = true;
					break;
				}
				data.clear();
			}			
			return isAdmin;
		} catch (Exception e) {
			return isAdmin;
		}		
	}
	
	private int getIdEmployee(String token) {
		Claims claims = getAllClaimsFromToken(token,false);
		Employee employee = (Employee) claims.get("employee");
		return employee.getEmployeeId();
	}
	
	
}







