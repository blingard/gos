/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Blingard
 */
@Entity
@Table(name = "rules")
@NamedQueries({
    @NamedQuery(name = "Rules.findAll", query = "SELECT r FROM Rules r")})
public class Rules implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rules_id")
    private Integer rulesId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "rules_name")
    private String rulesName;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 255)
    @Column(name = "sous_zone")
    private String sousZone;
    @Size(max = 255)
    @Column(name = "code")
    private String code;
    @Size(max = 255)
    @Column(name = "localisation")
    private String localisation;
    @Size(max = 255)
    @Column(name = "superficie_constructible")
    private String superficieConstructible;
    @Size(max = 255)
    @Column(name = "hauteur_maximale")
    private String hauteurMaximale;
    @Size(max = 255)
    @Column(name = "coefficient_occupation_sol")
    private String coefficientOccupationSol;
    @Size(max = 255)
    @Column(name = "coefficient_emprise_sol")
    private String coefficientEmpriseSol;
    @Size(max = 255)
    @Column(name = "recul_rapport_aux_voies")
    private String reculRapportAuxVoies;
    @Size(max = 255)
    @Column(name = "implantation_limites_separatives")
    private String implantationLimitesSeparatives;
    @Size(max = 255)
    @Column(name = "distance_entre_constructions")
    private String distanceEntreConstructions;
    @Size(max = 255)
    @Column(name = "espace_libre")
    private String espaceLibre;
    @Size(max = 255)
    @Column(name = "espace_plante")
    private String espacePlante;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private short status;
  /*  @OneToMany(cascade = CascadeType.ALL, mappedBy = "rules", fetch = FetchType.LAZY)
    private List<DistrictRules> districtRulesList;
*/
    public Rules() {
    }

    public Rules(Integer rulesId) {
        this.rulesId = rulesId;
    }

    public Rules(Integer rulesId, String rulesName, short status) {
        this.rulesId = rulesId;
        this.rulesName = rulesName;
        this.status = status;
    }

    public Integer getRulesId() {
        return rulesId;
    }

    public void setRulesId(Integer rulesId) {
        this.rulesId = rulesId;
    }

    public String getRulesName() {
        return rulesName;
    }

    public void setRulesName(String rulesName) {
        this.rulesName = rulesName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSousZone() {
        return sousZone;
    }

    public void setSousZone(String sousZone) {
        this.sousZone = sousZone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLocalisation() {
        return localisation;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public String getSuperficieConstructible() {
        return superficieConstructible;
    }

    public void setSuperficieConstructible(String superficieConstructible) {
        this.superficieConstructible = superficieConstructible;
    }

    public String getHauteurMaximale() {
        return hauteurMaximale;
    }

    public void setHauteurMaximale(String hauteurMaximale) {
        this.hauteurMaximale = hauteurMaximale;
    }

    public String getCoefficientOccupationSol() {
        return coefficientOccupationSol;
    }

    public void setCoefficientOccupationSol(String coefficientOccupationSol) {
        this.coefficientOccupationSol = coefficientOccupationSol;
    }

    public String getCoefficientEmpriseSol() {
        return coefficientEmpriseSol;
    }

    public void setCoefficientEmpriseSol(String coefficientEmpriseSol) {
        this.coefficientEmpriseSol = coefficientEmpriseSol;
    }

    public String getReculRapportAuxVoies() {
        return reculRapportAuxVoies;
    }

    public void setReculRapportAuxVoies(String reculRapportAuxVoies) {
        this.reculRapportAuxVoies = reculRapportAuxVoies;
    }

    public String getImplantationLimitesSeparatives() {
        return implantationLimitesSeparatives;
    }

    public void setImplantationLimitesSeparatives(String implantationLimitesSeparatives) {
        this.implantationLimitesSeparatives = implantationLimitesSeparatives;
    }

    public String getDistanceEntreConstructions() {
        return distanceEntreConstructions;
    }

    public void setDistanceEntreConstructions(String distanceEntreConstructions) {
        this.distanceEntreConstructions = distanceEntreConstructions;
    }

    public String getEspaceLibre() {
        return espaceLibre;
    }

    public void setEspaceLibre(String espaceLibre) {
        this.espaceLibre = espaceLibre;
    }

    public String getEspacePlante() {
        return espacePlante;
    }

    public void setEspacePlante(String espacePlante) {
        this.espacePlante = espacePlante;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

   /* public List<DistrictRules> getDistrictRulesList() {
        return districtRulesList;
    }

    public void setDistrictRulesList(List<DistrictRules> districtRulesList) {
        this.districtRulesList = districtRulesList;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rulesId != null ? rulesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rules)) {
            return false;
        }
        Rules other = (Rules) object;
        if ((this.rulesId == null && other.rulesId != null) || (this.rulesId != null && !this.rulesId.equals(other.rulesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.secel.devopteam.GosMiddleware.beans.Rules[ rulesId=" + rulesId + " ]";
    }
    
}
