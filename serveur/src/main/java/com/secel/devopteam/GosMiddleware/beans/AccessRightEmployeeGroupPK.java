/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ken
 */
@Embeddable
public class AccessRightEmployeeGroupPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "employee_group_id")
    private long employeeGroupId;
    @Basic(optional = false)
    @Column(name = "access_right_id")
    private long accessRightId;
    @Basic(optional = false)
    @Column(name = "from_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;

    public AccessRightEmployeeGroupPK() {
    }

    public AccessRightEmployeeGroupPK(long employeeGroupId, long accessRightId, Date fromDate) {
        this.employeeGroupId = employeeGroupId;
        this.accessRightId = accessRightId;
        this.fromDate = fromDate;
    }

    public long getEmployeeGroupId() {
        return employeeGroupId;
    }

    public void setEmployeeGroupId(long employeeGroupId) {
        this.employeeGroupId = employeeGroupId;
    }

    public long getAccessRightId() {
        return accessRightId;
    }

    public void setAccessRightId(long accessRightId) {
        this.accessRightId = accessRightId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) employeeGroupId;
        hash += (int) accessRightId;
        hash += (fromDate != null ? fromDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccessRightEmployeeGroupPK)) {
            return false;
        }
        AccessRightEmployeeGroupPK other = (AccessRightEmployeeGroupPK) object;
        if (this.employeeGroupId != other.employeeGroupId) {
            return false;
        }
        if (this.accessRightId != other.accessRightId) {
            return false;
        }
        if ((this.fromDate == null && other.fromDate != null) || (this.fromDate != null && !this.fromDate.equals(other.fromDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.AccessRightEmployeeGroupPK[ employeeGroupId=" + employeeGroupId + ", accessRightId=" + accessRightId + ", fromDate=" + fromDate + " ]";
    }
    
}
