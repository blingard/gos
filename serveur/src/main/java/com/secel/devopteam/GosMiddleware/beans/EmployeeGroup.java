/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;


import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ken
 */
@Entity
@Table(name = "employee_group")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmployeeGroup.findAll", query = "SELECT e FROM EmployeeGroup e")
    , @NamedQuery(name = "EmployeeGroup.findByEmployeeGroupId", query = "SELECT e FROM EmployeeGroup e WHERE e.employeeGroupId = :employeeGroupId")
    , @NamedQuery(name = "EmployeeGroup.findByGroupName", query = "SELECT e FROM EmployeeGroup e WHERE e.groupName = :groupName")
    , @NamedQuery(name = "EmployeeGroup.findByGroupStatus", query = "SELECT e FROM EmployeeGroup e WHERE e.groupStatus = :groupStatus")})
public class EmployeeGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "employee_group_id")
    private Integer employeeGroupId;
    @Column(name = "group_name")
    private String groupName;
    @Basic(optional = false)
    @Column(name = "group_status")
    private short groupStatus;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeGroup", fetch = FetchType.LAZY)
    private List<EmployeeGroupEmployee> employeeGroupEmployeeList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employeeGroup", fetch = FetchType.LAZY)
    private List<AccessRightEmployeeGroup> accessRightEmployeeGroupList;

    public EmployeeGroup() {
    }

    public EmployeeGroup(Integer employeeGroupId) {
        this.employeeGroupId = employeeGroupId;
    }

    public EmployeeGroup(Integer employeeGroupId, short groupStatus) {
        this.employeeGroupId = employeeGroupId;
        this.groupStatus = groupStatus;
    }

    public Integer getEmployeeGroupId() {
        return employeeGroupId;
    }

    public void setEmployeeGroupId(Integer employeeGroupId) {
        this.employeeGroupId = employeeGroupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public short getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(short groupStatus) {
        this.groupStatus = groupStatus;
    }

    @XmlTransient
    public List<EmployeeGroupEmployee> getEmployeeGroupEmployeeList() {
        return employeeGroupEmployeeList;
    }

    public void setEmployeeGroupEmployeeList(List<EmployeeGroupEmployee> employeeGroupEmployeeList) {
        this.employeeGroupEmployeeList = employeeGroupEmployeeList;
    }

    @XmlTransient
    public List<AccessRightEmployeeGroup> getAccessRightEmployeeGroupList() {
        return accessRightEmployeeGroupList;
    }

    public void setAccessRightEmployeeGroupList(List<AccessRightEmployeeGroup> accessRightEmployeeGroupList) {
        this.accessRightEmployeeGroupList = accessRightEmployeeGroupList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeeGroupId != null ? employeeGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeGroup)) {
            return false;
        }
        EmployeeGroup other = (EmployeeGroup) object;
        if ((this.employeeGroupId == null && other.employeeGroupId != null) || (this.employeeGroupId != null && !this.employeeGroupId.equals(other.employeeGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.EmployeeGroup[ employeeGroupId=" + employeeGroupId + " ]";
    }
    
}
