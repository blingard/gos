/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "documents")
@NamedQueries({
    @NamedQuery(name = "Documents.findAll", query = "SELECT d FROM Documents d"),
    @NamedQuery(name = "Documents.findByDocumentId", query = "SELECT d FROM Documents d WHERE d.documentId = :documentId"),
    @NamedQuery(name = "Documents.findByDocumentName", query = "SELECT d FROM Documents d WHERE d.documentName = :documentName"),
    @NamedQuery(name = "Documents.findByDocumentCode", query = "SELECT d FROM Documents d WHERE d.documentCode = :documentCode"),
    @NamedQuery(name = "Documents.findByDocumentNumber", query = "SELECT d FROM Documents d WHERE d.documentNumber = :documentNumber"),
    @NamedQuery(name = "Documents.findByDocumentPath", query = "SELECT d FROM Documents d WHERE d.documentPath = :documentPath"),
    @NamedQuery(name = "Documents.findByDocumentExtension", query = "SELECT d FROM Documents d WHERE d.documentExtension = :documentExtension"),
    @NamedQuery(name = "Documents.findBySecretKey", query = "SELECT d FROM Documents d WHERE d.secretKey = :secretKey"),
    @NamedQuery(name = "Documents.findByCreatedDate", query = "SELECT d FROM Documents d WHERE d.createdDate = :createdDate"),
    @NamedQuery(name = "Documents.findByStatus", query = "SELECT d FROM Documents d WHERE d.status = :status")})
public class Documents implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "document_id")
    private Integer documentId;
    @Column(name = "document_name")
    private String documentName;
    @Column(name = "document_code")
    private String documentCode;
    @Column(name = "document_number")
    private String documentNumber;
    @Column(name = "document_path")
    private String documentPath;
    @Column(name = "document_extension")
    private String documentExtension;
    @Column(name = "secret_key")
    private String secretKey;
    @Column(name = "created_date")
    private String createdDate;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "doc_type_id", referencedColumnName = "doc_type_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private DocType docTypeId;
    @JoinColumn(name = "site_id", referencedColumnName = "site_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Site siteId;
    @OneToMany(mappedBy = "documentId", fetch = FetchType.LAZY)
    private List<AgentActivity> agentActivityList;

    public Documents() {
    }

    public Documents(Integer documentId) {
        this.documentId = documentId;
    }

    public Documents(Integer documentId, short status) {
        this.documentId = documentId;
        this.status = status;
    }

    public Integer getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Integer documentId) {
        this.documentId = documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentCode() {
        return documentCode;
    }

    public void setDocumentCode(String documentCode) {
        this.documentCode = documentCode;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentExtension() {
        return documentExtension;
    }

    public void setDocumentExtension(String documentExtension) {
        this.documentExtension = documentExtension;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public DocType getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(DocType docTypeId) {
        this.docTypeId = docTypeId;
    }

    public Site getSiteId() {
        return siteId;
    }

    public void setSiteId(Site siteId) {
        this.siteId = siteId;
    }

    public List<AgentActivity> getAgentActivityList() {
        return agentActivityList;
    }

    public void setAgentActivityList(List<AgentActivity> agentActivityList) {
        this.agentActivityList = agentActivityList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentId != null ? documentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documents)) {
            return false;
        }
        Documents other = (Documents) object;
        if ((this.documentId == null && other.documentId != null) || (this.documentId != null && !this.documentId.equals(other.documentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.secel.devopteam.GosMiddleware.beans.Documents[ documentId=" + documentId + " ]";
    }
    
}
