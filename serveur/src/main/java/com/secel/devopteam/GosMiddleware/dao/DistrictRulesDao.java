package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.District;
import com.secel.devopteam.GosMiddleware.beans.DistrictRules;
import com.secel.devopteam.GosMiddleware.beans.Rules;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class DistrictRulesDao extends AbstractDAO<DistrictRules>{

	public DistrictRulesDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	
	public DistrictRules saveUpdateDistrictRules(DistrictRules districtRule) {
		return persist(districtRule);
	}
	
	public List<DistrictRules> getAllRulesForDistrict(District district){
		return this.list(this.query("SELECT r FROM DistrictRules r WHERE r.district.districtId = :districtId AND r.status= :status").setParameter("districtId", district.getDistrictId()).setParameter("status", GosMiddlewareConstants.STATE_ACTIVATED));		
	}
	
	public List<DistrictRules> getAllDistrictForRules(Rules rule){
		return this.list(this.query("SELECT r FROM DistrictRules r WHERE r.rules.rulesId = :rulesId AND r.status= :status").setParameter("rulesId", rule.getRulesId()).setParameter("status", GosMiddlewareConstants.STATE_ACTIVATED));
	}

}
