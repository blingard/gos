package com.secel.devopteam.GosMiddleware.util;

import java.io.Serializable;
import java.util.List;

import com.secel.devopteam.GosMiddleware.beans.Employee;

public class GlobalEmployee implements Serializable{
	private static final long serialVersionUID = 1L;
	private Employee employee;
	private List<EmployeeAccessRights> rightsList;
	
	public GlobalEmployee () {
		
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<EmployeeAccessRights> getRightsList() {
		return rightsList;
	}

	public void setRightsList(List<EmployeeAccessRights> rightsList) {
		this.rightsList = rightsList;
	}
	
	
}
