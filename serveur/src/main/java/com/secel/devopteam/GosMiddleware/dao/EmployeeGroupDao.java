package com.secel.devopteam.GosMiddleware.dao;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.SessionFactory;

import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroup;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

import io.dropwizard.hibernate.AbstractDAO;

public class EmployeeGroupDao extends AbstractDAO<EmployeeGroup>{

	public EmployeeGroupDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		// TODO Auto-generated constructor stub
	}
	public EmployeeGroup saveOrUpdateEmployeeGroup(@Valid EmployeeGroup employeeGroup) {
		return persist(employeeGroup);
	}
	
	public EmployeeGroup findById(int id) {
        return get(id);
    }
	
	public List<EmployeeGroup> getAllGroups() {
		return this.list(this.query("SELECT a FROM EmployeeGroup a WHERE a.groupStatus = :groupStatus").setShort("groupStatus", GosMiddlewareConstants.STATE_ACTIVATED));
	}
	
	public EmployeeGroup getEmployeeGroupByName(String groupName) {
		List<EmployeeGroup> l1 = this.list(this.query("SELECT g FROM EmployeeGroup g WHERE g.groupName= :groupName").setString("groupName", groupName));
		if (!l1.isEmpty())
			return l1.get(0);
		return null;
    }
	

}
