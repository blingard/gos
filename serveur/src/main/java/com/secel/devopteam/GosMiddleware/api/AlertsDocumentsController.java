package com.secel.devopteam.GosMiddleware.api;


import java.util.Date;
import java.util.List;

import com.secel.devopteam.GosMiddleware.beans.Alerts;
import com.secel.devopteam.GosMiddleware.beans.AlertsDocuments;
import com.secel.devopteam.GosMiddleware.dao.AlertsDocumentsDao;
import com.secel.devopteam.GosMiddleware.util.GosMiddlewareConstants;

public class AlertsDocumentsController {
	
	public static AlertsDocuments createAlertsDocuments (AlertsDocuments documents, AlertsDocumentsDao alertsDocumentsDao) throws Exception {
		documents.setDocumentExtension(documents.getDocumentPath().substring(documents.getDocumentPath().lastIndexOf(".")+1, documents.getDocumentPath().length()));
		documents.setDocumentName(documents.getDocumentPath().substring(documents.getDocumentPath().lastIndexOf("/")+1, documents.getDocumentPath().lastIndexOf(".")));
		documents.setDocType(GosMiddlewareConstants.ACTION_DOC_TYPE);
		return alertsDocumentsDao.saveOrUpdateAlertsDocuments(documents);
	}
	
	public static List<AlertsDocuments> getAllAlertsDocuments (AlertsDocumentsDao alertsDocumentsDao) throws Exception {
		return alertsDocumentsDao.getAllAlertsDocuments();
	}
	public static AlertsDocuments getAlertsDocumentsById (AlertsDocuments alertsDocuments, AlertsDocumentsDao alertsDocumentsDao) throws Exception {
		return alertsDocumentsDao.findAlertsDocumentsById(alertsDocuments.getDocumentId());
	}
	public static List<AlertsDocuments> getAllDocumentsOfAlerts (Alerts alerts, AlertsDocumentsDao alertsDocumentsDao) throws Exception {
		return alertsDocumentsDao.getAllDocumentsForAlerts(alerts);
	}
}
