/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "employee")
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e"),
    @NamedQuery(name = "Employee.findByEmployeeId", query = "SELECT e FROM Employee e WHERE e.employeeId = :employeeId"),
    @NamedQuery(name = "Employee.findByEmployeeName", query = "SELECT e FROM Employee e WHERE e.employeeName = :employeeName"),
    @NamedQuery(name = "Employee.findByEmployeeSurname", query = "SELECT e FROM Employee e WHERE e.employeeSurname = :employeeSurname"),
    @NamedQuery(name = "Employee.findByEmployeeEmail", query = "SELECT e FROM Employee e WHERE e.employeeEmail = :employeeEmail"),
    @NamedQuery(name = "Employee.findByEmployeePassword", query = "SELECT e FROM Employee e WHERE e.employeePassword = :employeePassword"),
    @NamedQuery(name = "Employee.findByEmployeePhoneNumber", query = "SELECT e FROM Employee e WHERE e.employeePhoneNumber = :employeePhoneNumber"),
    @NamedQuery(name = "Employee.findByEmployeeMatricule", query = "SELECT e FROM Employee e WHERE e.employeeMatricule = :employeeMatricule"),
    @NamedQuery(name = "Employee.findByEmployeeLogin", query = "SELECT e FROM Employee e WHERE e.employeeLogin = :employeeLogin"),
    @NamedQuery(name = "Employee.findByEmployeeBirthdate", query = "SELECT e FROM Employee e WHERE e.employeeBirthdate = :employeeBirthdate"),
    @NamedQuery(name = "Employee.findByEmployeePlaceOfBirth", query = "SELECT e FROM Employee e WHERE e.employeePlaceOfBirth = :employeePlaceOfBirth"),
    @NamedQuery(name = "Employee.findByEmployeeNationality", query = "SELECT e FROM Employee e WHERE e.employeeNationality = :employeeNationality"),
    @NamedQuery(name = "Employee.findByEmployeeAdress", query = "SELECT e FROM Employee e WHERE e.employeeAdress = :employeeAdress"),
    @NamedQuery(name = "Employee.findByLastConnectionDate", query = "SELECT e FROM Employee e WHERE e.lastConnectionDate = :lastConnectionDate"),
    @NamedQuery(name = "Employee.findByToken", query = "SELECT e FROM Employee e WHERE e.token = :token"),
    @NamedQuery(name = "Employee.findByIsFirstConnection", query = "SELECT e FROM Employee e WHERE e.isFirstConnection = :isFirstConnection"),
    @NamedQuery(name = "Employee.findByEmployeeGender", query = "SELECT e FROM Employee e WHERE e.employeeGender = :employeeGender"),
    @NamedQuery(name = "Employee.findByStatus", query = "SELECT e FROM Employee e WHERE e.status = :status")})
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "employee_id")
    private Integer employeeId;
    @Basic(optional = false)
    @Column(name = "employee_name")
    private String employeeName;
    @Column(name = "employee_surname")
    private String employeeSurname;
    @Column(name = "employee_email")
    private String employeeEmail;
    @Column(name = "employee_password")
    private String employeePassword;
    @Column(name = "employee_phone_number")
    private String employeePhoneNumber;
    @Column(name = "employee_matricule")
    private String employeeMatricule;
    @Column(name = "employee_login")
    private String employeeLogin;
    @Column(name = "employee_birthdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date employeeBirthdate;
    @Column(name = "employee_place_of_birth")
    private String employeePlaceOfBirth;
    @Column(name = "employee_nationality")
    private String employeeNationality;
    @Column(name = "employee_adress")
    private String employeeAdress;
    @Column(name = "last_connection_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastConnectionDate;
    @Column(name = "token")
    private String token;
    @Basic(optional = false)
    @Column(name = "is_first_connection")
    private boolean isFirstConnection;
    @Basic(optional = false)
    @Column(name = "employee_gender")
    private String employeeGender;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employee", fetch = FetchType.LAZY)
    private List<EmployeeGroupEmployee> employeeGroupEmployeeList;

    public Employee() {
    }

    public Employee(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Employee(Integer employeeId, String employeeName, boolean isFirstConnection, String employeeGender, short status) {
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.isFirstConnection = isFirstConnection;
        this.employeeGender = employeeGender;
        this.status = status;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeSurname() {
        return employeeSurname;
    }

    public void setEmployeeSurname(String employeeSurname) {
        this.employeeSurname = employeeSurname;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeePassword() {
        return employeePassword;
    }

    public void setEmployeePassword(String employeePassword) {
        this.employeePassword = employeePassword;
    }

    public String getEmployeePhoneNumber() {
        return employeePhoneNumber;
    }

    public void setEmployeePhoneNumber(String employeePhoneNumber) {
        this.employeePhoneNumber = employeePhoneNumber;
    }

    public String getEmployeeMatricule() {
        return employeeMatricule;
    }

    public void setEmployeeMatricule(String employeeMatricule) {
        this.employeeMatricule = employeeMatricule;
    }

    public String getEmployeeLogin() {
        return employeeLogin;
    }

    public void setEmployeeLogin(String employeeLogin) {
        this.employeeLogin = employeeLogin;
    }

    public Date getEmployeeBirthdate() {
        return employeeBirthdate;
    }

    public void setEmployeeBirthdate(Date employeeBirthdate) {
        this.employeeBirthdate = employeeBirthdate;
    }

    public String getEmployeePlaceOfBirth() {
        return employeePlaceOfBirth;
    }

    public void setEmployeePlaceOfBirth(String employeePlaceOfBirth) {
        this.employeePlaceOfBirth = employeePlaceOfBirth;
    }

    public String getEmployeeNationality() {
        return employeeNationality;
    }

    public void setEmployeeNationality(String employeeNationality) {
        this.employeeNationality = employeeNationality;
    }

    public String getEmployeeAdress() {
        return employeeAdress;
    }

    public void setEmployeeAdress(String employeeAdress) {
        this.employeeAdress = employeeAdress;
    }

    public Date getLastConnectionDate() {
        return lastConnectionDate;
    }

    public void setLastConnectionDate(Date lastConnectionDate) {
        this.lastConnectionDate = lastConnectionDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean getIsFirstConnection() {
        return isFirstConnection;
    }

    public void setIsFirstConnection(boolean isFirstConnection) {
        this.isFirstConnection = isFirstConnection;
    }

    public String getEmployeeGender() {
        return employeeGender;
    }

    public void setEmployeeGender(String employeeGender) {
        this.employeeGender = employeeGender;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public List<EmployeeGroupEmployee> getEmployeeGroupEmployeeList() {
        return employeeGroupEmployeeList;
    }

    public void setEmployeeGroupEmployeeList(List<EmployeeGroupEmployee> employeeGroupEmployeeList) {
        this.employeeGroupEmployeeList = employeeGroupEmployeeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeeId != null ? employeeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.employeeId == null && other.employeeId != null) || (this.employeeId != null && !this.employeeId.equals(other.employeeId))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", employeeSurname="
				+ employeeSurname + ", employeeEmail=" + employeeEmail + ", employeePassword=" + employeePassword
				+ ", employeePhoneNumber=" + employeePhoneNumber + ", employeeMatricule=" + employeeMatricule
				+ ", employeeLogin=" + employeeLogin + ", employeeBirthdate=" + employeeBirthdate
				+ ", employeePlaceOfBirth=" + employeePlaceOfBirth + ", employeeNationality=" + employeeNationality
				+ ", employeeAdress=" + employeeAdress + ", lastConnectionDate=" + lastConnectionDate + ", token="
				+ token + ", isFirstConnection=" + isFirstConnection + ", employeeGender=" + employeeGender
				+ ", status=" + status + "]";
	}
    



}
