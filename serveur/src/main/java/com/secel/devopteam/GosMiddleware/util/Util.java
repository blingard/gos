package com.secel.devopteam.GosMiddleware.util;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.ws.rs.core.HttpHeaders;
import java.io.File;
import java.io.FileOutputStream;

import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.secel.devopteam.GosMiddleware.beans.AccessRight;
import com.secel.devopteam.GosMiddleware.beans.AccessRightEmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.Employee;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroup;
import com.secel.devopteam.GosMiddleware.beans.EmployeeGroupEmployee;
import com.secel.devopteam.GosMiddleware.dao.AccessRightEmployeeGroupDao;
import com.secel.devopteam.GosMiddleware.dao.EmployeeGroupEmployeeDao;


/**
 * 
 * @author JAGHO Brel
 *
 */
public class Util {
	
	public static String 
	CLIENT_ID_KEY = "TcWlVp4QhnscuHn182TFXHeX7ph8fqwm8IHNOxyEQtzTRs4udppKpGC7HOXljVek",
	CLIENT_ID = "gMnW0SpOnVgoHC4gtsElPAmEwRnVWehOLcfRU3wTiGVlpFq4hmVQPrBBh1AQrWi-)-N8UfnhnIvBgT8NpCu8SB2h7drGYwiIUY2cOz0Kwye69JGtCU2h0FfT65QgctMLR2h0TTpZtH-)-BYAM2IQHUyWHtt2yAEXkyOZln005AYjqOwb-@",
	TOKEN_KEY = "GosToken",
	LOCAL_STORAGE = "Storage",
	LOCAL_VIDEO_STORAGE = "Storage/Videos",
	LOCAL_AUDIO_STORAGE = "Storage/Audios",
	LOCAL_IMAGE_STORAGE = "Storage/Images",
	LOCAL_TEXT_STORAGE = "Storage/Textes";
	
	public static boolean isRequestValid(HttpHeaders httpHeaders){
		String appId = httpHeaders.getHeaderString(CLIENT_ID_KEY);
		System.err.println(httpHeaders);
//		return appId != null && CLIENT_ID.equals(appId);
		return true;
	}
	
	/*public static String getTenatIdInHeader(HttpHeaders httpHeaders) {
		return httpHeaders..getHeaderString("tenant_domain");
	}*/
	
	public static String generateToken () {
		Random rand = new Random();
		String token = "";
		for (int i=0; i<11; i++) {
			char c = (char)(rand.nextInt(26) + 97);
			token += c;
		}
		return token + DateTime.now().getMillisOfDay();
	}
	
	/**
	 * Cette méthode permet de créer un dossier s'il n'existe pas encore
	 */
	public static void createFolderIfNotExists(String dirName) throws SecurityException {
		File theDir = new File(dirName);
		if (!theDir.exists()) {
			theDir.mkdirs();
		}
	}
	
	/**
	 * Utility method to save InputStream data to target location/file
	 */
	public static void saveToFile(InputStream inStream, String target) throws IOException {
		OutputStream out = null;
		int read = 0;
		byte[] bytes = new byte[1024];
		out = new FileOutputStream(new File(target));
		while ((read = inStream.read(bytes)) != -1) {
			out.write(bytes, 0, read);
		}
		out.flush();
		out.close();
	}
	
	/**
	 * Cette fonction permet de retrouver la forme objet d'une chaîne Json
	 * @param jsonObject
	 * @param objectClass
	 * @return
	 */
	public static Object getObjectFromJson (String jsonObject, Class objectClass) {
		try {
			Object obj;
	    	final GsonBuilder builder = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	        final Gson gson = builder.create();
	        obj = gson.fromJson(jsonObject, objectClass);
	    	return obj;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Cette fonction permet de vérifier la validité de l'utilisateur qui envoie une requête au serveur
	 * en terme de token correct ou de timout valide
	 * @param connectedUsersList
	 * @param usertoken
	 * @return
	 */
//	public static boolean checkUser (ArrayList<User> connectedUsersList, String usertoken) {
//		boolean userFound = false;
//		for (User u:connectedUsersList) {
//			if (u.getToken().equalsIgnoreCase(usertoken)) {
//				if (u.getLastConnectionDate().plusMinutes(ActicomConstants.TIMEOUT_VALUE).getMinuteOfDay() > DateTime.now().getMinuteOfDay()) {
//					userFound = true;
//					u.setLastConnectionDate(DateTime.now());
//					break;
//				}
//				else {
//					userFound = false;
//					connectedUsersList.remove(u);
//				}
//			}
//		}
//		return userFound;
//	}
	
	/**
	 * Cette fonction permet de retrouver un utilisateur à partir de son token
	 * @param connectedUsersList
	 * @param usertoken
	 * @return
	 */
//	public static User getUserFromToken (ArrayList<User> connectedUsersList, String usertoken) {
//		User user = new User();
//		for (User u:connectedUsersList) {
//			if (u.getToken().equalsIgnoreCase(usertoken)) {
//				user.setSchemaName(u.getSchemaName());;
//				break;
//			}
//		}
//		return user;
//	}
	
	/**
	 * Fonction permettant de retrouver le format Json d'un objet
	 * @param obj
	 * @param ojectClass
	 * @return
	 */
	public static String getJsonFromObject (Object obj, Class ojectClass) {
		try {
			String json = null;
	    	final GsonBuilder builder = new GsonBuilder();
	        final Gson gson = builder.create();
	        json = gson.toJson(obj, ojectClass);
	    	return json;
		}
		catch (Exception e) {
			String error = "Errors while converting";
			String json = null;
	    	final GsonBuilder builder = new GsonBuilder();
	        final Gson gson = builder.create();
	        json = gson.toJson(error, String.class);
	    	return json;
		}
	}
	
	/**
	 * Cette fonction envoie des données simples de type String à un serveur distant
	 * @param request
	 * @return
	 */
	public static String sendSimpleDataToRemoteServer (String request) {
		try{
			URL url = new URL(request);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			DataOutputStream paramsWriter = new DataOutputStream(conn.getOutputStream());
			paramsWriter.flush();
			paramsWriter.close();
			InputStream remoteResponse = conn.getInputStream();
			OutputStream localResponder = new OutputStream() {
				private StringBuilder string = new StringBuilder();
				
				@Override
				public void write(int x) throws IOException {
					this.string.append((char) x );
				}

				public String toString(){
					return this.string.toString();
				}
			};//.getOutputStream.getOutputStream();
			int c;
			while((c = remoteResponse.read()) != -1){
				localResponder.write(c);
			}	
			//			localResponder.close();
			//			remoteResponse.close();
			conn.disconnect();
			String answerJson = localResponder.toString();
			return answerJson;
		}catch(Exception e){
			return null;
		}
	}
	
	/**
	 * Cette fonction permet d'envoyer un objet au format Json à un server distant
	 * @param request
	 * @param jsonObject
	 * @return
	 */
	public static String sendObjectToRemoteServer (String request, String jsonObject) {
		try {
			URL url = new URL(request);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jsonObject);
			out.close();

			InputStream remoteResponse = connection.getInputStream();
			OutputStream localResponder = new OutputStream() {
				private StringBuilder string = new StringBuilder();
				
				@Override
				public void write(int x) throws IOException {
					this.string.append((char) x );
				}

				public String toString(){
					return this.string.toString();
				}
			};
			int c;
			while((c = remoteResponse.read()) != -1){
				localResponder.write(c);
			}
			String answerJson = localResponder.toString();
			return answerJson;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String generatePassword () {
		Random rand = new Random();
		String generatedPass = "";
		for (int i=0; i<5; i++) {
			char c = (char)(rand.nextInt(26) + 97);
			generatedPass += c;
		}
		return generatedPass;
	}
	
	//Old version
	public static boolean isAccessGranted(String access, EmployeeGroupEmployeeDao empGroupEmpDao, AccessRightEmployeeGroupDao arEmpGroupDao, Employee emp){
		if(access.equals(GosMiddlewareConstants.EMPLOYEE_ACCESS))
			return true;
		if(access.equals(GosMiddlewareConstants.ADMIN_ACCESS))
			return haveRight(GosMiddlewareConstants.ADMIN_ACCESS_RIGHT, empGroupEmpDao, arEmpGroupDao, emp);
		return false;
	}
	
	//New version
	public static boolean isAccessGranted(String access, Employee emp){
		if(access.equals(GosMiddlewareConstants.EMPLOYEE_ACCESS_RIGHT))
			return haveRight(GosMiddlewareConstants.EMPLOYEE_ACCESS_RIGHT, emp);
		if(access.equals(GosMiddlewareConstants.ADMIN_ACCESS))
			return haveRight(GosMiddlewareConstants.ADMIN_ACCESS_RIGHT, emp);
		return false;
	}
	
	//Latest version
		public static boolean isAccessGranted(String access, GlobalEmployee emp){
			if(access.equals(GosMiddlewareConstants.EMPLOYEE_ACCESS_RIGHT))
				return haveRight(GosMiddlewareConstants.EMPLOYEE_ACCESS_RIGHT, emp);
			if(access.equals(GosMiddlewareConstants.ADMIN_ACCESS))
				return haveRight(GosMiddlewareConstants.ADMIN_ACCESS_RIGHT, emp);
			return false;
		}
	
	
	//Old version
	public static boolean haveRight(String right, EmployeeGroupEmployeeDao empGroupEmpDao, AccessRightEmployeeGroupDao arEmpGroupDao, Employee emp){
		ArrayList<EmployeeGroup> groupList = new ArrayList<EmployeeGroup>();
		try{
				List<EmployeeGroupEmployee> l = empGroupEmpDao.getAllGroupsForEmployee(emp);
				List<AccessRightEmployeeGroup> l1;
				EmployeeGroup group;
				for(EmployeeGroupEmployee empge : l){
					group = empge.getEmployeeGroup();
					l1 = arEmpGroupDao.getAllAccessRightForEmployeeGroup(group);
					group.setAccessRightEmployeeGroupList(l1);
					groupList.add(group);
				}
			}
			catch(Exception e){
				
			}
		for(EmployeeGroup g : groupList){
			for(AccessRightEmployeeGroup ar : g.getAccessRightEmployeeGroupList())
				if(ar.getAccessRight().getAccessRightName().equals(GosMiddlewareConstants.ROOT_RIGHT) || ar.getAccessRight().getAccessRightName().equals(right))
					return true;
		}
		return false;
	}
	
	//New version
	public static boolean haveRight(String right, Employee emp){
			List<EmployeeGroupEmployee> empGroupList = new ArrayList<EmployeeGroupEmployee>();
			empGroupList = emp.getEmployeeGroupEmployeeList();
			for (EmployeeGroupEmployee empGroup:empGroupList) {
				EmployeeGroup group = new EmployeeGroup();
				group = empGroup.getEmployeeGroup();
				for(AccessRightEmployeeGroup ar : group.getAccessRightEmployeeGroupList())
					if(ar.getAccessRight().getAccessRightName().equals(GosMiddlewareConstants.ROOT_RIGHT) || ar.getAccessRight().getAccessRightName().equals(right))
						return true;
			}
			return false;
	}
	
	//Latest version
	public static boolean haveRight(String right, GlobalEmployee globalEmp){
		if (globalEmp.getRightsList().isEmpty())
			return false;
		for (EmployeeAccessRights accRight:globalEmp.getRightsList()) {
			if (accRight.getAccessRightName().equals(GosMiddlewareConstants.ROOT_RIGHT) || accRight.getAccessRightName().equals(right))
				return true;
		}
		return false;
	}
}
