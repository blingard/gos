/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secel.devopteam.GosMiddleware.beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "alerts_documents")
@NamedQueries({
    @NamedQuery(name = "AlertsDocuments.findAll", query = "SELECT a FROM AlertsDocuments a"),
    @NamedQuery(name = "AlertsDocuments.findByDocumentId", query = "SELECT a FROM AlertsDocuments a WHERE a.documentId = :documentId"),
    @NamedQuery(name = "AlertsDocuments.findByDocumentName", query = "SELECT a FROM AlertsDocuments a WHERE a.documentName = :documentName"),
    @NamedQuery(name = "AlertsDocuments.findByDocumentPath", query = "SELECT a FROM AlertsDocuments a WHERE a.documentPath = :documentPath"),
    @NamedQuery(name = "AlertsDocuments.findByDocumentExtension", query = "SELECT a FROM AlertsDocuments a WHERE a.documentExtension = :documentExtension"),
    @NamedQuery(name = "AlertsDocuments.findByDocType", query = "SELECT a FROM AlertsDocuments a WHERE a.docType = :docType"),
    @NamedQuery(name = "AlertsDocuments.findBySecretKey", query = "SELECT a FROM AlertsDocuments a WHERE a.secretKey = :secretKey"),
    @NamedQuery(name = "AlertsDocuments.findByCreatedDate", query = "SELECT a FROM AlertsDocuments a WHERE a.createdDate = :createdDate"),
    @NamedQuery(name = "AlertsDocuments.findByStatus", query = "SELECT a FROM AlertsDocuments a WHERE a.status = :status")})
public class AlertsDocuments implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "document_id")
    private Integer documentId;
    @Column(name = "document_name")
    private String documentName;
    @Column(name = "document_path")
    private String documentPath;
    @Column(name = "document_extension")
    private String documentExtension;
    @Column(name = "doc_type")
    private String docType;
    @Column(name = "secret_key")
    private String secretKey;
    @Column(name = "created_date")
    private String createdDate;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "alerts_id", referencedColumnName = "alerts_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Alerts alertsId;

    public AlertsDocuments() {
    }

    public AlertsDocuments(Integer documentId) {
        this.documentId = documentId;
    }

    public AlertsDocuments(Integer documentId, short status) {
        this.documentId = documentId;
        this.status = status;
    }

    public Integer getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Integer documentId) {
        this.documentId = documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentExtension() {
        return documentExtension;
    }

    public void setDocumentExtension(String documentExtension) {
        this.documentExtension = documentExtension;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Alerts getAlertsId() {
        return alertsId;
    }

    public void setAlertsId(Alerts alertsId) {
        this.alertsId = alertsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentId != null ? documentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlertsDocuments)) {
            return false;
        }
        AlertsDocuments other = (AlertsDocuments) object;
        if ((this.documentId == null && other.documentId != null) || (this.documentId != null && !this.documentId.equals(other.documentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.apptest.AlertsDocuments[ documentId=" + documentId + " ]";
    }
    
}
