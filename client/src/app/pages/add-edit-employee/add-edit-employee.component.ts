import { MatSnackBar } from '@angular/material/';
import { Component, OnInit, ErrorHandler } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import {AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';

import {Employee, GlobalEmployee, ErrorObject} from '../../_generated/exports';

@Component({
  selector: 'app-add-edit-employee',
  templateUrl: './add-edit-employee.component.html',
  styleUrls: ['./add-edit-employee.component.css']
})
export class AddEditEmployeeComponent implements OnInit {

  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  employeeToSaveOrUpdate: Employee;
  currentEmployee: GlobalEmployee;
  isLoginUsed = false;
  isMatriculeUsed = false;
  isPhoneNumberUsed = false;
  isEMailUsed = false;
  isOk = false;
  isSubmit=false;
  currentLogin = '';
  dateToday: string;
  todayDate: Date;
  dateToday1: string;

  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService
) { }

  ngOnInit() {
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    if (this.data.add) {
      this.employeeToSaveOrUpdate = this.createNewEmployee();
    }
    else {
      this.isLoading = true;
      this.employeeToSaveOrUpdate = this.localDaoService.getEmployee('employee_to_edit');
      if (this.employeeToSaveOrUpdate == null) {
        this.alertService.error('Veuilez choisir un employer à modifier', true);
        this.router.navigate(['employees']);
      } else {
        if (this.actionOnModelsService.isEmployeeDeleted(this.employeeToSaveOrUpdate)) {
          this.router.navigate(['/error-500']);
        }
        this.currentLogin = this.employeeToSaveOrUpdate.employeeLogin;
      }
    }
    
  }

  createNewEmployee(): Employee {
    return {
      employeeId: null,
      employeeName: null,
      employeeSurname: null,
      employeeEmail: null,
      employeePassword: null,
      status: null,
      employeePhoneNumber: null,
      employeeMatricule: null,
      employeeLogin: null,
      employeeBirthdate: null,
      employeePlaceOfBirth: null,
      employeeNationality: null,
      isFirstConnection:null,
      employeeAdress: null,
      employeeGender: null,
     
    };
  }

  onSubmit(){
    if (this.data.add) {
      this.createEmployee();
    } else {
      this.editEmployee();
    }
    return false;
  }

  createEmployee() {
    this.isSubmit=true;
    this.todayDate = new Date();
    this.dateToday1 = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    this.dateToday = (this.employeeToSaveOrUpdate.employeeBirthdate+' 00:30:20.871');
    this.employeeToSaveOrUpdate.employeeBirthdate=this.dateToday;
    this.apiClientService.createEmployee(this.employeeToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          const emp: Employee = resp.body;
          if (emp) {
            this.alertService.success('employé créé avec succès!', true);
			      this.router.navigate(['employees']);
          } else {
              this.alertService.error('Une erreur interne est survenue.');
          }
        },
        error => {
          const errorObj: any = error;
          this.isSubmit=false;
          this.alertService.error(errorObj.error.errorText);
        }
      );
    return false;
  }

  editEmployee() {
    this.isSubmit=true;
    this.apiClientService.updateEmployee(this.employeeToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const emp: Employee = resp.body;
          this.alertService.success('Succès lors de la modification', true);
          this.localDaoService.save('employee_to_show', emp);
          this.router.navigate(['employees']);
        },
        error => {
          this.alertService.error('Echec lors de la mise à jour');
        }
      );
    return false;
  }

  detailsOfEmployee() {
    this.localDaoService.save('employee_to_show', this.employeeToSaveOrUpdate);
    this.router.navigate(['details-of-employee']);
  }

  deleteEmployee() {
    this.employeeToSaveOrUpdate.status = 2;
    this.apiClientService.updateEmployee(this.employeeToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const emp: Employee = resp.body;
          this.employeeToSaveOrUpdate.status = emp.status;
          this.alertService.success('employer supprimé avec succès.', true);
          this.router.navigate(['employees']);
        },
        error => {
          this.alertService.error('Echec lors de la suppression de l\'employer', true);
          this.router.navigate(['employees']);
        }
      );
  }

  cancel() {
      if (this.localDaoService.getEmployee('employee_to_edit') != null) {
        this.localDaoService.removeData('employee_to_edit');
      }
    this.router.navigate(['employees']);
  }

  verifyLogin () {
    this.apiClientService.verifyLogin(this.employeeToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const emplList: Array<Employee> = resp.body;
          if (emplList != null && !emplList.length) {
            this.isLoginUsed = false;
          } else {
            this.isLoginUsed = true;
            if(this.isMatriculeUsed && this.isPhoneNumberUsed && this.isEMailUsed){
              this.isOk=true;
            }
          }
        },
        error => {
        }
      );
    return false;
  }

  verifyMatricule () {
    this.employeeToSaveOrUpdate.employeeMatricule=this.employeeToSaveOrUpdate.employeeMatricule.toUpperCase().trim();
    this.apiClientService.verifyMatricule(this.employeeToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const emplList: Array<Employee> = resp.body;
          if (emplList != null && !emplList.length) {
            this.isMatriculeUsed = false;
          } else {
            this.isMatriculeUsed = true;
            if(this.isLoginUsed && this.isPhoneNumberUsed && this.isEMailUsed){
              this.isOk=true;
            }
          }
        },
        error => {
        }
      );
    return false;
  }

  verifyPhoneNumber () {
    this.apiClientService.verifyPhoneNumber(this.employeeToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const emplList: Array<Employee> = resp.body;
          if (emplList != null && !emplList.length) {
            this.isPhoneNumberUsed = false;
          } else {
            this.isPhoneNumberUsed = true;
            if(this.isMatriculeUsed && this.isLoginUsed && this.isEMailUsed){
              this.isOk=true;
            }
          }
        },
        error => {
        }
      );
    return false;
  }

  verifyMail () {
    this.apiClientService.verifyMail(this.employeeToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const emplList: Array<Employee> = resp.body;
          if (emplList != null && !emplList.length) {
            this.isEMailUsed = false;
          } else {
            this.isEMailUsed = true;
            if(this.isMatriculeUsed && this.isPhoneNumberUsed && this.isLoginUsed){
              this.isOk=true;
            }
          }
        },
        error => {
        }
      );
    return false;
  }
  handleError(error: any) {
    
}

}
