import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService, AlertService, ApiClientService, LocalDaoService,
  ActionOnModelsService, CheckErrorService, LoadingMarkerService } from '../../_services/exports';

import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { Sector,AgentSector, Site, District} from '../../_generated/exports';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-details-of-sector',
  templateUrl: './details-of-sector.component.html',
  styleUrls: ['./details-of-sector.component.css']
})
export class DetailsOfSectorComponent implements OnInit {




  page = 1;
  pageSize = 5;
  collectionSize = 0;  
  isEmpty = false;
  pageSites = 1;
  pageSizeSites = 5;
  collectionSizeSites = 0;  
  isEmptySites = false;
  data: any;
  params: any;
  nameDistrict = "";
  isLoading = true;
  isNotEmptyDistrictList = false;
  sector: Sector;
  sites: Array<Site>;
  sitesSort: Array<Site>;
  seeSite = false;
  siteEmpty = false;
  districtList: Array<District>;
  districtListSort: Array<District>;

  constructor( 
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private checkErrorService: CheckErrorService,
    private loadingMarkerservice: LoadingMarkerService,
  ) { }

  onViewDistrict(district: District){
    this.nameDistrict = district.districtName;
    this.apiClientService.getAllSiteOfDistrict(district).subscribe(
      (response)=>{
        
        const errorObject: any = response.body;
        this.checkErrorService.checkRemoteData(errorObject);
        this.loadingMarkerservice.hide();
        const SiteList: Array<Site> = response.body;
        if (SiteList != null && !SiteList.length) {
          this.sites = null;
          this.siteEmpty = false
          this.seeSite = false;
          this.alertService.error('Ce secteur n\'a pas encore des sites enregistrés');
        } else {
          this.alertService.success('ok');
          this.sites = SiteList;
          this.isEmptySites = false;
          this.sitesSort = this.sites;
          this.collectionSizeSites = this.sites.length;
          this.isLoading = false;
          if(this.sites.length!=0){
            this.siteEmpty = true
          }else{
            this.siteEmpty = false
          }
          this.seeSite = true;
        }

      },
      error => {
        this.isLoading = false;
        this.isEmptySites = false;
        this.loadingMarkerservice.hide();
        this.alertService.error('Impossible d\'obtenir la liste des sites pour ce secteur');
      }
    );
    
    
    
  }
  onViewDeleteDistrict(district: District){
    let districtsAffects: Array<District> = [];
    district.status=0;
    district.sectorId = null;
    districtsAffects.push(district);
    this.apiClientService.updateDistrict(districtsAffects).subscribe(
      (response)=>{
        const distList: boolean = response.body;
        if (!distList) {
          this.alertService.error('Impossible d\'obtenir la liste des quartiers');
        } else {
          this.alertService.success('updated',true);
          this.siteEmpty = false;
          this.seeSite = false;
          this.isLoading = true;
          this.sites = [];
          this.sitesSort = [];
          this.apiClientService.getAllDistrictOfSector(this.sector).subscribe(
            resp => {
              const errorObject: any = resp.body;
              this.checkErrorService.checkRemoteData(errorObject);
              this.loadingMarkerservice.hide();
              const districtList: Array<District> = resp.body;
              if (districtList != null && !districtList.length) {
                //this.alertService.error('Ce secteur n\'a pas encore des sites enregistrés');
                this.isNotEmptyDistrictList = false;
              } else {
                this.districtList = districtList;
                this.isNotEmptyDistrictList = true;
                this.districtListSort = this.districtList;
                this.collectionSize = this.districtList.length;
                this.isLoading = false;
              }
            },
            error => {
              this.isLoading = false;
              this.isNotEmptyDistrictList = false;
              this.loadingMarkerservice.hide();
              this.alertService.error('Impossible d\'obtenir la liste des sites pour ce secteur');
            }
          );
        }
      },
      (error)=>{
        const errorObj: any = error;
        this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Impossible d\'obtenir la liste des quartiers, l\'erreur est: ' + error);
      }
    );
  }
  

  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.districtList=this.districtListSort;
    this.collectionSize=this.districtList.length;
    if(values.length==0){
     this.pageSize=5;
     this.isEmpty=false;
     this.districtList=this.districtListSort;
     this.collectionSize=this.districtList.length;
    }else{
      let tab: District[]=[];
      this.pageSize=5;
      let a = this.districtList.filter(district => {
        if(district.districtName.toLowerCase().includes(values.toLowerCase()) || district.commune.toLowerCase().includes(values.toLowerCase())){
          tab.push(district);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.districtList=this.districtListSort;
       this.collectionSize=this.districtList.length;
       this.pageSize=5;
      }
      else{
       this.isEmpty=false;
       this.districtList=tab;
       this.collectionSize=this.districtList.length;
      }
      
    }     
  }
  onFilterSite(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.sites=this.sitesSort;
    this.collectionSizeSites=this.sites.length;
    if(values.length==0){
     this.pageSizeSites=5;
     this.isEmptySites=false;
     this.sites=this.sitesSort;
     this.collectionSizeSites=this.sites.length;
    }else{
      let tab: Site[]=[];
      this.pageSizeSites=5;
      let a = this.sites.filter(site => {
        if(site.siteName.toLowerCase().includes(values.toLowerCase()) || site.ownerName.toLowerCase().includes(values.toLowerCase())){
          tab.push(site);
        }
      });
      if(tab.length==0){
       this.isEmptySites=true;
       this.sites=this.sitesSort;
       this.collectionSizeSites=this.sites.length;
       this.pageSizeSites=5;
      }
      else{
       this.isEmptySites=false;
       this.sites=tab;
       this.collectionSizeSites=this.sites.length;
      }
      
    }     
  }

  ngOnInit() {
    this.siteEmpty = false;
    this.seeSite = false;
    this.isLoading = true;
    this.sites = [];
    this.sitesSort = [];
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.sector = this.localDaoService.getSector('sector_to_show');
    if (this.sector == null) {
      this.alertService.error('Veuillez choisir un secteur pour afficher ses détails');
      this.router.navigate(['sector']);
    }else{
      this.apiClientService.getAllDistrictOfSector(this.sector)
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.loadingMarkerservice.hide();
            const districtList: Array<District> = resp.body;
            if (districtList != null && !districtList.length) {
              //this.alertService.error('Ce secteur n\'a pas encore des sites enregistrés');
              this.isLoading = false;
            } else {
              this.districtList = districtList;
              this.isNotEmptyDistrictList = true;
              this.districtListSort = this.districtList;
              this.collectionSize = this.districtList.length;
              this.isLoading = false;
            }
          },
          error => {
            this.isLoading = false;
            this.isNotEmptyDistrictList = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Impossible d\'obtenir la liste des sites pour ce secteur');
          }
        );
    }

    
  }

}
