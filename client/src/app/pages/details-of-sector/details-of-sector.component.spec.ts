import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsOfSectorComponent } from './details-of-sector.component';

describe('DetailsOfSectorComponent', () => {
  let component: DetailsOfSectorComponent;
  let fixture: ComponentFixture<DetailsOfSectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsOfSectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsOfSectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
