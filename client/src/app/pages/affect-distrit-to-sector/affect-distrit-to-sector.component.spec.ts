import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectDistritToSectorComponent } from './affect-distrit-to-sector.component';

describe('AffectDistritToSectorComponent', () => {
  let component: AffectDistritToSectorComponent;
  let fixture: ComponentFixture<AffectDistritToSectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectDistritToSectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectDistritToSectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
