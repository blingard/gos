import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService, ApiClientService, LocalDaoService } from 'src/app/_services/exports';
import { Sector, District, GlobalEmployee } from '../../_generated/exports';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-affect-distrit-to-sector',
  templateUrl: './affect-distrit-to-sector.component.html',
  styleUrls: ['./affect-distrit-to-sector.component.css']
})
export class AffectDistritToSectorComponent implements OnInit {

  currentEmployee: GlobalEmployee;
  district : District;
  districts: District[];
  districtsAffects: Array<District>=[];
  districtsList: District[];
  todayDate: Date;
  dateToday: string;
  sector: Sector;
  page = 1;
  pageSize = 5;
  collectionSize = 0;  
  districtEmpty=false;
  isEmpty = false;
  isSubmit: boolean = false;

  affectForm : FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
  ) { }
  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.districts=this.districtsList;
    this.collectionSize=this.districtsList.length;
    if(values.length==0){
     this.pageSize=5;
     this.isEmpty=false;
     this.districts=this.districtsList;
     this.collectionSize=this.districtsList.length;
    }else{
      let tab: District[]=[];
      this.pageSize=5;
      let a = this.districts.filter(district => {
        if(district.districtName.toLowerCase().includes(values.toLowerCase()) || district.commune.toLowerCase().includes(values.toLowerCase())){
          tab.push(district);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.districts=this.districtsList;
       this.collectionSize=this.districtsList.length;
       this.pageSize=5;
      }
      else{
       this.isEmpty=false;
       this.districts=tab;
       this.collectionSize=this.districts.length;
      }
      
    }     
  }

  onSaveUpdate(){
    this.isSubmit = true;
    if(this.districtsAffects.length==0){
      this.alertService.error('Choisir un quartier');
    }else{
      this.apiClientService.updateDistrict(this.districtsAffects).subscribe(
        (response)=>{
          const distList: boolean = response.body;
          if (!distList) {
            this.alertService.error('Impossible d\'obtenir la liste des quartiers');
          } else {
            this.alertService.success('updated',true);
            this.districtsAffects=[];
            
    this.isSubmit = false;
            this.refresh();
          }
        },
        (error)=>{
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.alertService.error('Impossible d\'obtenir la liste des quartiers, l\'erreur est: ' + error);
          this.isSubmit = false;
        }
      );
    }
  }

  onChangeState(distric: District){
      this.districtsAffects=[]; 
      for(var item in this.districts){
        if(this.districts[item].districtId===distric.districtId){
          if(distric.status==0){
            distric.status=1;
            distric.sectorId=this.sector;
            this.districtsAffects.push(distric);
          }else{
            distric.status=0;
            distric.sectorId=null;
          }         
        }else{
          if(this.districts[item].status==1){
            this.districtsAffects.push(this.districts[item]);
          }
        }
      }
  }

  ngOnInit() {
    this.districtEmpty=false;
    if(this.localDaoService.exists("sector_to_affect")){
      this.sector = this.localDaoService.getSector("sector_to_affect");
    }else{
      this.alertService.error("Aucun agent traité");        
      this.router.navigate(['/sector']);
    }
    this.apiClientService.getNonAffectedDistrictToSector().subscribe(
      (response)=>{
        const distList: Array<District> = response.body;
        if (distList != null && !distList.length) {
          this.alertService.error('Impossible d\'obtenir la liste des quartiers');
          this.districtEmpty=false;
        } else {
          this.districts = distList;
          this.districtsList = distList;
          this.collectionSize = distList.length;
          this.districtEmpty=true;
        }
      },
      (error)=>{
        const errorObj: any = error;
        this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Impossible d\'obtenir la liste des quartiers, l\'erreur est: ' + error);
      }
    );
  }

  private refresh(){
    
    this.districtEmpty=false;
    if(this.localDaoService.exists("sector_to_affect")){
      this.sector = this.localDaoService.getSector("sector_to_affect");
    }else{
      this.alertService.error("Aucun agent traité");        
      this.router.navigate(['/sector']);
    }
    this.apiClientService.getNonAffectedDistrictToSector().subscribe(
      (response)=>{
        const distList: Array<District> = response.body;
        if (distList != null && !distList.length) {
          this.alertService.error('Impossible d\'obtenir la liste des quartiers');
          this.districtEmpty=false;
        } else {
          this.districts = distList;
          this.districtsList = distList;
          this.collectionSize = distList.length;
          this.districtEmpty=true;
        }
      },
      (error)=>{
        const errorObj: any = error;
        this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Impossible d\'obtenir la liste des quartiers, l\'erreur est: ' + error);
      }
    );
  }

}
