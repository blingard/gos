import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {District, GlobalEmployee } from '../../_generated/exports';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-list-district',
  templateUrl: './list-district.component.html',
  styleUrls: ['./list-district.component.css']
})
export class ListDistrictComponent implements OnInit {

  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty = false;
  data: any;
  params: any;
  isLoading = true;  
  updateErrors = false;
  displayedColumns: Array<String>;
  selection: SelectionModel<District>;
  districtList: Array<District>;
  districtListSort: Array<District>;
  currentEmployee: GlobalEmployee;

  constructor(
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    private modalService: NgbModal,
    private checkErrorService: CheckErrorService,
  ) {
   }

   onFilter(event: any){
     let values:string = event.target.value;     
     values=values.trim();     
     this.districtList=this.districtListSort;
     this.collectionSize=this.districtList.length;
     if(values.length==0){
      this.pageSize=10;
      this.isEmpty=false;
      this.districtList=this.districtListSort;
      this.collectionSize=this.districtList.length;
     }else{
       let tab: District[]=[];
       this.pageSize=5;
       let a = this.districtList.filter(district => {
         if(district.districtName.toLowerCase().includes(values.toLowerCase())){
           tab.push(district);
         }
       });
       if(tab.length==0){
        this.isEmpty=true;
        this.districtList=this.districtListSort;
        this.collectionSize=this.districtList.length;
        this.pageSize=10;
       }
       else{
        this.isEmpty=false;
        this.districtList=tab;
        this.collectionSize=this.districtList.length;
       }
       
     }     
   }

  ngOnInit() {
    this.isLoading = true;
    this.districtList= null;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
        this.loadingMarkerservice.show();
        /*this.apiClientService.getAllDistricts()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const disList: Array<District> = resp.body;
            if (disList != null && !disList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.districtList = disList;
              this.districtListSort=this.districtList;
            }
            this.collectionSize=this.districtList.length;
            this.selection = new SelectionModel<District>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); */
  }
  isDistrictListEmpty() {
    return this.districtList == null || this.districtList.length === 0;
  }
  isDistrictDeleted(dis: District) {
    return this.actionOnModelsService.isDistrictDeleted(dis);
  }
  canViewDetails() {
    return this.selection.selected.length !== 0;
  }
  canEditDistrict() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isDistrictDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  canDeleteDistrict() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isDistrictDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  clearSelection() {
    this.selection.clear();
  }
  toggleSelection(row: District) {
    if (this.selection.isSelected(row)) {
      this.selection.clear();
    } else {
      this.selection.clear();
      this.selection.select(row);
    }
  }
  refresh() {
    this.isLoading = true;
    this.districtList = null;
    this.loadingMarkerservice.show();
        /*this.apiClientService.getAllDistricts()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const disList: Array<District> = resp.body;
            if (disList != null && !disList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.districtList = disList;
            }
            //this.dataSource = new MatTableDataSource<District>(this.sectorList);
            this.selection = new SelectionModel<District>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); */
  }
  openSelectedDistrict() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un secteur');
    } else {
      const district: District = this.selection.selected[0];
      this.localDaoService.save('selected_disttrict', district);
      this.router.navigate(['dashboard']);
    }
  }
  editSelectedDistrict() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un quartier');
    } else {
      let district: District;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        district = this.selection.selected[i];
        if (!this.isDistrictDeleted(district)) {
          break;
        }
      }
      this.localDaoService.save('district_to_edit', district);
      this.router.navigate(['edit-sector']);
    }
  }
  deleteSelectedDistrict() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un secteur');
    } else {
      this.updateErrors = false;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isDistrictDeleted(this.selection.selected[i])) {
          if (i !== this.selection.selected.length - 1) {
            this.updateDistrictStatus(this.selection.selected[i], 2);
          } else {
            this.updateDistrictStatus(this.selection.selected[i], 2, 2);
          }
        }
      }
    }
  }
  detailsOfDistrict(district: District) {
    this.localDaoService.save('district_to_show', district);
    this.router.navigate(['details-of-sector']);
  }
  detailsOfSelectedDistrict() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un quartier');
    } else {
      const dis: District = this.selection.selected[0];
      this.localDaoService.save('sector_to_show', dis);
      this.router.navigate(['details-of-sector']);
    }
  }
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  }
  editDistrict(district: District) {
    this.localDaoService.save('district_to_edit', district);
    this.router.navigate(['edit-sector']);
  }

  deleteDistrict(district: District) {
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){
    this.updateDistrictStatus(district, 2, 1);
    } 
  }  

  addDistrict() {
    this.router.navigate(['add-sector']);
  }
  updateDistrictStatus(district: District, status: number, showMessage: number = 0) {
    this.currentEmployee = this.localDaoService.getGlobalEmployee('currentEmployee');
    const lastStatus: number = district.status;
    district.status = status;
    /*this.apiClientService.updateDistrict(district, this.currentEmployee)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const dis: District = resp.body;
          dis.status = dis.status;
          if (showMessage === 1) {
            this.alertService.success('secteur supprimer avec succès.');
          }
          if (showMessage === 2 && !this.updateErrors) {
            this.alertService.success('Les secteurs ont été supprimer avec succès.');
          }
          if (showMessage === 2) {
            this.refresh();
          }

          this.apiClientService.getAllDistricts().subscribe(
            (data)=>{
              const errorObject: any = data.body;
              this.checkErrorService.checkRemoteData(errorObject);
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              const disList: Array<District> = data.body;
              if (disList != null && !disList.length) {
                this.alertService.error('Impossible d\'obtenir la liste des quartiers');
              } else {
                this.districtList = disList;
              }
            },
            (err)=>{
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              this.alertService.error('Impossible d\'obtenir la liste des quartiers');
            }
          )
          

        },
        error => {
          this.updateErrors = true;
          district.status = lastStatus;
          this.alertService.error('Echec lors de la suppression du quartier');
        }
      );*/
  }

}
 