import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {Sector, GlobalEmployee, District, Rules } from '../../_generated/exports';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-list-sector',
  templateUrl: './list-sector.component.html',
  styleUrls: ['./list-sector.component.css']
})
export class ListSectorComponent implements OnInit {

  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty = false;
  data: any;
  params: any;
  isLoading = true;  
  updateErrors = false;
  isRoot = false;
  displayedColumns: Array<String>;
  selection: SelectionModel<Sector>;
  sectorList: Array<Sector>;
  sectorListSort: Array<Sector>;
  currentEmployee: GlobalEmployee;
  districtList: Array<District>;

  constructor(
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    private modalService: NgbModal,
    private checkErrorService: CheckErrorService,
    private translate: TranslateService,
  ) { }
  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.sectorList=this.sectorListSort;
    this.collectionSize=this.sectorList.length;
    if(values.length==0){
     this.pageSize=10;
     this.isEmpty=false;
     this.sectorList=this.sectorListSort;
     this.collectionSize=this.sectorList.length;
    }else{
      let tab: Sector[]=[];
      this.pageSize=5;
      let a = this.sectorList.filter(sector => {
        if(sector.sectorName.toLowerCase().includes(values.toLowerCase())){
          tab.push(sector);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.sectorList=this.sectorListSort;
       this.collectionSize=this.sectorList.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.sectorList=tab;
       this.collectionSize=this.sectorList.length;
      }
      
    }     
  }
  ngOnInit() {
    this.currentEmployee = this.localDaoService.getGlobalEmployee('currentEmployee');
    for(let i in this.currentEmployee.rightsList){
      if(this.currentEmployee.rightsList[i].accessRightName=="administrator_right"){
        this.isRoot = true;
      }        
    }
    this.isLoading = true;
    this.sectorList= null;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.loadingMarkerservice.show();
    this.apiClientService.getAllSector()
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          this.isLoading = false;
          this.loadingMarkerservice.hide();
          const secList: Array<Sector> = resp.body;
          if (secList != null && !secList.length) {
            //this.alertService.error('Erreur:');
          } else {
            this.sectorList = secList;
            this.sectorListSort=this.sectorList;
            this.collectionSize=this.sectorList.length;
            this.selection = new SelectionModel<Sector>(true, []);
          }          
        },
        error => {
          this.isLoading = false;
          this.loadingMarkerservice.hide();
          //this.alertService.error('Erreur:');
        }
      );
  }

  isSectorListEmpty() {
    return this.sectorList == null || this.sectorList.length === 0;
  }
  isSectorDeleted(sec: Sector) {
    return this.actionOnModelsService.isSectorDeleted(sec);
  }
  canViewDetails() {
    return this.selection.selected.length !== 0;
  }
  canEditSector() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isSectorDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  canDeleteSector() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isSectorDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  clearSelection() {
    this.selection.clear();
  }
  toggleSelection(row: Sector) {
    if (this.selection.isSelected(row)) {
      this.selection.clear();
    } else {
      this.selection.clear();
      this.selection.select(row);
    }
  }

  refresh() {
    this.isLoading = true;
    this.sectorList = null;
    this.loadingMarkerservice.show();
        this.apiClientService.getAllSectors()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const secList: Array<Sector> = resp.body;
            if (secList != null && !secList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.sectorList = secList;
            }
            //this.dataSource = new MatTableDataSource<District>(this.sectorList);
            this.selection = new SelectionModel<Sector>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
           // this.alertService.error('Erreur:');
          }
        ); 
    }


    editSelectedSector() {
      if (this.selection.selected.length === 0) {
        this.alertService.error('Veuillez sélectionner au moins un secteur');
      } else {
        let sector: Sector;
        let i = 0;
        for (i = 0; i < this.selection.selected.length; i++) {
          sector = this.selection.selected[i];
          if (!this.isSectorDeleted(sector)) {
            break;
          }
        }
        this.localDaoService.save('sector_to_edit', sector);
        this.router.navigate(['edit-sector']);
      }
    }
    detailsOfSector(sector: Sector) {
      this.localDaoService.save('sector_to_show', sector);
      this.router.navigate(['details-of-sector']);
    }
    detailsOfSelectedSector() {
      if (this.selection.selected.length === 0) {
        this.alertService.error('Veuillez sélectionner au moins un secteur');
      } else {
        const sec: Sector = this.selection.selected[0];
        this.localDaoService.save('sector_to_show', sec);
        this.router.navigate(['details-of-sector']);
      }
    }
    getTenFirstCharacter(chaine:string):string{
      return this.actionOnModelsService.getTenFirstCharacter(chaine);
    }
    editSector(sector: Sector) {
      this.localDaoService.save('sector_to_edit', sector);
      this.router.navigate(['edit-sector']);
    }
    editRule(rule: Rules) {
      this.localDaoService.save('rule_to_edit', rule);
      this.router.navigate(['urban_rules/edit-rule']);
    }
    deleteSector(sector: Sector) {
      let msg = null;
      this.translate.get("CONFIRMREMOVESECTOR").subscribe((data:String)=> {
        msg=data;
      });
      let v= confirm(msg+sector.sectorName+" ?");
      if(v==true){
      this.updateSectorStatus(sector);
      } 
    }  
  
    addSector() {
      this.router.navigate(['add-sector']);
    }
    updateSectorStatus(sector: Sector) {
      this.apiClientService.getAllDistrictOfSector(sector)
        .subscribe(
          resp => {      
            if(resp.status == 200){
              const errorObject: any = resp.body;
              this.checkErrorService.checkRemoteData(errorObject);
              this.loadingMarkerservice.hide();
              const districtList: Array<District> = resp.body;
              if (districtList != null && !districtList.length) {
                this.apiClientService.deleteSector(sector).subscribe(
                  (respUpdate)=>{
                    const verif: boolean = respUpdate.body;
                    if(respUpdate.status == 200){
                      this.alertService.success('Success');
                      this.ngOnInit();
          
                    }else{
                      this.alertService.error('Error: ');
                    }
                  }
                );
              } else {
                let msg = null;
                this.translate.get("CONFIRMREMOVESECTORDISTRICT").subscribe((data:String)=> {
                  msg=data;
                });
                let v= confirm(msg);
                if(v==true){
                  this.detailsOfSector(sector);
                }else{

                }
                              
              }
            }else{

            }

            
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Error: ');
          }
        );
    }

    affectOfSector(sector:Sector){
      this.localDaoService.save('sector_to_affect', sector);
      this.router.navigate(['affect-to-sector']);
    }

    affectOfRule(rule:Rules){
      this.localDaoService.save('rule_to_affect', rule);
      this.router.navigate(['affect-to-district']);
    }
}
