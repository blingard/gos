import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';



@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {
  nameFormGroup: FormGroup;
  rightFormGroup: FormGroup;
  isEditable = false;


  employee: FormGroup;
  agent: FormGroup;
  toppings: FormGroup;

  constructor(private _formBuilder: FormBuilder) {
    this.toppings = _formBuilder.group({
      pepperoni: false,
      extracheese: false,
      mushroom: false
    });
  }

  test(){
    console.log(this.rightFormGroup);
  }

  ngOnInit() {
    this.nameFormGroup = this._formBuilder.group({
      groupName: ['', Validators.required]
    });
    this.rightFormGroup = this._formBuilder.group({
      addEmployee: false,
      delEmployee: false,
      listEmployée: false,
      addAgent: false,
      delAgent: false,
      listAgent: false,
      listAgentLog: false,
      addDevice: false,
      delDevice: false,
      listDevice: false,
      setDevice: false,
      affectDevice:false,
      addHabinitation:false,
      delHabinitation:false,
      listHabinitation:false,
      setHabinitation:false,
      affectHabinitation:false,
      interumpSite:false,
      redemarerSite:false,
      listSite:true,
      addDocSite:false,
      addSector:false,
      listSector:true,
      delSector:false,
      detailSector:false,
    });
  }
}
