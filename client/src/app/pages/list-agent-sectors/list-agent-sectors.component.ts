import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {Agent, AgentSector, GlobalEmployee } from '../../_generated/exports';

@Component({
  selector: 'app-list-agent-sectors',
  templateUrl: './list-agent-sectors.component.html',
  styleUrls: ['./list-agent-sectors.component.css']
})
export class ListAgentSectorsComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty = false;
  currentEmployee: GlobalEmployee;
  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  displayedColumns: Array<String>;
  dataSource: MatTableDataSource<AgentSector>;
  selection: SelectionModel<AgentSector>;
  agentsSectorsList: Array<AgentSector>;
  agentsSectorsListSort: Array<AgentSector>;
  todayDate: Date;
  dateToday: string;


  constructor( private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService, 
  ) { }

  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.agentsSectorsList=this.agentsSectorsListSort;
    this.collectionSize=this.agentsSectorsList.length;
    if(values.length==0){
     this.pageSize=10;
     this.isEmpty=false;
     this.agentsSectorsList=this.agentsSectorsListSort;
     this.collectionSize=this.agentsSectorsList.length;
    }else{
      let tab: AgentSector[]=[];
      this.pageSize=5;
      let a = this.agentsSectorsList.filter(agentsector => {
        if(agentsector.agent.agentName.toLowerCase().includes(values.toLowerCase()) || agentsector.sector.sectorName.toLowerCase().includes(values.toLowerCase())){
          tab.push(agentsector);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.agentsSectorsList=this.agentsSectorsListSort;
       this.collectionSize=this.agentsSectorsList.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.agentsSectorsList=tab;
       this.collectionSize=this.agentsSectorsList.length;
      }
      
    }     
  }

  ngOnInit() {
    this.isLoading = true;
    this.agentsSectorsList= null;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
        this.loadingMarkerservice.show();
        this.apiClientService.getAgentsSectorsActives()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const agsectorList: Array<AgentSector> = resp.body;
            if (agsectorList != null && !agsectorList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.agentsSectorsList = agsectorList;
              this.agentsSectorsListSort = this.agentsSectorsList;
              this.collectionSize = this.agentsSectorsList.length;
            }
            this.dataSource = new MatTableDataSource<AgentSector>(this.agentsSectorsList);
            this.selection = new SelectionModel<AgentSector>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
    this.displayedColumns = ['select', 'agent', 'sector', 'beginDate', 'actions'];
  }

  refresh(){
    this.isLoading = true;
    this.agentsSectorsList= null;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
        this.loadingMarkerservice.show();
        this.apiClientService.getAgentsSectorsActives()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const agsectorList: Array<AgentSector> = resp.body;
            if (agsectorList != null && !agsectorList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.agentsSectorsList = agsectorList;
              this.agentsSectorsListSort = this.agentsSectorsList;
              this.collectionSize = this.agentsSectorsList.length;
            }
            this.dataSource = new MatTableDataSource<AgentSector>(this.agentsSectorsList);
            this.selection = new SelectionModel<AgentSector>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
    this.displayedColumns = ['select', 'agent', 'sector', 'beginDate', 'actions'];

  }

  ngAfterViewInit() {
    if (!this.isAgentSectorListEmpty()) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }
  
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  
  }
  getformatDater(chaine:string):string{
    return this.actionOnModelsService.formatDate(chaine);
  }  

  formatDate17Characters(chaine:string):string{
    return this.actionOnModelsService.formatDate17Characters(chaine);
  }  

  isAgentSectorListEmpty() {
    return this.agentsSectorsList == null || this.agentsSectorsList.length === 0;
  }
  isAgentSectorDeleted(agSector: AgentSector) {
    return this.actionOnModelsService.isAgentSectorDeleted(agSector);
  }
  canViewDetails() {
    return this.selection.selected.length !== 0;
  }
  canEditAgentSector() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isAgentSectorDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  canDeleteAgentSector() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isAgentSectorDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  clearSelection() {
    this.selection.clear();
  }
  toggleSelection(row: AgentSector) {
    if (this.selection.isSelected(row)) {
      this.selection.clear();
    } else {
      this.selection.clear();
      this.selection.select(row);
    }
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  deleteAgentSector(agSector: AgentSector) { 
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){
      this.updateAgentSectorEndDate(agSector);
    }
  }
  updateAgentSectorEndDate(agSector: AgentSector) { 
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    agSector.endDate=this.dateToday;
    this.apiClientService.updateAgentSectorWhereEndDate(agSector)
    .subscribe(
      resp => {
        const agSector: AgentSector = resp.body;
        agSector.endDate = agSector.endDate;
          this.alertService.success('affection supprimer avec succès.');
          this.apiClientService.getAgentsSectorsActives()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const agsectorList: Array<AgentSector> = resp.body;
            if (agsectorList != null && !agsectorList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.agentsSectorsList = agsectorList;
              this.agentsSectorsListSort = this.agentsSectorsList;
              this.collectionSize = this.agentsSectorsList.length;
            }
            this.dataSource = new MatTableDataSource<AgentSector>(this.agentsSectorsList);
            this.selection = new SelectionModel<AgentSector>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
      },
      error => {
        const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Echec lors de la suppression');
      }
    );
  }

}
