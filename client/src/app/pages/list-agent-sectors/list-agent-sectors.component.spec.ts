import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAgentSectorsComponent } from './list-agent-sectors.component';

describe('ListAgentSectorsComponent', () => {
  let component: ListAgentSectorsComponent;
  let fixture: ComponentFixture<ListAgentSectorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAgentSectorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAgentSectorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
