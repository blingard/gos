import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifiedDocumentsComponent } from './verified-documents.component';

describe('VerifiedDocumentsComponent', () => {
  let component: VerifiedDocumentsComponent;
  let fixture: ComponentFixture<VerifiedDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifiedDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifiedDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
