import { Component, OnInit } from '@angular/core';
import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
  LocalDaoService, LoadingMarkerService, CheckErrorService
 } from '../../_services/exports';
 import { SelectionModel } from '@angular/cdk/collections';
 import {DocumentsVerified, InterfaçageModelDoc, GlobalEmployee,Apis} from '../../_generated/exports';
 import { TranslateService } from '@ngx-translate/core';
 
import { ActivatedRoute, Router } from '@angular/router';
import actes_administratives from './../../../assets/_files/actes_administratives.json';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ViewDocComponent } from '../exports';


@Component({
  selector: 'app-verified-documents',
  templateUrl: './verified-documents.component.html',
  styleUrls: ['./verified-documents.component.css']
})
export class VerifiedDocumentsComponent implements OnInit {
  api:number=2;
  slideIndex = 0;
  collectionSize = 0;  
  selection: SelectionModel<DocumentsVerified>;
  isLoading= false;
  isEmpty = false;
  isEmptyList = false;
  docV:DocumentsVerified;
  docsToVerifiedsList:Array<DocumentsVerified>;
  docsList: InterfaçageModelDoc[]=[];
  docsListSort: InterfaçageModelDoc[]=[];
  docsToVerifiedsListSort: Array<DocumentsVerified>;
  apiString: string;
  updateErrors = false;
  currentEmployee: GlobalEmployee;
  modalRef: BsModalRef;
  actesAdministrativeList: {id:number, acte:string, code:string}[]=actes_administratives;

  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService,
    public translate: TranslateService,
    private apiClientSevice: ApiClientService,
    private modalService : BsModalService,
  ) { }
  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();  
    this.docsList=this.docsListSort;
    this.collectionSize=this.docsList.length;
    if(values.length==0){
     this.docsList=this.docsListSort;
     this.isEmpty=false;
     this.collectionSize=this.docsList.length;
    }else{
      let tab: InterfaçageModelDoc[]=[];
      let a = this.docsList.filter(doc => {
        if(doc.documentselect.documentNumber.toLowerCase().includes(values.toLowerCase()) || doc.acteName.toLowerCase().includes(values.toLowerCase()) || doc.documentselect.documentNumber.toLowerCase().includes(values.toLowerCase())){
          tab.push(doc);
        }
      });

      if(tab.length==0){
       this.isEmpty=true;
       this.docsList=this.docsListSort;
       this.collectionSize=this.docsList.length;
      }
      else{
       this.isEmpty=false;
       this.docsList=tab;
       this.collectionSize=this.docsList.length;
      }
      
    } 
  }
  ngOnInit() {
    this.docsListSort=[];
    this.docsList=[];
    this.apiClientSevice.getDocumentsToVerifieds(this.api)
      .subscribe(
        resp => {          
          const errorObject: any = resp.body;
          this.isLoading = false;
          const docToVList: Array<DocumentsVerified> = resp.body;
          if (docToVList != null && !docToVList.length) {
            this.isEmptyList=true;            
          } else {
            const docsList1: InterfaçageModelDoc[]=[];
            docToVList.forEach(doc => {
              let docv: InterfaçageModelDoc=this.createNewInterfaçageModelDoc();
              docv.chemin=this.apiClientSevice.fileDownload+doc.documentPath;
              docv.documentselect=doc;
              this.actesAdministrativeList.forEach(e => {
                if(e.code==doc.documentCode){
                  docv.acteName=e.acte;
                }
              });
              docsList1.push(docv);
            });
            this.docsList=docsList1;
            this.docsListSort = this.docsList;
            
            this.isEmptyList=false;
          }
        },
        error => {
          this.isLoading = false;
          this.loadingMarkerservice.hide();
          this.alertService.error('Erreur:');
        }
      ); 
      
  }
  createNewInterfaçageModelDoc(): InterfaçageModelDoc {
    return {
      chemin: null,
      acteName: null,
      acteCode:null,
      documentselect:this.createNewDocumentsVerified()
    };
  }
  createNewApis(): Apis {
    return {
      apisId: null,
      apisCode:null,
      apisName:null,
      apisDescription:null,
      createdDate: null,
      status:null,
      documentsList: null,
      documentsList1: null
    };
  }
  createNewDocumentsVerified(): DocumentsVerified {
    return {
      documentId: null,
      documentName: null,
      documentCode: null,
      documentNumber:null,
      documentPath: null,
      secretKey: null,
      createdDate: null,
      status: null,
        apisSender: {
          apisId:null,
          apisCode:1,
          apisName:null,
          apisDescription:null,
          createdDate: null,
          status:null,
          documentsList: null,
          documentsList1: null,
        },
      apisReceder: {
        apisId:null,
        apisCode:2,
        apisName:null,
        apisDescription:null,
        createdDate: null,
        status:null,
        documentsList: null,
        documentsList1: null,
      },
      
      gosDocId: null,
      lastUpdate: null
    }  
  }

  DocExist(doc: DocumentsVerified){
    let apiSender :Apis=this.createNewApis();
    apiSender.apisCode=1;
    apiSender.apisId=1;
    let apisReceder :Apis=this.createNewApis();
    apisReceder.apisCode=2;
    apisReceder.apisId=2;
    let docToV: DocumentsVerified=this. createNewDocumentsVerified();
    doc.apisSender=apiSender;
    doc.apisReceder=apisReceder;
    this.updateDocumentStatus(doc,1);
    this.refresh();
  }

  DocExistPas(doc: DocumentsVerified){
    let apiSender :Apis=this.createNewApis();
    apiSender.apisCode=1;
    apiSender.apisId=1;
    
    let apisReceder :Apis=this.createNewApis();
    apisReceder.apisCode=2;
    apisReceder.apisId=2;
    let docToV: DocumentsVerified=this. createNewDocumentsVerified();
    doc.apisSender=apiSender;
    doc.apisReceder=apisReceder;
    this.updateDocumentStatus(doc,2);
    this.refresh();
  }
  getExtension(path: string) {
    var basename = path.split(/[\\/]/).pop(),  
        pos = basename.lastIndexOf(".");       

    if (basename === "" || pos < 1)           
        return "";                             

    if(basename.slice(pos + 1)=='pdf')
      return "pdf"; 
    if(basename.slice(pos + 1)=='doc' || basename.slice(pos + 1)=='docx')
      return "word";
    if(basename.slice(pos + 1)=='png' || basename.slice(pos + 1)=='jpeg' || basename.slice(pos + 1)=='jpg')
      return "img";
}

  refresh(){
    this.docsListSort=[];
    this.docsList=[];
    this.apiClientSevice.getDocumentsToVerifieds(this.api)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.isLoading = false;
          const docToVList: Array<DocumentsVerified> = resp.body;
          if (docToVList != null && !docToVList.length) {
            this.isEmptyList=true;
          } else {
            const docsList1: InterfaçageModelDoc[]=[];
            docToVList.forEach(doc => {
              let docv: InterfaçageModelDoc=this.createNewInterfaçageModelDoc();
              docv.chemin=this.apiClientSevice.fileDownload+doc.documentPath;
              docv.documentselect=doc;
              this.actesAdministrativeList.forEach(e => {
                if(e.code==doc.documentCode){
                  docv.acteName=e.acte;
                }
              });
              docsList1.push(docv);
            });
            this.docsList=docsList1;
            this.docsListSort = this.docsList;
            this.isEmptyList=false;
          }
        },
        error => {
          this.isLoading = false;
          this.loadingMarkerservice.hide();
          this.alertService.error('Erreur:');
        }
      ); 
      
  }

  updateDocumentStatus(doc: DocumentsVerified, status: number, showMessage: number = 0) {
    const lastStatus: number = doc.status;
    doc.status = status;
    this.apiClientService.reponseVerificationDocuments(doc)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const docv: DocumentsVerified = resp.body;
          doc.status = docv.status;
          if (showMessage === 1) {
            this.alertService.success('Reponse envoyée avec succès.');
          }
          if (showMessage === 2 && !this.updateErrors) {
            this.alertService.success('Pas de reponse.');
          }
          if (showMessage === 2) {
            this.refresh();
          }
          this.refresh();
        },
        error => {
          this.updateErrors = true;
          doc.status = lastStatus;
          this.alertService.error('Echec lors de l\'envoi de cette reponse');
        }
      );
  }


   plusSlides(n: number) {
    this.showSlides(this.slideIndex += n);
   }
   currentSlide(n: number) {
    this.showSlides(this.slideIndex = n);
   }
  // showSlides(slideIndex);
   showSlides(n: number) {
    let i;
    const slides = document.getElementsByClassName("img-slides") as HTMLCollectionOf < HTMLElement > ;
    const dots = document.getElementsByClassName("images") as HTMLCollectionOf < HTMLElement > ;
    if (n > slides.length) {
     this.slideIndex = 1
    }
    if (n < 1) {
     this.slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
     slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[this.slideIndex - 1].style.display = "block";
    if (dots && dots.length > 0) {
     dots[this.slideIndex - 1].className += " active";
    }
   }

   openModal(doc: string){
     let type: string = null;
     switch(this.getExtension(doc)){
        case "img":
         type= "IMAGE";
         break;
        case "pdf":
          type= "TEXT";
          break;
        case "word":
          type= "TEXT";
          break;
        default:
     }
    this.modalRef = this.modalService.show(ViewDocComponent,{
      initialState: {
        title: type,
        data: doc,
        component: 1,
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
   }

}
