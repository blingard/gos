import { Component, OnInit } from '@angular/core';
import {AlertService, ApiClientService, ActionOnModelsService,
  LocalDaoService, LoadingMarkerService, CheckErrorService } from '../../_services/exports';
 import { Router, ActivatedRoute } from '@angular/router';
import {EmployeeGroup, GlobalEmployee} from '../../_generated/exports';

@Component({
  selector: 'app-edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.css']
})
export class EditGroupComponent implements OnInit {

  currentEmployee: GlobalEmployee;
  employeeGroup: EmployeeGroup;
  isSubmit: boolean = false;

  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService
  ) { }

  ngOnInit() {
    this.employeeGroup = this.localDaoService.getEmployeeGroup('employeeGroup_to_edit');
  }
  onSubmitGroup(){
    this.editAccessRight();
  }
editAccessRight() {
  this.isSubmit = true;
  this.apiClientService.updateEmployeeGroup(this.employeeGroup)
    .subscribe(
      resp => {
        const errorObject: any = resp.body;
        this.checkErrorService.checkRemoteData(errorObject);
        const empGroup: EmployeeGroup = resp.body;
        this.alertService.success('Succès lors de la modification', true);
        this.localDaoService.save('employeeGroup_to_show', empGroup);
        this.router.navigate(['habilitations']);
      },
      error => {
        const errorObj: any = error;
        this.alertService.error(errorObj.error.errorText);
      }
    );
    
  this.isSubmit = false;
  return false;
}

}
