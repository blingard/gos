import { MatSnackBar } from '@angular/material/';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import {AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';

import {Agent, Employee, GlobalEmployee} from '../../_generated/exports';
@Component({
  selector: 'app-add-edit-agent',
  templateUrl: './add-edit-agent.component.html',
  styleUrls: ['./add-edit-agent.component.css']
})
export class AddEditAgentComponent implements OnInit {

  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  onCliked=false;
  currentEmployee: GlobalEmployee;
  agentToSaveOrUpdate: Agent;
  isLoginUsed = false;
  employee: Employee;
  currentLogin = '';

  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService
) { }

  ngOnInit() {
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    if (this.data.add) {
      this.agentToSaveOrUpdate = this.createNewAgent();
    }
    else {
      this.isLoading = true;
      this.agentToSaveOrUpdate = this.localDaoService.getAgent('agent_to_edit', true);
      if (this.agentToSaveOrUpdate == null) {
        this.alertService.error('Veuilez choisir un agent à modifier', true);
        this.router.navigate(['agent']);
      } else {
        if (this.actionOnModelsService.isAgentDeleted(this.agentToSaveOrUpdate)) {
          this.router.navigate(['/error-500']);
        }
        this.currentLogin = this.agentToSaveOrUpdate.agentLogin;
      }
    }
    
  }

  createNewAgent(): Agent {
    return {
      agentId: null,
      agentName: null,
      agentLogin: null,
      agentPassword: null,
      agentPhoneNumber: null,
      status: null,
      isFirstConnection:null,
      agentDeviceList: null,
      agentSectorList: null
    };
  }

  onSubmit(){
    this.onCliked = true;
    if (this.data.add) {
      this.createAgent();
    } else {
      this.editAgent();
    }
    return false;
  }

  createAgent() {
    this.apiClientService.createAgent(this.agentToSaveOrUpdate)
      .subscribe(
        resp => {
          const ag: Agent = resp.body;
          if (ag) {
            this.alertService.success('agent créé avec succès!', true);
			      this.router.navigate(['/agent']);
          } else {
            this.onCliked = false;
              this.alertService.error('Echec');
          }
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.onCliked = false;
        }
      );
    return false;
  }

  editAgent() {
    this.apiClientService.updateAgent(this.agentToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const agt: Agent = resp.body;
          this.alertService.success('Succès lors de la modification', true);
          this.localDaoService.save('agent_to_show', agt);
          this.router.navigate(['agent']);
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.onCliked = false;
        }
      );
    return false;
  }

  detailsOfAgent() {
    this.localDaoService.save('agent_to_show', this.agentToSaveOrUpdate);
    this.router.navigate(['details-of-agent']);
  }


  deleteAgent() {
    this.agentToSaveOrUpdate.status = 2;
    this.apiClientService.updateAgent(this.agentToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const agt: Agent = resp.body;
          this.agentToSaveOrUpdate.status = agt.status;
          this.alertService.success('Agent supprimé avec succès.', true);
          this.router.navigate(['agent']);
        },
        error => {
         const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.router.navigate(['agent']);
        }
      );
  }

  cancel() {
    this.router.navigate(['/agent']);
  }

  verifyLogin () {
    this.apiClientService.verifyAgentLogin(this.agentToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const agtList: Array<Agent> = resp.body;
          if (agtList != null && !agtList.length) {
            this.isLoginUsed = false;
          } else {
            this.isLoginUsed = true;
          }
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        }
      );
    return false;
  }

}
