import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {  AlertService, LocalDaoService, ActionOnModelsService} from '../../_services/exports';
import { Employee} from '../../_generated/exports';

@Component({
  selector: 'app-details-of-employee',
  templateUrl: './details-of-employee.component.html',
  styleUrls: ['./details-of-employee.component.css']
})
export class DetailsOfEmployeeComponent implements OnInit {

  data: any;
  params: any;
  employee: Employee ;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    ) { }

  ngOnInit() {
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.employee = this.localDaoService.getEmployee('employee_to_show');
    if (this.employee == null) {
      this.alertService.error('Veuillez choisir un employer pour afficher ses détails', true);
      this.router.navigate(['employees']);
    }
  }
  changePassword(){
    this.router.navigate(['change-employee-password']);
  }
  getformatDater(chaine:string):string{
    return this.actionOnModelsService.formatDate(chaine);
  } 

}
