import { MatTableDataSource } from '@angular/material';
import { Employee } from './../../_generated/models/employee.model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import {AlertService, ApiClientService, ActionOnModelsService,
  LocalDaoService, LoadingMarkerService, CheckErrorService
 } from '../../_services/exports';
import {AccessRight, EmployeeGroup, AccessRightEmployeeGroup, GlobalEmployee, EmployeeGroupEmployee} from '../../_generated/exports';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';

@Component({
  selector: 'app-habilitation',
  templateUrl: './habilitation.component.html',
  styleUrls: ['./habilitation.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]
})
export class HabilitationComponent implements OnInit {
  toppings: FormGroup;
  pageemployeeGroupEmployees = 1;
  pageSizeemployeeGroupEmployees = 10;
  collectionSizeemployeeGroupEmployees = 0;  
  isEmptyemployeeGroupEmployees = false;

  pageaccessRights = 1;
  pageSizeaccessRights = 10;
  collectionSizeaccessRights = 0;  
  isEmptyaccessRights = false;

  isSubmit: boolean = false;
  isSubmit1: boolean = false;
  isSubmit2: boolean = false;
  isSubmit3: boolean = false;
  pageaccessRightEmployeeGroups = 1;
  pageSizeaccessRightEmployeeGroups = 10;
  collectionSizeaccessRightEmployeeGroups = 0;  
  isEmptyaccessRightEmployeeGroups = false;

  pageemployeeGroups = 1;
  pageSizeemployeeGroups = 10;
  collectionSizeemployeeGroups = 0;  
  isEmptyemployeeGroups = false;
  data: any;
  params: any;
  accessRight: AccessRight;
  displayedColumns: string[] = ['position', 'accessRightName', 'action'];
  accessData: MatTableDataSource<AccessRight>;

  displayedColumnsEG: string[] = ['employeeId', 'employeeGroupId', 'fromDate', 'action'];
  EgEData: MatTableDataSource<EmployeeGroupEmployee>;

  displayedColumnsAG: string[] = ['accessRightId', 'employeeGroupId', 'fromDate', 'action'];
  ARData: MatTableDataSource<AccessRightEmployeeGroup>;

  displayedColumnss: string[] = ['position', 'groupName', 'action'];
  groupData: MatTableDataSource<EmployeeGroup>;
  employees: Employee[];
  employeeGroupEmployees: EmployeeGroupEmployee[];
  employeeGroupEmployeesSort: EmployeeGroupEmployee[];
  accessRightEmployeeGroups:AccessRightEmployeeGroup[];
  accessRightEmployeeGroupsSort:AccessRightEmployeeGroup[];
  employeeGroups: EmployeeGroup[];
  employeeGroupsSort: EmployeeGroup[];
  accessRights : AccessRight[];
  accessRightsSort : AccessRight[];
  accessRGoup: AccessRightEmployeeGroup;


  accessRightList: AccessRight[];
  employeeGroupList: EmployeeGroup[];
  EmployeeGroupEmployeeList: EmployeeGroupEmployee[];
  accessRightEmployeeGroupList: AccessRightEmployeeGroup[];

  
  empGoupemp: EmployeeGroupEmployee;
  employeeGroup: EmployeeGroup
  employee_GroupForm: FormGroup;
  currentEmployee: GlobalEmployee;
  updateErrors = false;
  isLoading = true;
  dataSource: MatTableDataSource<AccessRight>;
  selection: SelectionModel<AccessRight>;
  dataSourceG: MatTableDataSource<EmployeeGroup>;
  selectionG: SelectionModel<EmployeeGroup>;
  todayDate: Date;
  dateToday: string;

  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    fb: FormBuilder,
    
    private checkErrorService: CheckErrorService
  ) { 
    this.toppings = fb.group({
      pepperoni: false,
      extracheese: false,
      mushroom: false
    });
  }
  onFilteremployeeGroupEmployees(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.employeeGroupEmployees=this.employeeGroupEmployeesSort;
    this.collectionSizeemployeeGroupEmployees=this.employeeGroupEmployees.length;
    if(values.length==0){
     this.pageSizeemployeeGroupEmployees=10;
     this.isEmptyemployeeGroupEmployees=false;
     this.employeeGroupEmployees=this.employeeGroupEmployeesSort;
     this.collectionSizeemployeeGroupEmployees=this.employeeGroupEmployees.length;
    }else{
      let tab: EmployeeGroupEmployee[]=[];
      this.pageSizeemployeeGroupEmployees=5;
      let a = this.employeeGroupEmployees.filter(employeeGroupEmployee => {
        if(employeeGroupEmployee.employee.employeeName.toLowerCase().includes(values.toLowerCase()) || employeeGroupEmployee.employee.employeeMatricule.toLowerCase().includes(values.toLowerCase()) || employeeGroupEmployee.employeeGroup.groupName.toLowerCase().includes(values.toLowerCase())){
          tab.push(employeeGroupEmployee);
        }
      });
      if(tab.length==0){
       this.isEmptyemployeeGroupEmployees=true;
       this.employeeGroupEmployees=this.employeeGroupEmployeesSort;
       this.collectionSizeemployeeGroupEmployees=this.employeeGroupEmployees.length;
       this.pageSizeemployeeGroupEmployees=10;
      }
      else{
       this.isEmptyemployeeGroupEmployees=false;
       this.employeeGroupEmployees=tab;
       this.collectionSizeemployeeGroupEmployees=this.employeeGroupEmployees.length;
      }
      
    }     
  }
  onFilteraccessRightEmployeeGroups(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.accessRightEmployeeGroups=this.accessRightEmployeeGroupsSort;
    this.collectionSizeaccessRightEmployeeGroups=this.accessRightEmployeeGroups.length;
    if(values.length==0){
     this.pageSizeaccessRightEmployeeGroups=10;
     this.isEmptyaccessRightEmployeeGroups=false;
     this.accessRightEmployeeGroups=this.accessRightEmployeeGroupsSort;
     this.collectionSizeaccessRightEmployeeGroups=this.accessRightEmployeeGroups.length;
    }else{
      let tab: AccessRightEmployeeGroup[]=[];
      this.pageSizeaccessRightEmployeeGroups=5;
      let a = this.accessRightEmployeeGroups.filter(accessRightEmployeeGroup => {
        if(accessRightEmployeeGroup.accessRight.accessRightName.toLowerCase().includes(values.toLowerCase()) || accessRightEmployeeGroup.employeeGroup.groupName.toLowerCase().includes(values.toLowerCase())){
          tab.push(accessRightEmployeeGroup);
        }
      });
      if(tab.length==0){
       this.isEmptyaccessRightEmployeeGroups=true;
       this.accessRightEmployeeGroups=this.accessRightEmployeeGroupsSort;
       this.collectionSizeaccessRightEmployeeGroups=this.accessRightEmployeeGroups.length;
       this.pageSizeaccessRightEmployeeGroups=10;
      }
      else{
       this.isEmptyaccessRightEmployeeGroups=false;
       this.accessRightEmployeeGroups=tab;
       this.collectionSizeaccessRightEmployeeGroups=this.accessRightEmployeeGroups.length;
      }
      
    }     
  }
  onFilteraccessRights(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.accessRights=this.accessRightsSort;
    this.collectionSizeaccessRights=this.accessRights.length;
    if(values.length==0){
     this.pageSizeaccessRights=10;
     this.isEmptyaccessRights=false;
     this.accessRights=this.accessRightsSort;
     this.collectionSizeaccessRights=this.accessRights.length;
    }else{
      let tab: AccessRight[]=[];
      this.pageSizeaccessRights=5;
      let a = this.accessRights.filter(accessRight => {
        if(accessRight.accessRightName.toLowerCase().includes(values.toLowerCase())){
          tab.push(accessRight);
        }
      });
      if(tab.length==0){
       this.isEmptyaccessRights=true;
       this.accessRights=this.accessRightsSort;
       this.collectionSizeaccessRights=this.accessRights.length;
       this.pageSizeaccessRights=10;
      }
      else{
       this.isEmptyaccessRights=false;
       this.accessRights=tab;
       this.collectionSizeaccessRights=this.accessRights.length;
      }
      
    }     
  }
  onFilteremployeeGroups(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.employeeGroups=this.employeeGroupsSort;
    this.collectionSizeemployeeGroups=this.employeeGroups.length;
    if(values.length==0){
     this.pageSizeemployeeGroups=10;
     this.isEmptyemployeeGroups=false;
     this.employeeGroups=this.employeeGroupsSort;
     this.collectionSizeemployeeGroups=this.employeeGroups.length;
    }else{
      let tab: EmployeeGroup[]=[];
      this.pageSizeemployeeGroups=5;
      let a = this.employeeGroups.filter(employeeGroup => {
        if(employeeGroup.groupName.toLowerCase().includes(values.toLowerCase())){
          tab.push(employeeGroup);
        }
      });
      if(tab.length==0){
       this.isEmptyemployeeGroups=true;
       this.employeeGroups=this.employeeGroupsSort;
       this.collectionSizeemployeeGroups=this.employeeGroups.length;
       this.pageSizeemployeeGroups=10;
      }
      else{
       this.isEmptyemployeeGroups=false;
       this.employeeGroups=tab;
       this.collectionSizeemployeeGroups=this.employeeGroups.length;
      }
      
    }     
  }

  ngOnInit() {
    
    
    this.initFormObject();
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    if (this.data.add) {
      this.accessRight = this.createNewAccessRight();
    }
    else {
      this.isLoading = true;
      this.accessRight = this.localDaoService.getAccessRight('accessRight_to_show', true);
      if (this.accessRight == null) {
        this.alertService.error('Veuilez choisir une entrée à modifier', true);
      } else {
        if (this.actionOnModelsService.isAccessRightDeleted(this.accessRight)) {
          this.router.navigate(['/error-500']);
        }
      }
    }

    this.apiClientService.getAllAccessRights().subscribe((res)=> {
      const data: AccessRight[] = res.body
      if(data != null){
        this.accessData = new MatTableDataSource(data);
        this.accessRights=data;
        this.accessRightsSort = this.accessRights;
        this.collectionSizeaccessRights = this.accessRights.length;
      }
    },
    (error=>{
    })
    );

    this.apiClientService.getAllEmployeeGroups().subscribe((res)=> {
      const data: EmployeeGroup[] = res.body
      if(data != null){
        this.groupData = new MatTableDataSource(data);
        this.employeeGroups=data;
        this.employeeGroupsSort = this.employeeGroups;
        this.collectionSizeemployeeGroups = this.employeeGroupsSort.length;
      }
    },
    (error=>{
    })
    );

    this.apiClientService.getAllEmployees().subscribe((res)=> {
      const data: Employee[] = res.body
      if(data != null){
        this.employees=data;
      }
    },
    (error=>{
    })
    );
    this.apiClientService.getAllEmployeeGroupEmployees().subscribe((res)=> {
      const data: EmployeeGroupEmployee[] = res.body
      if(data != null){
        this.EgEData = new MatTableDataSource(data);
        this.employeeGroupEmployees=data;
        this.employeeGroupEmployeesSort = this.employeeGroupEmployees;
        this.collectionSizeemployeeGroupEmployees = this.employeeGroupEmployeesSort.length;
      }
    },
    (error=>{
      const errorObj: any = error;
      this.alertService.error(errorObj.error.errorText);
    })
    );
    this.apiClientService.getAllAccessRightGroups().subscribe((res)=> {
      const data: AccessRightEmployeeGroup[] = res.body
      if(data != null){
        this.ARData = new MatTableDataSource(data);
        this.accessRightEmployeeGroups=data;
        this.accessRightEmployeeGroupsSort = this.accessRightEmployeeGroups;
        this.collectionSizeaccessRightEmployeeGroups = this.accessRightEmployeeGroupsSort.length;
      }
    },
    (error=>{
      const errorObj: any = error;
      this.alertService.error(errorObj.error.errorText);
    })
    );

    
  }

  createNewAccessRight(): AccessRight {
    return {
      accessRightId : null,
      accessRightName: null,
      accessRightEmployeeGroupList: null,
      accessRightStatus: null           
    };
  }
  private initAccessRight(): AccessRight {
    return {
      accessRightId : null,
      accessRightName: null,
      accessRightEmployeeGroupList: null,
      accessRightStatus: null
    };
  }

  private initEmployeeGroup(): EmployeeGroup {
    return {
      employeeGroupId : null,
      groupName: null,
      employeeGroupEmployeeList: null,
      accessRightEmployeeGroupList: null,
      groupStatus: null
    };
  }

  private initEmpGoupEmp(): EmployeeGroupEmployee {
    return {
      endDate: null,
      employee: {
        employeeId: null,
        employeeName: null,
        employeeSurname: null,
        employeeEmail: null,
        employeePassword: null,
        status: null,
        employeePhoneNumber: null,
        employeeMatricule: null,
        employeeLogin: null,
        employeeBirthdate: null,
        employeePlaceOfBirth: null,
        employeeNationality: null,
        isFirstConnection:null,
        employeeAdress: null,
        employeeGender: null,
      },
      employeeGroupEmployeePK: {
        employeeGroupId: null,
        employeeId: null,
        fromDate: null
      },
      employeeGroup: {
        employeeGroupId : null,
        groupName: null,
        employeeGroupEmployeeList: null,
        accessRightEmployeeGroupList: null,
        groupStatus: null
      }
    };
  }

  private initAccessRGoup(): AccessRightEmployeeGroup {
    return {
      endDate : null,
      accessRightEmployeeGroupPK:{
        employeeGroupId : null,
        accessRightId : null,
        fromDate : null
      },
      accessRight:{
        accessRightId : null,
        accessRightName : null,
        accessRightEmployeeGroupList : null,
        accessRightStatus : null
      },
      employeeGroup: {
        employeeGroupId : null,
        groupName: null,
        employeeGroupEmployeeList : null,
        accessRightEmployeeGroupList : null,
        groupStatus : null
      }

    };
  }

  initFormObject(){
    this.accessRight = this.initAccessRight();
    this.employeeGroup = this.initEmployeeGroup();
    this.empGoupemp = this.initEmpGoupEmp();
    this.accessRGoup= this.initAccessRGoup();

  }
  onSubmitEmployeeGroup(){
  }
  cancel(){
    this.ngOnInit();
  }
  createEmployeeGroup() {
    this.isSubmit3 = true;
    let employeegroup: EmployeeGroup =this.employeeGroup;
    this.employeeGroup = this.initEmployeeGroup();
    this.apiClientService.createEmployeeGroup(employeegroup)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const empG: EmployeeGroup = resp.body;
          if (empG) {
            this.alertService.success('groupe créé avec succès!', true);
            this.apiClientService.getAllEmployeeGroups().subscribe((res)=> {
              const data: EmployeeGroup[] = res.body
              if(data != null){
                this.groupData = new MatTableDataSource(data);
                this.employeeGroups=data;
                this.employeeGroupsSort = this.employeeGroups;
                this.collectionSizeemployeeGroups = this.employeeGroupsSort.length;
              }
              this.isSubmit3 = false;
            },
            (error=>{
              this.isSubmit3 = false;
            })
            );
			 } else {
        this.isSubmit3 = false;
              this.alertService.error('Echec de création');
          }
        },
        error => {
          this.alertService.error('Echec');
        }
      );      
    return false;
  }
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  }
  getformatDater(chaine:string):string{
    return this.actionOnModelsService.formatDate(chaine);
  }  
  isAccessRightListEmpty() {
    return this.accessRightList == null || this.accessRightList.length === 0;
  }
  isAccessRightDeleted(accessRight: AccessRight) {
    return this.actionOnModelsService.isAccessRightDeleted(accessRight);
  }

  isEmployeeGroupListEmpty() {
    return this.employeeGroupList == null || this.employeeGroupList.length === 0;
  }
  isEmployeeGroupDeleted(employeeGroup: EmployeeGroup) {
    return this.actionOnModelsService.isEmployeeGroupDeleted(employeeGroup);
  }
  deleteEmployeeGroup(employeeGroup: EmployeeGroup) {
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){
      this.updateEmployeeGroupStatus(employeeGroup, 2, 1);
    }
  }
  deleteAccessRight(accessRight: AccessRight) {
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){ 
      this.updateAccessRightStatus(accessRight, 2, 1);
    }
  }
  isEmployeeGroupEmployeeListEmpty() {
    return this.EmployeeGroupEmployeeList == null || this.EmployeeGroupEmployeeList.length === 0;
  }
  isEmployeeGroupEmployeeDeleted(empGemp: EmployeeGroupEmployee) {
    return this.actionOnModelsService.isEmployeeGroupEmployeeDeleted(empGemp);
  }
  deleteEmployeeGroupEmployee(empGemp: EmployeeGroupEmployee) {
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){ 
      this.updateEmployeeGroupEmployeeEndDate(empGemp);
    }
  }
  updateEmployeeGroupEmployeeEndDate(empGemp: EmployeeGroupEmployee) { 

    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    empGemp.endDate = this.dateToday;
    console.log(empGemp);
    this.apiClientService.updateEmployeeGroupEmployeeWhereEndDate(empGemp)
    .subscribe(
      resp => {
        const empGEmp: EmployeeGroupEmployee = resp.body;
        empGemp.endDate = empGEmp.endDate;
          this.alertService.success('affection supprimer avec succès.');
          this.apiClientService.getAllEmployeeGroupEmployees().subscribe((res)=> {
            const data: EmployeeGroupEmployee[] = res.body
            if(data != null){
              this.EgEData = new MatTableDataSource(data);
              this.employeeGroupEmployees=data;
              this.employeeGroupEmployeesSort = this.employeeGroupEmployees;
              this.collectionSizeemployeeGroupEmployees = this.employeeGroupEmployeesSort.length;
            }
          },
          (error=>{
          })
          );
      },
      error => {
        const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Echec lors de la mise à jour du droit');
      }
    );
  }  

  updateEmployeeGroupStatus(employeeGroup: EmployeeGroup, status: number, showMessage: number = 0) {
    const lastStatus: number = employeeGroup.groupStatus;
    employeeGroup.groupStatus = status;
    this.apiClientService.updateEmployeeGroup(employeeGroup)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const empG: EmployeeGroup = resp.body;
          employeeGroup.groupStatus = empG.groupStatus;
          if (showMessage === 1) {
            this.alertService.success('groupe mis à jour avec succès.');
            this.apiClientService.getAllEmployeeGroups().subscribe((res)=> {
              const data: EmployeeGroup[] = res.body
              if(data != null){
                this.groupData = new MatTableDataSource(data);
                this.employeeGroups=data;
                this.employeeGroupsSort = this.employeeGroups;
                this.collectionSizeemployeeGroups = this.employeeGroupsSort.length
              }
            },
            (error=>{
            })
            );
          }
          if (showMessage === 2 && !this.updateErrors) {
            this.alertService.success('Les groupes ont été mis à jour avec succès.');
          }
        },
        error => {
          this.updateErrors = true;
          employeeGroup.groupStatus = lastStatus;
          this.alertService.error('Echec lors de la mise à jour du droit');
        }
      );
  }
  updateAccessRightStatus(accessRight: AccessRight, status: number, showMessage: number = 0) {
    const lastStatus: number = accessRight.accessRightStatus;
    accessRight.accessRightStatus = status;
    this.apiClientService.updateAccessRight(accessRight)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const access: AccessRight = resp.body;
          accessRight.accessRightStatus = access.accessRightStatus;
          if (showMessage === 1) {
            this.alertService.success('droits mis à jour avec succès.');
            this.apiClientService.getAllAccessRights().subscribe((res)=> {
              const data: AccessRight[] = res.body
              if(data != null){
                this.accessData = new MatTableDataSource(data);
                this.accessRights=data;
                this.accessRightsSort = this.accessRights;
                this.collectionSizeaccessRights = this.accessRights.length;
              }
            },
            (error=>{
            })
            );
          }
        },
        error => {
          this.updateErrors = true;
          accessRight.accessRightStatus = lastStatus;
          this.alertService.error('Echec lors de la mise à jour du droit');
        }
      );
  }

  createAccessRight() {
    this.isSubmit2=true;
    let accessright: AccessRight = this.accessRight;
    this.apiClientService.createAccessRight(accessright)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const accesR: AccessRight = resp.body;
          if (accesR) {
            
            this.alertService.success('droit d\'acces créé avec succès!', true);
            this.apiClientService.getAllAccessRights().subscribe((res)=> {
              
              const data: AccessRight[] = res.body;
              if(data != null){
                this.accessData = new MatTableDataSource(data);
                this.accessRights=data;
                this.accessRightsSort = this.accessRights;
                this.collectionSizeaccessRights = this.accessRights.length;
              }
              
    this.isSubmit2=false;
            },
            (error=>{
            })
            );
            this.isSubmit2=false;
            this.accessRight = this.initAccessRight();
			 } else {
              this.alertService.error('Echec de création');
              this.isSubmit2=false;
              this.accessRight = this.initAccessRight();
          }
        },
        error => {
          this.isSubmit2=false;
          this.alertService.error('Echec');
        }
      );
      
    return false;
  }
  editAccessRight(accessRight: AccessRight) {
    this.localDaoService.save('accessRight_to_edit', accessRight);
    this.router.navigate(['edit-accessRight']);

  }
  editEmployeeGroup(employeeGroup: EmployeeGroup) {
    this.localDaoService.save('employeeGroup_to_edit', employeeGroup);
    this.router.navigate(['edit-employeeGroup']);

  }

  editEmployeeGroupEmployee(empGroupEmp: EmployeeGroupEmployee) {
  this.localDaoService.save('employeeGroupEmployee_to_edit', empGroupEmp);
  this.router.navigate(['edit-employeeGroupEmployee']);
  }
  editAccessRightGroup(arGroupEmp: AccessRightEmployeeGroup) {
    this.localDaoService.save('accessRightEmployeeGroup_to_edit', arGroupEmp);
    this.router.navigate(['edit-accessRightGroup']);
    }

  affectEmployeeGoup(){ 
    this.isSubmit=true;
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
   this.empGoupemp.employee.employeeId = this.empGoupemp.employeeGroupEmployeePK.employeeId;
    this.empGoupemp.employeeGroup.employeeGroupId = this.empGoupemp.employeeGroupEmployeePK.employeeGroupId;
    this.empGoupemp.employeeGroupEmployeePK.fromDate= this.dateToday;
    let empGroupEmp :EmployeeGroupEmployee = this.empGoupemp;
    this.apiClientService.affectEmployeeToGroup(empGroupEmp).subscribe(
      (response)=>{
        if(response.body !=null){
          this.alertService.success("groupe assigné avec succèes");
          this.apiClientService.getAllEmployeeGroupEmployees().subscribe((res)=> {
            const data: EmployeeGroupEmployee[] = res.body
            if(data != null){
              this.EgEData = new MatTableDataSource(data);
              this.employeeGroupEmployees=data;
              this.employeeGroupEmployeesSort = this.employeeGroupEmployees;
              this.collectionSizeemployeeGroupEmployees = this.employeeGroupEmployeesSort.length;
            }
          },
          (error=>{
          })
          );
        }        
        this.empGoupemp = this.initEmpGoupEmp();
        this.isSubmit=false;
      },
      (err)=>{
        this.alertService.error("Impossible d'affecter cet employé" + err.body);
        this.isSubmit=false;
      }
    )
  }

  affectAccessRightToGoup(){
    this.isSubmit1 = true;
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    this.accessRGoup.accessRight.accessRightId = this.accessRGoup.accessRightEmployeeGroupPK.accessRightId;
    this.accessRGoup.employeeGroup.employeeGroupId = this.accessRGoup.accessRightEmployeeGroupPK.employeeGroupId;    
    this.accessRGoup.accessRightEmployeeGroupPK.fromDate=this.dateToday;
    let accessGroup :AccessRightEmployeeGroup = this.accessRGoup;
    this.apiClientService.affectAccessRightToGroup(accessGroup).subscribe(
      (response)=>{
        if(response.body !=null){
          this.alertService.success("droit assigné avec succèes");
          this.apiClientService.getAllAccessRightGroups().subscribe((res)=> {
            const data: AccessRightEmployeeGroup[] = res.body
            if(data != null){
              this.ARData = new MatTableDataSource(data);
              this.accessRightEmployeeGroups=data;
              this.accessRightEmployeeGroupsSort = this.accessRightEmployeeGroups;
              this.collectionSizeaccessRightEmployeeGroups = this.accessRightEmployeeGroupsSort.length;
            }
          },
          (error=>{
          })
          );
        }
        
    this.accessRGoup = this.initAccessRGoup();
      },
      (err)=>{
        this.alertService.error("Impossible d'affecter ce droit" + err.body)
      }
    );    
    this.isSubmit1 = false;
  }

  isAccessRightEmployeeGroupListEmpty() {
    return this.accessRightEmployeeGroupList == null || this.accessRightEmployeeGroupList.length === 0;
  }
  isAccessRightEmployeeGroupDeleted(arempGroup: AccessRightEmployeeGroup) {
    return this.actionOnModelsService.isAccessRightEmployeeGroupDeleted(arempGroup);
  }
  deleteAccessRightEmployeeGroup(empGemp: AccessRightEmployeeGroup) {
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){
      this.updateAccessRightEmployeeGroupEndDate(empGemp);
    }
  }
  updateAccessRightEmployeeGroupEndDate(arempGroup: AccessRightEmployeeGroup) {
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    arempGroup.endDate = this.dateToday;
    this.apiClientService.updateAccessRightEmployeeGroupEndDate(arempGroup)
    .subscribe(
      resp => {
        const arempGroup: AccessRightEmployeeGroup = resp.body;
        arempGroup.endDate = arempGroup.endDate;
          this.alertService.success('affection supprimer avec succès.');
          this.apiClientService.getAllAccessRightGroups().subscribe((res)=> {
            const data: AccessRightEmployeeGroup[] = res.body
            if(data != null){
              this.ARData = new MatTableDataSource(data);
              this.accessRightEmployeeGroups=data;
              this.accessRightEmployeeGroupsSort = this.accessRightEmployeeGroups;
              this.collectionSizeaccessRightEmployeeGroups = this.accessRightEmployeeGroupsSort.length;
            }
          },
          (error=>{
          })
          );
      },
      error => {
        const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Echec lors de la suppression ');
      }
    );
  }  


}
