import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {AlertService, ApiClientService, ActionOnModelsService,
  LocalDaoService, LoadingMarkerService, CheckErrorService
 } from '../../_services/exports';
 import {Sector, GlobalEmployee} from '../../_generated/exports';

@Component({
  selector: 'app-add-edit-sector',
  templateUrl: './add-edit-sector.component.html',
  styleUrls: ['./add-edit-sector.component.css']
})
export class AddEditSectorComponent implements OnInit {
  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  isSubmit = false;
  sectorToSaveOrUpdate: Sector;
  currentEmployee: GlobalEmployee;
  myForm: FormGroup;
 
  constructor(private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService) { }

  ngOnInit() {
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    if (this.data.add) {
      this.myForm = this.fb.group(
        {
          sectorName : new FormControl('', Validators.required),
          description : new FormControl('', Validators.required),
        }
      );
    }
    else {
      this.isLoading = true;
      let sector: Sector = this.localDaoService.getSector('sector_to_edit', true);
      this.sectorToSaveOrUpdate = sector;
      this.myForm = this.fb.group(
        {
          sectorName : new FormControl(sector.sectorName, Validators.required),
          description : new FormControl(sector.description, Validators.required),
        }
      );
      if (sector == null) {
        this.alertService.error('Veuilez choisir un secteur à modifier', true);
        this.router.navigate(['sector']);
      } else {
        if (this.actionOnModelsService.isSectorDeleted(this.sectorToSaveOrUpdate)) {
          this.router.navigate(['/error-500']);
        }
      }
    }
    
  }
  createNewSector(): Sector{
    return {
      sectorId : null,
      sectorName : null,
      description : null,
      status : null,
      agentSectorList: null,
    };
  }
  
  onSubmit(){
    this.isSubmit = true;
    if (this.data.add) {
      this.createSector();
    } else {
      this.editSector();
    }
    return false;
  }
  createSector() {
    let sector: Sector = {
      sectorId : null,
      sectorName : this.myForm.value.sectorName,
      description : this.myForm.value.description,
      status : null,
      agentSectorList: null,
    };
    this.apiClientService.createSector(sector)
      .subscribe(
        resp => {
          const sec: Sector = resp.body;
          if (sec) {
            this.alertService.success('Secteur créé avec succès!', true);
			      this.router.navigate(['sector']);
          } else {
              this.alertService.error('Echec');
              this.isSubmit = false;
          }
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.isSubmit = false;
        }
      );
    return false;
  }

  editSector() {
    let sector: Sector = {
      sectorId : this.sectorToSaveOrUpdate.sectorId,
      sectorName : this.myForm.value.sectorName,
      description : this.myForm.value.description,
      status : this.sectorToSaveOrUpdate.status,
      agentSectorList: null,
    };
    this.apiClientService.updateSector(sector)
      .subscribe(
        resp => {
          if(resp.status==200){
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            const sec: Sector = resp.body;
            this.alertService.success('Succès lors de la modification', true);
            //this.localDaoService.save('sector_to_show', sec);
            this.router.navigate(['sector']);
          }else{
            this.alertService.error('Erreur survenue lors de la modification');
            this.isSubmit = false;
            //this.router.navigate(['sector']);
          }
          
        },
        error => {
          const errorObj: any = error;
          this.alertService.error("Error");
          this.isSubmit = false;
        }
      );
    return false;
  }

  detailsOfSector() {
    this.localDaoService.save('sector_to_show', this.sectorToSaveOrUpdate);
    this.router.navigate(['details-of-sector']);
  }

  deleteSector() {
    this.sectorToSaveOrUpdate.status = 2;
    this.apiClientService.updateSector(this.sectorToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const sec: Sector = resp.body;
          this.sectorToSaveOrUpdate.status = sec.status;
          this.alertService.success('secteur supprimé avec succès.', true);
          this.router.navigate(['sector']);
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.router.navigate(['sector']);
        }
      );
  }

  cancel() {
    this.router.navigate(['sector']);
  }



}
