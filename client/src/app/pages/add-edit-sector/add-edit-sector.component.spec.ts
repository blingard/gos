import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditSectorComponent } from './add-edit-sector.component';

describe('AddEditDistrictComponent', () => {
  let component: AddEditSectorComponent;
  let fixture: ComponentFixture<AddEditSectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditSectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditSectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
