import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {Employee, GlobalEmployee } from '../../_generated/exports';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.css']
})
export class ListEmployeeComponent implements OnInit {
  

  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty=false;
  currentEmployee: GlobalEmployee;
  isAdminConnected = true;
  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  displayedColumns: Array<String>;
  dataSource: MatTableDataSource<Employee>;
  selection: SelectionModel<Employee>;
  employeeList: Array<Employee>;
  employeeListSort: Array<Employee>;

  constructor(
    private route: ActivatedRoute,
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService,
  ) { }


  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.employeeList=this.employeeListSort;
    this.collectionSize=this.employeeList.length;
    if(values.length==0){
     this.pageSize=10;
     this.isEmpty=false;
     this.employeeList=this.employeeListSort;
     this.collectionSize=this.employeeList.length;
    }else{
      let tab: Employee[]=[];
      this.pageSize=5;
      let a = this.employeeList.filter(employee => {
        if(employee.employeeName.toLowerCase().includes(values.toLowerCase())){
          tab.push(employee);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.employeeList=this.employeeListSort;
       this.collectionSize=this.employeeList.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.employeeList=tab;
       this.collectionSize=this.employeeList.length;
      }
      
    }     
  }

  isCurrentEmployee(employee: Employee){
    if(this.currentEmployee.employee.employeeMatricule==employee.employeeMatricule){
      return true;
    }else{
      return false;
    }
  }




  ngOnInit() {
    this.currentEmployee = this.localDaoService.getGlobalEmployee('currentEmployee');
    this.isAdminConnected = this.actionOnModelsService.isAdminConnected(this.currentEmployee);
    this.isLoading = true;
    this.employeeList= null;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
        this.loadingMarkerservice.show();
    this.apiClientService.getAllEmployees()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const empList: Array<Employee> = resp.body;
            if (empList != null && !empList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.employeeList = empList;
              this.employeeListSort = this.employeeList;
              this.collectionSize=this.employeeList.length;
            }
            
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
    this.displayedColumns = ['select', 'position', 'employeeName', 'employeeSurname','employeePhoneNumber','employeeMatricule', 'actions'];
  }

  ngAfterViewInit() {
    if (!this.isEmployeeListEmpty()) {
      //this.dataSource.paginator = this.paginator;
      //this.dataSource.sort = this.sort;
    }

  }
  isEmployeeListEmpty() {
    return this.employeeList == null || this.employeeList.length === 0;
  }
  isEmployeeDeleted(employee: Employee) {
    return this.actionOnModelsService.isEmployeeDeleted(employee);
  }
  canViewDetails() {
    return this.selection.selected.length !== 0;
  }
  canEditEmployee() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isEmployeeDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  canDeleteEmployee() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isEmployeeDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
   /** Whether the number of selected elements matches the total number of rows. */
   isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
   /** Selects all rows if they are not all selected; otherwise clear selection. */
   masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  clearSelection() {
    this.selection.clear();
  }
  toggleSelection(row: Employee) {
    if (this.selection.isSelected(row)) {
      this.selection.clear();
    } else {
      this.selection.clear();
      this.selection.select(row);
    }
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  refresh() {
    this.isLoading = true;
    this.employeeList = null;
    this.loadingMarkerservice.show();
        this.apiClientService.getAllEmployees()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const empList: Array<Employee> = resp.body;
            if (empList != null && !empList.length) {
              this.collectionSize=empList.length;
              this.alertService.error('Erreur:');
            } else {
              this.employeeList = empList;
              this.employeeListSort = this.employeeList;
              this.collectionSize=this.employeeList.length;
            }
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
  }
  openSelectedEmployee() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un employé');
    } else {
      const employee: Employee = this.selection.selected[0];
      this.localDaoService.save('selected_employee', employee);
      this.router.navigate(['dashboard']);
    }
  }
  editSelectedEmployee() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un employé');
    } else {
      let employee: Employee;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        employee = this.selection.selected[i];
        if (!this.isEmployeeDeleted(employee)) {
          break;
        }
      }
      this.localDaoService.save('employee_to_edit', employee);
      this.router.navigate(['edit-employee']);
    }
  }
  deleteSelectedEmployee() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un employé');
    } else {
      this.updateErrors = false;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isEmployeeDeleted(this.selection.selected[i])) {
          if (i !== this.selection.selected.length - 1) {
            this.updateEmployeeStatus(this.selection.selected[i], 2);
          } else {
            this.updateEmployeeStatus(this.selection.selected[i], 2, 2);
          }
        }
      }
    }
  }
  detailsOfEmployee(employee: Employee) {
    this.localDaoService.save('employee_to_show', employee);
    this.router.navigate(['details-of-employee']);
  }
  detailsOfSelectedEmployee(employee: Employee) {
      const emp: Employee = employee;
      this.localDaoService.save('employee_to_show', emp);
      this.router.navigate(['details-of-employee']);
  }
  editEmployee(employee: Employee) {
    this.localDaoService.save('employee_to_edit', employee);
    this.router.navigate(['edit-employee']);
  }

  deleteEmployee(employee: Employee) {
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){
      this.updateEmployeeStatus(employee, 2, 1);
    }
  }
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  }   

  addEmployee() {
    this.router.navigate(['add-employee']);
  }
  updateEmployeeStatus(employee: Employee, status: number, showMessage: number = 0) {
    const lastStatus: number = employee.status;
    employee.status = status;
    this.apiClientService.updateEmployee(employee)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const emp: Employee = resp.body;
          employee.status = emp.status;
          if (showMessage === 1) {
            this.alertService.success('employé mis à jour avec succès.');
          }
          if (showMessage === 2 && !this.updateErrors) {
            this.alertService.success('Les employé ont été mis à jour avec succès.');
          }
          if (showMessage === 2) {
            this.refresh();
          }
          this.refresh();
        },
        error => {
          this.updateErrors = true;
          employee.status = lastStatus;
          this.alertService.error('Echec lors de la mise à jour de l\'employé');
        }
      );
  }

}
