

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, AuthenticationService, ApiClientService,
         LocalDaoService, CheckErrorService
        } from '../../_services/exports';
import { Employee, GlobalEmployee } from '../../_generated/exports';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import Validation from './validation';

@Component({
  selector: 'app-define-new-identifiers',
  templateUrl: './define-new-identifiers.component.html',
  styleUrls: ['./define-new-identifiers.component.css']
})
export class DefineNewIdentifiers implements OnInit {

  employee: Employee;
  employeeRemember: boolean;
  isLoginUsed = false;
  currentLogin = '';
  form: FormGroup;
  isSubmit: boolean = false;


  constructor(
	private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private checkErrorService: CheckErrorService
  ) { 
    let patter = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,12}$";
    this.form = this.formBuilder.group(
      {
        login: [this.currentLogin, Validators.required],
        newPassword: ['', [Validators.required, Validators.pattern(patter)]],
        cpassword: ['', [Validators.required, Validators.pattern(patter)]]
        
        
      },
      {
        validators: [Validation.match('newPassword', 'cpassword')]
      }
    );
   }

  ngOnInit() {
    this.employee = this.localDaoService.getGlobalEmployee('empl_to_define_identifiers', true).employee;
    this.currentLogin = this.employee.employeeLogin;
    this.employeeRemember = false;
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  defineNewIdentifiers() { 
    this.isSubmit = true;
    this.employee.isFirstConnection = false;
    this.employee.employeePassword = this.form.controls.cpassword.value;
    this.apiClientService.defineNewIdentifiers(this.employee)
      .subscribe(
        resp => {
          /**
           * 
           * 
           * if(resp.status==200){
            if (this.localDaoService.saveToken(resp.body.token) == false){
              this.isSubmit=false;
              this.router.navigate(['define-new-identifiers']);
            }
            else{
              let empl: GlobalEmployee = this.localDaoService.getGlobalEmployee('empl_temp');
              this.authenticationService.connectEmployee(empl, this.employeeRemember);
              this.localDaoService.removeData('empl_temp');
              this.isSubmit=false;
              this.router.navigate(['/dashboard']);
            }
          }else {
            this.isSubmit=false;
              this.alertService.error('Echec de connexion');
          }
           */


          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const empl: GlobalEmployee = resp.body;
          if (empl) {
            this.authenticationService.connectEmployee(empl, this.employeeRemember);
            this.localDaoService.removeData('empl_to_define_identifiers');
            this.router.navigate(['dashboard']);
          } else {
            this.isSubmit = false;
              this.alertService.error('Les nouveaux identifiants ne peuvent être enregistrés contacter l\'administrateur');
          }
        },
        error => {
          console.log(error);
          this.isSubmit = false;
          this.alertService.error('Echec de l\'enregistrement');
        }
      );
    return false;
  }

  employeeRememberTrigered() {
    this.employeeRemember = !this.employeeRemember;
  }

  

  verifyLogin () {
    this.employee.employeeLogin = this.form.value.login;
    this.apiClientService.verifyLogin(this.employee)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const emplList: Array<Employee> = resp.body;
          if (emplList != null && !emplList.length) {
            this.isLoginUsed = false;
          } else {
            this.isLoginUsed = true;
          }
        },
        error => {
        }
      );
    return false;
  }
}
