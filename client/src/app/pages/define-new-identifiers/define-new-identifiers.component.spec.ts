import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineNewIdentifiers } from './define-new-identifiers.component';

describe('DefineNewIdentifiers', () => {
  let component: DefineNewIdentifiers;
  let fixture: ComponentFixture<DefineNewIdentifiers>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefineNewIdentifiers ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineNewIdentifiers);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
