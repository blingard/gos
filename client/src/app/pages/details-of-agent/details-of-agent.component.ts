import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthenticationService, AlertService, ApiClientService, LocalDaoService,
         ActionOnModelsService } from '../../_services/exports';
import { Agent} from '../../_generated/exports';

@Component({
  selector: 'app-details-of-agent',
  templateUrl: './details-of-agent.component.html',
  styleUrls: ['./details-of-agent.component.css']
})
export class DetailsOfAgentComponent implements OnInit {

  data: any;
  params: any;
  agent: Agent;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.agent = this.localDaoService.getAgent('agent_to_show');
    if (this.agent == null) {
      this.alertService.error('Veuillez choisir un agent pour afficher ses détails', true);
      this.router.navigate(['agent']);
    }
  }

  getAgentSectors(){
    this.router.navigate(['sector-agent']);
  }

  onAffect() {
    this.localDaoService.save("agent_to_affect", this.agent);
    this.router.navigate(["affect-agent-device"]);
  }

  onAffectSector() {
    this.localDaoService.save("agent_to_affect_sector", this.agent);
    this.router.navigate(["affect-sector-to-agent"]);
  }
  
  getAgentDevices() {
    this.localDaoService.save("agent_to_display_devices", this.agent);
    this.router.navigate(['list-agent-devices']);
  }
  
}
