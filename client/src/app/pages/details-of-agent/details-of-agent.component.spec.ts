import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsOfAgentComponent } from './details-of-agent.component';

describe('DetailsOfAgentComponent', () => {
  let component: DetailsOfAgentComponent;
  let fixture: ComponentFixture<DetailsOfAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsOfAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsOfAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
