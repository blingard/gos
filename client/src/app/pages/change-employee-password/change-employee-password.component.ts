import { Router } from '@angular/router';
import { AlertService, ApiClientService, LocalDaoService, CheckErrorService} from 'src/app/_services/exports';
import { Employee, GlobalEmployee } from '../../_generated/exports';

import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import Validation from './validation';

@Component({
  selector: 'app-change-employee-password',
  templateUrl: './change-employee-password.component.html',
  styleUrls: ['./change-employee-password.component.css']
})
export class ChangeEmployeePasswordComponent implements OnInit {
  employee: Employee;
  form: FormGroup;
  currentEmployee: GlobalEmployee;
  submitted = false;
  isSubmit: boolean = false;

  constructor(private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private checkErrorService: CheckErrorService) {
      let patter = '^(?=.{8,12}$)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*';
      this.form = this.formBuilder.group(
        {
          resetPassword: ['', Validators.required],
          newPassword: ['', [Validators.required, Validators.pattern(patter)]],
          cpassword: ['', Validators.required]
          
          
        },
        {
          validators: [Validation.match('newPassword', 'cpassword')]
        }
      );
  }

  ngOnInit(): void {
    this.employee = this.localDaoService.getEmployee('employee_to_show');
    this.employee.employeePassword='';
    if (this.employee == null) {
      this.alertService.error('Veuillez choisir un employer pour afficher ses détails', true);
      this.router.navigate(['employees']);
    }
    
  }
  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  onSubmit(): void {
    this.isSubmit = true;
    let newPassword = this.form.value.newPassword;
    this.currentEmployee= this.localDaoService.getGlobalEmployee('currentEmployee');
    this.employee.employeePassword=this.form.value.resetPassword;
    this.apiClientService.changeEmployeePassword(this.employee, newPassword, this.currentEmployee).subscribe(
      (response)=>{
        const errorObject: any = response.body;
        if(!this.checkErrorService.checkRemoteData(errorObject)){          
          this.isSubmit=false;
          this.router.navigate(['/error-404']);
        }
        if(response.body !=null){
          this.isSubmit=false;
          this.router.navigate(["details-of-employee"])
          this.alertService.success("Mot de passe modifier avec succèes");
        }
      },
      (err)=>{        
        this.isSubmit=false;
        this.alertService.error("Impossible de changer le mot de passe" + err.body)
      }
    );
  }

  onReset(): void {
    this.submitted = false;
    this.form.reset();
  }
/*
  
  newPassword: '';
  cpassword = '';
  passconf = true;
  


  private ConfirmedValidator(controlName: string, matchingControlName: string){
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
            return;
        }
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ confirmedValidator: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}

get f() { return this.changePasswordForm.controls; }

  constructor(
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private checkErrorService: CheckErrorService) {
      let patter = "/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/";
      this.changePasswordForm  = formBuilder.group(
        {
          
  
        }, {
          validator: MustMatch('cpassword', 'newPassword')
        }
      );
    }
  

  ngOnInit() {
    
  }
  
  
  onSubmit(){
    this.currentEmployee= this.localDaoService.getGlobalEmployee('currentEmployee');
    this.apiClientService.changeEmployeePassword(this.employee, this.newPassword, this.currentEmployee).subscribe(
      (response)=>{
        const errorObject: any = response.body;
        if(!this.checkErrorService.checkRemoteData(errorObject)){
          this.router.navigate(['/error-404']);
        }
        if(response.body !=null){
          this.router.navigate(["details-of-employee"])
          this.alertService.success("Mot de passe modifier avec succèes");
        }
      },
      (err)=>{
        this.alertService.error("Impossible de changer le mot de passe" + err.body)
      }
    )
  }*/
}

