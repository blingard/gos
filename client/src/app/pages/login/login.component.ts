import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';


import { AlertService, AuthenticationService, ApiClientService,
         LocalDaoService, CheckErrorService
        } from '../../_services/exports';
import { Employee, GlobalEmployee } from '../../_generated/exports';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  employee: Employee;
  employeeRemember: boolean;
  isSubmit: boolean = false;
  isLog: boolean;
  currentEmployee: GlobalEmployee;
  logSubscription = new Subscription();

  constructor(
	  private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private authService: AuthenticationService,
    private checkErrorService: CheckErrorService
  ) {  }

  

  ngOnInit() {
    this.authenticationService.enableHeaderBar();
    this.employee = {
      employeeId: null,
      employeeName: null,
      employeeSurname: null,
      employeeEmail: null,
      status: null,
      employeePassword: null,
      employeePhoneNumber: null,
      employeeMatricule: null,
      employeeLogin: null,
      employeeBirthdate: null,
      employeePlaceOfBirth: null,
      employeeNationality: null,
      employeeAdress: null,
      employeeGender: null,
      isFirstConnection: null
    };
    this.employeeRemember = true;
    
    if(this.localDaoService.existToken()){
      if(this.localDaoService.checkExpiration()==false){ 
        if(confirm("Se Connecter avec l'utilisateur "+this.localDaoService.getUserSession().employee.employeeName+" ?")){
          this.authenticationService.connectEmployee(this.localDaoService.getUserSession(), this.employeeRemember);
          this.router.navigate(['/dashboard'])
        }else{
          this.localDaoService.clearMemory();
        } 
      }
    }
  }

  login() {
    this.isSubmit = true;
    this.apiClientService.connectEmployeeWithLoginAndPass(this.employee)
      .subscribe(
        resp => {
          if(resp.status==200){
            if (this.localDaoService.saveToken(resp.body)){
              this.isSubmit=false;
              this.router.navigate(['/define-new-identifiers']);
            }
            else{
              let empl: GlobalEmployee = this.localDaoService.getGlobalEmployee('empl_temp');
              this.authenticationService.connectEmployee(empl, this.employeeRemember);
              this.localDaoService.removeData('empl_temp');
              this.isSubmit=false;
              this.router.navigate(['/dashboard']);
            }
          }else {
            this.isSubmit=false;
              this.alertService.error('Echec de connexion');
          }
        },
        error => {
          this.isSubmit=false;
          this.alertService.error('Echec de connexion');
        }
      );
    return false;
  }

  employeeRememberTrigered() {
    this.employeeRemember = !this.employeeRemember;
  }
}
