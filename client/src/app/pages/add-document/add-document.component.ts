import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/';
import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import actes_administratives from './../../../assets/_files/actes_administratives.json';

import { BsModalRef } from 'ngx-bootstrap/modal';

import {AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';

import {Documents, DocType, UploadResponse, Site, GlobalEmployee} from '../../_generated/exports';
@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.css']
})
export class AddDocumentComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  data: any;
  params: any;
  okinterump = false;
  okreload = false;
  okadddoc = false;
  isLoading = true;
  updateErrors = false;
  docToSave: Documents;
  site: Site;
  docTypes: DocType[];
  docFile: File;
  uploadR: UploadResponse;
  typeFile="*"
  title;
  donnee:Site;
  type;
  myInterruptForm: FormGroup;
  myReloadForm: FormGroup;
  ngForm: FormGroup;
  actesAdministrativeList: {id:number, acte:String, code:String}[]=actes_administratives;
  isSubmit: boolean = false;

  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private _snackBar: MatSnackBar,
    private checkErrorService: CheckErrorService
) {
  this.myInterruptForm = formBuilder.group(
    {
      interrupt: new FormControl('', Validators.required),
    }
  );
  this.myReloadForm = formBuilder.group(
    {
      reloadSite: new FormControl('', Validators.required),
    }
  );
  this.ngForm = formBuilder.group(
    {
      documentName: new FormControl('', Validators.required),
      addDocNameType: new FormControl('', Validators.required),
      file: new FormControl('', Validators.required),
      documentCode: new FormControl('', Validators.required),
      documentNumber: new FormControl('', Validators.required),
    }
  );
 }


 openSnackBar(message: string, action: string) {
  this._snackBar.open(message, action,{
    duration: 5000,
    horizontalPosition: this.horizontalPosition,
    verticalPosition: this.verticalPosition,

  });
}
  ngOnInit() {
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.isLoading = true;
    this.site = this.localDaoService.getSite('site_to_show');
    this.apiClientService.getAllDocTypes().subscribe(
      (response)=>{
        const docTypesList: Array<DocType> = response.body;
        if (docTypesList != null && !docTypesList.length) {
          this.openSnackBar('Impossible d\'obtenir la liste des types de documents','');
        } else {
          this.docTypes = docTypesList;
        }
      },
      (error)=>{
        
        this.alertService.error('Impossible d\'obtenir la liste des types de documents');
      }
    );

    this.docToSave = this.createNewDocument();  
  }
  chooseDocType(){
    let i: number=parseInt(this.ngForm.value.addDocNameType);
    this.docTypes.forEach(docType =>{
      if(docType.docTypeId==i){
        this.docToSave.docTypeId=docType;
      }
    });
    if (this.docToSave.docTypeId.docTypeId == 2) {
      this.typeFile = "image/png, image/jpg, image/jpeg";

    }else if(this.docToSave.docTypeId.docTypeId == 1){
      this.typeFile = "application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf, application/msword";

    }else if (this.docToSave.docTypeId.docTypeId == 3){
      this.typeFile = "audio/*";

    }else if (this.docToSave.docTypeId.docTypeId == 4){
      this.typeFile = "video/*";
    }
  }

  createNewDocument(): Documents {
    return {
      documentId: null,
      documentName: null,
      documentCode: null,
      documentNumber:null,
      documentPath: null,
      secretKey: null,
      createdDate: null,
      status: null,
      documentExtension: null,
      docTypeId: {
        docTypeId: null,
        docTypeName: null,
        description: null,
        documentsList: null
      },
      siteId: {
        siteId: null,
        siteName: null,
        refSite: null,
        ownerName: null,
        ownerPhone: null,
        ownerCni: null,
        description: null,
        status: null,
        districtId: {
          districtId: null,
          districtName: null,
          commune: null,
          standing:null,
          population:null,
          densite: null,
          description: null,
          status: null,
          siteList: null,
          sectorId:{
            sectorId: null,
            sectorName: null,
            description: null,
            status: null,
            agentSectorList: null,
          },
        },
       documentsList: null,
       gpsCoordinatesList: null
      }
    };
  }

  createUploadResponse(): UploadResponse {
    return {
      uploadLocation: '',
      uploadDate: ''
    };
  }

  onSubmit(){
    this.isSubmit = true;
      this.createDocument();
  }

  onSubmitInterrupt(){
    this.isSubmit = true;
    this.onInterrumpre();
    
  }

  onSubmitReload(){
    this.isSubmit = true;
    this.onRelancerWorkOfSite();
  }

  private onInterrumpre(){
    this.site.description=this.myInterruptForm.value.interrupt;
    this.myInterruptForm.reset();
    this.okinterump=false;
    this.apiClientService.interruptWorkOfSite(this.site).subscribe(
      resp => {
        if(resp.status==200){
          this.site = resp.body;
          //this.openSnackBar("Site Interompus avec success","");
          this.okinterump=true;
          
    this.isSubmit = false;
          this.close();
          this.alertService.success('Travaux sur le Site/Chantier interrompus avec success');

        }else{
          this.isSubmit = false;
        }
        
      },
      error => {
        this.isSubmit = false;

      }

    );

  }

  private onRelancerWorkOfSite(){
    this.site.description=this.myReloadForm.value.reloadSite;
    this.myReloadForm.reset();
    this.okreload=false;
    this.apiClientService.relancerWorkOfSite(this.site).subscribe(
      resp => {
        if(resp.status==200){
          this.site = resp.body;
          this.okreload=true;
          this.isSubmit = false;
          this.close();
          this.alertService.success('Travaux sur le Site/Chantier relance avec success');
        }
        
    this.isSubmit = false;
      },
      error => {
        this.isSubmit = false;

      }

    );

  }

 


  createDocument() {

    this.okadddoc=false;
    let formData: FormData = new FormData();
    formData.append('file', this.docFile);
    this.uploadR = this.createUploadResponse();
    /*
    lors de la creation d'un document de type : Titre Foncier, Permit d'Implanter, Certificat d'Urbanisme, 
    Permit de Construire, Certificat de conformité, Permis de démolir, metre le status a 5 pour attente de verification,
    04 pour encours de verification, 0 pour verifier et document authentique, 1 pour verifier et non authentique
    
    */
    this.docToSave.documentName = this.ngForm.value.documentName;
    this.docToSave.documentCode= this.ngForm.value.documentCode;
    this.docToSave.documentNumber= this.ngForm.value.documentNumber;
    if(this.ngForm.value.documentNumber!='' || this.ngForm.value.documentCode!=''){
      this.docToSave.status= 5;
    }
    this.ngForm.reset();
    if (this.docToSave.docTypeId.docTypeId == 2) {
      this.apiClientService.uploadImageFile(formData).subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const uplRes: UploadResponse = resp.body;
          if (uplRes) {
            this.docToSave.documentPath = uplRes.uploadLocation;
            this.docToSave.createdDate = uplRes.uploadDate;
            this.docToSave.documentName
            this.docToSave.siteId = this.site;
            this.apiClientService.createDocument(this.docToSave)
            .subscribe(
            resp => {
              const errorObject: any = resp.body;
              this.checkErrorService.checkRemoteData(errorObject);
              const doc: Documents = resp.body;
              if (doc) {
                this.okadddoc = true;
                this.isSubmit = false;
                this.close();
               this.alertService.success('Document enregistre avec success raffraichir la page ');
              } else {
                this.isSubmit = false;
                this.close();
                this.openSnackBar('Echec car le document retourné est vide','');
              }
            },
            error => {
              this.isSubmit = false;
              this.alertService.error('Echec de l\'enregistrement');
            }
        );
          } else {
            this.isSubmit = false;
              this.alertService.error('Retour objet echoué');
          }
        },
        error => {
          this.isSubmit = false;
          this.alertService.error('Echec du téléversement');
        }
      );
      return false;
    }
    else if (this.docToSave.docTypeId.docTypeId == 1) {
      this.apiClientService.uploadTextFile(formData).subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const uplRes: UploadResponse = resp.body;
          if (uplRes) {
            this.docToSave.documentPath = uplRes.uploadLocation;
            this.docToSave.createdDate = uplRes.uploadDate;
            this.docToSave.siteId = this.site;
            this.apiClientService.createDocument(this.docToSave)
            .subscribe(
            resp => {
              const errorObject: any = resp.body;
              this.checkErrorService.checkRemoteData(errorObject);
              const doc: Documents = resp.body;
              if (doc) {
                this.isSubmit = false;
                this.close();
               this.alertService.success('Document enregistre avec success  raffraichir la page ');
              } else {
                this.isSubmit = false;
                this.close();
                this.openSnackBar('Echec car le document retourné est vide','');
              }
            },
            error => {
              this.isSubmit = false;
              this.alertService.error('Echec de l\'enregistrement');
            }
        );
          } else {
            this.isSubmit = false;
              this.alertService.error('Retour objet echoué');
          }
        },
        error => {
          this.isSubmit = false;
          this.alertService.error('Echec du téléversement');
        }
      );
      return false;
    }
    else if (this.docToSave.docTypeId.docTypeId == 3) {
      this.apiClientService.uploadAudioFile(formData).subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const uplRes: UploadResponse = resp.body;
          if (uplRes) {
            this.docToSave.documentPath = uplRes.uploadLocation;
            this.docToSave.createdDate = uplRes.uploadDate;
            this.docToSave.siteId = this.site;
            this.apiClientService.createDocument(this.docToSave)
            .subscribe(
            resp => {
              const errorObject: any = resp.body;
              this.checkErrorService.checkRemoteData(errorObject);
              const doc: Documents = resp.body;
              if (doc) {
                this.isSubmit = false;
                this.close();
               this.alertService.success('Document enregistre avec success  raffraichir la page ');
              } else {
                this.isSubmit = false;
                  this.alertService.error('Echec car le document retourné est vide');
              }
            },
            error => {
              this.isSubmit = false;
              this.alertService.error('Echec de l\'enregistrement');
            }
        );
          } else {
            this.isSubmit = false;
              this.alertService.error('Retour objet echoué');
          }
        },
        error => {
          this.isSubmit = false;
          this.alertService.error('Echec du téléversement');
        }
      );
      return false;
    }
    else if (this.docToSave.docTypeId.docTypeId == 4) {
      this.apiClientService.uploadVideoFile(formData).subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const uplRes: UploadResponse = resp.body;
          if (uplRes) {
            this.docToSave.documentPath = uplRes.uploadLocation;
            this.docToSave.createdDate = uplRes.uploadDate;
            this.docToSave.siteId = this.site;
            this.apiClientService.createDocument(this.docToSave)
            .subscribe(
            resp => {
              const errorObject: any = resp.body;
              this.checkErrorService.checkRemoteData(errorObject);
              const doc: Documents = resp.body;
              if (doc) {
                this.isSubmit = false;
                this.close();
               this.alertService.success('Document enregistre avec success  raffraichir la page ');
              } else {
                this.isSubmit = false;
                  this.alertService.error('Echec car le document retourné est vide');
              }
            },
            error => {
              this.isSubmit = false;
              this.alertService.error('Echec de l\'enregistrement');
            }
        );
          } else {
            this.isSubmit = false;
              this.alertService.error('Retour objet echoué');
          }
        },
        error => {
          this.isSubmit = false;
          this.alertService.error('Echec du téléversement');
        }
      );
      return false;
    }
  }
  close() {
    this.bsModalRef.hide();
  }

  cancel() {
    this.router.navigate(['carte']);
    //this.router.navigate(['details-of-site']);
  }

  fileChange(event: any) {
    this.saveImageBeforeUpload(event.target.files);
  }
  saveImageBeforeUpload (fileList: FileList) {
    if (fileList.length > 0) {
      this.docFile = fileList[0];
    }
  }

}
