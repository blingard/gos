import { Component, OnInit } from '@angular/core';
import message from './../../../assets/_files/message.json';
import {Alerts, UploadResponse,AlertsDocuments, ViewsAlertsDocuments} from '../../_generated/exports';
import { Router, ActivatedRoute } from '@angular/router';
import {AlertService, ApiClientService, ActionOnModelsService,
  LocalDaoService, LoadingMarkerService, CheckErrorService, AuthenticationService
 } from '../../_services/exports';
 import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import { Subscription } from 'rxjs';
 

@Component({
  selector: 'app-add-alert',
  templateUrl: './add-alert.component.html',
  styleUrls: ['./add-alert.component.css']
})
export class AddAlertComponent implements OnInit {
  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);

  isSubmit: boolean = false;
  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  alerts: Alerts;
  todayDate: Date;
  dateToday: string;
  imageFile: File;
  uploadR: UploadResponse;
  docToSave: AlertsDocuments;
  viewsAlertDoc:ViewsAlertsDocuments;
  myForm: FormGroup;
  messageList: {id:number, message:String}[]=message;
 


  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService,
    private authenticationService: AuthenticationService
  ) {
    var patter = '^[6]{1}[0-9]{8}$';
    this.myForm = formBuilder.group(
      {
        personName: new FormControl('', Validators.required),
        personPhone: new FormControl('', [Validators.required, Validators.pattern(patter)]),
        alertsDistrict: new FormControl('', Validators.required),
        alertsMessage: new FormControl('', Validators.required),
        alertsDescription: new FormControl('', [Validators.required, Validators.maxLength(250)]),
        file: new FormControl('', Validators.required)

      }
    );
   }

  ngOnInit() {

    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.isLoading = true;
    this.alerts = this.createNewAlerts();
    const empl = this.localDaoService.getEmployee("currentEmployee");
    if (empl != null) {
      this.authenticationService.logout();
    }
    this.authenticationService.disableHeaderBar();
  }

  createNewAlerts(): Alerts {
    return {
      alertsId: null,
      alertsMessage:null,
      alertsDescription:null,
      personName:null,
      personPhone:null,
      alertsDistrict: null,
      alertsDate: null,
      status: null,
      alertsDocumentsList:null
    };
  }
  createAlertsDocuments(): AlertsDocuments {
    return {
      documentId: null,
      documentName: null,
      documentPath: null,
      secretKey: null,
      createdDate: null,
      status: null,
      documentExtension: null,
      docType:null,
      alertsId: {
      alertsId: null,
      alertsMessage:null,
      alertsDescription:null,
      personName:null,
      personPhone:null,
      alertsDistrict: null,
      alertsDate: null,
      status: null,
      alertsDocumentsList: null
      }
    };
  }

  createViewsAlertsDocuments(): ViewsAlertsDocuments {
    return {
      alertsDocuments:{
        documentId: null,
      documentName: null,
      documentPath: null,
      secretKey: null,
      createdDate: null,
      status: null,
      documentExtension: null,
      docType:null,
      alertsId: {
      alertsId: null,
      alertsMessage:null,
      alertsDescription:null,
      personName:null,
      personPhone:null,
      alertsDistrict: null,
      alertsDate: null,
      status: null,
      alertsDocumentsList: null
      },
      },      
      alerts: {
        alertsId: null,
        alertsMessage:null,
        alertsDescription:null,
        personName:null,
        personPhone:null,
        alertsDistrict: null,
        alertsDate: null,
        status: null,
        alertsDocumentsList: null
        },
    };
  }
  createNewDoc(): AlertsDocuments {
    return {
      documentId: null,
      documentName: null,
      documentPath: null,
      secretKey: null,
      documentExtension: null,
      createdDate: null,
      status: null,
      docType:null,
      alertsId: {
        alertsId: null,
        alertsMessage:null,
        alertsDescription:null,
        personName:null,
        personPhone:null,
        alertsDistrict: null,
        alertsDate: null,
        status: null,
        alertsDocumentsList:null
      }
    };
  }
  /*fileChange(event: any) {
    this.saveImageBeforeUpload(event.target.files);
  }
  saveImageBeforeUpload (fileList: FileList) {
    if (fileList.length > 0) {
      const file: File = fileList[0];
      this.imageFile = fileList[0];
      //this.gosFile = this.createGosFile(file.name, file.size, file.type, file);
      //this.localDaoService.save('siteImageToUpload', this.gosFile);
    }
  }*/






  onSubmit(){
    alert(this.isSubmit);
   this.createAlerts();
  }
  createUploadResponse(): UploadResponse {
    return {
      uploadLocation: '',
      uploadDate: ''
    };
  }
  createAlerts() {    
    this.isSubmit = true;
    alert(this.isSubmit);
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    this.alerts.alertsDate= this.dateToday;
    let formData: FormData = new FormData();
    formData.append('file', this.imageFile);
    this.uploadR = this.createUploadResponse();
    this.apiClientService.uploadImageFile(formData).subscribe(
      resp => {
        const errorObject: any = resp.body;
        this.checkErrorService.checkRemoteData(errorObject);
        const uplRes: UploadResponse = resp.body;
        if (uplRes) {
          this.viewsAlertDoc = this. createViewsAlertsDocuments();
          this.viewsAlertDoc.alertsDocuments.documentPath=uplRes.uploadLocation;
          this.viewsAlertDoc.alertsDocuments.createdDate=this.dateToday;
          this.alerts.personName = this.myForm.value.personName;
          this.alerts.personPhone = this.myForm.value.personPhone;
          this.alerts.alertsDistrict = this.myForm.value.alertsDistrict;
          this.alerts.alertsMessage = this.myForm.value.alertsMessage;
          this.alerts.alertsDescription = this.myForm.value.alertsDescription;
          this.viewsAlertDoc.alerts=this.alerts;
          this.apiClientService.createViewsAlertsDocument(this.viewsAlertDoc)
          .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            const alerts: Alerts = resp.body;
            if (alerts) {
              this.alertService.success('Alerte envoyé avec succès!', true);
              this.isSubmit=false;
              this.myForm.reset();
            } else {
                this.alertService.error('Echec car le document retourné est vide');
                this.isSubmit=false;
            }
          },
          error => {
            this.alertService.error('Echec d\'envoi d\'alerte');
            this.isSubmit=false;
          }
          );
        } else {
          this.alertService.error('Retour objet echoué');
          this.isSubmit=false;
      }
    },
    error => {
      this.alertService.error('Echec du téléversement');
      this.isSubmit=false;
    }
    );
    return false;
  }
  /*clearForm(){
    const controsObj=this.myForm.controls;
    for(let prop in controsObj){
      if(controsObj.hasOwnProperty(prop)){
        (controsObj[prop] as Controls).updateValue();
      }
    }
  }*/
  goToAdminPage() {
    this.authenticationService
    this.router.navigate(['authentication']);
  }

  fileChange(event: any) {
    this.saveImageBeforeUpload(event.target.files);
  }
  saveImageBeforeUpload (fileList: FileList) {
    if (fileList.length > 0) {
      this.imageFile = fileList[0];
    }
  }
}
