import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {Agent, GlobalEmployee } from '../../_generated/exports';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list-agent',
  templateUrl: './list-agent.component.html',
  styleUrls: ['./list-agent.component.css']
})
export class ListAgentComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty = false;
  currentEmployee: GlobalEmployee;
  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  displayedColumns: Array<String>;
  dataSource: MatTableDataSource<Agent>;
  selection: SelectionModel<Agent>;
  agentsList: Array<Agent>;
  agentsListSort: Array<Agent>;

  constructor( private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService,
    private translate: TranslateService,
  ) { }
  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.agentsList=this.agentsListSort;
    this.collectionSize=this.agentsList.length;
    if(values.length==0){
     this.pageSize=10;
     this.isEmpty=false;
     this.agentsList=this.agentsListSort;
     this.collectionSize=this.agentsList.length;
    }else{
      let tab: Agent[]=[];
      this.pageSize=5;
      let a = this.agentsList.filter(agent => {
        if(agent.agentName.toLowerCase().includes(values.toLowerCase()) || agent.agentPhoneNumber.toLowerCase().includes(values.toLowerCase())){
          tab.push(agent);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.agentsList=this.agentsListSort;
       this.collectionSize=this.agentsList.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.agentsList=tab;
       this.collectionSize=this.agentsList.length;
      }
      
    }     
  }

  ngOnInit() {
    this.isLoading = true;
    this.agentsList= null;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
        this.loadingMarkerservice.show();
        this.apiClientService.getAllAgents()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const agList: Array<Agent> = resp.body;
            if (agList != null && !agList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.agentsList = agList;
              this.agentsListSort=this.agentsList;
              this.collectionSize = this.agentsList.length;
            }
            this.dataSource = new MatTableDataSource<Agent>(this.agentsList);
            this.selection = new SelectionModel<Agent>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
    this.displayedColumns = ['select', 'position', 'agentName', 'agentPhoneNumber', 'actions','actions','actions'];
  }
  ngAfterViewInit() {
    if (!this.isAgentListEmpty()) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

  }
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  }
  isAgentListEmpty() {
    return this.agentsList == null || this.agentsList.length === 0;
  }
  isAgentDeleted(agent: Agent) {
    return this.actionOnModelsService.isAgentDeleted(agent);
  }
  canViewDetails() {
    return this.selection.selected.length !== 0;
  }
  canEditAgent() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isAgentDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  canDeleteAgents() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isAgentDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  clearSelection() {
    this.selection.clear();
  }
  toggleSelection(row: Agent) {
    if (this.selection.isSelected(row)) {
      this.selection.clear();
    } else {
      this.selection.clear();
      this.selection.select(row);
    }
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  refresh() {
    this.isLoading = true;
    this.agentsList = null;
    this.loadingMarkerservice.show();
        this.apiClientService.getAllAgents()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const agList: Array<Agent> = resp.body;
            if (agList != null && !agList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.agentsList = agList;              
              this.agentsListSort=this.agentsList;
              this.collectionSize = this.agentsList.length;
            }
            this.dataSource = new MatTableDataSource<Agent>(this.agentsList);
            this.selection = new SelectionModel<Agent>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
  }
  openSelectedAgent(agentSee: Agent) {
      const agent: Agent = agentSee;
      this.localDaoService.save('agent_to_show', agent);
      this.router.navigate(['details-of-agent']);
  }
  editSelectedAgent() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un agent');
    } else {
      let agent: Agent;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        agent = this.selection.selected[i];
        if (!this.isAgentDeleted(agent)) { 
          break;
        }
      }
      this.localDaoService.save('agent_to_edit', agent);
      this.router.navigate(['edit-agent']);
    }
  }
  deleteSelectedAgent() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un agent');
    } else {
      this.updateErrors = false;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isAgentDeleted(this.selection.selected[i])) {
          if (i !== this.selection.selected.length - 1) {
            this.updateAgentStatus(this.selection.selected[i], 2);
          } else {
            this.updateAgentStatus(this.selection.selected[i], 2, 2);
          }
        }
      }
    }
  }
  detailsOfAgent(agent: Agent) {
    this.localDaoService.save('agent_to_show', agent);
    this.router.navigate(['details-of-agent']);
  }

  detailsOfSelectedAgent() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un agent');
    } else {
      const aget: Agent = this.selection.selected[0];
      this.localDaoService.save('agent_to_show', aget);
      this.router.navigate(['details-of-agent']);
    }
  }

  editAgent(agent: Agent) {
    this.localDaoService.save('agent_to_edit', agent);
    this.router.navigate(['edit-agent']);
  }
  deleteAgent(agent: Agent) { 
    let msg = null;
      this.translate.get("CONFIRMREMOVESECTOR").subscribe((data:String)=> {
        msg=data;
      });
      let v= confirm(msg+agent.agentName+" ?");
    if(v==true){
      this.updateAgentStatus(agent, 2, 1);
    }
  }
  reabilityAgent(agent: Agent) { 
    let msg = null;
      this.translate.get("CONFIRMRRETABLIR").subscribe((data:String)=> {
        msg=data;
      });
      let v= confirm(msg+agent.agentName+" ?");
    if(v==true){
      this.updateAgentStatus(agent, 0, 2);
    }
  }
  addAgent() {
    this.router.navigate(['add-agent']);
  }
  listAgentSectorActive(){
    this.router.navigate(['sector-agent']);
  }
  
  updateAgentStatus(agent: Agent, status: number, showMessage: number = 0) {
    const lastStatus: number = agent.status;
    agent.status = status;
    this.apiClientService.updateAgent(agent)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const ag: Agent = resp.body;
          agent.status = ag.status;
          if (showMessage === 1) {
            this.alertService.success('agent supprimer avec succès.');
          }
          if (showMessage === 2 && !this.updateErrors) {
            this.alertService.success('Les agents ont été mis à jour avec succès.');
          }
          if (showMessage === 2) {
            this.refresh();
          }

          this.apiClientService.getAllAgents().subscribe(
            (data)=>{
              const errorObject: any = data.body;
              this.checkErrorService.checkRemoteData(errorObject);
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              const agList: Array<Agent> = data.body;
              if (agList != null && !agList.length) {
                this.alertService.error('Impossible d\'obtenir la liste des agents');
              } else {
                this.agentsList = agList;                
              this.agentsListSort=this.agentsList;
              this.collectionSize = this.agentsList.length;
              }
              this.dataSource = new MatTableDataSource<Agent>(this.agentsList);
              this.selection = new SelectionModel<Agent>(true, []);
            },
            (err)=>{
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              this.alertService.error('Impossible d\'obtenir la liste des agents');
            }
          )
          

        },
        error => {
          this.updateErrors = true;
          agent.status = lastStatus;
          this.alertService.error('Echec lors de la mise à jour de ce agent');
        }
      );
  }

}
