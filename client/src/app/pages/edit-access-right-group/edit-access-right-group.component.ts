import { Component, OnInit } from '@angular/core';
import {AlertService, ApiClientService, ActionOnModelsService,
  LocalDaoService, LoadingMarkerService, CheckErrorService } from '../../_services/exports';
 import { Router, ActivatedRoute } from '@angular/router';
import {AccessRightEmployeeGroup, GlobalEmployee, AccessRight, EmployeeGroup} from '../../_generated/exports';

@Component({
  selector: 'app-edit-access-right-group',
  templateUrl: './edit-access-right-group.component.html',
  styleUrls: ['./edit-access-right-group.component.css']
})
export class EditAccessRightGroupComponent implements OnInit {

  currentEmployee: GlobalEmployee;
  arempGroupN: AccessRightEmployeeGroup;
  arempGroup: AccessRightEmployeeGroup;
  accessRightEmployeeGroups: AccessRightEmployeeGroup[];
  accessRights: AccessRight[];
  employeeGroups: EmployeeGroup[];
  todayDate: Date;
  dateToday: string;
  isSubmit: boolean = false;

  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService
  ) { }

  ngOnInit() {
      this.arempGroup = this.localDaoService.getAccessRightEmployeeGroup('accessRightEmployeeGroup_to_edit');
      this.apiClientService.getAllAccessRightGroups().subscribe((res)=> {
        const data: AccessRightEmployeeGroup[] = res.body
        if(data != null){
          this.accessRightEmployeeGroups=data;
        }
      },
      (error=>{
      })
      );
      this.apiClientService.getAllAccessRights().subscribe((res)=> {
        const data: AccessRight[] = res.body
        if(data != null){
          this.accessRights=data;
        }
      },
      (error=>{
      })
      );
  
      this.apiClientService.getAllEmployeeGroups().subscribe((res)=> {
        const data: EmployeeGroup[] = res.body
        if(data != null){
           this.employeeGroups=data;
        }
      },
      (error=>{
      })
      );
  }
  onSubmitAccessRightEmployeeGroup(){
    this.editAccessRightEmployeeGroup();
  }
  editAccessRightEmployeeGroup() {
    this.isSubmit = true;
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    this.arempGroupN = this.localDaoService.getAccessRightEmployeeGroup('accessRightEmployeeGroup_to_edit');
    this.arempGroup.accessRightEmployeeGroupPK.fromDate=this.dateToday;
    this.arempGroupN.endDate=this.dateToday;
    this.apiClientService.updateAccessRightEmployeeGroup(this.arempGroup, this.arempGroupN)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const arempGroup: AccessRightEmployeeGroup = resp.body;
          this.alertService.success('Succès lors de la modification', true);
          this.localDaoService.save('AccessRightEmployeeGroup_to_show', arempGroup);
          this.router.navigate(['habilitations']);
          this.isSubmit = false;
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.isSubmit = false;
        }
      );
    return false;
  }

}
