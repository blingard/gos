import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAccessRightGroupComponent } from './edit-access-right-group.component';

describe('EditAccessRightGroupComponent', () => {
  let component: EditAccessRightGroupComponent;
  let fixture: ComponentFixture<EditAccessRightGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAccessRightGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAccessRightGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
