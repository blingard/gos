import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAccessRightComponent } from './edit-access-right.component';

describe('EditAccessRightComponent', () => {
  let component: EditAccessRightComponent;
  let fixture: ComponentFixture<EditAccessRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAccessRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAccessRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
