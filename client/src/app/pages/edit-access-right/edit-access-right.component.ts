import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import {AlertService, ApiClientService, ActionOnModelsService,
  LocalDaoService, LoadingMarkerService, CheckErrorService
 } from '../../_services/exports';
 import { FormGroup} from '@angular/forms';
import {AccessRight, GlobalEmployee} from '../../_generated/exports';

@Component({
  selector: 'app-edit-access-right',
  templateUrl: './edit-access-right.component.html',
  styleUrls: ['./edit-access-right.component.css']
})
export class EditAccessRightComponent implements OnInit {

  accessRight: AccessRight;
  displayedColumns: string[] = ['position', 'accessRightName', 'action'];
  accessData: MatTableDataSource<AccessRight>;
  changePasswordForm: FormGroup;
  currentEmployee: GlobalEmployee;
  accessRights: AccessRight[];
  isSubmit: boolean = false;

  constructor(private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService
  ) { }

  ngOnInit() {
  
    this.accessRight = this.localDaoService.getAccessRight('accessRight_to_edit');
    
    this.apiClientService.getAllAccessRights().subscribe((res)=> {
      const data: AccessRight[] = res.body
      if(data != null){
        this.accessData = new MatTableDataSource(data);
        this.accessRights=data;
      }
    },
    (error=>{
    })
    );
  }
  onSubmitAccess(){
    this.isSubmit = true;
      this.editAccessRight();
    }
  editAccessRight() {
    this.apiClientService.updateAccessRight(this.accessRight)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const access: AccessRight = resp.body;
          this.alertService.success('Succès lors de la modification', true);
          this.localDaoService.save('accessRight_to_show', access);
          this.router.navigate(['habilitations']);

          this.apiClientService.getAllAccessRights().subscribe((res)=> {
            const data: AccessRight[] = res.body
            if(data != null){
              this.accessData = new MatTableDataSource(data);
              this.accessRights=data;
            }
            
    this.isSubmit = false;
          },
          (error=>{
            
    this.isSubmit = false;
          })
          );
        },
        error => {
          this.isSubmit = false;
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        }
      );
    return false;
  }

}
