import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';

import {AgentActivity, AgentOccurenceWork, Agent, Site, Notification, GlobalEmployee, Sector } from '../../_generated/exports';
import { TranslateService } from '@ngx-translate/core';
import { Chart } from 'chart.js';
import { interval, Subscription } from 'rxjs';









@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  siteok:number=0;
  siteinterup:number=0;
  sitedestroy:number=0;
  sitesuivi:number=0;
  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty = false;
  data: any;
  params: any;
  isLoading = true;
  isLoadingSector = true;
  isLoadingSite = true;
  counter: any = interval(1000);
  c: number = 0;
  counterSubsciptio: Subscription;

  displayedactivitiesColumns: Array<String>;
  agentActivitiesDataSource: MatTableDataSource<AgentActivity>;
  selection: SelectionModel<AgentActivity>;
  agentActivitiesList: Array<AgentActivity>;
  agentActivitiesListIsEmpty=false;
  agentActivitiesListSort: Array<AgentActivity>;
  currentEmployee: GlobalEmployee;
  //Les plus actifs
  mostWorkerAgentsList: Array<AgentOccurenceWork>;
  mostWorkerAgentsDataSource: MatTableDataSource<AgentOccurenceWork>;
  displayedmostWorkerAgentsColumns: Array<String>;
  
  //Les moins actifs
  worstWorkerAgentsList: Array<AgentOccurenceWork>;
  worstWorkerAgentsDataSource: MatTableDataSource<AgentOccurenceWork>;
  displayedworstWorkerAgentsColumns: Array<String>;
  //Pour les sites
  sitesList: Array<Site>;
  //Sites achevés
  completedSitesList: Array<Site>;
  completedSitesDataSource: MatTableDataSource<Site>;
  displayedcompletedSitesColumns: Array<String>;
  //Sites interrompus
  interruptedSitesList: Array<Site>;
  interruptedSitesDataSource: MatTableDataSource<Site>;
  displayedinterruptedSitesColumns: Array<String>;

  /////notification 
  notification: Notification;
  notificationText: String= null;

  todayDate: Date;
  sectorStat = [
    {
      name:"",
      color:"",
      activities:0
    }
  ];
  sectorListString: string[]=[];
  sectorListactivities: number[]=[]
  dateToday: string;
  //siteActivity: SiteActivity;



  

  constructor(
    private route: ActivatedRoute,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    public translate: TranslateService,
    private checkErrorService: CheckErrorService,
  ) { }
  ngOnDestroy() {
    //this.apiClientService.onGetAllNotification(false);
  }

  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.agentActivitiesList=this.agentActivitiesListSort;
    this.collectionSize=this.agentActivitiesList.length;
    if(values.length==0){
     this.pageSize=10;
     this.isEmpty=false;
     this.agentActivitiesList=this.agentActivitiesListSort;
     this.collectionSize=this.agentActivitiesList.length;
    }else{
      let tab: AgentActivity[]=[];
      this.pageSize=5;
      let a = this.agentActivitiesList.filter(agentActivity => {
        if(agentActivity.agentId.agentName.toLowerCase().includes(values.toLowerCase()) || agentActivity.agentId.agentLogin.toLowerCase().includes(values.toLowerCase()) || agentActivity.agentAction.toLowerCase().includes(values.toLowerCase())){
          tab.push(agentActivity);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.agentActivitiesList=this.agentActivitiesListSort;
       this.collectionSize=this.agentActivitiesList.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.agentActivitiesList=tab;
       this.collectionSize=this.agentActivitiesList.length;
      }
      
    }     
  }

  ngOnInit() {
   // this.apiClientService.onGetAllNotification(true);
    this.initFormObject();
    this.isLoading = true;
    this.agentActivitiesList = null;
    this.sectorListactivities = [];
    this.mostWorkerAgentsList = null;
    this.worstWorkerAgentsList = new Array;
    this.sitesList = new Array;
    this.completedSitesList = new Array;
    this.interruptedSitesList = new Array;
    this.route.data.subscribe(data => this.data = data);
    this.loadingMarkerservice.show();
    this.getAllAgentActivities();
  //  this.getAllSite();

    this.displayedactivitiesColumns = ['position', 'agentName', 'agentAction', 'actionDate'];

     

    


    
    //Meilleurs agents
    this.displayedmostWorkerAgentsColumns = ['agentName', 'nberActions', 'notifier'];

    //Les agents les moins actifs
    this.displayedworstWorkerAgentsColumns = ['agentName', 'nberActions', 'notifier'];

    //Sites achevés
    this.displayedcompletedSitesColumns = ['siteName', 'siteStatus','siteDetail', 'siteRelance'];

    //Sites interrompus
    this.displayedinterruptedSitesColumns = ['siteName', 'siteStatus','Details', 'relancer'];

  }
  
  refresh(){
    this.ngOnInit();
  }

  async getAllAgentActivities(){
    await this.apiClientService.getAllAgentActivities()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const activitiesList: Array<AgentActivity> = resp.body;
            if (activitiesList != null && !activitiesList.length) {
              this.agentActivitiesList = new Array;
              this.worstWorkerAgents(this.agentActivitiesList);
            } else {
              this.agentActivitiesList = activitiesList;
              this.agentActivitiesListSort = this.agentActivitiesList;
              this.collectionSize = this.agentActivitiesList.length;
              this.worstWorkerAgents(this.agentActivitiesList);
              this.mostWorkerAgentsList = this.mostWorkerAgents(this.agentActivitiesList);
              this.apiClientService.getAllSector().subscribe(
                resps => {
                  const errorObject: any = resps.body;
                  this.checkErrorService.checkRemoteData(errorObject);
                  this.isLoading = false;
                  this.loadingMarkerservice.hide();
                  const secList: Array<Sector> = resps.body;
                  if (secList != null && !secList.length) {
                    //this.alertService.error('Erreur:');
                  } else {
                    this.sectorListString=[];
                    this.sectorListactivities = [];
                    secList.forEach(sector=>{
                      this.sectorListString.push(sector.sectorName);
                    });
                    let colors: string[]=
            [
              "rgba(128,0,0)",
              "rgba(220,20,60)",
              "rgba(255,0,0)",
              "rgba(255,127,80)",
              "rgba(205,92,92)",
              "rgba(240,128,128)",
              "rgba(233,150,122)",
              "rgba(250,128,114)",
              "rgba(255,160,122)",
              "rgba(255,69,0)",
              "rgba(255,140,0)",
              "rgba(184,134,11)",
              "rgba(218,165,32)",
              "rgba(189,183,107)",
              "rgba(240,230,140)",
              "rgba(128,128,0)",
              "rgba(255,255,0)",
              "rgba(154,205,50)",
              "rgba(85,107,47)",
              "rgba(107,142,35)",
              "rgba(124,252,0)",
              "rgba(127,255,0)",
            /*  rgb(173,255,47)rgb(0,100,0)rgb(0,128,0)
             forest green	#228B22	(34,139,34)
             lime	#00FF00	(0,255,0)
             lime green	#32CD32	(50,205,50)
             light green	#90EE90	(144,238,144)
             pale green	#98FB98	(152,251,152)
             dark sea green	#8FBC8F	(143,188,143)
             medium spring green	#00FA9A	(0,250,154)
             spring green	#00FF7F	(0,255,127)
             sea green	#2E8B57	(46,139,87)
             medium aqua marine	#66CDAA	(102,205,170)
             medium sea green	#3CB371	(60,179,113)
             light sea green	#20B2AA	(32,178,170)
             dark slate gray	#2F4F4F	(47,79,79)
             teal	#008080	(0,128,128)
             dark cyan	#008B8B	(0,139,139)
             aqua	#00FFFF	(0,255,255)
             cyan	#00FFFF	(0,255,255)
             light cyan	#E0FFFF	(224,255,255)
             dark turquoise	#00CED1	(0,206,209)
             turquoise	#40E0D0	(64,224,208)
             medium turquoise	#48D1CC	(72,209,204)
             pale turquoise	#AFEEEE	(175,238,238)
             aqua marine	#7FFFD4	(127,255,212)
             powder blue	#B0E0E6	(176,224,230)
             cadet blue	#5F9EA0	(95,158,160)
             steel blue	#4682B4	(70,130,180)
             corn flower blue	#6495ED	(100,149,237)
             deep sky blue	#00BFFF	(0,191,255)
             dodger blue	#1E90FF	(30,144,255)
             light blue	#ADD8E6	(173,216,230)
             sky blue	#87CEEB	(135,206,235)
             light sky blue	#87CEFA	(135,206,250)
             midnight blue	#191970	(25,25,112)
             navy	#000080	(0,0,128)
             dark blue	#00008B	(0,0,139)
             medium blue	#0000CD	(0,0,205)
             blue	#0000FF	(0,0,255)
             royal blue	#4169E1	(65,105,225)
             blue violet	#8A2BE2	(138,43,226)
             indigo	#4B0082	(75,0,130)
             dark slate blue	#483D8B	(72,61,139)
             slate blue	#6A5ACD	(106,90,205)
             medium slate blue	#7B68EE	(123,104,238)
             medium purple	#9370DB	(147,112,219)
             dark magenta	#8B008B	(139,0,139)
             dark violet	#9400D3	(148,0,211)
             dark orchid	#9932CC	(153,50,204)
             medium orchid	#BA55D3	(186,85,211)
             purple	#800080	(128,0,128)
             thistle	#D8BFD8	(216,191,216)
             plum	#DDA0DD	(221,160,221)
             violet	#EE82EE	(238,130,238)
             magenta / fuchsia	#FF00FF	(255,0,255)
             orchid	#DA70D6	(218,112,214)
             medium violet red	#C71585	(199,21,133)
             pale violet red	#DB7093	(219,112,147)
             deep pink	#FF1493	(255,20,147)
             hot pink	#FF69B4	(255,105,180)
             light pink	#FFB6C1	(255,182,193)
             pink	#FFC0CB	(255,192,203)
             antique white	#FAEBD7	(250,235,215)
             beige	#F5F5DC	(245,245,220)
             bisque	#FFE4C4	(255,228,196)
             blanched almond	#FFEBCD	(255,235,205)
             wheat	#F5DEB3	(245,222,179)
             corn silk	#FFF8DC	(255,248,220)
             lemon chiffon	#FFFACD	(255,250,205)
             light golden rod yellow	#FAFAD2	(250,250,210)
             light yellow	#FFFFE0	(255,255,224)
             saddle brown	#8B4513	(139,69,19)
             sienna	#A0522D	(160,82,45)
             chocolate	#D2691E	(210,105,30)
             peru	#CD853F	(205,133,63)
             sandy brown	#F4A460	(244,164,96)
             burly wood	#DEB887	(222,184,135)
             tan	#D2B48C	(210,180,140)
             rosy brown	#BC8F8F	(188,143,143)
             moccasin	#FFE4B5	(255,228,181)
             navajo white	#FFDEAD	(255,222,173)
             peach puff	#FFDAB9	(255,218,185)
             misty rose	#FFE4E1	(255,228,225)
             lavender blush	#FFF0F5	(255,240,245)
             linen	#FAF0E6	(250,240,230)
             old lace	#FDF5E6	(253,245,230)
             papaya whip	#FFEFD5	(255,239,213)
             sea shell	#FFF5EE	(255,245,238)
             mint cream	#F5FFFA	(245,255,250)
             slate gray	#708090	(112,128,144)
             light slate gray	#778899	(119,136,153)*/
                    ];
                    let z:number=0;
                    let e:number=0;
                    let color: string[]=[];
                    this.sectorStat = [];
                    this.sectorListString.forEach(secteur=>{
                      let i:number=0;              
                      z=z+Math.random();
                      activitiesList.forEach(agentActivity=>{
                        try {
                          if(agentActivity.documentId.siteId.districtId.sectorId.sectorName==secteur){
                            z=z+2;
                            i=i+1;
                          }
                          z=z+8;
                        } catch (error) {
                        }                        
                      });
                      this.sectorListactivities.push(i);
                      let a: number = Math.random();
                      this.sectorStat.push({
                        name:secteur,
                        color:colors[e],
                        activities:i
                      });
                      color.push(colors[e]);
                      e++;
                      
                    });
            
            const dataDoughnut = {
              labels: this.sectorListString,
              datasets: [{
                label: '',
                data: this.sectorListactivities,
                backgroundColor: color,
                hoverOffset: 4
              }]
            };
            const typeDoughnut ="doughnut";
            this.chart(typeDoughnut,dataDoughnut,"mychart");
            this.isLoadingSector = false;
          }
        },
        error => {
          this.isLoading = false;
          this.loadingMarkerservice.hide();
        //  this.alertService.error('Erreur:');
        }
      ); 
      this.apiClientService.getAllSitesWithoutConditions()
      .subscribe(
        response => {
          const errorObject: any = response.body;
          this.checkErrorService.checkRemoteData(errorObject);
          this.isLoading = false;
          this.loadingMarkerservice.hide();
          const sitList: Array<Site> = response.body;
          if (sitList != null && !sitList.length) {
            //this.alertService.error('Erreur: 1');
          } else {
            let siteoke: number=0;
            let siteinterupe: number=0;
            let sitedestroye: number=0;
            let sitesuivie: number=0;
            for(let site in sitList){
              switch(sitList[site].status){
                case 1: siteoke=siteoke+1;
                  break;
                case 0: sitesuivie=sitesuivie+1;
                  break;
                case 3: sitedestroye=sitedestroye+1;
                  break;
                case 4: siteinterupe=siteinterupe+1;
                  break;
                default:
                  break;
              }
            }
            siteoke=siteoke;
            siteinterupe=siteinterupe;
            sitedestroye=sitedestroye;
            sitesuivie=sitesuivie;
            const dataPie = {
              labels: [
                'site detruit ',
                'site suivie',
                'site interrompus',      
                'site ok'
              ],
              datasets: [{
                label: '',
                data: [sitedestroye, sitesuivie, siteinterupe, siteoke],
                backgroundColor: [
                  'rgba(255, 99, 132)',
                  'rgba(54, 162, 235)',
                  'rgba(255, 206, 86)',
                  'rgba(75, 192, 192)'
                ],
                hoverOffset: 4
              }]
            };
            const typePie = 'pie';
            this.chart(typePie,dataPie,"mychartsite");
            this.isLoadingSite = false;   
          }
        },
        error => {
          this.isLoading = false;
          this.loadingMarkerservice.hide();
        //  this.alertService.error('Erreur:');
        }
      );
            }
            
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
          //  this.alertService.error('Erreur:');
          }
        );
  }

  chart(typeDoughnut: any, dataDoughnut:any, name:string ){
    new Chart(name, 
              {
                type: typeDoughnut,
                data: dataDoughnut,
              }
            ); 
  }



  initFormObject() {
    this.notification = {
      notifMessage: null,
      status: null,
      notificationPK: {
        agentId: null,
        employeeId: null,
        notifDate: null
      }
    }
  }
  

  buildAgentOccurenceWork (): AgentOccurenceWork{
    return {
      agent: {
        agentId: null,
        agentName: null,
        agentLogin: null,
        agentPassword: null,
        isFirstConnection: null,
        status: null,
        agentPhoneNumber: null,
        lastConnectionDate: null,
        token: null,
        agentDeviceList: null,
        agentSectorList: null
      },
      numberOfActions: null,           
    };
  }

  //Liste des meilleurs agents
  mostWorkerAgents (activitiesList: Array<AgentActivity>): Array<AgentOccurenceWork>{
    if (activitiesList.length == 0) {
      return;
    }
    else {
      let agentOccurenceWOrkList : Array<AgentOccurenceWork> = new Array;
      for (let i = 0; i < activitiesList.length; i++) {
        const element = activitiesList[i];
        if (agentOccurenceWOrkList == null || agentOccurenceWOrkList.length == 0) {
          let agentWithOccurenceToAdd:  AgentOccurenceWork = this.buildAgentOccurenceWork();
          agentWithOccurenceToAdd.agent = element.agentId;
          agentWithOccurenceToAdd.numberOfActions = 1;
          agentOccurenceWOrkList[0] = agentWithOccurenceToAdd;
        }
        else {
          // Vérifions si l'agent courant n'existe pas déjà dans la liste à retourner
          let isAgentAlreadyAdded = false;
          let existingAgentPosition = -1; //Position de l'agent existant dans la liste à retourner
          for (let j = 0; j < agentOccurenceWOrkList.length; j++) {
            if (element.agentId.agentId == agentOccurenceWOrkList[j].agent.agentId) {
              isAgentAlreadyAdded = true;
              existingAgentPosition = j;
            }
          }
          if (isAgentAlreadyAdded) {
            agentOccurenceWOrkList[existingAgentPosition].numberOfActions = agentOccurenceWOrkList[existingAgentPosition].numberOfActions + 1;
          }
          else {
            //(avant) Si l'agent courant n'existe pas encore dans la liste à retourner, on compte son nombre d'occurrence et on l'ajoute dans la liste à retourner
            /*let nberOfOccurence = 1; // On initialise à 1 parce qu'on est sûr qu'il est là au moins 1e fois
            for (let k = i+1; k < activitiesList.length; k++) {
              if (element.agentId.agentId == activitiesList[k].agentId.agentId) {
                nberOfOccurence = nberOfOccurence+1;
                //activitiesList.splice(k,1);
              }
            }*/
            // (nouveau) Si l'agent n'existe pas encore, on l'initialise juste dans la liste avec son nombre d'occurrence =1
            let agentWithOccurenceToAdd:  AgentOccurenceWork = this.buildAgentOccurenceWork();
            agentWithOccurenceToAdd.agent = element.agentId;
            //agentWithOccurenceToAdd.numberOfActions = nberOfOccurence;
            agentWithOccurenceToAdd.numberOfActions = 1;
            agentOccurenceWOrkList.push(agentWithOccurenceToAdd);
          }
        }   
      }
      let listToReturn : Array<AgentOccurenceWork> = new Array;
      listToReturn = this.sortAgentActivities(agentOccurenceWOrkList);
      //On retourne toute la liste si la taille est inférieure à 3, dans le cas contraire, on retourne juste les 3 premiers éléments
      if (listToReturn.length < 3){
        //Taille du tableau recensant les agents les moins actifs
        const size = this.worstWorkerAgentsList.length;
        if (size == 0){
          this.worstWorkerAgentsList[0] = listToReturn[listToReturn.length-1];
        }
        if (size == 1){
          this.worstWorkerAgentsList[0] = listToReturn[listToReturn.length-1];
        }
        if (size == 2){
          this.worstWorkerAgentsList[0] = listToReturn[listToReturn.length-1];
        }
        return listToReturn
      }
      else {
        let listToReturn1 : Array<AgentOccurenceWork> = new Array;
        listToReturn1[0] = listToReturn[0];
        listToReturn1[1] = listToReturn[1];
        listToReturn1[2] = listToReturn[2];
        //Taille du tableau recensant les agents les moins actifs
        const size = this.worstWorkerAgentsList.length;
        if (size == 0){
          this.worstWorkerAgentsList[0] = listToReturn[listToReturn.length-1];
          this.worstWorkerAgentsList[1] = listToReturn[listToReturn.length-2];
          this.worstWorkerAgentsList[2] = listToReturn[listToReturn.length-3];
        }
        if (size == 1){
          this.worstWorkerAgentsList[1] = listToReturn[listToReturn.length-2];
          this.worstWorkerAgentsList[2] = listToReturn[listToReturn.length-3];
        }
        if (size == 2){
          this.worstWorkerAgentsList[2] = listToReturn[listToReturn.length-3];
        }
        return listToReturn1;
        //return listToReturn.splice(3, listToReturn.length-1)
      }
    }
  }

  //Liste des pires agents
  worstWorkerAgents (activitiesList: Array<AgentActivity>) {
    let worstWorkers: Array<AgentOccurenceWork> = new Array;
    this.apiClientService.getAllAgents()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const agList: Array<Agent> = resp.body;
            if (agList != null && !agList.length) {
            //  this.alertService.error('Impossible d\'obtenir la liste d\'agents');
            } else {
              for (let i = 0; i < agList.length; i++) {
                const element = agList[i];
                let isAgentWorking = 0;
                for (let j = 0; j < activitiesList.length; j++) {
                  if (element.agentId == activitiesList[j].agentId.agentId) {
                    isAgentWorking = isAgentWorking+1;
                  }
                }
                if (isAgentWorking == 0) {
                  let agentWithOccurenceToAdd:  AgentOccurenceWork = this.buildAgentOccurenceWork();
                  agentWithOccurenceToAdd.agent = element;
                  agentWithOccurenceToAdd.numberOfActions = 0;
                  worstWorkers.push(agentWithOccurenceToAdd);
                }
              }
              const principalListSize = this.worstWorkerAgentsList.length;
              const worstWorkersListSize = worstWorkers.length;
              if (worstWorkersListSize >= 3) {
                this.worstWorkerAgentsList[0] = worstWorkers[0];
                this.worstWorkerAgentsList[1] = worstWorkers[1];
                this.worstWorkerAgentsList[2] = worstWorkers[2];
              }
              else if (worstWorkersListSize == 2) {
                if (principalListSize >= 1) {
                  let temp:  AgentOccurenceWork = this.buildAgentOccurenceWork();
                  temp = this.worstWorkerAgentsList[0];
                  this.worstWorkerAgentsList[0] = worstWorkers[0];
                  this.worstWorkerAgentsList[1] = worstWorkers[1];
                  this.worstWorkerAgentsList[2] = temp;
                }
                else {
                  this.worstWorkerAgentsList[0] = worstWorkers[0];
                  this.worstWorkerAgentsList[1] = worstWorkers[1];
                }
              }
              else if (worstWorkersListSize == 1) {
                if (principalListSize == 0) {
                  this.worstWorkerAgentsList[0] = worstWorkers[0];
                }
                else if (principalListSize == 1) {
                  let temp:  AgentOccurenceWork = this.buildAgentOccurenceWork();
                  temp = this.worstWorkerAgentsList[0];
                  this.worstWorkerAgentsList[0] = worstWorkers[0];
                  this.worstWorkerAgentsList[1] = temp;
                }
                else if  (principalListSize >= 2) {
                  let temp:  AgentOccurenceWork = this.buildAgentOccurenceWork();
                  temp = this.worstWorkerAgentsList[0];
                  let temp1:  AgentOccurenceWork = this.buildAgentOccurenceWork();
                  temp1 = this.worstWorkerAgentsList[1];
                  this.worstWorkerAgentsList[0] = worstWorkers[0];
                  this.worstWorkerAgentsList[1] = temp;
                  this.worstWorkerAgentsList[2] = temp1;
                }
              }
            }
            //La mise à jour de cette variable se fait ici parce que c'est cette fonction qui a la version la plus à jour des agents les plus actifs.
            this.worstWorkerAgentsDataSource = new MatTableDataSource<AgentOccurenceWork>(this.worstWorkerAgentsList);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
          //  this.alertService.error('Erreur:');
          }
        );
  }


  sortAgentActivities(agentOccurenceWOrkList : Array<AgentOccurenceWork>): Array<AgentOccurenceWork> {
      //Trier la liste d'agents par ordre, du plus travailleurs au moins travailleurs, en fonction du nombre d'occurrence dans la liste agentOccurenceWOrkList
      //Le tri est fait par odre décroissant, du plus grand nombre d'occurrences au plus petit
      for (let i = 0; i < agentOccurenceWOrkList.length; i++) {
        for (let j = i+1; j < agentOccurenceWOrkList.length; j++) {
          if (agentOccurenceWOrkList[i].numberOfActions < agentOccurenceWOrkList[j].numberOfActions) {
            let tempAgent:  AgentOccurenceWork;
            tempAgent = agentOccurenceWOrkList[i];
            agentOccurenceWOrkList[i] = agentOccurenceWOrkList[j];
            agentOccurenceWOrkList[j] = tempAgent;
          }
        }
      }
      return agentOccurenceWOrkList;
  }

  isAgentActivitiesListEmpty() {
    return this.agentActivitiesList == null || this.agentActivitiesList.length === 0;
  }
  getformatDater(chaine:string):string{
    return this.actionOnModelsService.formatDate(chaine);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.agentActivitiesDataSource.filter = filterValue;
  }
  openSite(site: Site) {
    this.localDaoService.save('site_to_show', site);
    this.router.navigate(['details-of-site']);
  }

  /*createNewSiteActivity(): SiteActivity{
    return {
      siteActivityId: null,
      opDate: null,
      lastUpdate: null,
      description: null,
      status: null,
      siteId: {
        siteId: null,
        siteName: null,
        refSite: null,
        ownerName: null,
        ownerPhone: null,
        ownerCni: null,
        description: null,
        status: null,
        districtId: {
          districtId:null,
          sectorId: null,
          districtName: null,
          commune: null,
          standing:null,
          population:null,
          densite: null,
          description: null,
          status: null,
          siteList: null,
        },
       documentsList: null,
       gpsCoordinatesList: null
      }          
    };
  }*/

  

  

}
