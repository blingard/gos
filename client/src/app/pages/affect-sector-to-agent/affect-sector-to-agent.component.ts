import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService, ApiClientService, LocalDaoService } from 'src/app/_services/exports';
import { Sector, Agent, AgentSector, GlobalEmployee } from '../../_generated/exports';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-affect-sector-to-agent',
  templateUrl: './affect-sector-to-agent.component.html',
  styleUrls: ['./affect-sector-to-agent.component.css']
})
export class AffectSectorToAgentComponent implements OnInit {

  affectForm : FormGroup;

  currentEmployee: GlobalEmployee;
  agent : Agent;
  sectors: Sector[];
  agtSector: AgentSector;
  todayDate: Date;
  dateToday: string;
  isSubmit: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,) { }

  ngOnInit() {
    if(this.localDaoService.exists("agent_to_affect_sector")){
      this.agent = this.localDaoService.getAgent("agent_to_affect_sector");
    }else{
      this.alertService.error("Aucun agent traité");
    }

    //////////////// get Sectors
   /* this.apiClientService.getNonAffectedSectorsToAgent(this.agent).subscribe(
      (response)=>{
        const sectorList: Array<Sector> = response.body;
        if (sectorList != null && !sectorList.length) {
          this.alertService.error('Impossible d\'obtenir la liste des Secteurs');
        } else {
          this.sectors = sectorList;
        }
      },
      (error)=>{
        const errorObj: any = error;
        this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Impossible d\'obtenir la liste des Secteurs, l\'erreur est: ' + error);
      }
    );*/
    this.apiClientService.getAllSectors().subscribe(
      (response)=>{
        const sectorList: Array<Sector> = response.body;
        if (sectorList != null && !sectorList.length) {
          this.alertService.error('Impossible d\'obtenir la liste des Secteurs');
        } else {
          this.sectors = sectorList;
        }
      },
      (error)=>{
        const errorObj: any = error;
        this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Impossible d\'obtenir la liste des Secteurs, l\'erreur est: ' + error);
      }
    );
    this.initForm();
  }

  initForm(){
    this.affectForm = this.fb.group({
      agent : [{value: this.agent.agentName + ' ' + this.agent.agentLogin, disabled: true}, Validators.required],
      sector: ['', Validators.required]
    });
    this.agtSector = {
      agent : null,
      sector : null,
      endDate: null,
      agentSectorPK : null
    }
  }

  onSubmit(){
    this.isSubmit = true;
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    this.agtSector.sector = this.affectForm.value.sector;
    this.agtSector.agent = this.agent;
    this.agtSector.agentSectorPK = {
      agentId: this.agent.agentId,
      sectorId: this.affectForm.value.sector.sectorId,
      beginDate: null
    }
    this.agtSector.agentSectorPK.beginDate= this.dateToday;
    this.apiClientService.affectSectorToAgent(this.agtSector).subscribe(
      (response)=>{
        if(response.body !=null){
          this.router.navigate(["sector-agent"])
          this.alertService.success("Secteur assigné avec succèes");
        }
        
    this.isSubmit = false;
      },
      (error)=>{
        const errorObj: any = error;
        this.alertService.error(errorObj.error.errorText);
        this.isSubmit = false;
      }
    )

  }

}
