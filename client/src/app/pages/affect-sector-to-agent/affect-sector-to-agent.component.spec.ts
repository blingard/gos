import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectSectorToAgentComponent } from './affect-sector-to-agent.component';

describe('AffectSectorToAgentComponent', () => {
  let component: AffectSectorToAgentComponent;
  let fixture: ComponentFixture<AffectSectorToAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectSectorToAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectSectorToAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
