import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {Alerts, AlertsSystems, GlobalEmployee, GpsCoordinates, Site} from '../../_generated/exports';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service'
import { ModalAlertComponent } from '../exports';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-list-alerts',
  templateUrl: './list-alerts.component.html',
  styleUrls: ['./list-alerts.component.css']
})
export class ListAlertsComponent implements OnInit, OnDestroy {


  

  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty = false;
  notifEmpty=false;
  alEmpty=false;
  pageAlarmSystem = 1;
  pageSizeAlarmSystem = 10;
  collectionSizeAlarmSystem = 0;  
  isEmptyAlarmSystem = false;
  currentEmployee: GlobalEmployee;
  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  selection: SelectionModel<Alerts>;
  alertsList: Array<Alerts>;
  alertsListSort: Array<Alerts>;
  notifications: Array<AlertsSystems>;
  notificationsSort: Array<AlertsSystems>;
  notificationsSubscription = new Subscription();
  constructor( private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private modalService : BsModalService,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService,
    private translate: TranslateService,
  ) { }
  ngOnDestroy() {
    this.notificationsSubscription.unsubscribe();
  }
  onGetLocation(s: Site){
    this.apiClientService.getAllCoordinatesSite(s)
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            const pointList: Array<GpsCoordinates> = resp.body;
            if (pointList != null && !pointList.length) {
              this.alertService.error('Impossible d\'obtenir la liste des points');
            } else {
              s.gpsCoordinatesList=pointList;
              this.apiClientService.s=s;
              this.localDaoService.save('site_to_show', s);
            }
            
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Impossible d\'obtenir la liste des points');
          }
        );
  }
  openDialog(notification: AlertsSystems) {
    let msg = null;
    this.translate.get("ALSYSTEM").subscribe((data:String)=> {
      msg=data;
    });
    this.localDaoService.save('site_to_show', notification.agentActivitiesConvocation.documentId.siteId);
    const dialogRef = this.modalService.show(ModalAlertComponent,{
      initialState: {
        title: msg+": "+notification.agentActivitiesConvocation.documentId.documentName,
        data: notification,
        type:true
      },

    });
  }

  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.alertsList=this.alertsListSort;
    this.collectionSize=this.alertsList.length;
    if(values.length==0){
     this.pageSize=10;
     this.isEmpty=false;
     this.alertsList=this.alertsListSort;
     this.collectionSize=this.alertsList.length;
    }else{
      let tab: Alerts[]=[];
      this.pageSize=5;
      let a = this.alertsList.filter(alert => {
        if(alert.alertsDistrict.toLowerCase().includes(values.toLowerCase()) || alert.personName.toLowerCase().includes(values.toLowerCase()) || alert.alertsDate.toLowerCase().includes(values.toLowerCase())){
          tab.push(alert);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.alertsList=this.alertsListSort;
       this.collectionSize=this.alertsList.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.alertsList=tab;
       this.collectionSize=this.alertsList.length;
      }
      
    }     
  }

  onFilterAlarmSystem(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.notifications=this.notificationsSort;
    this.collectionSizeAlarmSystem=this.notifications.length;
    if(values.length==0){
     this.pageSizeAlarmSystem=10;
     this.isEmptyAlarmSystem=false;
     this.notifications=this.notificationsSort;
     this.collectionSize=this.notifications.length;
    }else{
      let tab: AlertsSystems[]=[];
      this.pageSize=5;
      let a = this.notifications.filter(notification => {
        if(notification.agentActivitiesConvocation.documentId.siteId.ownerName.toLowerCase().includes(values.toLowerCase()) || notification.agentActivitiesConvocation.documentId.siteId.siteName.toLowerCase().includes(values.toLowerCase())){
          tab.push(notification);
        }
      });
      if(tab.length==0){
       this.isEmptyAlarmSystem=true;
       this.notifications=this.notificationsSort;
       this.collectionSizeAlarmSystem=this.notifications.length;
       this.pageSizeAlarmSystem=10;
      }
      else{
       this.isEmptyAlarmSystem=false;
       this.notifications=tab;
       this.collectionSizeAlarmSystem=this.notifications.length;
      }
      
    }     
  }

  ngOnInit() {
    this.isLoading = true;
    this.alertsList= [];
    this.notifications=[];
    this.notifEmpty = false;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.loadingMarkerservice.show();
    this.apiClientService.getAllActivateAlerts()
      .subscribe(
        resp => {
          if(resp.status==200){
            const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          this.isLoading = false;
          this.loadingMarkerservice.hide();
          const alList: Array<Alerts> = resp.body;
          if (alList != null && !alList.length) {
            this.alEmpty = false;
          } else {
            this.alertsList = alList;
            this.alertsListSort = this.alertsList;
            this.collectionSize = this.alertsListSort.length;
            this.alEmpty = true;
          }
            }
          
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
      );

    this.apiClientService.getAllSystemAlert().subscribe(
      response => {
        if(response.status == 200){
/**
 * const activitiesList: Array<AlertsSystems> = response.body;
          activitiesList.forEach(al=>{
            if(al.agentActivitiesConvocation.status==0){
              this.notifications.push(al);
            }
          });
          this.notificationsSort= this.notifications;
          this.collectionSizeAlarmSystem = this.notificationsSort.length;
          if(this.notifications.length == 0){
            this.notifEmpty=false;
          }else{
            this.notifEmpty=true;
          }
 */


          this.notifications = response.body;
          this.notificationsSort= this.notifications;
          this.collectionSizeAlarmSystem = this.notificationsSort.length;
          this.notifEmpty=true;
        }else{
          this.notifications=[];
          this.notificationsSort= this.notifications;
          this.collectionSizeAlarmSystem = 0;
          this.notifEmpty = false;

        }
        },
        error => {


        }
      );
    }
  ngAfterViewInit() {
  

  }
  isAlertsListEmpty() {
    if(this.alertsList==null && this.notifications==null)
      return true;
    else
      return false;
  }
  isAlertsDeleted(alert: Alerts) {
    return this.actionOnModelsService.isAlertsDeleted(alert);
  }
  canViewDetails() {
    return this.selection.selected.length !== 0;
  }
  detailsOfSelectedAlert() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins une alerte');
    } else {
      const al: Alerts = this.selection.selected[0];
      this.localDaoService.save('alerts_to_show', al);
      this.router.navigate(['details-of-alerts']);
    }
  }
  canDeleteAlerts() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isAlertsDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
 
  clearSelection() {
    this.selection.clear();
  }
  toggleSelection(row: Alerts) {
    if (this.selection.isSelected(row)) {
      this.selection.clear();
    } else {
      this.selection.clear();
      this.selection.select(row);
    }
  }
  refresh() {
    this.ngOnInit();
  }
  deleteAlert(alert: Alerts) {
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){ 
      this.updateAlertsStatus(alert, 2, 1);
    }
  }
  updateAlertsStatus(alert: Alerts, status: number, showMessage: number = 0) {
    this.currentEmployee= this.localDaoService.getGlobalEmployee('currentEmployee');
    const lastStatus: number = alert.status;
    alert.status = status;
    this.apiClientService.updateAlerts(alert, this.currentEmployee)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const al: Alerts = resp.body;
          alert.status = al.status;
          if (showMessage === 1) {
            this.alertService.success('alerte mis à jour avec succès.');
          }
          if (showMessage === 2 && !this.updateErrors) {
            this.alertService.success('Les alertes ont été mis à jour avec succès.');
          }
          if (showMessage === 2) {
            this.refresh();
          }
          this.apiClientService.getAllActivateAlerts().subscribe(
            (data)=>{
              const errorObject: any = data.body;
              this.checkErrorService.checkRemoteData(errorObject);
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              const alList: Array<Alerts> = data.body;
              if (alList != null && !alList.length) {
                this.alertService.error('Impossible d\'obtenir la liste des alertes');
              } else {
                this.alertsList = alList;
                this.alertsListSort = this.alertsList;
                this.collectionSize = this.alertsListSort.length;
              }
            },
            (err)=>{
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              this.alertService.error('Impossible d\'obtenir la liste des alertes');
            }
          )
          

        },
        error => {
          this.updateErrors = true;
            alert.status = lastStatus;
          this.alertService.error('Echec lors de la mise à jour de ce alerte');
        }
      );
    this.refresh();
  }
  displayDetails(alerts:Alerts){
    this.localDaoService.save('alerts_to_show', alerts);
    let msg = null;
    this.translate.get("ALCITOYEN").subscribe((data:String)=> {
      msg=data;
    });
    
    const dialogRef = this.modalService.show(ModalAlertComponent,{
      initialState: {
        title: msg+": "+alerts.personName,
        data: alerts,
        type:false
      }
    });
  }
  onSubmitNotif(notification: AlertsSystems){
    this.apiClientService.setSystemAlert(notification).subscribe(
      resp =>{
        if(resp.status==200){
          this.refresh();
        }
      }, 
      error =>{
      }
    );
  }
  getColor(notification: AlertsSystems): string{
    if(notification.days<=4){
      return 'white';
    }else{
      return 'red';
    }

  }
  getdate(notification: AlertsSystems): boolean{
    if(notification.days<=4){
      return true;
    }else{
      return false;
    }

  }
}

