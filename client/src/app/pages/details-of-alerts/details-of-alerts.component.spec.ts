import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsOfAlertsComponent } from './details-of-alerts.component';

describe('DetailsOfAlertsComponent', () => {
  let component: DetailsOfAlertsComponent;
  let fixture: ComponentFixture<DetailsOfAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsOfAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsOfAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
