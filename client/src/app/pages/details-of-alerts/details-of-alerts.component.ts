import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Alerts,AlertsDocuments} from '../../_generated/exports';

import { AuthenticationService, AlertService, ApiClientService, LocalDaoService,
         ActionOnModelsService,CheckErrorService,LoadingMarkerService } from '../../_services/exports';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ViewDocComponent } from '../../pages/exports';

@Component({
  selector: 'app-details-of-alerts',
  templateUrl: './details-of-alerts.component.html',
  styleUrls: ['./details-of-alerts.component.css']
})
export class DetailsOfAlertsComponent implements OnInit {
  data: any;
  params: any;
  alerts: Alerts;
  documentsList: Array<AlertsDocuments>;
  isLoading = true;
  updateErrors = false;
  displayedColumns: Array<String>;
  modalRef: BsModalRef;
  

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService,
    private modalService : BsModalService,
  ) { }

  ngOnInit() {

    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.alerts = this.localDaoService.getAlerts('alerts_to_show');
    if (this.alerts == null) {
      this.alertService.error('Veuillez choisir une alerte pour afficher ses détails', true);
      this.router.navigate(['alerts']);
    }

    this.apiClientService.getAllDocumentsforAlerts(this.alerts)
    .subscribe(
      resp => {
        const errorObject: any = resp.body;
        this.checkErrorService.checkRemoteData(errorObject);
        this.isLoading = false;
        this.loadingMarkerservice.hide();
        const docsList: Array<AlertsDocuments> = resp.body;
        if (docsList != null && !docsList.length) {
          this.alertService.error('Erreur:');
        } else {
          this.documentsList = docsList;
        }
      },
      error => {
        this.isLoading = false;
        this.loadingMarkerservice.hide();
        this.alertService.error('Erreur:');
      }
    ); 
  }

  
  getAlerts(){
    this.router.navigate(['alerts']);
  }
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  }
  getformatDater(chaine:string):string{
    return this.actionOnModelsService.formatDate(chaine);
  }
  downloadDocument(doc:AlertsDocuments){
    this.isLoading = true;
    this.loadingMarkerservice.show();
    const extension = doc.documentExtension;
    this.apiClientService.downloadFile(doc.documentPath, extension );
  }
  openModal(doc: AlertsDocuments){
    this.modalRef = this.modalService.show(ViewDocComponent,{
      initialState: {
        title: doc.docType+": "+doc.documentName,
        data: doc
      }
    });
  
   }
  onDelete(doc:AlertsDocuments){

  }


}
