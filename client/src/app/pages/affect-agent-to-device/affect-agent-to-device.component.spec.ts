import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectAgentToDeviceComponent } from './affect-agent-to-device.component';

describe('AffectAgentToDeviceComponent', () => {
  let component: AffectAgentToDeviceComponent;
  let fixture: ComponentFixture<AffectAgentToDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectAgentToDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectAgentToDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
