import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService, ApiClientService, LocalDaoService } from 'src/app/_services/exports';
import { Device, Agent, AgentDevice, GlobalEmployee} from '../../_generated/exports';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-affect-agent-to-device',
  templateUrl: './affect-agent-to-device.component.html',
  styleUrls: ['./affect-agent-to-device.component.css']
})
export class AffectAgentToDeviceComponent implements OnInit {

  affectForm : FormGroup;

  agent : Agent;
  devices: Device[];
  agtDevice: AgentDevice;
  todayDate: Date;
  dateToday: string;
  isSubmit: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService) { }

  ngOnInit() {
    if(this.localDaoService.exists("agent_to_affect")){
      this.agent = this.localDaoService.getAgent("agent_to_affect");
    }else{
      this.alertService.error("Aucun agent traité");
    }

    /*if(this.localDaoService.exists("agentDevice_to_edit")){

      let data: any = this.localDaoService.getAgent("agentDevice_to_edit");
      this.agent = data.agent;
    }else{
      //this.alertService.error("Aucun agent traité ");
    }*/

    //////////////// get Devices
    this.apiClientService.getAllFreeDevices().subscribe(
      (response)=>{
        const datas : any = response.body;
        if(datas != null){
          this.devices = datas;
        }
        else{
          this.alertService.error("Aucun périphérique disponible");
        }
      }
    ) 
    this.initForm();
  }

  initForm(){
    this.affectForm = this.fb.group({
      agent : [{value: this.agent.agentName + ' ' + this.agent.agentLogin, disabled: true}, Validators.required],
      device: ['', Validators.required]
    });
    this.agtDevice = {
      agent : null,
      device : null,
      endDate: null,
      agentDevicePK : null
    }
  }

  onSubmit(){
    this.isSubmit = true;
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    this.agtDevice.device = this.affectForm.value.device;
    this.agtDevice.agent = this.agent;
    this.agtDevice.agentDevicePK = {
      agentId: this.agent.agentId,
      deviceId: this.affectForm.value.device.deviceId,
      beginDate: null
    }
    this.agtDevice.agentDevicePK.beginDate=this.dateToday;
    this.apiClientService.affectDeviceToAgent(this.agtDevice).subscribe(
      (response)=>{
        if(response.body !=null){
          const errorObj: any = response.body;
          this.localDaoService.save('agent_to_display_devices',this.agtDevice.agent);
          this.router.navigate(["device"])
          //this.alertService.error(errorObj.error.errorText);
          this.alertService.success("Périphérique assigné avec succèes");
        }
        this.isSubmit = false;
      },
      (error)=>{
        const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        this.alertService.error("Impossible d'assigner ce périphérique" + error.body);
        this.isSubmit = false;
      }
    )

  }

}
