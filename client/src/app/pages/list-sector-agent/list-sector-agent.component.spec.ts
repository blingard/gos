import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSectorAgentComponent } from './list-sector-agent.component';

describe('ListSectorAgentComponent', () => {
  let component: ListSectorAgentComponent;
  let fixture: ComponentFixture<ListSectorAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSectorAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSectorAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
