import { Agent } from './../../_generated/models/agent.model';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {Site, GpsCoordinates, AgentSector, GlobalEmployee} from '../../_generated/exports';

@Component({
  selector: 'app-list-sector-agent',
  templateUrl: './list-sector-agent.component.html',
  styleUrls: ['./list-sector-agent.component.css']
})
export class ListSectorAgentComponent implements OnInit {
  
  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty = false;
  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  agentSectorsList: Array<AgentSector>;
  currentEmployee: GlobalEmployee;
  agentSectors:Array<AgentSector>;
  todayDate: Date;
  dateToday: string;

  constructor( private route: ActivatedRoute,
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService,
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.agentSectorsList= null;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.loadingMarkerservice.show();
    const agent : Agent = this.localDaoService.getAgent('agent_to_show');
    this.apiClientService.getAgentsSectorsByAgent(agent)
      .subscribe(
          resp => {
            const errorObject: any = resp.body; 
            this.checkErrorService.checkRemoteData(errorObject);
            const agSList: Array<AgentSector> = resp.body;
            if (agSList != null && !agSList.length) {
              //this.alertService.error('Erreur:');
            } else {
              this.agentSectorsList = agSList;
              this.agentSectors = this.agentSectorsList;
              this.collectionSize=this.agentSectorsList.length;
            }
            this.isLoading = false;
            this.loadingMarkerservice.hide();
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            //this.alertService.error('Erreur:');
          }
    ); 
  }

  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.agentSectors=this.agentSectorsList;
    this.collectionSize=this.agentSectors.length;
    if(values.length==0){
     this.pageSize=10;
     this.isEmpty=false;
     this.agentSectors=this.agentSectorsList;
     this.collectionSize=this.agentSectors.length;
    }else{
      let tab: AgentSector[]=[];
      this.pageSize=5;
      let a = this.agentSectors.filter(agentSector => {
        if(agentSector.sector.sectorName.toLowerCase().includes(values.toLowerCase())){
          tab.push(agentSector);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.agentSectors=this.agentSectorsList;
       this.collectionSize=this.agentSectors.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.agentSectors=tab;
       this.collectionSize=this.agentSectors.length;
      }
      
    }     
  }


  formatDate17Characters(chaine:string):string{
    return this.actionOnModelsService.formatDate17Characters(chaine);
  }

  isAgentSectorListEmpty() {
    return this.agentSectors == null || this.agentSectors.length == 0;
  }

 



  refresh() {
    this.isLoading = true;
    this.agentSectorsList = null;
    this.loadingMarkerservice.show();
    const agent : Agent = this.localDaoService.getAgent('agent_to_show');
    this.apiClientService.getAgentsSectorsByAgent(agent)
    .subscribe(
      resp => {
        const errorObject: any = resp.body;
        this.checkErrorService.checkRemoteData(errorObject);
        this.isLoading = false;
        this.loadingMarkerservice.hide();
        const agSList: Array<AgentSector> = resp.body;
        if (agSList != null && !agSList.length) {
          //this.alertService.error('Erreur:');
        } else {
          this.agentSectorsList = agSList;
          this.agentSectors = this.agentSectorsList;
          this.collectionSize=this.agentSectorsList.length;
        }
      },
      error => {
        this.isLoading = false;
        this.loadingMarkerservice.hide();
        this.alertService.error('Erreur:');
        //const errorObj: any = error;
        //this.alertService.error(errorObj.error.errorText);
      }
    ); 
  }

  
  isAgentSectorDeleted(agSector: AgentSector) {
    return this.actionOnModelsService.isAgentSectorDeleted(agSector);
  }
  deleteAgentSector(agSector: AgentSector) { 
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){
      this.updateAgentSectorEndDate(agSector);
    }
    this.router.navigate(['agent']);
  }
  updateAgentSectorEndDate(agSector: AgentSector) { 
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    agSector.endDate=this.dateToday;
    this.apiClientService.updateAgentSectorWhereEndDate(agSector)
    .subscribe(
      resp => {
        const agSector: AgentSector = resp.body;
        agSector.endDate = agSector.endDate;
          this.alertService.success('affection supprimer avec succès.');
          this.apiClientService.getAllAgentDevice().subscribe((res)=> {
            const data: AgentSector[] = res.body
            if(data != null){
              this.agentSectors=data;
            }
          },
          (error=>{
          })
          );
      },
      error => {
        const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Echec lors de la suppression');
      }
    );
  }
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  } 
  getformatDater(chaine:string):string{
    return this.actionOnModelsService.formatDate(chaine);
  }  

  detailsOfAgent(agent: Agent) {
    this.localDaoService.save('agent_to_show', agent);
    this.router.navigate(['details-of-agent']);
  }

  editAgentSector(agentSector: AgentSector) {
    this.localDaoService.save('agent_sector_to_edit', agentSector);
    this.router.navigate(['edit-agent'], {relativeTo: this.route});
  }

}
