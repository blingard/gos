import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsOfDeviceComponent } from './details-of-device.component';

describe('DetailsOfDeviceComponent', () => {
  let component: DetailsOfDeviceComponent;
  let fixture: ComponentFixture<DetailsOfDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsOfDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsOfDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
