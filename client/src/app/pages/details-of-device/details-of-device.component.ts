import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthenticationService, AlertService, ApiClientService, LocalDaoService,
         ActionOnModelsService } from '../../_services/exports';
import { Device} from '../../_generated/exports';

@Component({
  selector: 'app-details-of-device',
  templateUrl: './details-of-device.component.html',
  styleUrls: ['./details-of-device.component.css']
})
export class DetailsOfDeviceComponent implements OnInit {

  data: any;
  params: any;
  device: Device;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.device = this.localDaoService.getDevice('device_to_show', true);
    if (this.device == null) {
      this.alertService.error('Veuillez choisir un périphérique pour afficher ses détails', true);
      this.router.navigate(['device']);

  }
}
getformatDater(chaine:string):string{
  return this.actionOnModelsService.formatDate(chaine);
}  

}
