import { Component, AfterViewInit, OnInit } from '@angular/core';
import { ApiClientService, LoadingMarkerService } from './../../_services/exports';
import { TranslateService } from '@ngx-translate/core';
import { Coordonee, Quartier, PlanEau, Jardin, Foret, ReseauAssainissament,Marecages, Documents, Zonesinondables, PlanZonnage } from './../../_generated/exports';
import * as L from 'leaflet';
import * as S from 'leaflet-spin';
import * as turf from '@turf/turf/';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit, OnInit {
  private map;
  private centroid;
  max: number = 21;
  min: number = 16;
  name: String="...";
  private owner:String="...";
  private ownerPhone: String="...";
  private ownerCni: String="...";
  private fillOpacity: number = 0.3;
  private doc : Documents;

  constructor(private api: ApiClientService,public translate: TranslateService) {
  }
  ngOnInit(){    
    this.name = this.api.s.siteName;
    this.owner = this.api.s.ownerName;
    this.ownerPhone = this.api.s.ownerPhone;
    this.ownerCni = this.api.s.ownerCni;
    let doc : Documents={
      documentId: 0,
      documentName: null,
      documentCode: null,
      documentPath: "assets/images/img/hero-slider/1.jpg",
      secretKey: null,
      documentExtension: null,
      createdDate: null,
      status: 0,
      docTypeId: undefined,
      siteId: undefined
    };
    for(let aa in this.api.s.documentsList){
      if(this.api.s.documentsList[aa].docTypeId.docTypeName=="IMAGE"){
        doc = this.api.s.documentsList[aa];
      }  
    }
    if(doc.documentId==0){      
      this.doc = doc;
    }else{
      doc.documentPath = this.api.fileDownload+doc.documentPath;
      this.doc = doc;
    }
  }
  ngAfterViewInit(): void {
    S;
   this.centroid = L.LatLngExpression = [this.api.s.gpsCoordinatesList[0].ycoord,this.api.s.gpsCoordinatesList[0].xcoord];
   this.map=L.map('map', {
      center: this.centroid,
      zoom: 16
    });
    const oms = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      minZoom: this.min,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    const googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
      maxZoom: this.max,
      minZoom: this.min,
      subdomains:['mt0','mt1','mt2','mt3']
    });    
    googleHybrid.addTo(this.map);    
    var baseMaps = {
      "Satelite": googleHybrid,
      "Street": oms,
    };
    var overlayMaps = { 
    };
    L.control.layers(baseMaps, overlayMaps).addTo(this.map);   
    this.map.spin(true);
    this.posLegend();
    this.posLegendZone();
    this.drawAll();      
    this.map.spin(false);

  } 
  private posLegendZone(){    
    var legende = L.control({position: 'bottomleft'});
    legende.onAdd = function () {
      var div = L.DomUtil.create('div','legende'),
      grades = [
        {
          style: 'color: #dfaf2c;',
          icon: 'fa fa-square fax3',
          name:'ZONE ADMINISTRATIVE (Zone H1)',
        },
        {
          style: 'color: tan;',
          icon: 'fa fa-square fax3',
          name:'ZONE NATURELLE NON AEDIFICANDI (Zone H2)',
        },
        {
          style: 'color: silver;',
          icon: 'fa fa-square fax3',
          name:'ZONE NATURELLE NON AEDIFICANDI  (Zone H3)',
        },
        {
          style: 'color: orange;',
          icon: 'fa fa-square fax3',
          name:'ZONE VERTE (Zone H4)',
        },
        {
          style: 'color: crimson;',
          icon: 'fa fa-square fax3',
          name:'ZONE D\'EXTENSION URBAINE (Zone H6)',
        },
        {
          style: 'color: purple;',
          icon: 'fa fa-square fax3',
          name:'ZONE INDUSTRIELLE (Zone H7)',
        },
        {
          style: 'color: brown;',
          icon: 'fa fa-square fax3',
          name:'ZONE NATURELLE NON AEDIFICANDI (Zone H8)',
        }
      ],
      labels = [];
      div.setAttribute('style',' padding: 6px 8px;font: 14px/16px Arial, Helvetica, sans-serif;background: white;background: rgba(255,255,255,0.8);box-shadow: 0 0 15px rgba(0,0,0,0.2);border-radius: 5px;');
      for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<h6><i class="'+grades[i].icon+'" style="'+grades[i].style+'"></i> '+grades[i].name+'</h6>';
      }
      return div;
    };
    legende.addTo(this.map);
  }
  private posLegend(){    
    var legend = L.control({position: 'bottomleft'});
    legend.onAdd = function () {
      var div = L.DomUtil.create('div','legend'),
      grades = [
        {
          style: 'color: lime;',
          icon: 'fa fa-square fax3',
          name:'Forets urbaines et Jardin publique',
        },
        {
          style: 'color: red;',
          icon: 'fa fa-minus fax3',
          name:'Reseaux d\'assainnissement',
        },
        {
          style: 'color: white;',
          icon: 'fa fa-square fax3',
          name:'Plan d\'eau',
        },
        {
          style: 'color: yellow;',
          icon: 'fa fa-square fax3',
          name:'Zones Innondables',
        },
        {
          style: 'color: black;',
          icon: 'fa fa-square fax3',
          name:'Marécage',
        },
        {
          style: 'color: blue;',
          icon: 'fa fa-square fax3',
          name:'Site / Chantier',
        }
      ];
      div.setAttribute('style','visibility: visible; padding: 6px 8px;font: 14px/16px Arial, Helvetica, sans-serif;background: white;background: rgba(255,255,255,0.8);box-shadow: 0 0 15px rgba(0,0,0,0.2);border-radius: 5px;');
      for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<h5><i class="'+grades[i].icon+'" style="'+grades[i].style+'"></i> '+grades[i].name+'</h5>';
      }
      return div;
    };
    legend.addTo(this.map);
  }
  private drawSiteOrChantier(){
    let siteName: String;
    let ownerName: String;
    let ownerPhone: String;
    let ownerCni: String;
    this.translate.get("SITENAME").subscribe((data:String)=> {
      siteName=data;
    });
    this.translate.get("OWNERNAME").subscribe((data:String)=> {
      ownerName=data;
    });
    this.translate.get("OWNERPHONE").subscribe((data:String)=> {
      ownerPhone=data;
    });
    this.translate.get("CNI").subscribe((data:String)=> {
      ownerCni=data;
    });
    let siteCoord=[];
    for(let l in this.api.s.gpsCoordinatesList){
      const gps = this.api.s.gpsCoordinatesList[parseInt(l)];
      const coord = [gps.ycoord,gps.xcoord];
      siteCoord.push(coord);
    }  
    var center = turf.centerOfMass(turf.polygon([siteCoord]));
    let doc: Documents = this.doc;
    var info = L.control({position: 'bottomright'});
        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info');
            this.update();
            return this._div;
        };
        info.update = function (props: any) {
          if(props){
            this._div.setAttribute('style',' visibility: visible;padding: 6px 8px;font: 14px/16px Arial, Helvetica, sans-serif;max-width:75%;width: auto; min-width:45%; background: white;background: rgb(255,255,255);box-shadow: 0 0 15px rgba(0,0,0,0.2);border-radius: 5px;');
            this._div.innerHTML = props.data;
          }else{
            this._div.setAttribute('style','visibility: hidden;');
          }
        };
        info.addTo(this.map);  
    let site = L.polygon([siteCoord],{
      fillColor:'blue',
      color:'blue',
      weight: 2,
      opacity: 1.0,
      dashArray: '3',
      fillOpacity: 0.9
    });
    site.on('mouseover', function (e) {
      site.setStyle({
          weight: 5,
          color: 'blue',
          dashArray: '',
          fillOpacity: 0.9
      });
    });
    site.on('mouseout', function (e) {info.update();
      site.setStyle({
        color:'blue',
        fillColor:'blue',
        weight: 2,
        opacity: 0.9,
        dashArray: '3',
        fillOpacity: 0.9
      });
    });
    site.addTo(this.map);
    var redIcon = new L.Icon({
      iconUrl: 'assets/images/marker-icon-2x-red.png',
      shadowUrl: 'assets/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
    var littleton = L.marker(center.geometry.coordinates, {icon: redIcon}).bindPopup('<img src="'+doc.documentPath+'" alt="'+this.owner+'"  width="100%" height="150px">'+
    siteName+ ': '+ this.name +' </br> '+ownerName+': <b>'+ this.owner + '</b> </br>'+ownerPhone+': <b>' + this.ownerPhone + '</b> </br>'+ownerCni+': <b>' + this.ownerCni + '</b>').openPopup();
    littleton.addTo(this.map);
  }
  private  drawAll(){    
    var info = L.control({ position: 'bottomright' });
    info.onAdd = function (map) {
      this._div = L.DomUtil.create('div', 'info');
      this.update();
      return this._div;
    };
    info.update = function (props: any) {
      if (props) {
        this._div.setAttribute('style', ' visibility: visible;padding: 6px 8px;font: 14px/16px Arial, Helvetica, sans-serif;max-width:75%;width: auto; min-width:45%; background: white;background: rgba(255,255,255,0.7);box-shadow: 0 0 15px rgba(0,0,0,0.2);border-radius: 5px;');
        this._div.innerHTML = props.data;
      } else {
        this._div.setAttribute('style', 'visibility: hidden;');
      }
    };
    info.addTo(this.map);
    this.map.spin(true);  
    this.api.getAllQuartier().subscribe(
      respQuartier => {
        let zonesInnondablesLayer = null;
        let planseauxLayer = null;
        let foretLayer = null;
        let jardinLayer = null;
        let reseauAssainsLayer = null;
        let quartierWithZoneList: any = respQuartier.body;
        for(let a in quartierWithZoneList){
          let quartierResponse = quartierWithZoneList[a].quartier;
          let planZonnageResponse = quartierWithZoneList[a].planZonnageList;
          let planZonnagesInfos: Array<PlanZonnage> = [];
          for (let z in planZonnageResponse) {
            const donneeplanZonnage: PlanZonnage = {
              ces: planZonnageResponse[z].ces,
              autoriser: planZonnageResponse[z].autoriser,
              cos: planZonnageResponse[z].cos,
              estEncour: planZonnageResponse[z].estEncour,
              hauteurMa: planZonnageResponse[z].hauteurMa,
              interdicti: planZonnageResponse[z].interdicti,
              largeurVo: planZonnageResponse[z].largeurVo,
              profondeur: planZonnageResponse[z].profondeur,
              reculImpo: planZonnageResponse[z].reculImpo,
              reculImpo12: planZonnageResponse[z].reculImpo12,
              restrictio: planZonnageResponse[z].restrictio,
              reculIm: planZonnageResponse[z].reculIm,
              stationAu: planZonnageResponse[z].stationAu,
              stationEt: planZonnageResponse[z].stationEt,
              stationHa: planZonnageResponse[z].stationHa,
              surfaceMa: planZonnageResponse[z].surfaceMa,
              vocation: planZonnageResponse[z].vocation,
              zone_habia: planZonnageResponse[z].zone_habia,
              data: planZonnageResponse[z].geom,
              coordonnee: this.returnCoordonnePolygon(planZonnageResponse[z].geom.coordinates[0][0]),
            };
            planZonnagesInfos.push(donneeplanZonnage);
          }          
          let coordonateDistrict: Array<any> = this.returnCoordonnePolygon(quartierResponse.geom.coordinates[0][0]);
          let coordonateDistrictZone: Array<any>;
          var polyZone;
          var polyDistrict;
          var intersection;
          let areaList : number[]=[];  
          for (let t in planZonnagesInfos) {
            var area = turf.polygon([planZonnagesInfos[t].coordonnee]);
            areaList.push(turf.area(area));
          }
          areaList.sort((a, b) => {
            if (a > b)
                return -1;
            if (a < b)
                return 1;
            return 0;
          });
          for (let i in areaList) {
            for (let t in planZonnagesInfos) {            
              var area = turf.polygon([planZonnagesInfos[t].coordonnee]);
              if(areaList[i]==turf.area(area)){
                polyZone = turf.polygon([planZonnagesInfos[t].coordonnee]);
                polyDistrict = turf.polygon([coordonateDistrict]);
                intersection = turf.intersect(polyZone, polyDistrict); 
                coordonateDistrictZone = intersection.geometry.coordinates;
                if (planZonnagesInfos[t].zone_habia == "H1") {
                  let planZonnagePolyLayer = L.polygon([coordonateDistrictZone], {
                    color: 'white',
                    fillColor: '#dfaf2c',
                    weight: 2,
                    opacity: 0.9,
                    dashArray: '3',
                    fillOpacity: this.fillOpacity
                  });
                  planZonnagePolyLayer.on('mouseover', function (e) {
                    let quartierInfos: Quartier = {
                      name: quartierResponse.name,
                      commune: quartierResponse.commune,
                      standing: quartierResponse.standing,
                      data: [],
                      coordonnee: []
                    };
                    let data: any = {
                      "data": "<div><span><h4><b>Quartier</b>: " + quartierInfos.name + "</h4></span></div>"+
                          "<div><span><b>Commune :</b>" + quartierInfos.commune + "</span></div>"+                    
                          "<div><span><b>Standing :</b> " + quartierInfos.standing + "</span></div>"+
                          "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>",
                    };
                    info.update(data);
                  });
                  planZonnagePolyLayer.on('mouseout', function (e) {
                    info.update();
                  });
                  planZonnagePolyLayer.bindPopup(
                    "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>"+
                    "<div><span><b>Autorisations :</b>" + planZonnagesInfos[t].autoriser + "</span></div>"+                    
                    "<div><span><b>Profondeur minimale des lots :</b> " + planZonnagesInfos[t].profondeur + "</span></div>"+
                    "<div><span><b>Interdictions :</b> " + planZonnagesInfos[t].interdicti + "</span></div>"+
                    "<div><span><b>Restrictions :</b> " + planZonnagesInfos[t].restrictio + "</span></div>"+
                    "<div><span><b>Superficie minimale du terrain :</b> " + planZonnagesInfos[t].surfaceMa + "</span></div>"+
                    "<div><span><b>COS :</b> " + planZonnagesInfos[t].cos + "</span></div>"+
                    "<div><span><b>CES :</b> " + planZonnagesInfos[t].ces + "</span></div>"+                                         
                    "<div><span><b>Recul à partir des bornes de la voie public :</b> " + planZonnagesInfos[t].reculImpo + "</span></div>"+
                    "<div><span><b>Distance entre les construction :</b> " + planZonnagesInfos[t].reculIm + "</span></div>"+
                    "<div><span><b>Hauteur Maximale :</b> " + planZonnagesInfos[t].hauteurMa + "</span></div>"+
                    "<div><span><b>Unite d'habitation :</b> " + planZonnagesInfos[t].stationHa + "</span></div>"+
                    "<div><span><b>Etablissement recevant du public :</b> " + planZonnagesInfos[t].stationEt + "</span></div>"+
                    "<div><span><b>Autres Types de construction :</b> " + planZonnagesInfos[t].stationAu + "</span></div>"
                  );
                  planZonnagePolyLayer.addTo(this.map);
                } else if (planZonnagesInfos[t].zone_habia == "H2") {
                  let planZonnagePolyLayer = L.polygon([coordonateDistrictZone], {
                    color: 'white',
                    fillColor: 'tan',
                    weight: 2,
                    opacity: 0.9,
                    dashArray: '3',
                    fillOpacity: this.fillOpacity
                  });             
                  planZonnagePolyLayer.on('mouseover', function (e) {
                    let quartierInfos: Quartier = {
                      name: quartierResponse.name,
                      commune: quartierResponse.commune,
                      standing: quartierResponse.standing,
                      data: [],
                      coordonnee: []
                    };
                    let data: any = {
                      "data": "<div><span><h4><b>Quartier</b>: " + quartierInfos.name + "</h4></span></div>"+
                          "<div><span><b>Commune :</b>" + quartierInfos.commune + "</span></div>"+                    
                          "<div><span><b>Standing :</b> " + quartierInfos.standing + "</span></div>"+
                          "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>",
                    };
                    info.update(data);
                  });
                  planZonnagePolyLayer.on('mouseout', function (e) {
                    info.update();
                  });
                  planZonnagePolyLayer.bindPopup(
                    "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>"+
                    "<div><span><b>Autorisations :</b>" + planZonnagesInfos[t].autoriser + "</span></div>"+                    
                    "<div><span><b>Profondeur minimale des lots :</b> " + planZonnagesInfos[t].profondeur + "</span></div>"+
                    "<div><span><b>Interdictions :</b> " + planZonnagesInfos[t].interdicti + "</span></div>"+
                    "<div><span><b>Restrictions :</b> " + planZonnagesInfos[t].restrictio + "</span></div>"+
                    "<div><span><b>Superficie minimale du terrain :</b> " + planZonnagesInfos[t].surfaceMa + "</span></div>"+
                    "<div><span><b>COS :</b> " + planZonnagesInfos[t].cos + "</span></div>"+
                    "<div><span><b>CES :</b> " + planZonnagesInfos[t].ces + "</span></div>"+                                         
                    "<div><span><b>Recul à partir des bornes de la voie public :</b> " + planZonnagesInfos[t].reculImpo + "</span></div>"+
                    "<div><span><b>Distance entre les construction :</b> " + planZonnagesInfos[t].reculIm + "</span></div>"+
                    "<div><span><b>Hauteur Maximale :</b> " + planZonnagesInfos[t].hauteurMa + "</span></div>"+
                    "<div><span><b>Unite d'habitation :</b> " + planZonnagesInfos[t].stationHa + "</span></div>"+
                    "<div><span><b>Etablissement recevant du public :</b> " + planZonnagesInfos[t].stationEt + "</span></div>"+
                    "<div><span><b>Autres Types de construction :</b> " + planZonnagesInfos[t].stationAu + "</span></div>"
                  );
                  planZonnagePolyLayer.addTo(this.map);
                } else if (planZonnagesInfos[t].zone_habia == "H3") {
                  let planZonnagePolyLayer = L.polygon([coordonateDistrictZone], {
                    color: 'white',
                    fillColor: 'silver',
                    weight: 2,
                    opacity: 0.9,
                    dashArray: '3',
                    fillOpacity: this.fillOpacity
                  });
                  planZonnagePolyLayer.on('mouseover', function (e) {
                    let quartierInfos: Quartier = {
                      name: quartierResponse.name,
                      commune: quartierResponse.commune,
                      standing: quartierResponse.standing,
                      data: [],
                      coordonnee: []
                    };
                    let data: any = {
                      "data": "<div><span><h4><b>Quartier</b>: " + quartierInfos.name + "</h4></span></div>"+
                          "<div><span><b>Commune :</b>" + quartierInfos.commune + "</span></div>"+                    
                          "<div><span><b>Standing :</b> " + quartierInfos.standing + "</span></div>"+
                          "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>",
                    };
                    info.update(data);
                  });
                  planZonnagePolyLayer.on('mouseout', function (e) {
                    info.update();
                  });
                  planZonnagePolyLayer.bindPopup(
                    "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>"+
                    "<div><span><b>Autorisations :</b>" + planZonnagesInfos[t].autoriser + "</span></div>"+                    
                    "<div><span><b>Profondeur minimale des lots :</b> " + planZonnagesInfos[t].profondeur + "</span></div>"+
                    "<div><span><b>Interdictions :</b> " + planZonnagesInfos[t].interdicti + "</span></div>"+
                    "<div><span><b>Restrictions :</b> " + planZonnagesInfos[t].restrictio + "</span></div>"+
                    "<div><span><b>Superficie minimale du terrain :</b> " + planZonnagesInfos[t].surfaceMa + "</span></div>"+
                    "<div><span><b>COS :</b> " + planZonnagesInfos[t].cos + "</span></div>"+
                    "<div><span><b>CES :</b> " + planZonnagesInfos[t].ces + "</span></div>"+                                         
                    "<div><span><b>Recul à partir des bornes de la voie public :</b> " + planZonnagesInfos[t].reculImpo + "</span></div>"+
                    "<div><span><b>Distance entre les construction :</b> " + planZonnagesInfos[t].reculIm + "</span></div>"+
                    "<div><span><b>Hauteur Maximale :</b> " + planZonnagesInfos[t].hauteurMa + "</span></div>"+
                    "<div><span><b>Unite d'habitation :</b> " + planZonnagesInfos[t].stationHa + "</span></div>"+
                    "<div><span><b>Etablissement recevant du public :</b> " + planZonnagesInfos[t].stationEt + "</span></div>"+
                    "<div><span><b>Autres Types de construction :</b> " + planZonnagesInfos[t].stationAu + "</span></div>"
                  );
                  planZonnagePolyLayer.addTo(this.map);
                } else if (planZonnagesInfos[t].zone_habia == "H4") {
                  let planZonnagePolyLayer = L.polygon([coordonateDistrictZone], {
                    color: 'white',
                    fillColor: 'orange',
                    weight: 2,
                    opacity: 0.9,
                    dashArray: '3',
                    fillOpacity: this.fillOpacity
                  });
                  planZonnagePolyLayer.on('mouseover', function (e) {
                    let quartierInfos: Quartier = {
                      name: quartierResponse.name,
                      commune: quartierResponse.commune,
                      standing: quartierResponse.standing,
                      data: [],
                      coordonnee: []
                    };
                    let data: any = {
                      "data": "<div><span><h4><b>Quartier</b>: " + quartierInfos.name + "</h4></span></div>"+
                          "<div><span><b>Commune :</b>" + quartierInfos.commune + "</span></div>"+                    
                          "<div><span><b>Standing :</b> " + quartierInfos.standing + "</span></div>"+
                          "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>",
                    };
                    info.update(data);
                  });
                  planZonnagePolyLayer.on('mouseout', function (e) {
                    info.update();
                  });
                  planZonnagePolyLayer.bindPopup(
                    "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>"+
                    "<div><span><b>Autorisations :</b>" + planZonnagesInfos[t].autoriser + "</span></div>"+                    
                    "<div><span><b>Profondeur minimale des lots :</b> " + planZonnagesInfos[t].profondeur + "</span></div>"+
                    "<div><span><b>Interdictions :</b> " + planZonnagesInfos[t].interdicti + "</span></div>"+
                    "<div><span><b>Restrictions :</b> " + planZonnagesInfos[t].restrictio + "</span></div>"+
                    "<div><span><b>Superficie minimale du terrain :</b> " + planZonnagesInfos[t].surfaceMa + "</span></div>"+
                    "<div><span><b>COS :</b> " + planZonnagesInfos[t].cos + "</span></div>"+
                    "<div><span><b>CES :</b> " + planZonnagesInfos[t].ces + "</span></div>"+                                         
                    "<div><span><b>Recul à partir des bornes de la voie public :</b> " + planZonnagesInfos[t].reculImpo + "</span></div>"+
                    "<div><span><b>Distance entre les construction :</b> " + planZonnagesInfos[t].reculIm + "</span></div>"+
                    "<div><span><b>Hauteur Maximale :</b> " + planZonnagesInfos[t].hauteurMa + "</span></div>"+
                    "<div><span><b>Unite d'habitation :</b> " + planZonnagesInfos[t].stationHa + "</span></div>"+
                    "<div><span><b>Etablissement recevant du public :</b> " + planZonnagesInfos[t].stationEt + "</span></div>"+
                    "<div><span><b>Autres Types de construction :</b> " + planZonnagesInfos[t].stationAu + "</span></div>"
                  );
                  planZonnagePolyLayer.addTo(this.map);
                } else if (planZonnagesInfos[t].zone_habia == "H6") {
                  let planZonnagePolyLayer = L.polygon([coordonateDistrictZone], {
                    color: 'white',
                    fillColor: 'crimson',
                    weight: 2,
                    opacity: 0.9,
                    dashArray: '3',
                    fillOpacity: this.fillOpacity
                  });         
                  planZonnagePolyLayer.on('mouseover', function (e) {
                    let quartierInfos: Quartier = {
                      name: quartierResponse.name,
                      commune: quartierResponse.commune,
                      standing: quartierResponse.standing,
                      data: [],
                      coordonnee: []
                    };
                    let data: any = {
                      "data": "<div><span><h4><b>Quartier</b>: " + quartierInfos.name + "</h4></span></div>"+
                          "<div><span><b>Commune :</b>" + quartierInfos.commune + "</span></div>"+                    
                          "<div><span><b>Standing :</b> " + quartierInfos.standing + "</span></div>"+
                          "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>",
                    };
                    info.update(data);
                  });
                  planZonnagePolyLayer.on('mouseout', function (e) {
                    info.update();
                  });
                  planZonnagePolyLayer.bindPopup(
                    "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>"+
                    "<div><span><b>Autorisations :</b>" + planZonnagesInfos[t].autoriser + "</span></div>"+                    
                    "<div><span><b>Profondeur minimale des lots :</b> " + planZonnagesInfos[t].profondeur + "</span></div>"+
                    "<div><span><b>Interdictions :</b> " + planZonnagesInfos[t].interdicti + "</span></div>"+
                    "<div><span><b>Restrictions :</b> " + planZonnagesInfos[t].restrictio + "</span></div>"+
                    "<div><span><b>Superficie minimale du terrain :</b> " + planZonnagesInfos[t].surfaceMa + "</span></div>"+
                    "<div><span><b>COS :</b> " + planZonnagesInfos[t].cos + "</span></div>"+
                    "<div><span><b>CES :</b> " + planZonnagesInfos[t].ces + "</span></div>"+                                         
                    "<div><span><b>Recul à partir des bornes de la voie public :</b> " + planZonnagesInfos[t].reculImpo + "</span></div>"+
                    "<div><span><b>Distance entre les construction :</b> " + planZonnagesInfos[t].reculIm + "</span></div>"+
                    "<div><span><b>Hauteur Maximale :</b> " + planZonnagesInfos[t].hauteurMa + "</span></div>"+
                    "<div><span><b>Unite d'habitation :</b> " + planZonnagesInfos[t].stationHa + "</span></div>"+
                    "<div><span><b>Etablissement recevant du public :</b> " + planZonnagesInfos[t].stationEt + "</span></div>"+
                    "<div><span><b>Autres Types de construction :</b> " + planZonnagesInfos[t].stationAu + "</span></div>"
                  );
                  planZonnagePolyLayer.addTo(this.map);
                } else if (planZonnagesInfos[t].zone_habia == "H7") {
                  let planZonnagePolyLayer = L.polygon([coordonateDistrictZone], {
                    color: 'white',
                    fillColor: 'purple',
                    weight: 2,
                    opacity: 0.9,
                    dashArray: '3',
                    fillOpacity: this.fillOpacity
                  });
                  planZonnagePolyLayer.on('mouseover', function (e) {
                    let quartierInfos: Quartier = {
                      name: quartierResponse.name,
                      commune: quartierResponse.commune,
                      standing: quartierResponse.standing,
                      data: [],
                      coordonnee: []
                    };
                    let data: any = {
                      "data": "<div><span><h4><b>Quartier</b>: " + quartierInfos.name + "</h4></span></div>"+
                          "<div><span><b>Commune :</b>" + quartierInfos.commune + "</span></div>"+                    
                          "<div><span><b>Standing :</b> " + quartierInfos.standing + "</span></div>"+
                          "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>",
                    };
                    info.update(data);
                  });
                  planZonnagePolyLayer.on('mouseout', function (e) {
                    info.update();
                  });
                  planZonnagePolyLayer.bindPopup(
                    "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>"+
                    "<div><span><b>Autorisations :</b>" + planZonnagesInfos[t].autoriser + "</span></div>"+                    
                    "<div><span><b>Profondeur minimale des lots :</b> " + planZonnagesInfos[t].profondeur + "</span></div>"+
                    "<div><span><b>Interdictions :</b> " + planZonnagesInfos[t].interdicti + "</span></div>"+
                    "<div><span><b>Restrictions :</b> " + planZonnagesInfos[t].restrictio + "</span></div>"+
                    "<div><span><b>Superficie minimale du terrain :</b> " + planZonnagesInfos[t].surfaceMa + "</span></div>"+
                    "<div><span><b>COS :</b> " + planZonnagesInfos[t].cos + "</span></div>"+
                    "<div><span><b>CES :</b> " + planZonnagesInfos[t].ces + "</span></div>"+                                         
                    "<div><span><b>Recul à partir des bornes de la voie public :</b> " + planZonnagesInfos[t].reculImpo + "</span></div>"+
                    "<div><span><b>Distance entre les construction :</b> " + planZonnagesInfos[t].reculIm + "</span></div>"+
                    "<div><span><b>Hauteur Maximale :</b> " + planZonnagesInfos[t].hauteurMa + "</span></div>"+
                    "<div><span><b>Unite d'habitation :</b> " + planZonnagesInfos[t].stationHa + "</span></div>"+
                    "<div><span><b>Etablissement recevant du public :</b> " + planZonnagesInfos[t].stationEt + "</span></div>"+
                    "<div><span><b>Autres Types de construction :</b> " + planZonnagesInfos[t].stationAu + "</span></div>"
                  );
                  planZonnagePolyLayer.addTo(this.map);
                } else {
                  let planZonnagePolyLayer = L.polygon([coordonateDistrictZone], {
                    color: 'white',
                    fillColor: 'brown',
                    weight: 2,
                    opacity: 0.9,
                    dashArray: '3',
                    fillOpacity: this.fillOpacity
                  });
                  planZonnagePolyLayer.on('mouseover', function (e) {
                    let quartierInfos: Quartier = {
                      name: quartierResponse.name,
                      commune: quartierResponse.commune,
                      standing: quartierResponse.standing,
                      data: [],
                      coordonnee: []
                    };
                    let data: any = {
                      "data": "<div><span><h4><b>Quartier</b>: " + quartierInfos.name + "</h4></span></div>"+
                          "<div><span><b>Commune :</b>" + quartierInfos.commune + "</span></div>"+                    
                          "<div><span><b>Standing :</b> " + quartierInfos.standing + "</span></div>"+
                          "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>",
                    };
                    info.update(data);
                  });
                  planZonnagePolyLayer.on('mouseout', function (e) {
                    info.update();
                  });
                  planZonnagePolyLayer.bindPopup(
                    "<h6><b>Zone " + planZonnagesInfos[t].zone_habia + ": " + planZonnagesInfos[t].vocation + " : </b></h6>"+
                    "<div><span><b>Autorisations :</b>" + planZonnagesInfos[t].autoriser + "</span></div>"+                    
                    "<div><span><b>Profondeur minimale des lots :</b> " + planZonnagesInfos[t].profondeur + "</span></div>"+
                    "<div><span><b>Interdictions :</b> " + planZonnagesInfos[t].interdicti + "</span></div>"+
                    "<div><span><b>Restrictions :</b> " + planZonnagesInfos[t].restrictio + "</span></div>"+
                    "<div><span><b>Superficie minimale du terrain :</b> " + planZonnagesInfos[t].surfaceMa + "</span></div>"+
                    "<div><span><b>COS :</b> " + planZonnagesInfos[t].cos + "</span></div>"+
                    "<div><span><b>CES :</b> " + planZonnagesInfos[t].ces + "</span></div>"+                                         
                    "<div><span><b>Recul à partir des bornes de la voie public :</b> " + planZonnagesInfos[t].reculImpo + "</span></div>"+
                    "<div><span><b>Distance entre les construction :</b> " + planZonnagesInfos[t].reculIm + "</span></div>"+
                    "<div><span><b>Hauteur Maximale :</b> " + planZonnagesInfos[t].hauteurMa + "</span></div>"+
                    "<div><span><b>Unite d'habitation :</b> " + planZonnagesInfos[t].stationHa + "</span></div>"+
                    "<div><span><b>Etablissement recevant du public :</b> " + planZonnagesInfos[t].stationEt + "</span></div>"+
                    "<div><span><b>Autres Types de construction :</b> " + planZonnagesInfos[t].stationAu + "</span></div>"
                  );
                  planZonnagePolyLayer.addTo(this.map);
                }
              }
            }
          }
        } 
        this.api.getAllMarecage().subscribe(
          respMarecage=>{
            let marecages = respMarecage.body;
              let marecagesInfos: Array<Marecages>=[];
              for(let z in marecages){
                const donneemarecages:Marecages={
                  vocation:marecages[z].vocation,
                  data:[],
                  coordonnee:this.returnCoordonnePolygon(marecages[z].geom.coordinates[0][0]),
                  indication:marecages[z].indication,
                }
                marecagesInfos.push(donneemarecages);          
              }
              for(let t in marecagesInfos){
                let marecage =L.polygon([marecagesInfos[t].coordonnee],{
                  fillColor:'black',
                  color:'blank',
                  weight: 2,
                  opacity: 0.3,
                  dashArray: '3',
                  fillOpacity: 0.9
                });
                marecage.on('mouseover', function () {
                  let data: any = {
                    "data": "<h1>Marecage</h1><h3>Vocation: <b>"+marecagesInfos[t].vocation+"</b></h3><h6>Indication: <b>"+ marecagesInfos[t].indication +"</b>",
                  };
                  info.update(data);
                });
                marecage.on('mouseout', function (e) {
                  info.update();
                });
                marecage.addTo(this.map);
              }
              this.api.getAllZonesInnondables().subscribe(
                respZonesInnondables => {
                  let zonesInnondables = respZonesInnondables.body;
                  let zonesInnondablesInfos: Array<Zonesinondables> = [];
                  for (let z in zonesInnondables) {
                    const donneeZonesInnondables: Zonesinondables = {
                      actionsM2: zonesInnondables[z].actionsM2,
                      bv: zonesInnondables[z].bv,
                      classeM1: zonesInnondables[z].classeM1,
                      codeM2: zonesInnondables[z].codeM2,
                      commune: zonesInnondables[z].commune,
                      data: zonesInnondables[z].data,
                      hMax: zonesInnondables[z].hMax,
                      idZone: zonesInnondables[z].idZone,
                      lieuDit: zonesInnondables[z].lieuDit,
                      notesM2: zonesInnondables[z].notesM2,
                      origine: zonesInnondables[z].origine,
                      sSbv: zonesInnondables[z].sSbv,
                      sbv: zonesInnondables[z].sbv,
                      coordonnee: this.returnCoordonnePolygon(zonesInnondables[z].geom.coordinates[0][0])
                    };
                    zonesInnondablesInfos.push(donneeZonesInnondables);
                  }
                  for (let t in zonesInnondablesInfos) {
                    zonesInnondablesLayer = L.polygon([zonesInnondablesInfos[t].coordonnee], {
                      color: 'yellow',
                      fillColor: 'yellow',
                      weight: 2,
                      opacity: 0.3,
                      dashArray: '3',
                      fillOpacity: 0.9
                    });
                    zonesInnondablesLayer.on('mouseover', function () {
                      let data: any = {
                        "data": "<h1>Zone Innondable</h1><h3>Lieu dit: <b>" + zonesInnondablesInfos[t].lieuDit + "</b></h3><h6>Commune: <b>" + zonesInnondablesInfos[t].commune + "</b></h6><h6>Origine: " + zonesInnondablesInfos[t].origine + "</h6><h6>Action: " + zonesInnondablesInfos[t].actionsM2 + "</h6>",
                      };
                      info.update(data);
                    });
                    zonesInnondablesLayer.on('mouseout', function (e) {
                      info.update();
                    });
                    zonesInnondablesLayer.addTo(this.map);
                  }
                  this.api.getAllPlanEau().subscribe(
                    respPlanEau => {
                      let plansEaux = respPlanEau.body;
                      let respPlanEauInfos: Array<PlanEau> = [];
                      for (let z in plansEaux) {
                        const donneeplansEaux: PlanEau = {
                          perimetre: plansEaux[z].perimetre,
                          surface: plansEaux[z].area,
                          coordonnee: this.returnCoordonnePolygon(plansEaux[z].geom.coordinates[0][0])
                        };
                        respPlanEauInfos.push(donneeplansEaux);
                      }
                      for (let t in respPlanEauInfos) {
                        planseauxLayer = L.polygon([respPlanEauInfos[t].coordonnee], {
                          color: 'white',
                          fillColor: 'white',
                          weight: 2,
                          opacity: 1.0,
                          dashArray: '3',
                          fillOpacity: 0.9
                        });
                        planseauxLayer.on('mouseover', function (e) {
                          let data: any = {
                            "data": "<h1>Plan d'eau</h1><h3>Perimetre: <b>" + respPlanEauInfos[t].perimetre + "</b></h3><h6>Surface: <b>" + respPlanEauInfos[t].surface + "</b></h6>",
                          };
                          info.update(data);
                        });
                        planseauxLayer.on('mouseout', function (e) {
                          info.update();
                        });
                        planseauxLayer.addTo(this.map);
                      }
                      this.api.getAllForet().subscribe(
                        respforet => {
                          let foretsUrbaines = respforet.body;
                          let foretsUrbainesInfos: Array<Foret> = [];
                          for (let z in foretsUrbaines) {
                            const donneeforet: Foret = {
                              name: foretsUrbaines[z].name,
                              data: [],
                              coordonnee: this.returnCoordonnePolygon(foretsUrbaines[z].geom.coordinates[0][0]),
                              gid: foretsUrbaines[z].gid,
                              superficie: foretsUrbaines[z].superficie,
                            };
                            foretsUrbainesInfos.push(donneeforet);
                          }
                          for (let t in foretsUrbainesInfos) {
                            foretLayer = L.polygon([foretsUrbainesInfos[t].coordonnee], {
                              fillColor: 'lime',
                              color: 'lime',
                              weight: 2,
                              opacity: 1.0,
                              dashArray: '3',
                              fillOpacity: 0.9
                            });
                            foretLayer.on('mouseover', function (e) {
                              let data: any = {
                                "data": "<h3><b>Foret Urbaine</b></h3><h6>Superficie: <b>" + foretsUrbainesInfos[t].superficie + "</b> ha</h6>",
                              };
                              info.update(data);
                            });
                            foretLayer.on('mouseout', function (e) {
                              info.update();
                            });
                            foretLayer.addTo(this.map);
                          }
                          this.api.getAllJardin().subscribe(
                            respJardin => {
                              let jardins = respJardin.body;
                              let jardinsInfos: Array<Jardin> = [];
                              for (let z in jardins) {
                                const donneejardins: Jardin = {
                                  name: jardins[z].name,
                                  coordonnee: this.returnCoordonnePolygon(jardins[z].geom.coordinates[0][0]),
                                  localisation: jardins[z].localisation,
                                  tourism: jardins[z].tourism,
                                  sport: jardins[z].sport,
                                  leisure: jardins[z].leisure,
                                };
                                jardinsInfos.push(donneejardins);
                              }
                              for (let t in jardinsInfos) {
                                jardinLayer = L.polygon([jardinsInfos[t].coordonnee], {
                                  fillColor: 'lime',
                                  color: 'lime',
                                  weight: 2,
                                  opacity: 1.0,
                                  dashArray: '3',
                                  fillOpacity: 0.9
                                });
                                jardinLayer.on('mouseover', function (e) {
                                  let data: any = {
                                    "data": "<h1>Jardin Publique</h1><h3>Name: <b>" + jardinsInfos[t].name + "</b></h3><h6>Localisation: <b>" + jardinsInfos[t].localisation + "</b></h6><h6>Sport: " + jardinsInfos[t].sport + "</h6><h6>Tourism: " + jardinsInfos[t].tourism + "</h6>",
                                  };
                                  info.update(data);
                                });
                                jardinLayer.on('mouseout', function (e) {
                                  info.update();
                                });
                                jardinLayer.addTo(this.map);
                              }
                              this.api.getAllReseauAssainnissament().subscribe(
                                respReseauAssai => {
                                  let reseauAssains = respReseauAssai.body;
                                  let reseauAssainsInfos: Array<ReseauAssainissament> = [];
                                  for (let z in reseauAssains) {
                                    const donneereseauAssains: ReseauAssainissament = {
                                      etat: reseauAssains[z].etat,
                                      hierarchie: reseauAssains[z].hierarchie,
                                      longueur: reseauAssains[z].length,
                                      name: reseauAssains[z].namerue,
                                      numero: reseauAssains[z].numrue,
                                      coordonnee: this.returnCoordonneLine(reseauAssains[z].geom.coordinates)
                                    };
                                    reseauAssainsInfos.push(donneereseauAssains);
                                  }
                                  for (let t in reseauAssainsInfos) {
                                    reseauAssainsLayer = L.polyline([reseauAssainsInfos[t].coordonnee], {
                                      color: 'red',
                                      fillOpacity: 0.9,
                                      weight: 3,
                                      opacity: 1.0,
                                    });
                                    reseauAssainsLayer.on('mouseover', function (e) {
                                      let data: any = {
                                        "data": "<h1>Reseau Assainissement</h1><h3>Name: <b>" + reseauAssainsInfos[t].name + "</b></h3><h6>Numero de rue: <b>" + reseauAssainsInfos[t].numero + "</b></h6><h6>Longueur: " + reseauAssainsInfos[t].longueur + "</h6><h6>Etat: " + reseauAssainsInfos[t].etat + "</h6>",
                                      };
                                      info.update(data);
                                    });
                                    reseauAssainsLayer.on('mouseout', function (e) {
                                      info.update();
                                    });
                                    reseauAssainsLayer.addTo(this.map);
                                  }
                                  this.drawSiteOrChantier();
                                  this.map.spin(false); 
                                },
                                error => {
                                }
                              );
                            }
                          );
                        }
                      );
                    }
                  );
                }
              ); 
          },
          error =>{
            this.api.getAllZonesInnondables().subscribe(
              respZonesInnondables => {
                let zonesInnondables = respZonesInnondables.body;
                let zonesInnondablesInfos: Array<Zonesinondables> = [];
                for (let z in zonesInnondables) {
                  const donneeZonesInnondables: Zonesinondables = {
                    actionsM2: zonesInnondables[z].actionsM2,
                    bv: zonesInnondables[z].bv,
                    classeM1: zonesInnondables[z].classeM1,
                    codeM2: zonesInnondables[z].codeM2,
                    commune: zonesInnondables[z].commune,
                    data: zonesInnondables[z].data,
                    hMax: zonesInnondables[z].hMax,
                    idZone: zonesInnondables[z].idZone,
                    lieuDit: zonesInnondables[z].lieuDit,
                    notesM2: zonesInnondables[z].notesM2,
                    origine: zonesInnondables[z].origine,
                    sSbv: zonesInnondables[z].sSbv,
                    sbv: zonesInnondables[z].sbv,
                    coordonnee: this.returnCoordonnePolygon(zonesInnondables[z].geom.coordinates[0][0])
                  };
                  zonesInnondablesInfos.push(donneeZonesInnondables);
                }
                for (let t in zonesInnondablesInfos) {
                  zonesInnondablesLayer = L.polygon([zonesInnondablesInfos[t].coordonnee], {
                    color: 'yellow',
                    fillColor: 'yellow',
                    weight: 2,
                    opacity: 0.3,
                    dashArray: '3',
                    fillOpacity: 0.9
                  });
                  zonesInnondablesLayer.on('mouseover', function () {
                    let data: any = {
                      "data": "<h1>Zone Innondable</h1><h3>Lieu dit: <b>" + zonesInnondablesInfos[t].lieuDit + "</b></h3><h6>Commune: <b>" + zonesInnondablesInfos[t].commune + "</b></h6><h6>Origine: " + zonesInnondablesInfos[t].origine + "</h6><h6>Action: " + zonesInnondablesInfos[t].actionsM2 + "</h6>",
                    };
                    info.update(data);
                  });
                  zonesInnondablesLayer.on('mouseout', function (e) {
                    info.update();
                  });
                  zonesInnondablesLayer.addTo(this.map);
                }
                this.api.getAllPlanEau().subscribe(
                  respPlanEau => {
                    let plansEaux = respPlanEau.body;
                    let respPlanEauInfos: Array<PlanEau> = [];
                    for (let z in plansEaux) {
                      const donneeplansEaux: PlanEau = {
                        perimetre: plansEaux[z].perimetre,
                        surface: plansEaux[z].area,
                        coordonnee: this.returnCoordonnePolygon(plansEaux[z].geom.coordinates[0][0])
                      };
                      respPlanEauInfos.push(donneeplansEaux);
                    }
                    for (let t in respPlanEauInfos) {
                      planseauxLayer = L.polygon([respPlanEauInfos[t].coordonnee], {
                        color: 'white',
                        fillColor: 'white',
                        weight: 2,
                        opacity: 1.0,
                        dashArray: '3',
                        fillOpacity: 0.9
                      });
                      planseauxLayer.on('mouseover', function (e) {
                        let data: any = {
                          "data": "<h1>Plan d'eau</h1><h3>Perimetre: <b>" + respPlanEauInfos[t].perimetre + "</b></h3><h6>Surface: <b>" + respPlanEauInfos[t].surface + "</b></h6>",
                        };
                        info.update(data);
                      });
                      planseauxLayer.on('mouseout', function (e) {
                        info.update();
                      });
                      planseauxLayer.addTo(this.map);
                    }
                    this.api.getAllForet().subscribe(
                      respforet => {
                        let foretsUrbaines = respforet.body;
                        let foretsUrbainesInfos: Array<Foret> = [];
                        for (let z in foretsUrbaines) {
                          const donneeforet: Foret = {
                            name: foretsUrbaines[z].name,
                            data: [],
                            coordonnee: this.returnCoordonnePolygon(foretsUrbaines[z].geom.coordinates[0][0]),
                            gid: foretsUrbaines[z].gid,
                            superficie: foretsUrbaines[z].superficie,
                          };
                          foretsUrbainesInfos.push(donneeforet);
                        }
                        for (let t in foretsUrbainesInfos) {
                          foretLayer = L.polygon([foretsUrbainesInfos[t].coordonnee], {
                            fillColor: 'lime',
                            color: 'lime',
                            weight: 2,
                            opacity: 1.0,
                            dashArray: '3',
                            fillOpacity: 0.9
                          });
                          foretLayer.on('mouseover', function (e) {
                            let data: any = {
                              "data": "<h3><b>Foret Urbaine</b></h3><h6>Superficie: <b>" + foretsUrbainesInfos[t].superficie + "</b> ha</h6>",
                            };
                            info.update(data);
                          });
                          foretLayer.on('mouseout', function (e) {
                            info.update();
                          });
                          foretLayer.addTo(this.map);
                        }
                        this.api.getAllJardin().subscribe(
                          respJardin => {
                            let jardins = respJardin.body;
                            let jardinsInfos: Array<Jardin> = [];
                            for (let z in jardins) {
                              const donneejardins: Jardin = {
                                name: jardins[z].name,
                                coordonnee: this.returnCoordonnePolygon(jardins[z].geom.coordinates[0][0]),
                                localisation: jardins[z].localisation,
                                tourism: jardins[z].tourism,
                                sport: jardins[z].sport,
                                leisure: jardins[z].leisure,
                              };
                              jardinsInfos.push(donneejardins);
                            }
                            for (let t in jardinsInfos) {
                              jardinLayer = L.polygon([jardinsInfos[t].coordonnee], {
                                fillColor: 'lime',
                                color: 'lime',
                                weight: 2,
                                opacity: 1.0,
                                dashArray: '3',
                                fillOpacity: 0.9
                              });
                              jardinLayer.on('mouseover', function (e) {
                                let data: any = {
                                  "data": "<h1>Jardin Publique</h1><h3>Name: <b>" + jardinsInfos[t].name + "</b></h3><h6>Localisation: <b>" + jardinsInfos[t].localisation + "</b></h6><h6>Sport: " + jardinsInfos[t].sport + "</h6><h6>Tourism: " + jardinsInfos[t].tourism + "</h6>",
                                };
                                info.update(data);
                              });
                              jardinLayer.on('mouseout', function (e) {
                                info.update();
                              });
                              jardinLayer.addTo(this.map);
                            }
                            this.api.getAllReseauAssainnissament().subscribe(
                              respReseauAssai => {
                                let reseauAssains = respReseauAssai.body;
                                let reseauAssainsInfos: Array<ReseauAssainissament> = [];
                                for (let z in reseauAssains) {
                                  const donneereseauAssains: ReseauAssainissament = {
                                    etat: reseauAssains[z].etat,
                                    hierarchie: reseauAssains[z].hierarchie,
                                    longueur: reseauAssains[z].length,
                                    name: reseauAssains[z].namerue,
                                    numero: reseauAssains[z].numrue,
                                    coordonnee: this.returnCoordonneLine(reseauAssains[z].geom.coordinates)
                                  };
                                  reseauAssainsInfos.push(donneereseauAssains);
                                }
                                for (let t in reseauAssainsInfos) {
                                  reseauAssainsLayer = L.polyline([reseauAssainsInfos[t].coordonnee], {
                                    color: 'red',
                                    fillOpacity: 0.9,
                                    weight: 3,
                                    opacity: 1.0,
                                  });
                                  reseauAssainsLayer.on('mouseover', function (e) {
                                    let data: any = {
                                      "data": "<h1>Reseau Assainissement</h1><h3>Name: <b>" + reseauAssainsInfos[t].name + "</b></h3><h6>Numero de rue: <b>" + reseauAssainsInfos[t].numero + "</b></h6><h6>Longueur: " + reseauAssainsInfos[t].longueur + "</h6><h6>Etat: " + reseauAssainsInfos[t].etat + "</h6>",
                                    };
                                    info.update(data);
                                  });
                                  reseauAssainsLayer.on('mouseout', function (e) {
                                    info.update();
                                  });
                                  reseauAssainsLayer.addTo(this.map);
                                }
                                this.drawSiteOrChantier();
                                this.map.spin(false); 
                              },
                              error => {
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              }
            );
          }
          
        );
          
      }
    );        
  }
  private returnCoordonneLine(teee:any):any[]{
    let geodata3:Array<any>=[];
    for(let c in teee){
      let geodata2:Array<any>=[];                     
      for(let zaa in teee[c]){
        let geodata1:any=null;
        let latitude:number=0;
        let longitude:number=0;
        let i:number=0;
        for(let zz in teee[c][zaa]){
          if(i==0){
            longitude=teee[c][zaa][zz];
          }else{
            latitude=teee[c][zaa][zz];
          }
          i++;
        }
        geodata1=[latitude,longitude];
        geodata2.push(geodata1);                        
      }
      geodata3.push(geodata2);                     
    } 
    return geodata3;
  }
  private returnCoordonnePolygon(yaounde2:any):any[]{
    let geodata2:Array<any>=[];
    let latitude:number=0;
    let longitude:number=0;
    for(let c in yaounde2){
      let i:number=0;
      for(let a in yaounde2[c]){
        if(i==0){
          longitude=yaounde2[c][a];
        }else{
          latitude=yaounde2[c][a];
        }
        i++;
      }
      geodata2.push([latitude,longitude]);
    }
    return geodata2;
  }
  private convertData(yaounde2:any):Coordonee[]{
    let geodata2:Array<Coordonee>=[];
    let coord:Coordonee={
      xcoord:0,
      ycoord:0,
    };
    for(let c in yaounde2){
      let i:number=0;
      for(let a in yaounde2[c]){
        if(i==0){
          coord.ycoord=yaounde2[c][a];
        }else{
          coord.xcoord=yaounde2[c][a];
        }
        i++;
      }
      geodata2.push(coord);
    }
    return geodata2;
  }

   

}
