import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDeviceAgentComponent } from './list-device-agent.component';

describe('ListDeviceAgentComponent', () => {
  let component: ListDeviceAgentComponent;
  let fixture: ComponentFixture<ListDeviceAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDeviceAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDeviceAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
