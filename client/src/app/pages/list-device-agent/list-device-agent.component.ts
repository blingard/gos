
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import {AlertService, ApiClientService, ActionOnModelsService,
  LocalDaoService, LoadingMarkerService, CheckErrorService
 } from '../../_services/exports';
 import {GlobalEmployee, Agent, AgentDevice, Device} from '../../_generated/exports';


@Component({
  selector: 'app-list-device-agent',
  templateUrl: './list-device-agent.component.html',
  styleUrls: ['./list-device-agent.component.css']
})
export class ListDeviceAgentComponent implements OnInit {

  isLoading = true;
  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty = false;
  agtDeviceList: Array<AgentDevice>;
  currentEmployee: GlobalEmployee;
  agentDevices: Array<AgentDevice>;
  
  displayedColumns: Array<String> = ['select', 'agent', 'device', 'beginDate', 'endDate','actions'];
  dataSource: MatTableDataSource<AgentDevice>;
  selection: SelectionModel<AgentDevice>;
  todayDate: Date;
  dateToday: string;

  agentToGetDevices: Agent;

  constructor(
              private alertService: AlertService,
              private apiClientService: ApiClientService,
              private router: Router,
              private localDaoService: LocalDaoService,
              private actionOnModelsService: ActionOnModelsService,
              private loadingMarkerservice: LoadingMarkerService,
              private checkErrorService: CheckErrorService,) { }

  ngOnInit() {
    this.initDataTable();
  }

  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();    
    this.agentDevices = this.agtDeviceList;
    this.collectionSize=this.agentDevices.length;
    if(values.length==0){
     this.pageSize=10;
     this.isEmpty=false;
     this.agentDevices=this.agtDeviceList;
     this.collectionSize=this.agentDevices.length;
    }else{
      let tab: AgentDevice[]=[];
      this.pageSize=5;
      let a = this.agentDevices.filter(agentdevic => {
        if(agentdevic.agent.agentName.toLowerCase().includes(values.toLowerCase()) || agentdevic.device.deviceName.toLowerCase().includes(values.toLowerCase()) || agentdevic.device.deviceImei.toLowerCase().includes(values.toLowerCase())){
          tab.push(agentdevic);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.agentDevices=this.agtDeviceList;
       this.collectionSize=this.agentDevices.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.agentDevices=tab;
       this.collectionSize=this.agentDevices.length;
      }
      
    }  
  }

  // init dataSource table with All AgentDevice
  initDataTable(){ 
    this.isLoading = true;
    this.agtDeviceList = null;
    this.loadingMarkerservice.show()
    this.agentToGetDevices = this.localDaoService.getAgent('agent_to_display_devices',true);
    if (this.agentToGetDevices == null) {
      this.getAllAgentDevice();
    }
    else {
      this.getAgentDevices(this.agentToGetDevices);
    }
  }


  getAllAgentDevice(){
    this.apiClientService.getAllAgentDevice().subscribe(
      data =>{
        const errorObject: any = data.body;
        this.checkErrorService.checkRemoteData(errorObject);
        this.isLoading = false;
        this.loadingMarkerservice.hide();
        const agtDevListInit: Array<AgentDevice> = data.body;
        if (agtDevListInit != null && !agtDevListInit.length) {
          this.alertService.error('Erreur:');
        } else {
          this.agtDeviceList = agtDevListInit;
          this.agentDevices = this.agtDeviceList;
          this.collectionSize = this.agtDeviceList.length;
        }
        this.dataSource = new MatTableDataSource<AgentDevice>(this.agtDeviceList);
        this.selection = new SelectionModel<AgentDevice>(true, []);        
      },
      err =>{
        this.isLoading = false;
        this.loadingMarkerservice.hide();
        this.alertService.error('Erreur:');
      }
    );
  }
 
  getAgentDevices(agentToGetDevices: Agent){
    
    this.apiClientService.getAllAgentDevicesByAgent(agentToGetDevices).subscribe(
      data =>{
        const errorObject: any = data.body;
        this.checkErrorService.checkRemoteData(errorObject);
        this.isLoading = false;
        this.loadingMarkerservice.hide();
        const agtDevListInit: Array<AgentDevice> = data.body;
        if (agtDevListInit != null && !agtDevListInit.length) {
          this.alertService.error('Impossible d\'obtenir la liste des périphériques de cet agent');
        } else {
          this.agtDeviceList = agtDevListInit;
          this.agtDeviceList = agtDevListInit;
          this.agentDevices = this.agtDeviceList;
          this.collectionSize = this.agtDeviceList.length;
        }
        this.dataSource = new MatTableDataSource<AgentDevice>(this.agtDeviceList);
        this.selection = new SelectionModel<AgentDevice>(true, []);        
      },
      err =>{
        this.isLoading = false;
        this.loadingMarkerservice.hide();
        this.alertService.error('Impossible d\'obtenir la liste des périphériques de cet agent');
      }
    );
  }

  // redirect to add sector form

  /*addSector(){
    this.router.navigate(['add-sector']);
  }*/
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  } 
  getformatDater(chaine:string):string{
    return this.actionOnModelsService.formatDate(chaine);
  }  
  isAgentSectorListEmpty() {
    return this.agtDeviceList == null || this.agtDeviceList.length === 0;
  }

  formatDate17Characters(chaine:string):string{
    return this.actionOnModelsService.formatDate17Characters(chaine);
  }

  canViewDetails() {
    return this.selection.selected.length !== 0;
  }

  canOpen() {
    return this.selection.selected.length !== 0;
  }


  toggleSelection(row: AgentDevice) {
    if (this.selection.isSelected(row)) {
      this.selection.clear();
    } else {
      this.selection.clear();
      this.selection.select(row);
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }



  editAgtDevice(sector: AgentDevice) {
    this.localDaoService.save('agentDevice_to_edit', sector);
    this.router.navigate(['/agent/affect-agent']);
  }

  deleteAgtDevice(sector: AgentDevice) {
    
  }

  isAgentDeviceDeleted(agDevice: AgentDevice) {
    return this.actionOnModelsService.isAgentDeviceDeleted(agDevice);
  }
  deleteAgentDevice(agDevice: AgentDevice) {
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v==true){ 
      this.updateAgentDeviceEndDate(agDevice);
    }
  }
  updateAgentDeviceEndDate(agDevice: AgentDevice) { 
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    agDevice.endDate=this.dateToday;
    this.apiClientService.updateAgentDeviceWhereEndDate(agDevice)
    .subscribe(
      resp => {
        const agDevice: AgentDevice = resp.body;
        agDevice.endDate = agDevice.endDate;
          this.alertService.success('supprimer avec succès.');
          this.apiClientService.getAllAgentDevice().subscribe((res)=> {
            const data: AgentDevice[] = res.body
            if(data != null){
              this.dataSource = new MatTableDataSource(data);
              this.agentDevices=data;
            }
          },
          (error=>{
          })
          );
      },
      error => {
        const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Echec lors de la mise à jour du droit');
      }
    );
  }  

  
  
}
 