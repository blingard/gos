import { MatSnackBar } from '@angular/material/';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import {AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {Device, GlobalEmployee} from '../../_generated/exports';

@Component({
  selector: 'app-add-edit-device',
  templateUrl: './add-edit-device.component.html',
  styleUrls: ['./add-edit-device.component.css']
})
export class AddEditDeviceComponent implements OnInit {

  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  deviceToSaveOrUpdate: Device;
  currentEmployee: GlobalEmployee;
  onclicked = false;

  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService
) { }

  ngOnInit() {
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    if (this.data.add) {
      this.deviceToSaveOrUpdate = this.createNewDevice();
    }
    else {
      this.isLoading = true;
      this.deviceToSaveOrUpdate = this.localDaoService.getDevice('device_to_edit', true);
      if (this.deviceToSaveOrUpdate == null) {
        this.alertService.error('Veuilez choisir un périphérique à modifier', true);
        this.router.navigate(['device']);
      } else {
        if (this.actionOnModelsService.isDeviceDeleted(this.deviceToSaveOrUpdate)) {
          this.router.navigate(['/error-500']);
        }
      }
    }
    
  }
  createNewDevice(): Device {
    return {
      deviceId: null,
      deviceName: null,
      status: null,
      deviceImei: null,
      description: null,
      agentDeviceList: null,
      createddate: null,            
    };
  }

  onSubmit(){
    if (this.data.add) {
      this.createDevice();
    } else {
      this.editDevice();
    }
    return false;
  }

  createDevice() {
    this.onclicked = true;
    this.apiClientService.createDevice(this.deviceToSaveOrUpdate)
      .subscribe(
        resp => {
          const dev: Device = resp.body;
          if (dev) {
            this.alertService.success('périphérique créé avec succès!', true);
			      this.router.navigate(['device']);
          } else {
              this.alertService.error('Echec de création');
              this.onclicked = false;
          }
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.onclicked = false;
        }
      );
    return false;
  }

  editDevice() {
    this.onclicked = true;
    this.apiClientService.updateDevice(this.deviceToSaveOrUpdate)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const dev: Device = resp.body;
          this.alertService.success('Succès lors de la modification', true);
          this.localDaoService.save('device_to_show', dev);
          this.router.navigate(['device']);
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.onclicked = false;
        }
      );
    return false;
  }

  detailsOfDevice() {
    this.localDaoService.save('device_to_show', this.deviceToSaveOrUpdate);
    this.router.navigate(['details-of-devices']);
  }

  deleteDevice() {
    this.deviceToSaveOrUpdate.status = 2;
    this.apiClientService.updateDevice(this.deviceToSaveOrUpdate)
      .subscribe(
        resp => {
          const dev: Device = resp.body;
          this.deviceToSaveOrUpdate.status = dev.status;
          this.alertService.success('Périphérique supprimé avec succès.', true);
          this.router.navigate(['device']);
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
          this.router.navigate(['device']);
        }
      );
  }

  cancel() {
    this.router.navigate(['device']);
  }



}
