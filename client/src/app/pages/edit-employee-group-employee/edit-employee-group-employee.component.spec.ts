import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEmployeeGroupEmployeeComponent } from './edit-employee-group-employee.component';

describe('EditEmployeeGroupEmployeeComponent', () => {
  let component: EditEmployeeGroupEmployeeComponent;
  let fixture: ComponentFixture<EditEmployeeGroupEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditEmployeeGroupEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEmployeeGroupEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
