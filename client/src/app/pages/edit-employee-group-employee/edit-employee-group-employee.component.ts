import { Component, OnInit } from '@angular/core';
import {AlertService, ApiClientService, ActionOnModelsService,
  LocalDaoService, LoadingMarkerService, CheckErrorService } from '../../_services/exports';
 import { Router, ActivatedRoute } from '@angular/router';
import {EmployeeGroupEmployee, GlobalEmployee, Employee, EmployeeGroup} from '../../_generated/exports';
@Component({
  selector: 'app-edit-employee-group-employee',
  templateUrl: './edit-employee-group-employee.component.html',
  styleUrls: ['./edit-employee-group-employee.component.css']
})
export class EditEmployeeGroupEmployeeComponent implements OnInit {

  currentEmployee: GlobalEmployee;
  empGroupEmpN: EmployeeGroupEmployee;
  empGroupEmp: EmployeeGroupEmployee;
  EmployeeGroupEmployees: EmployeeGroupEmployee[];
  employees: Employee[];
  employeeGroups: EmployeeGroup[];
  todayDate: Date;
  dateToday: string;
  isSubmit: boolean = false;
  constructor(
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService
  ) { }

  ngOnInit() {
    this.empGroupEmp = this.localDaoService.getEmployeeGroupEmployee('employeeGroupEmployee_to_edit');
    this.apiClientService.getAllEmployeeGroupEmployees().subscribe((res)=> {
      const data: EmployeeGroupEmployee[] = res.body
      if(data != null){
        this.EmployeeGroupEmployees=data;
      }
    },
    (error=>{
    })
    );
    this.apiClientService.getAllEmployees().subscribe((res)=> {
      const data: Employee[] = res.body
      if(data != null){
        this.employees=data;
      }
    },
    (error=>{
    })
    );

    this.apiClientService.getAllEmployeeGroups().subscribe((res)=> {
      const data: EmployeeGroup[] = res.body
      if(data != null){
         this.employeeGroups=data;
      }
    },
    (error=>{
    })
    );
  }
  onSubmiEmployeeGroupEmployee(){
    this.editEmployeeGroupEmployee();
  }
editEmployeeGroupEmployee() {
  this.isSubmit = true;
  this.todayDate = new Date();
  this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
  this.empGroupEmpN = this.localDaoService.getEmployeeGroupEmployee('employeeGroupEmployee_to_edit');
  this.empGroupEmp.employeeGroupEmployeePK.fromDate=this.dateToday;
  this.empGroupEmpN.endDate=this.dateToday;
  this.apiClientService.updateEmployeeGroupEmployee(this.empGroupEmp, this.empGroupEmpN)
    .subscribe(
      resp => {
        const errorObject: any = resp.body;
        this.checkErrorService.checkRemoteData(errorObject);
        const empGroupEmp: EmployeeGroupEmployee = resp.body;
        this.alertService.success('Succès lors de la modification', true);
        this.localDaoService.save('employeeGroup_to_show', empGroupEmp);
        this.router.navigate(['habilitations']);
      },
      error => {
        const errorObj: any = error;
        this.alertService.error(errorObj.error.errorText);
      }
    );
    this.isSubmit = false;
  return false;
}

}
