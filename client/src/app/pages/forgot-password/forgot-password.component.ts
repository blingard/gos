import { Component, OnInit } from '@angular/core';
import emailjs, { EmailJSResponseStatus, init } from 'emailjs-com';
import { Router } from '@angular/router';
import { AlertService, AuthenticationService, ApiClientService,
  LocalDaoService, CheckErrorService
 } from '../../_services/exports';
import { Employee } from '../../_generated/exports';

emailjs.init("user_Xk68yrap2DjVls0Qx5MrR");


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  employee: Employee;
  isSubmit: boolean = false;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private checkErrorService: CheckErrorService) { }
  
    public sendEmail(message: any) {
      let templateParams = {
        "from_name":"PAGEGOS",
        "to_name": this.employee.employeeLogin,
        "to_email":this.employee.employeeEmail,
        "message": message.employeePassword
      };
      emailjs.send('montest-pagegos', 'template_223tswi', templateParams)
        .then(function(response) {
          alert('Mail de réinitialisation envoyée a l\'adresse '+message.employeeEmail);
        }, function(error) {
          this.alertService.error('Tentative échouée. Ce nom d\'utilisateur n\'existe pas.');
        }
      );     
    }

  ngOnInit() {
    this.authenticationService.enableHeaderBar();
    this.employee = {
      employeeId: null,
      employeeName: null,
      employeeSurname: null,
      employeeEmail: null,
      status: null,
      employeePassword: null,
      employeePhoneNumber: null,
      employeeMatricule: null,
      employeeLogin: null,
      employeeBirthdate: null,
      employeePlaceOfBirth: null,
      employeeNationality: null,
      employeeAdress: null,
      employeeGender: null,
      isFirstConnection: null
    };
  }

  rememberEmployeePassword () {
    this.isSubmit=true;
    this.apiClientService.existEmployeeLoginAndMail(this.employee).subscribe(
        resp => {
          if(resp.status==200){
            this.sendEmail(resp.body);
          }else{
            this.alertService.error('Tentative échouée. Ce nom d\'utilisateur n\'existe pas.');
          }
        },
        error => {
          this.alertService.error("ERROR");
        }
      );
      this.isSubmit=false;
    return false;
  }

}
