import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {Device, GlobalEmployee} from '../../_generated/exports';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list-device',
  templateUrl: './list-device.component.html',
  styleUrls: ['./list-device.component.css']
})
export class ListDeviceComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  page = 1;
  pageSize = 10;
  collectionSize = 0;  
  isEmpty = false;
  currentEmployee: GlobalEmployee;
  data: any;
  params: any;
  isLoading = true;
  updateErrors = false;
  displayedColumns: Array<String>;
  dataSource: MatTableDataSource<Device>;
  selection: SelectionModel<Device>;
  devicesList: Array<Device>;
  devicesListSort: Array<Device>;

  constructor( private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private apiClientService: ApiClientService,
    private alertService: AlertService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService,
    public translate: TranslateService,
  ) { }


  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.devicesList=this.devicesListSort;
    this.collectionSize=this.devicesList.length;
    if(values.length==0){
     this.pageSize=10;
     this.isEmpty=false;
     this.devicesList=this.devicesListSort;
     this.collectionSize=this.devicesList.length;
    }else{
      let tab: Device[]=[];
      this.pageSize=5;
      let a = this.devicesList.filter(device => {
        if(device.deviceName.toLowerCase().includes(values.toLowerCase()) || device.deviceImei.toLowerCase().includes(values.toLowerCase())){
          tab.push(device);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.devicesList=this.devicesListSort;
       this.collectionSize=this.devicesList.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.devicesList=tab;
       this.collectionSize=this.devicesList.length;
      }
      
    }     
  }



  ngOnInit() {
    this.isLoading = true;
    this.devicesList= null;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
        this.loadingMarkerservice.show();
        this.apiClientService.getAllDevice()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const deList: Array<Device> = resp.body;
            if (deList != null && !deList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.devicesList = deList;
              this.devicesListSort = this.devicesList;
              this.collectionSize = this.devicesListSort.length;
            }
            this.dataSource = new MatTableDataSource<Device>(this.devicesList);
            this.selection = new SelectionModel<Device>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
    this.displayedColumns = ['deviceName','deviceImei', 'actions'];
  }
  ngAfterViewInit() {
    if (!this.isDeviceListEmpty()) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

  }
  isDeviceListEmpty() {
    return this.devicesList == null || this.devicesList.length === 0;
  }
  isDeviceDeleted(device: Device) {
    return this.actionOnModelsService.isDeviceDeleted(device);
  }
  canViewDetails() {
    return this.selection.selected.length !== 0;
  }
  canEditDevice() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isDeviceDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  canDeleteDevice() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isDeviceDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  clearSelection() {
    this.selection.clear();
  }
  toggleSelection(row: Device) {
    if (this.selection.isSelected(row)) {
      this.selection.clear();
    } else {
      this.selection.clear();
      this.selection.select(row);
    }
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  }
  refresh() {
    this.isLoading = true;
    this.devicesList = null;
    this.loadingMarkerservice.show();
        this.apiClientService.getAllDevice()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const deList: Array<Device> = resp.body;
            if (deList != null && !deList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.devicesList = deList;
              this.devicesListSort = this.devicesList;
              this.collectionSize = this.devicesListSort.length;
            }
            this.dataSource = new MatTableDataSource<Device>(this.devicesList);
            this.selection = new SelectionModel<Device>(true, []);
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
  }

  editSelectedDevice() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un périphérique');
    } else {
      let device: Device;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        device = this.selection.selected[i];
        if (!this.isDeviceDeleted(device)) {
          break;
        }
      }
      this.localDaoService.save('device_to_edit', device);
      this.router.navigate(['edit-device']);
    }
  }
  detailsOfDevice(device: Device) {
    this.localDaoService.save('device_to_show', device);
    this.router.navigate(['details-of-device'], {relativeTo: this.route});
  }
  detailsOfSelectedDevice(device: Device) {
      const dev: Device = device;
      this.localDaoService.save('device_to_show', dev);
      this.router.navigate(['details-of-device']);
  }
  editDevice(device: Device) {
    this.localDaoService.save('device_to_edit', device);
    this.router.navigate(['edit-device']);
  }
  deleteDevice(device: Device) {
    let msg = null;
      this.translate.get("CONFIRMREMOVESECTOR").subscribe((data:String)=> {
        msg=data;
      });
      let v= confirm(msg+device.deviceName+" ?");
    if(v==true){ 
      this.updateDeviceStatus(device, 2, 1);
    }
    
  }
  addDevice() {
    this.router.navigate(['add-device']);
  }
  updateDeviceStatus(device: Device, status: number, showMessage: number = 0) {
    const lastStatus: number = device.status;
    device.status = status;
    this.apiClientService.updateDevice(device)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const dev: Device = resp.body;
          device.status = dev.status;
          if (showMessage === 1) {
            this.alertService.success('Périphérique mis à jour avec succès.');
          }
          if (showMessage === 2 && !this.updateErrors) {
            this.alertService.success('Les périphériques ont été mis à jour avec succès.');
          }
          if (showMessage === 2) {
            this.refresh();
          }
          this.apiClientService.getAllDevice().subscribe(
            (data)=>{
              const errorObject: any = data.body;
              this.checkErrorService.checkRemoteData(errorObject);
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              const deList: Array<Device> = data.body;
              if (deList != null && !deList.length) {
                this.alertService.error('Impossible d\'obtenir la liste des périphériques');
              } else {
                this.devicesList = deList;
                this.devicesListSort = this.devicesList;
                this.collectionSize = this.devicesListSort.length;
              }
              this.dataSource = new MatTableDataSource<Device>(this.devicesList);
              this.selection = new SelectionModel<Device>(true, []);
            },
            (err)=>{
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              this.alertService.error('Impossible d\'obtenir la liste des périphériques');
            }
          )
          

        },
        error => {
          this.updateErrors = true;
            device.status = lastStatus;
          this.alertService.error('Echec lors de la mise à jour de ce périphérique');
        }
      );
  }

  listDeviceByAgent(){
    this.router.navigate(["list-devices-agents"]);
  }

  
}
