import { Component, OnInit } from '@angular/core';
import { ApiClientService } from 'src/app/_services/exports';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-view-doc',
  templateUrl: './view-doc.component.html',
  styleUrls: ['./view-doc.component.css']
})
export class ViewDocComponent implements OnInit {
  title;
  data:any;
  doc:String = null;
  type: String = null;
  component:number = 0;
  constructor( private api: ApiClientService,private bsModalRef: BsModalRef,){
    }
  ngOnInit(){
    this.doc = this.api.fileDownload;
    try{
      this.type = this.data.docTypeId.docTypeName;
       this.doc= this.doc+this.data.documentPath;
    }catch(e){
      if(this.component==0){
        this.type=this.data.docType;
        this.doc=this.doc+this.data.documentPath; 
      }else{
        this.doc=this.data;
        this.type=this.title;
      }      
    }
  }

confirm() {
  // do stuff
  this.close();
}

close() {
    this.bsModalRef.hide();
}

}
