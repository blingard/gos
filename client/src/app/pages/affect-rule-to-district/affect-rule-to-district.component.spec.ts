import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectRuleToDistrictComponent } from './affect-rule-to-district.component';

describe('AffectRuleToDistrictComponent', () => {
  let component: AffectRuleToDistrictComponent;
  let fixture: ComponentFixture<AffectRuleToDistrictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectRuleToDistrictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectRuleToDistrictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
