import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService, ApiClientService, LocalDaoService } from 'src/app/_services/exports';
import { Rules, District, GlobalEmployee } from '../../_generated/exports';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-affect-rule-to-district',
  templateUrl: './affect-rule-to-district.component.html',
  styleUrls: ['./affect-rule-to-district.component.css']
})
export class AffectRuleToDistrictComponent implements OnInit {

  currentEmployee: GlobalEmployee;
  district : District;
  districts: District[];
  districtsAffects: Array<District>=[];
  districtsList: District[];
  todayDate: Date;
  dateToday: string;
  rule: Rules;
  page = 1;
  pageSize = 5;
  collectionSize = 0;  
  districtEmpty=false;
  isEmpty = false;

  affectForm : FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
  ) { }
  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.districts=this.districtsList;
    this.collectionSize=this.districtsList.length;
    if(values.length==0){
     this.pageSize=5;
     this.isEmpty=false;
     this.districts=this.districtsList;
     this.collectionSize=this.districtsList.length;
    }else{
      let tab: District[]=[];
      this.pageSize=5;
      let a = this.districts.filter(district => {
        if(district.districtName.toLowerCase().includes(values.toLowerCase()) || district.commune.toLowerCase().includes(values.toLowerCase())){
          tab.push(district);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.districts=this.districtsList;
       this.collectionSize=this.districtsList.length;
       this.pageSize=5;
      }
      else{
       this.isEmpty=false;
       this.districts=tab;
       this.collectionSize=this.districts.length;
      }
      
    }     
  }

  ngOnInit() {
    this.districtEmpty=false;
    if(this.localDaoService.exists("rule_to_affect")){
      this.rule = this.localDaoService.getRule("rule_to_affect");
    }else{
      this.alertService.error("Aucun agent traité");      
      this.router.navigate(['/sector']);
    }
    this.apiClientService.getAllDistrict().subscribe(
      (response)=>{
        const distList: Array<District> = response.body;
        if (distList != null && !distList.length) {
          this.alertService.error('Impossible d\'obtenir la liste des quartiers');
          this.districtEmpty=false;
        } else {
          this.districts = distList;
          this.districtsList = distList;
          this.collectionSize = distList.length;
          this.districtEmpty=true;
        }
      },
      (error)=>{
        const errorObj: any = error;
        this.alertService.error(errorObj.error.errorText);
        this.alertService.error('Impossible d\'obtenir la liste des quartiers, l\'erreur est: ' + error);
      }
    );
  }

}
