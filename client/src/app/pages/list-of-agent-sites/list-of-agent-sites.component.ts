import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { AuthenticationService, AlertService, ApiClientService, ActionOnModelsService,
         LocalDaoService, LoadingMarkerService, CheckErrorService
        } from '../../_services/exports';
import {Site, GpsCoordinates, GlobalEmployee, Documents } from '../../_generated/exports';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service'

@Component({
  selector: 'app-list-of-agent-sites',
  templateUrl: './list-of-agent-sites.component.html',
  styleUrls: ['./list-of-agent-sites.component.css']
})
export class ListOfAgentsSitesComponent implements OnInit {
  page = 1;
  pageSize = 10;
  collectionSize = 0; 
  data: any;
  params: any;
  isLoading = true;
  isEmpty=false;
  updateErrors = false;
  pointToShow: Array<GpsCoordinates>;
  hasPosition: boolean = false;
  isInteraction: boolean = false;
  displayedColumns: Array<String>;
  dataSource: MatTableDataSource<Site>;
  selection: SelectionModel<Site>;
  sitesList: Array<Site>;
  sitesListSort: Array<Site>;
  gpsCoordList: Array<GpsCoordinates>;
  currentEmployee: GlobalEmployee;
  todayDate: Date;
  dateToday: String;
  modalRef: BsModalRef;
  siteClicked: number;
  seePublic: boolean = true;
  seeDetail: boolean = true;
  seePublictr: boolean = true;
  seeDetailtr: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private actionOnModelsService: ActionOnModelsService,
    private router: Router,
    private localDaoService: LocalDaoService,
    private loadingMarkerservice: LoadingMarkerService,
    private checkErrorService: CheckErrorService,   
    private modalService : BsModalService,
  ) { }

  onFilter(event: any){
    let values:string = event.target.value;     
    values=values.trim();     
    this.sitesList=this.sitesListSort;
    this.collectionSize=this.sitesList.length;
    if(values.length==0){
      this.isEmpty=false;
     this.pageSize=10;
     this.sitesList=this.sitesListSort;
     this.collectionSize=this.sitesList.length;
    }else{
      let tab: Site[]=[];
      this.pageSize=5;
      let a = this.sitesList.filter(site => {
        if(site.siteName.toLowerCase().includes(values.toLowerCase())){
          tab.push(site);
        }
      });
      if(tab.length==0){
       this.isEmpty=true;
       this.sitesList=this.sitesListSort;
       this.collectionSize=this.sitesList.length;
       this.pageSize=10;
      }
      else{
       this.isEmpty=false;
       this.sitesList=tab;
       this.collectionSize=this.sitesList.length;
      }
      
    }     
  }

  ngOnInit() {
    this.isLoading = true;
    this.sitesList = null;
    this.gpsCoordList = null;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
        this.loadingMarkerservice.show();
        this.apiClientService.getAllSitesWithoutConditions()
        .subscribe(
          resp => {
            console.log(resp)
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const sitList: Array<Site> = resp.body;
            if (sitList != null && !sitList.length) {
              //this.alertService.error('Erreur: 1');
            } else {
              this.sitesList = sitList;
              this.sitesListSort=this.sitesList;
            }
            this.collectionSize=this.sitesList.length;
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Erreur:');
          }
        ); 
    this.displayedColumns = ['select', 'position', 'siteName', 'ownerName', 'ownerPhone', 'districtId', 'actions'];
  }

  /*ngAfterViewInit() {
    if (!this.isAgentSiteListEmpty()) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }*/
  
  isAgentSiteListEmpty() {
    return this.sitesList == null || this.sitesList.length === 0;
  }

  isSiteActivated(site: Site) {
    return this.actionOnModelsService.isSiteActivated(site);
  }

  isSiteDeactivated(site: Site) {
    return this.actionOnModelsService.isSiteDeactivated(site);
  }

  isSiteDeleted(site: Site) {
    return this.actionOnModelsService.isSiteDeleted(site);
  }

  canViewDetails() {
    return this.selection.selected.length !== 0;
  }

  canInterruptWork() {
    return this.selection.selected.length !== 0;
  }
  canFinishWork() {
    return this.selection.selected.length !== 0;
  }

  canOpen() {
    return this.selection.selected.length !== 0;
  }

  canEditSite() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isSiteDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }

  canActivateSites() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isSiteActivated(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }

  canDeactivateSites() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (this.isSiteActivated(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }

  canDeleteSites() {
    if (this.selection.selected.length !== 0) {
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isSiteDeleted(this.selection.selected[i])) {
          return true;
        }
      }
    }
    return false;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  clearSelection() {
    this.selection.clear();
  }

  toggleSelection(row: Site) {
    if (this.selection.isSelected(row)) {
      this.selection.clear();
    } else {
      this.selection.clear();
      this.selection.select(row);
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  refresh() {
    this.isLoading = true;
    this.sitesList = null;
    this.hasPosition = false;
    this.loadingMarkerservice.show();
    this.apiClientService.getAllSitesWithoutConditions()
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const sitList: Array<Site> = resp.body;
            if (sitList != null && !sitList.length) {
              this.alertService.error('Erreur:');
            } else {
              this.sitesList = sitList;
              this.sitesListSort=this.sitesList;
            }
            this.collectionSize =this.sitesList.length;
          },
          error => {
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            //this.alertService.error('Erreur:');
          }
        ); 
  }
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  }

  addSite() {
    this.router.navigate(['add-site']);
  }

  detailsOfSelectedSite() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un site');
    } else {
      const site: Site = this.selection.selected[0];
      this.localDaoService.save('site_to_show', site);
      this.router.navigate(['details-of-site']);
    }
  }

  openSelectedSite() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un site');
    } else {
      const site: Site = this.selection.selected[0];
      this.localDaoService.save('selected_site', site);
      this.router.navigate(['details-of-site']);
    }
  }

  editSelectedSite() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un site');
    } else {
      let site: Site;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        site = this.selection.selected[i];
        if (!this.isSiteDeleted(site)) {
          break;
        }
      }
      this.localDaoService.save('site_to_edit', site);
      this.router.navigate(['edit-site'], {relativeTo: this.route});
    }
  }

  activateSelectedSite() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un site');
    } else {
      this.updateErrors = false;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (!this.isSiteActivated(this.selection.selected[i])) {
          if (i !== this.selection.selected.length - 1) {
            this.updateSiteStatus(this.selection.selected[i], 0);
          } else {
            this.updateSiteStatus(this.selection.selected[i], 0, 2);
          }
        }
      }
    }
  }

  deactivateSelectedSite() {
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un site');
    } else {
      this.updateErrors = false;
      let i = 0;
      for (i = 0; i < this.selection.selected.length; i++) {
        if (this.isSiteActivated(this.selection.selected[i])) {
          if (i !== this.selection.selected.length - 1) {
            this.updateSiteStatus(this.selection.selected[i], 1);
          } else {
            this.updateSiteStatus(this.selection.selected[i], 1, 2);
          }
        }
      }
    }
  }

  deleteSelectedSite() {
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v===true){
      if (this.selection.selected.length === 0) {
        this.alertService.error('Veuillez sélectionner au moins un site');
      } else {
        this.updateErrors = false;
        let i = 0;
        for (i = 0; i < this.selection.selected.length; i++) {
          if (!this.isSiteDeleted(this.selection.selected[i])) {
            if (i !== this.selection.selected.length - 1) {
              this.updateSiteStatus(this.selection.selected[i], 2);
            } else {
              this.updateSiteStatus(this.selection.selected[i], 2, 2);
            }
          }
        }
      }
    }
    
  }

  detailsOfSite(site: Site) {
    this.localDaoService.save('site_to_show', site);
    this.router.navigate(['details-of-site']);
  }

  openSite(site: Site) {
    this.siteClicked = site.siteId;
    this.localDaoService.save('site_to_show', site);
    this.resetPublicDetail();
    this.router.navigate(['details-of-site']);
  }

  editSite(site: Site) {
    this.localDaoService.save('site_to_edit', site);
    this.router.navigate(['edit-site'], {relativeTo: this.route});
  }

  activateSite(site: Site) {
    this.updateSiteStatus(site, 0, 1);
  }

  deactivateSite(site: Site) {
    this.updateSiteStatus(site, 1, 1);
  }

  deleteSite(site: Site) { 
    let v= confirm("Etes vous sûre de vouloir supprimer ? ");
    if(v===true){
    this.updateSiteStatus(site, 2, 1);
    }
    
  }

  updateSiteStatus(site: Site, status: number, showMessage: number = 0) {
    const lastStatus: number = site.status;
    site.status = status;
    this.apiClientService.updateSite(site)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          const sit: Site = resp.body;
          site.status = sit.status;
          if (showMessage === 1) {
            this.alertService.success('Site mis à jour avec succès.');
          }
          if (showMessage === 2 && !this.updateErrors) {
            this.alertService.success('Les sites ont été mis à jour avec succès.');
          }
          if (showMessage === 2) {
            this.refresh();
          }

          this.apiClientService.getAllSites().subscribe(
            (data)=>{
              const errorObject: any = data.body;
              this.checkErrorService.checkRemoteData(errorObject);
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              const sitList: Array<Site> = data.body;
              if (sitList != null && !sitList.length) {
                this.alertService.error('Erreur:');
              } else {
                this.sitesList = sitList;
              }
              this.collectionSize=this.sitesList.length;
            },
            (err)=>{
              this.isLoading = false;
              this.loadingMarkerservice.hide();
              this.alertService.error('Erreur:');
            }
          )
          

        },
        error => {
          this.updateErrors = true;
          site.status = lastStatus;
          this.alertService.error('Echec lors de la supprimer du site');
        }
      );
  }

  interruptWorkSelectedSite(){
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un site');
    } else {
      const site: Site = this.selection.selected[0];
      site.dateToStop=this.dateToday;
      this.apiClientService.interruptWorkOfSite(site).subscribe(
        resp => {
          const errorObject: any = resp.body;
              this.checkErrorService.checkRemoteData(errorObject);
              this.alertService.success('Travaux interrompu avec succès.');
              this.router.navigate(["site"]);
              this.refresh();
              this.apiClientService.getAllSites().subscribe(
                (data)=>{
                  const errorObject: any = data.body;
                  this.checkErrorService.checkRemoteData(errorObject);
                  this.isLoading = false;
                  this.loadingMarkerservice.hide();
                  this.dataSource = new MatTableDataSource<Site>(this.sitesList);
                  this.selection = new SelectionModel<Site>(true, []);
                  this.alertService.success('Travaux interrompu avec succès');
                },
                (err)=>{
                  this.isLoading = false;
                  this.loadingMarkerservice.hide();
                  this.alertService.error('Erreur2:');
                }
              )
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        }
      );
    }
  }
  finishWorkSelectedSite(){
    this.todayDate = new Date();
    this.dateToday = (this.todayDate.getFullYear() + '-' + ((this.todayDate.getMonth() + 1)) + '-' + this.todayDate.getDate() + ' ' +this.todayDate.getHours() + ':' + this.todayDate.getMinutes()+ ':' + this.todayDate.getSeconds()+ '.'+this.todayDate.getTimezoneOffset());
    if (this.selection.selected.length === 0) {
      this.alertService.error('Veuillez sélectionner au moins un site');
    } else {
      const site: Site = this.selection.selected[0];
      site.dateToFinish=this.dateToday;
      this.apiClientService.finishWorkOfSite(site).subscribe(
        resp => {
          const errorObject: any = resp.body;
              this.checkErrorService.checkRemoteData(errorObject);
              this.alertService.success('Cloture des Travaux avec succès');
              this.router.navigate(["site"]);
              this.refresh();
              this.apiClientService.getAllSites().subscribe(
                (data)=>{
                  const errorObject: any = data.body;
                  this.checkErrorService.checkRemoteData(errorObject);
                  this.isLoading = false;
                  this.loadingMarkerservice.hide();                  
                  this.dataSource = new MatTableDataSource<Site>(this.sitesList);
                  this.selection = new SelectionModel<Site>(true, []);
                },
                (err)=>{
                  this.isLoading = false;
                  this.loadingMarkerservice.hide();
                  this.alertService.error('Erreur:');
                }
              )
        },
        error => {
          const errorObj: any = error;
          this.alertService.error(errorObj.error.errorText);
        }
      );
    }
  }

  onClickedPublic(s: Site): boolean{
    if(this.siteClicked == s.siteId){
      this.seePublic = false;
      this.seeDetailtr = false;
      return true;
    }
    return false;
  }

  onClickedDetail(s: Site): boolean{
    if(this.siteClicked == s.siteId){
      this.seeDetail = false;
      this.seePublictr = false;
      return true;
    }
    return false;
  }
  private resetPublicDetail(){
    this.siteClicked = null;
    this.seePublic = true;
    this.seeDetail = true;
    this.seePublictr = true;
    this.seeDetailtr = true;
  }

  onGetLocation(s: Site){
    console.clear();
    this.siteClicked = s.siteId;
    this.apiClientService.getAllSiteDocuments(s)
      .subscribe(
        resp => {
          const errorObject: any = resp.body;
          const documentsList: Array<Documents> = resp.body;
          if (documentsList != null && !documentsList.length) {
            //put somme image to assets
          } else {            
            s.documentsList=documentsList;
          }
        },
        error => {
        }
      );
    this.apiClientService.getAllCoordinatesSite(s)
        .subscribe(
          resp => {
            const errorObject: any = resp.body;
            this.checkErrorService.checkRemoteData(errorObject);
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            const pointList: Array<GpsCoordinates> = resp.body;
            if (pointList != null && !pointList.length) {
              //this.alertService.error('Impossible d\'obtenir la liste des points');
              this.openSite(s);
            } else {
              this.gpsCoordList = pointList;
              s.gpsCoordinatesList=pointList;
              this.apiClientService.s=s;
              this.localDaoService.save('site_to_show', s);
              this.resetPublicDetail();
              this.router.navigate(["carte"]);
            }
            
          },
          error => {
            this.resetPublicDetail();
            this.isLoading = false;
            this.loadingMarkerservice.hide();
            this.alertService.error('Impossible d\'obtenir la liste des points');
          }
        );
  }

  onInteraction(){
    if(this.isInteraction){
      this.isInteraction = false;
      return;
    }
    this.isInteraction = true;
  }
}
