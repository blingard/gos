import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthenticationService, AlertService, ApiClientService, LocalDaoService,
         ActionOnModelsService, CheckErrorService, LoadingMarkerService } from '../../_services/exports';
import { Documents, Site, GpsCoordinates,DocumentsVerified,Apis} from '../../_generated/exports';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ViewDocComponent } from '../../pages/exports';
import { AddDocumentComponent } from '../add-document/add-document.component';
import apis_json from './../../../assets/_files/apis.json';
import { interval, Subscription } from 'rxjs';


@Component({
  selector: 'app-details-of-site',
  templateUrl: './details-of-site.component.html',
  styleUrls: ['./details-of-site.component.css']
})
export class DetailsOfSiteComponent implements OnInit, OnDestroy {
  
  page = 1;
  example=0;
  pageSize = 5;
  collectionSize = 0; 
  timer = 2000;
  pageDoc = 1;
  existDoc = 1;
  notExisteDoc = 2;
  pageSizeDoc = 5;
  collectionSizeDoc = 0; 
  data: any;
  params: any;
  default = 0;
  site: Site;
  isLoading = true;
  listDocs = false;
  displayedDocsColumns: Array<String>;
  displayedGPSCoordsColumns: Array<String>;
  dataSourceDocs: MatTableDataSource<Documents>;
  dataSourceCoords: MatTableDataSource<GpsCoordinates>;
  selectionOfDoc: SelectionModel<Documents>;
  selectionOfCoord: SelectionModel<GpsCoordinates>;
  docsList: Array<Documents>;
  docsListToVerified: Array<DocumentsVerified>;
  gpsCoordsList: Array<GpsCoordinates>;
  modalRef: BsModalRef;
  apiR: Apis;
  apiList: {apisId:number, apisCode:2, apisName:string, apisDescription:string, createdDate: string, status:number}[]=apis_json;
  second: number;



  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private apiClientService: ApiClientService,
    private localDaoService: LocalDaoService,
    private actionOnModelsService: ActionOnModelsService,
    private checkErrorService: CheckErrorService,
    private loadingMarkerservice: LoadingMarkerService,
    private modalService : BsModalService,
  ) { }

  ngOnDestroy() {
  }

  ngOnInit() {   
    
    this.isLoading = true;
    this.route.data.subscribe(data => this.data = data);
    this.route.params.subscribe(params => this.params = params);
    this.site = this.localDaoService.getSite('site_to_show');
    if (this.site == null) {
      this.alertService.error('Veuillez choisir un site pour afficher ses détails', true);
      this.router.navigate(['site']);
    }
    this.onGetAllDocsOfSite(this.site);    
  }

  private onGetAllDocsOfSite(site: Site) {
    this.apiClientService.getAllSiteDocuments(site)
      .subscribe(
        async resp => {
          const errorObject: any = resp.body;
          this.checkErrorService.checkRemoteData(errorObject);
          this.isLoading = false;
          this.loadingMarkerservice.hide();
          const documentsList: Array<Documents> = resp.body;
          if (documentsList != null && !documentsList.length) {
            this.listDocs = false;
            this.alertService.error('Impossible d\'obtenir la liste des documents de ce site');
          } else {
            this.docsList = documentsList;
            this.collectionSizeDoc = this.docsList.length;
            this.listDocs = true;
            this.onVerifDocuments();                      
          }
        },
        error => {
          this.isLoading = false;
          this.loadingMarkerservice.hide();
          this.alertService.error('Impossible d\'obtenir la liste des documents de ce site');
        }
      );
     
  }


  createNewDocumentsVerified(): DocumentsVerified {
    return {
      documentId: null,
      documentName: null,
      documentCode: null,
      documentNumber:null,
      documentPath: null,
      secretKey: null,
      createdDate: null,
      status: null,
        apisSender: {
          apisId:null,
          apisCode:1,
          apisName:null,
          apisDescription:null,
          createdDate: null,
          status:null,
          documentsList: null,
          documentsList1: null,
        },
      apisReceder: {
        apisId:null,
        apisCode:2,
        apisName:null,
        apisDescription:null,
        createdDate: null,
        status:null,
        documentsList: null,
        documentsList1: null,
      },
      
      gosDocId: null,
      lastUpdate: null
    }  
  }
  createNewApis(): Apis {
    return {
      apisId:null,
      apisCode:null,
      apisName:null,
      apisDescription:null,
      createdDate: null,
      status:null,
      documentsList: null,
      documentsList1: null
  }
}

onCreateDocVerification(doc:Documents){
  const docVerification:DocumentsVerified= this.createNewDocumentsVerified();
  docVerification.documentName=doc.documentName;
  docVerification.documentCode=doc.documentCode;
  docVerification.documentNumber=doc.documentNumber;
  docVerification.gosDocId=doc.documentId;
  docVerification.documentPath=doc.documentPath;
  this.apiClientService.createDocumentsVerification(docVerification).subscribe(
    resp => {
      if(resp.status==200){
        doc.status=4;
        this.apiClientService.updateDocument(doc).subscribe(
          response => {
            if(response.status==200){
              this.ngOnInit();
              //let docVerifications: Documents = resp.body;
              //this.docsList = this.docsList.map(u => u.documentId !== docVerifications.documentId ? u : docVerifications);
            }
          },
          error => {
          }
        );
      }
    },
    error => {
    } 
  );

}

  onVerifDocuments(){
    let docs: DocumentsVerified[]=[];            
    for(let a in this.docsList){
      let docVerif:DocumentsVerified=this.createNewDocumentsVerified();
      if(this.docsList[a].status==4){
        docVerif.documentCode=this.docsList[a].documentCode;
        docVerif.documentName=this.docsList[a].documentName;
        docVerif.documentNumber=this.docsList[a].documentNumber;
        docVerif.documentPath=this.docsList[a].documentPath;
        docVerif.gosDocId=this.docsList[a].documentId;
        docs.push(docVerif);
      }
    }
    if(docs.length==0){

    }else{
      this.apiClientService.documentsVerification(docs).subscribe(
        response =>{
          if(response.status==200){
            let docsListVerified: DocumentsVerified[]=response.body;
            let docs: Documents[]=[];
            for(let a in docsListVerified){
              let document: Documents=null;
              for(let b in this.docsList){
                if(docsListVerified[a].gosDocId==this.docsList[b].documentId){
                  if(docsListVerified[a].status==1){
                    this.docsList[b].status = 0;
                  }else if(docsListVerified[a].status==2){
                    this.docsList[b].status = 1;
                  }
                  this.apiClientService.updateDocument(this.docsList[b]).subscribe(
                    resp => {
                      if(resp.status==200){
                        this.refresh();
                     
                      }
                    },
                    error => {
                    }
                  );
                }
              }
            }
          }
        },
        error=>{

        }
      );
    }
    
    
   
  }
/**/
 openModal(doc: Documents){
    this.modalRef = this.modalService.show(ViewDocComponent,{
      initialState: {
        title: doc.docTypeId.description+": "+doc.documentName,
        data: doc
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
   }
   interumpre(){
    this.modalRef = this.modalService.show(AddDocumentComponent,{
      initialState: {
        title: this.site.ownerName,
        donnee: this.site,
        type:'INTERRUPTION'
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
   }
   adddocmument(){
    this.modalRef = this.modalService.show(AddDocumentComponent,{
      initialState: {
        title: this.site.ownerName,
        donnee: this.site,
        type:'ADDDOC'
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
   }

   reloadsite(){
    this.modalRef = this.modalService.show(AddDocumentComponent,{
      initialState: {
        title: this.site.ownerName,
        donnee: this.site,
        type:'RESTART'
      }
    });
    this.modalRef.content.closeBtnName = 'Close';
   }
  getTenFirstCharacter(chaine:string):string{
    return this.actionOnModelsService.getTenFirstCharacter(chaine);
  }
  getformatDater(chaine:string):string{
    return this.actionOnModelsService.formatDate(chaine);
  }

  downloadDocument(doc: Documents) {
    this.isLoading = true;
    this.loadingMarkerservice.show();
    const extension = doc.documentExtension; 
    this.apiClientService.downloadFile(doc.documentPath, extension);
  }

  addDocument() {
    this.router.navigate(['add-document']);
  }

  refresh() {
    this.onGetAllDocsOfSite(this.site);
  }

}

