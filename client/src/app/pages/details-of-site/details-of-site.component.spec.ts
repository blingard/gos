import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsOfSiteComponent } from './details-of-site.component';

describe('DetailsOfAgentComponent', () => {
  let component: DetailsOfSiteComponent;
  let fixture: ComponentFixture<DetailsOfSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsOfSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsOfSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
