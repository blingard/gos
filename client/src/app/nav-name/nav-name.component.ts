import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators/';
import { ApiClientService, AuthenticationService, CheckErrorService, LocalDaoService } from '../_services/exports';
import {AlertsSystems, GlobalEmployee, Site} from '../_generated/exports';


@Component({
  selector: 'app-nav-name',
  templateUrl: './nav-name.component.html',
  styleUrls: ['./nav-name.component.css']
})
export class NavNameComponent implements OnInit, OnDestroy {

  isCUMember : boolean;
  isLog: boolean;
  isEmpty: boolean;
  longe:number=0;
  currentEmployee: GlobalEmployee;
  logSubscription = new Subscription();
  alertsSystems = new Subscription();
  isCUMemberSubscription = new Subscription();
  notifications: Array<AlertsSystems>;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private authService: AuthenticationService,
              private localDaoService: LocalDaoService,
              private router: Router,
              private api: ApiClientService,
              private checkErrorService: CheckErrorService,
              public translate: TranslateService){
                this.translate.addLangs(['en','fr']);
                this.translate.setDefaultLang('en');
                const browserLang = translate.getBrowserLang();
                this.translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
              }

  ngOnInit(){
    this.logSubscription = this.authService.logSubject.subscribe((log)=>{
      if(log){
        this.api.getAllSystemAlert().subscribe(
          response => {
            let notifes: Array<AlertsSystems>=[];
            this.notifications = [];
            if(response.status == 200){
              const activitiesList: Array<AlertsSystems> = response.body;
              let notifs: Array<AlertsSystems>=[];
              let notif: Array<AlertsSystems>=[];              
              activitiesList.forEach(al=>{
                if(al.agentActivitiesConvocation.status==0){
                  let image = this.api.fileDownload;
                  image=image+al.agentActivitiesConvocation.documentId.documentPath;
                  al.agentActivitiesConvocation.documentId.documentPath = image;
                  notif.push(al);
                  notifs.push(al);
                }
              });              
              notifs.forEach(note1=>{
                let alarm: AlertsSystems = {
                  agentActivitiesConvocation: null,
                  days: 0
                }
                notif.forEach(note=>{
                  if(note1.agentActivitiesConvocation.documentId.siteId.siteId==note.agentActivitiesConvocation.documentId.siteId.siteId){
                    alarm = note;
                  }
                });
                notifes.push(alarm);
              });
              if(notifes.length<=3){
                this.notifications = notifes;  
              }else{
                let i : number=0;
                while(i<3){
                  this.notifications.push(notifes[i]);
                  i++;
                }
              }
            }else{
              this.notifications = [];
            }
            this.longe = notifes.length;
            if(this.longe==0){
              this.isEmpty=true;
            }else{
              this.isEmpty=false;
            }
          },
          error => {
            this.isEmpty=true;
          }
        );
        this.isLog = log;
      }else{
        this.isLog = log;
      }      
    });
    this.authService.emitLog();

    this.isCUMemberSubscription = this.authService.isCUMemberSubject.subscribe((cuMember)=>{
      if(cuMember==false){
        this.isCUMember = cuMember;
        this.authService.logout();
      }else{
        this.isCUMember = cuMember;
      }
    });
    this.authService.emitLogIsCUMember();
  }

  ngOnDestroy(){
    //this.logSubscription.unsubscribe();
    //this.isCUMemberSubscription.unsubscribe();
  }

  onLogout(){
    this.authService.logout();
    this.router.navigate(['/authentication']);
    
    
  }

  goToProfil() {
    
    this.currentEmployee = this.localDaoService.getGlobalEmployee('currentEmployee');
    this.localDaoService.save('employee_to_show', this.currentEmployee.employee);
    this.router.navigate(['details-of-employee']);
  }

}
