import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA  } from '@angular/core';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule, Routes, Route } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatStepperModule } from '@angular/material/stepper';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatTreeModule} from '@angular/material/tree';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatBadgeModule} from '@angular/material/badge';
import { MatSortModule } from '@angular/material/sort';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule, MatSnackBarModule, MatSelectModule, MatOptionModule, MatMenuModule } from '@angular/material/';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import * as pages from './pages/exports';
import * as services from './_services/exports';
import * as directives from './_directives/exports';
import { NavNameComponent } from './nav-name/nav-name.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { AngularFileUploaderModule} from 'angular-file-uploader';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BnNgIdleService } from 'bn-ng-idle';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { AuthGuard } from './_services/auth-guard.service';
export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http);
}




const authenticationRoute: Route = {
  path: 'authentication',
  children: [
    {
      path: '',
      //canActivate:[AuthGuard],
      component: pages.LoginComponent,
      data: {
        page: 'Agent',
        title: 'Liste des Agents',
        /*breadcrumb: [
          { routerLink: '/agent', text: 'Liste des agents' }
        ]*/
      }
    }
  ]
};



const forgotPasswordRoute: Route = {
  path: 'forgot-password',
  children: [
	  {
      path: '',
      component: pages.ForgotPasswordComponent,
      data: {
        title: 'Reset Password Page',
        page: 'forgot-password',
        breadcrumb: [
          {routerLink : '/forgot-password', text : 'Forgot Password'}
        ]
      }
    }
  ]
};
const agentsRouteList: Route = {
  path: 'agent',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListAgentComponent,
      data: {
        page: 'Agent',
        title: 'Liste des Agents',
        /*breadcrumb: [
          { routerLink: '/agent', text: 'Liste des agents' }
        ]*/
      }
    }
  ]
};
const agentsRouteAdd: Route = {
  path: 'add-agent',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddEditAgentComponent,
      data: {
        page: 'Add Agent',
        title: 'Ajouter un Agent',
        add: true,
        breadcrumb: [
          { routerLink: '/agent', text: 'Liste des agents' }
        ]
      }
    }
  ]
};
const agentsRouteEdit: Route = {
  path: 'edit-agent',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddEditAgentComponent,
      data: {        
        title: 'Modifier un agent',
        add: false,
        page: 'Agent modification',
        breadcrumb: [
          { routerLink: '/agent', text: 'Liste des agents' }
        ]
      }
    }
  ]
};
const agentsRouteDetail: Route = {
  path: 'details-of-agent',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.DetailsOfAgentComponent,
      data: {
        page: 'Agent Information',
        title: 'Details sur un Agent',
        add: true,
        breadcrumb: [
          { routerLink: '/agent', text: 'Liste des agents' }
        ]
      }
    }
  ]
};
const agentsRouteDevice: Route = {
  path: 'affect-agent-device',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AffectAgentToDeviceComponent,
      data: {
        page: 'Affect Device to Agent',
        title: 'Affecter un périphérique à un agent',
        breadcrumb: [
          { routerLink: '/agent', text: 'Liste des agents' }
        ]
      }
    }
  ]
};
const sitesRouteDetail: Route = {
  path: 'details-of-site',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.DetailsOfSiteComponent,
      data: {
        page: 'Site detail',
        title: 'Détails sur un site',
        breadcrumb: [
          { routerLink: '/site', text: 'Liste des sites' }
        ]
      }
    }
  ]
};
const sitesRouteList: Route = {
  path: 'site',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListOfAgentsSitesComponent,
      data: {
        page: 'Site List',
        title: 'Liste des sites',
        /*breadcrumb: [
          { routerLink: '', text: '' }
        ]*/
      }
    }
  ]
};
const sitesRouteAddDoc: Route = {
  path: 'add-document',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddDocumentComponent,
      data: {
        page: 'Enregistrer un nouveau document',
        title: 'Enregistrer un nouveau document',
        breadcrumb: [
          { routerLink: '/agent', text: 'Liste des agents' }
        ]
      }
    }
  ]
};

const manageDistrictsRoute:Route={
  path: 'district',
  canActivate:[AuthGuard],
  children :[
    {
      path: 'details-of-district',
      canActivate:[AuthGuard],
      component: pages.DetailsOfSectorComponent,
      data: {
        title: 'Détails sur un Secteur',
        add: true,
        breadcrumb: [
          { routerLink: '/list-district', text: 'Secteur' }
        ]
      }
    },
    {
      path: 'list-district',
      canActivate:[AuthGuard],
      component: pages.ListDistrictComponent,
      data: {
        title: 'Liste des Quatiers',
        breadcrumb: [
          { routerLink: '/list-district', text: 'Quartier' }
        ]
      }
    }
  ]
};

const sectorsRouteList: Route = {
  path: 'sector',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListSectorComponent,
      data: {
        page: 'Sector',
        title: 'Liste des Secteurs',
        /*breadcrumb: [
          { routerLink: '/sector', text: 'Liste des secteurs' }
        ]*/
      }
    }
  ]
};
const sectorsRouteAdd: Route = {
  path: 'add-sector',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddEditSectorComponent,
      data: {
        page: 'Add Sector',
        title: 'Ajouter un Secteur',
        add: true,
        breadcrumb: [
          { routerLink: '/sector', text: 'Liste des secteurs' }
        ]
      }
    }
  ]
};
const sectorsRouteAdistrict: Route = {
  path: 'affect-to-sector',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AffectDistritToSectorComponent,
      data: {
        page: 'Affect District to Sector',
        title: 'Ajouter un Secteur',
        add: true,
        breadcrumb: [
          { routerLink: '/sector', text: 'Liste des secteurs' }
        ]
      }
    }
  ]
};
const sectorsRouteEdit: Route = {
  path: 'edit-sector',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddEditSectorComponent,
      data: {
        page: 'Sector modification',
        title: 'Modified Sector',
        add: false,
        breadcrumb: [
          { routerLink: '/sector', text: 'Liste des secteurs' }
        ]
      }
    }
  ]
};
const sectorsRouteDetail: Route = {
  path: 'details-of-sector',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.DetailsOfSectorComponent,
      data: {
        page: 'Sector Information',
        title: 'Details sur un Secteur',
        add: true,
        breadcrumb: [
          { routerLink: '/sector', text: 'Liste des secteurs' }
        ]
      }
    }
  ]
};
const sectorsAgentRouteList: Route = {
  path: 'sector-agent',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListSectorAgentComponent,
      data: {
        title: 'Listes des Affectations',
        breadcrumb: [
          { routerLink: '/agent', text: 'Liste des Agents' }
        ]
      }
    }
  ]
};
const sectorsAgentRouteListSectors: Route = {
  path: 'list-sector-agent-active',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListAgentSectorsComponent,
      data: {
        title: 'liste des agents affectés aux secteurs',
        breadcrumb: [
          { routerLink: '/list-sector-agent-active', text: 'liste des agents affectés aux secteurs' }
        ]
      }
    }
  ]
};
const sectorsAgentRouteSectorListByAgent: Route = {
  path: 'list-sector-agent',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListSectorAgentComponent,
      data: {
        title: 'Lister des secteurs pour un agent',
        breadcrumb: [
          { routerLink: '/sector-agent/list-sector-agent', text: 'Lister des secteurs pour un agent ' }
        ]
      }
    }
  ]
};
const sectorsAgentRouteAffectToSecto: Route = {
  path: 'affect-sector-to-agent',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AffectSectorToAgentComponent,
      data: {
        title: 'Affecter un agent à un secteur',
        breadcrumb: [
          { routerLink: '/agent', text: 'Liste des agents' }
        ]
      }
    }
  ]
};


const employeesRouteList: Route = {
  path: 'employees',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListEmployeeComponent,
      data: {
        page: 'Employee',
        title: 'Liste des Employeess',
        /*breadcrumb: [
          { routerLink: '/sector', text: 'Liste des secteurs' }
        ]*/
      }
    }
  ]
};
const employeesRouteAdd: Route = {
  path: 'add-employee',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddEditEmployeeComponent,
      data: {
        page: 'Add Employee',
        title: 'Ajouter un employé',
        add: true,
        breadcrumb: [
          { routerLink: '/employees', text: 'Liste des Employées' }
        ]
      }
    }
  ]
};
const employeesRouteNewIdentified: Route = {
  path: 'define-new-identifiers',
  children: [
    {
      path: '',
     // canActivate:[AuthGuard],
      component: pages.DefineNewIdentifiers,
      data: {
        page: 'define-new-identifiers',
        title: 'Définir les nouveaux identifiants',
        breadcrumb: [
          { routerLink: '/define-new-identifiers', text: 'Définir les nouveaux identifiants' }
        ]
      }
    }
  ]
};
const employeesRouteEdit: Route = {
  path: 'edit-employee',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddEditEmployeeComponent,
      data: {
        page: 'Employee modification',
        title: 'Modifier un employer',
        add: false,
        breadcrumb: [
          { routerLink: '/employees', text: 'Liste des Employées' }
        ]
      }
    }
  ]
};
const employeesRouteDetail: Route = {
  path: 'details-of-employee',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.DetailsOfEmployeeComponent,
      data: {
        page: 'Employee Information',
        title: 'Details',
        add: true,
        breadcrumb: [
          { routerLink: '/employees', text: 'Liste des Employées' }
        ]
      }
    }
  ]
};
const defaultRoute: Route = {
  path: '**',
  component: pages.AddAlertComponent,
  data: {
    title: 'Alert page',
    page: 'alerts',
        breadcrumb: [
          {routerLink : '/add-alerts', text : 'Alerts'}
    ]
  }
};
const devicesRouteList: Route = {
  path: 'device',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListDeviceComponent,
      data: {
        page: 'Devices',
        title: 'Liste des Peripheriques',
        /*breadcrumb: [
          { routerLink: '/sector', text: 'Liste des secteurs' }
        ]*/
      }
    }
  ]
};
const devicesRouteAdd: Route = {
  path: 'add-device',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddEditDeviceComponent,
      data: {
        page: 'Add Device',
        title: 'Ajouter un périphérique',
        add: true,
        breadcrumb: [
          { routerLink: '/device', text: 'Liste des peripheriques' }
        ]
      }
    }
  ]
};
const devicesRouteEdit: Route = {
  path: 'edit-device',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddEditDeviceComponent,
      data: {
        page: 'Device modification',
        title: 'Modifier un périphérique',
        add: false,
        breadcrumb: [
          { routerLink: '/device', text: 'Liste des peripheriques' }
        ]
      }
    }
  ]
};
const devicesRouteDetail: Route = {
  path: 'details-of-device',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.DetailsOfDeviceComponent,
      data: {
        page: 'Device Information',
        title: 'Détails sur un périphérique',
        add: true,
        breadcrumb: [
          { routerLink: '/device', text: 'Liste des peripheriques' }
        ]
      }
    }
  ]
};
const devicesRouteListDeviceAgents: Route = {
  path: 'list-devices-agents',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListDeviceAgentComponent,
      data: {
        title: 'Liste des périphériques par Agent',
        breadcrumb: [
          { routerLink: '/device', text: 'Liste des periphériques' }
        ],
        page: 'list-devices-agents'
      }
    }
  ]
};
const devicesRouteListAgentDevices: Route = {
  path: 'list-agent-devices',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListDeviceAgentComponent,
      data: {
        title: 'Afficher la liste des périphériques de l\'agent',
        breadcrumb: [
          { routerLink: '/details-of-agent', text: 'Retour' }
        ],
        page: 'list-agent-devices'
      }
    }
  ]
};
const habilitationsRouteList: Route = {
  path: 'habilitations',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.HabilitationComponent,
      data: {
        page: 'Devices',
        title: 'Gestion des Habilitations',
        add: true,
        /*breadcrumb: [
          { routerLink: '/habilitations', text: 'Gestion des Habilitations' }
        ]*/
      }
    }
  ]
};
const habilitationsRouteEditAccessRight: Route = {
  path: 'edit-accessRight',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.EditAccessRightComponent,
      data: {
        page: 'edit-accessRight',
        title: 'Modifier les Droits d\'Access',
        add: true,
        breadcrumb: [
          { routerLink: '/habilitations', text: 'Habilitations' }
        ]
        
      }
    }
  ]
};
const habilitationsRouteEditAccessRightGroup: Route = {
  path: 'edit-accessRightGroup',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.EditAccessRightGroupComponent,
      data: {
        page: 'edit-accessRightGroup',
        title: 'Modifier les droits d\'un groupe',
        add: true,
        breadcrumb: [
          { routerLink: '/habilitations', text: 'Habilitations' }
        ]
      }
    }
  ]
};
const habilitationsRouteEditEmployeeGroup: Route = {
  path: 'edit-employeeGroup',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.EditGroupComponent,
      data: {
        page: 'edit-employeeGroup',
        title: 'Modifier un groupe d\employé',
        add: true,
        breadcrumb: [
          { routerLink: '/habilitations', text: 'Habilitations' }
        ]
      }
    }
  ]
};
const habilitationsRouteEditEmployeeEmployeeGroup: Route = {
  path: 'edit-employeeGroupEmployee',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.EditEmployeeGroupEmployeeComponent,
      data: {
        title: 'Modifier',
        add: true,
        breadcrumb: [
          { routerLink: '/habilitations', text: 'Habilitations' }
        ],
        page: 'edit-employeeGroupEmployee'
      }
    }
  ]
};

const manageChangePasswordRoute: Route = {
  path: 'change-employee-password',
  canActivate:[AuthGuard],
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ChangeEmployeePasswordComponent,
      data: {
        title: "Gestion des mots de passe",
       /* breadcrumb: [
          { routerLink: '', text: '' }
        ]*/
      }
    },
  ]
};

const interfacageRoute: Route = {
  path: 'interfacage',
  canActivate:[AuthGuard],
  children: [
    {
      path: '',
      component: pages.VerifiedDocumentsComponent,
      data: {
        title: 'Authenticité des documents',
        /*breadcrumb: [
          { routerLink: '/interfacage', text: 'Documents à vérifiers' }
        ]*/
      }
    },
  ]
};
const dashboardRoute: Route = {
  path: 'dashboard',
  canActivate:[AuthGuard],
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.DashboardComponent,
      data: {
        title: 'Dashboard Page',
        page: 'Dashboard',
       /* breadcrumb: [
          {routerLink : '/dashboard', text : 'Dashboad'}
        ]*/
      }
    }
  ]
};
const carteRoute: Route = {
  path: 'carte',
  //canActivate:[AuthGuard],
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.MapComponent,
      data: {
        title: 'Voir la carte',
        page: 'Carte',
        breadcrumb: [
          {routerLink : '/site', text : 'Site'}
        ]
      }
    }
  ]
};
const alertsRouteAdd: Route = {
  path: 'add-alerts',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.AddAlertComponent,
      data: {
        title: 'Alert page',
        page: 'alerts',
            breadcrumb: [
              {routerLink : '/add-alerts', text : 'Alerts'}
        ]
      }
    }
  ]
};
const alertsRouteList: Route = {
  path: 'alerts',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.ListAlertsComponent,
      data: {
        title: 'Alerts',
        page: 'Alerts',
       /* breadcrumb: [
          {routerLink : '/alerts/list-alerts', text : 'listes des alerts'}
        ]*/
      }
    }
  ]
};
const alertsRouteDetail: Route = {
  path: 'details-of-alerts',
  children: [
    {
      path: '',
      canActivate:[AuthGuard],
      component: pages.DetailsOfAlertsComponent,
      data: {
        title: 'Details',
        page: 'details Alerts',
        breadcrumb: [
          {routerLink : '/alert', text : 'Alerts'}
        ]
      }
    }
  ]
};

const routes: Routes = [
  alertsRouteDetail,
  alertsRouteList,
  authenticationRoute,
  agentsRouteAdd,
  agentsRouteDetail,
  agentsRouteDevice,
  agentsRouteEdit,
  agentsRouteList,
  alertsRouteAdd,
  carteRoute,
  dashboardRoute,
  devicesRouteAdd,
  devicesRouteDetail,
  devicesRouteEdit,
  devicesRouteList,
  devicesRouteListAgentDevices,
  devicesRouteListDeviceAgents,
  employeesRouteAdd,
  employeesRouteDetail,
  employeesRouteEdit,
  employeesRouteList,
  employeesRouteNewIdentified,  
  forgotPasswordRoute,
  habilitationsRouteEditAccessRight,
  habilitationsRouteEditAccessRightGroup,
  habilitationsRouteEditEmployeeEmployeeGroup,
  habilitationsRouteEditEmployeeGroup,
  habilitationsRouteList,
  interfacageRoute,
  sectorsAgentRouteAffectToSecto,
  sectorsAgentRouteList,
  sectorsAgentRouteListSectors,
  sectorsAgentRouteSectorListByAgent,
  sectorsRouteList,
  sectorsRouteAdd,
  sectorsRouteAdistrict,
  sectorsRouteEdit,
  sectorsRouteDetail,
  sitesRouteDetail,
  sitesRouteAddDoc,
  sitesRouteList,

  manageDistrictsRoute,
  
  manageChangePasswordRoute,
  
  defaultRoute
];

@NgModule({
  declarations: [
    AppComponent,
    NavNameComponent,
    directives.AlertComponent,
    directives.LoadingMarkerComponent,
    pages.LoginComponent,
    pages.FooterComponent,
    pages.ForgotPasswordComponent,
    pages.ListOfAgentsSitesComponent,
    pages.BreadcrumbComponent,
    pages.ListDistrictComponent,
    pages.AddEditAgentComponent,
    pages.ListAgentComponent,
    pages.DetailsOfAgentComponent,
    pages.DetailsOfDeviceComponent,
    pages.PageNotFoundComponent,
    pages.Error500Component,
    pages.AffectAgentToDeviceComponent,
    pages.AddEditDeviceComponent,
    pages.ListDeviceComponent,
    pages.DetailsOfSiteComponent,
    pages.ListSectorAgentComponent,
    pages.AffectSectorToAgentComponent,
    pages.AddEditSectorComponent,
    pages.DetailsOfSiteComponent,
    pages.DetailsOfSectorComponent,
    pages.ListDeviceAgentComponent,
    pages.AddDocumentComponent,
    pages.ListEmployeeComponent,
    pages.DefineNewIdentifiers,
    pages.AddEditEmployeeComponent,
    pages.DetailsOfEmployeeComponent,
    pages.HabilitationComponent,
    pages.ChangeEmployeePasswordComponent,
    pages.EditAccessRightComponent,
    pages.EditGroupComponent,
    pages.EditEmployeeGroupEmployeeComponent,
    pages.EditAccessRightGroupComponent,
    pages.DashboardComponent,
    pages.AddAlertComponent,
    pages.ListAgentSectorsComponent,
    pages.ListAlertsComponent,
    pages.DetailsOfAlertsComponent,
    pages.ViewDocComponent,
    pages.MapComponent,
    pages.ListSectorComponent,
    pages.AffectDistritToSectorComponent,
    pages.ModalAlertComponent,
    pages.CreateGroupComponent,
    pages.VerifiedDocumentsComponent,               
    pages.AffectRuleToDistrictComponent,
    
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTableModule,
    MatCheckboxModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTooltipModule,
    AngularFontAwesomeModule,
    MatDialogModule,
    MatSortModule,
    MatBadgeModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatTreeModule,
    AngularFileUploaderModule,
    MatOptionModule,
    MatSelectModule,
    MatTabsModule,
    MatExpansionModule,
    MatStepperModule,
    MatSlideToggleModule,


    NgbModule,
    MatMenuModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    NgxDocViewerModule,
    TranslateModule.forRoot({
      loader:{
        provide:TranslateLoader,
        useFactory:HttpLoaderFactory,
        deps:[HttpClient]
      }

    }),
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    services.AlertService,
    services.ApiClientService,
    services.AuthenticationService,
    services.LocalDaoService,
    services.ActionOnModelsService,
    services.LoadingMarkerService,
    services.CheckErrorService,
    AuthGuard,
    BnNgIdleService,
    {provide: 'appId', useValue: 'gMnW0SpOnVgoHC4gtsElPAmEwRnVWehOLcfRU3wTiGVlpFq4hmVQPrBBh1AQrWi-)-N8UfnhnIvBgT8NpCu8SB2h7drGYwiIUY2cOz0Kwye69JGtCU2h0FfT65QgctMLR2h0TTpZtH-)-BYAM2IQHUyWHtt2yAEXkyOZln005AYjqOwb-@' },
    {provide: 'appIdKey', useValue: 'TcWlVp4QhnscuHn182TFXHeX7ph8fqwm8IHNOxyEQtzTRs4udppKpGC7HOXljVek' },
    {provide: 'tokenKey', useValue: 'GosToken' }
  ],
  bootstrap: [AppComponent],
  entryComponents: [pages.ViewDocComponent,pages.ModalAlertComponent,pages.AddDocumentComponent]
})
export class AppModule { }
