import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingMarkerComponent } from './loading-marker.component';

describe('LoadingMarkerComponent', () => {
  let component: LoadingMarkerComponent;
  let fixture: ComponentFixture<LoadingMarkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingMarkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingMarkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
