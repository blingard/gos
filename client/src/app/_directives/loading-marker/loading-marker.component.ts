import { Component, OnInit } from '@angular/core';
import { LoadingMarkerService } from '../../_services/exports';

@Component({
  selector: 'app-loading-marker',
  templateUrl: './loading-marker.component.html',
  styleUrls: ['./loading-marker.component.css']
})
export class LoadingMarkerComponent implements OnInit {

  show = false;

  constructor(private loadingMarkerService: LoadingMarkerService) { }

  ngOnInit() {
    this.loadingMarkerService.isShow().subscribe(show => { this.show = show; });
  }

  hide() {
    this.loadingMarkerService.hide();
  }

}
