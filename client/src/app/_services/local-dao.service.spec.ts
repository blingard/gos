import { TestBed, inject } from '@angular/core/testing';

import { LocalDaoService } from './local-dao.service';

describe('LocalDaoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalDaoService]
    });
  });

  it('should be created', inject([LocalDaoService], (service: LocalDaoService) => {
    expect(service).toBeTruthy();
  }));
});
