import { AgentActivity } from './../../../../gosMobile/src/app/_generated/models/agentactivity.model';
import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import {AlertsSystems, Agent,Alerts, EmployeeGroup,AccessRight, AgentDevice, AgentSector, Device, Documents, GpsCoordinates, Sector, Site, UploadResponse, Employee, AccessRightEmployeeGroup, EmployeeGroupEmployee, GlobalEmployee,ViewsAlertsDocuments,District, DocumentsVerified, Rules } from '../_generated/exports';
import { CheckErrorService } from './exports';
import { Subject } from 'rxjs/Subject';
import apis_json from './../../assets/_files/apis.json';
import { LocalDaoService } from './local-dao.service';

/**
* Created with angular4-swagger-client-generator.
*/
@Injectable()
export class ApiClientService {
  s: Site;
  g:GpsCoordinates;
  apiList: {apisId:number, apisCode:number, apisName:string, apisDescription:string, createdDate: string, status:number}[]=apis_json;
  private alertsSystems:Array<AlertsSystems>;
  alertsSystemsSubject = new Subject<Array<AlertsSystems>>();
  emitLog(){
    this.alertsSystemsSubject.next(this.alertsSystems.slice());
  }

  fileDownload = "http://10.10.1.51:8051/gosfile/download?fileKey=";
  private domain = 'http://10.10.1.51:8051';
  private domainGis= 'http://10.10.1.51:8540';


  constructor(
    private http: HttpClient,
    private checkErrorService: CheckErrorService,
    private localDaoService: LocalDaoService,
	@Inject('appId') private appId: string,
    @Inject('appIdKey') private appIdKey: string,
    @Inject('tokenKey') private tokenKey: string,
  ) {
  }

  initState(){
    this.getAllSystemAlert().subscribe(
      response => {
        const errorObject: any = response.body;
        this.checkErrorService.checkRemoteData(errorObject);
        const activitiesList: Array<AlertsSystems> = response.body;
        if (activitiesList == null){
          this.alertsSystems = [];
        }else{
          this.alertsSystems = activitiesList;
        }
        this.emitLog();
      },
      error => {
      }
    );
  }


  /**
  * Method createAgent
  * @param body 
  * @return Full HTTP response as Observable
  */
  public createAgent(body: Agent): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agents/createAgent`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method createSector
  * @param body 
  * @return Full HTTP response as Observable
  */
  public createSector(body: Sector): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/sector/createSector`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  public updateDocument(body: Documents): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/documents/updateDocument`;
    const headers = new HttpHeaders();
    let params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  public createDocumentsVerification(doc: DocumentsVerified): Observable<HttpResponse<any>> {
    const uri = `/verified/CreateDocument`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domainGis + uri, headers, params, JSON.stringify(doc));
  }

  public documentsVerification(body: DocumentsVerified[]): Observable<HttpResponse<any>> {
    const uri = `/verified/getAllDocumentsResponse`;
    const headers = new HttpHeaders();
    let params = new HttpParams();
    return this.sendRequest<any>('post', this.domainGis + uri, headers, params, JSON.stringify(body));
  }
  /***
   * pour la zone d'echange
   */
  public getDocumentsToVerifieds(api:number): Observable<HttpResponse<any>> {
    const uri = "/verified/getAllDocuments/"+api;
    const headers = new HttpHeaders();
    let params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }
/**
 * pour la zone d'echange
 * @param body 
 * @returns 
 */
  public reponseVerificationDocuments(body: DocumentsVerified): Observable<HttpResponse<any>> {
    const uri = `/verified/reponseVerificationDocuments`;
    const headers = new HttpHeaders();
    let params = new HttpParams();
    return this.sendRequest<any>('post', this.domainGis + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method getAllSectors
  * @return Full HTTP response as Observable
  */
  public getAllSectors(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/sector/getAllSector`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }

  /**
  * Method getAllSector
  * @return Full HTTP response as Observable
  */
   public getAllSector(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/sector/getAllSector`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }

   /**
  * Method getAllRules
  * @return Full HTTP response as Observable
  */
    public getAllRules(): Observable<HttpResponse<any>> {
      const uri = `/maincontroller/urbanrule/getAllUrbanrules`;
      const headers = new HttpHeaders();
      const params = new HttpParams();
      return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
    }

  /**
  * Method getNonAffectedSectorsToAgent
  * @return Full HTTP response as Observable
  */
  public getNonAffectedSectorsToAgent(body: Agent): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agentsSectors/getNonAffectedSectorsToAgent`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  /**
  * Method getNonAffectedDistrictToSector
  * @return Full HTTP response as Observable
  */
   public getNonAffectedDistrictToSector(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/districtSectors/getNonAffectedDistrictToSector`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }
/**
  * Method getAllDistrict
  * @return Full HTTP response as Observable
  */
 public getAllDistrict(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/sector/getAllDistricts`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
}
  /**
  * Method getSectorById
  * @param body 
  * @return Full HTTP response as Observable
  */
  public getSectorById(body: Sector): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/sector/getSectorById`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method getDocumentsById
  * @param body 
  * @return Full HTTP response as Observable
  */
  public getDocumentsById(body: Documents): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/documents/getDocumentsById`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method getAllSiteDocuments
  * @return Full HTTP response as Observable
  */
 public getAllSiteDocuments(body: Site): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/documents/getAllSiteDocuments`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}
public getAllDistrictOfSector(body: Sector): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/sector/getAllDistrictOfSector`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}

public getAllSiteOfDistrict(body: District): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/sector/getAllSiteOfDistrict`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}
  /**
  * Method createGpsCoordinates
  * @param body 
  * @return Full HTTP response as Observable
  */
  public createGpsCoordinates(body: GpsCoordinates): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/gpsCoordinates/createGpsCoordinates`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

    /**
  * Method affectDeviceToAgent
  * @param body 
  * @return Full HTTP response as Observable
  */
 public affectDeviceToAgent(body: AgentDevice): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/agents/affectDeviceToAgent`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}


  /**
  * Method affectSectorToAgent
  * @param body 
  * @return Full HTTP response as Observable
  */
 public affectSectorToAgent(body: AgentSector): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/agents/affectSectorToAgent`;
  const headers = new HttpHeaders()
                                .append("Authorization",this.localDaoService.getAccessToken());
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}



  /**
  * Method getAllAgentsSectors
  * @param body 
  * @return Full HTTP response as Observable
  */
  public getAllAgentsSectors(body: Agent): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agents/getAllAgentsSectors`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method getAgentsSectorsByAgent
  * @param body 
  * @return Full HTTP response as Observable
  */
  public getAgentsSectorsByAgent(body: Agent): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agentsSectors/getAgentsSectorsByAgent`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  /**
  * Method getAllAgents
  * @return Full HTTP response as Observable
  */
  public getAllAgents(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agents/getAllAgents`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }
   /**
  * Method getAllEmployeeGroupEmployees
  * @return Full HTTP response as Observable
  */
 public getAllEmployeeGroupEmployees(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/getAllEmployeeGroupEmployees`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
}
/**
  * Method getAllEmployeeGroupEmployees
  * @return Full HTTP response as Observable
  */
 public getAllAccessRightGroups(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/accessRights/getAllAccessRightGroups`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
}
  /**
  * Method updateAgent
  * @param body 
  * @return Full HTTP response as Observable
  */
  public updateAgent(body: Agent): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agents/updateAgent`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  /**
  * Method updateSector
  * @param body 
  * @return Full HTTP response as Observable
  */
  public updateSector(body: Sector): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/sector/updateSector`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method deleteSector
  * @param body 
  * @return Full HTTP response as Observable
  */
   public deleteSector(body: Sector): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/sector/deleteSector`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method updateDistrict
  * @param body 
  * @return Full HTTP response as Observable
  */
   public updateDistrict(body: District[]): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/district/updateDistrict`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }


  /**
  * Method connectEmployeeWithLoginAndPass
  * @param body 
  * @return Full HTTP response as Observable
  */
 public connectEmployeeWithLoginAndPass(body: Employee): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/connectEmployeeWithLoginAndPass`;
  const headers = new HttpHeaders()
                                  .append("login",body.employeeLogin)
                                  .append("pwd",body.employeePassword);
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params,null);
  }



  public existEmployeeLoginAndMail(body: Employee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/employees/verifEmployeeLoginAndMail`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
    }

  /**
  * Method defineNewIdentifiers
  * @param body 
  * @return Full HTTP response as Observable
  */
  public defineNewIdentifiers(body: Employee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/employees/defineNewIdentifiers`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method verifyLogin
  * @param body 
  * @return Full HTTP response as Observable
  */
  public verifyLogin(body: Employee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/employees/verifyLogin`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }


  /**
  * Method verifyverifyMatricule
  * @param body 
  * @return Full HTTP response as Observable
  */
   public verifyMatricule(body: Employee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/employees/verifyMatricule`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method verifyTel
  * @param body 
  * @return Full HTTP response as Observable
  */
   public verifyPhoneNumber(body: Employee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/employees/verifyTel`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method verifyMail
  * @param body 
  * @return Full HTTP response as Observable
  */
   public verifyMail(body: Employee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/employees/verifyMail`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

    /**
  * Method verifyAgentLogin
  * @param body 
  * @return Full HTTP response as Observable
  */
 public verifyAgentLogin(body: Agent): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/agents/verifyLogin`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}

  /**
  * Method getAgentById
  * @param body 
  * @return Full HTTP response as Observable
  */
  public getAgentById(body: Agent): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agents/getAgentById`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method createEmployee
  * @param body 
  * @return Full HTTP response as Observable
  */
 public createEmployee(body: Employee): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/createEmployee`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}
/**
  * Method updateEmployee
  * @param body 
  * @return Full HTTP response as Observable
  */
 public updateEmployee(body: Employee): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/updateEmployee`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}
/**
  * Method getAllEmployees
  * @return Full HTTP response as Observable
  */
 public getAllEmployees(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/getAllEmployees`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
}
/**
  * Method getEmployeeById
  * @param body 
  * @return Full HTTP response as Observable
  */
 public getEmployeeById(body: Employee): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/getEmployeeById`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}

  /**
  * Method updateSite
  * @param body 
  * @return Full HTTP response as Observable
  */
  public updateSite(body: Site): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/site/updateSite`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
   
  /**
  * Method getAllSites
  * @return Full HTTP response as Observable
  */
  public getAllSites(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/site/getAllSites`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }

  /**
  * Method getAllSitesWithoutConditions
  * @return Full HTTP response as Observable
  */
 public getAllSitesWithoutConditions(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/site/getAllSitesWithoutConditions`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
}

  /**
  * Method getAllCoordinatesSite
  * @return Full HTTP response as Observable
  */
  public getAllCoordinatesSite(body: Site): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/gpsCoordinates/getAllCoordinatesSite`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  /**
  * Method updateGpsCoordinates
  * @param body 
  * @return Full HTTP response as Observable
  */
  public updateGpsCoordinates(body: GpsCoordinates): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/gpsCoordinates/createGpsCoordinates`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  

  /**
  * Method getSiteById
  * @param body 
  * @return Full HTTP response as Observable
  */
  public getSiteById(body: Site): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/site/getSiteById`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method createDocument
  * @param body 
  * @return Full HTTP response as Observable
  */
  public createDocument(body: Documents): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/documents/createDocumentDesktop`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  /**
  * Method createViewsAlertsDocument
  * @param body 
  * @return Full HTTP response as Observable
  */
   public createViewsAlertsDocument(body: ViewsAlertsDocuments): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/alerts/createAlerts`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method createDevice
  * @param body 
  * @return Full HTTP response as Observable
  */
  public createDevice(body: Device): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/device/createdDevice`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  /**
  * Method getAllDevice
  * @return Full HTTP response as Observable
  */
  public getAllDevice(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/device/getAllDevices`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }

  /**
  * Method getAllDevice
  * @return Full HTTP response as Observable
  */
 public getAllFreeDevices(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/device/getAllFreeDevices`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
}


    /**
  * Method getAllAgentDevice
  * @return Full HTTP response as Observable
  */
 public getAllAgentDevice(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/device/getAllAgentDevice`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
}

public getAllAgentDevicesByAgent(body: Agent): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/device/getAllDeviceForAgent`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}

  /**
  * Method getAgentDevices
  * @return Full HTTP response as Observable
  */
 public getAgentDevices(body: Agent): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/device/getAllDeviceForAgent`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
} 

/*public getAgentsSectorsByAgent(body: Agent): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/agentsSectors/getAgentsSectorsByAgent`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}*/

  /**
  * Method updateDevice
  * @param body 
  * @return Full HTTP response as Observable
  */
  public updateDevice(body: Device): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/device/updateDevice`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  public interruptWorkOfSite(body: Site): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/site/interruptWorkOfSite`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  public relancerWorkOfSite(body: Site): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/site/relancementWorkOfSite`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  public finishWorkOfSite(body: Site): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/site/finishWorkOfSite`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  public changeEmployeePassword(body: Employee, newPassword: String,  currentEmployee: GlobalEmployee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/password/changeEmployeePassword`;
    const headers = new HttpHeaders();
    let params = new HttpParams();
    if ( newPassword !== undefined && newPassword !== null) {
      params = params.set('newPassword', newPassword + '');
    }
    params = params.set('incommingEmployee', JSON.stringify(currentEmployee) + '');
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method getAllDocTypes
  * @return Full HTTP response as Observable
  */
  public getAllDocTypes(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/documents/getAllDocTypes`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }

  /**
  * Method getAllDocTypes
  * @return Full HTTP response as Observable
  */
 public getAllDocuments(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/documents/getAllDocuments`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }

  /**
  * Method getAgentsSectorsActives
  * @return Full HTTP response as Observable
  */
 public getAgentsSectorsActives(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/agents/getAgentsSectorsActives`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
}

  /**
  * Method uploadVideoFile
  * @param file 
  * @return Full HTTP response as Observable
  */
  public uploadVideoFile(formData: FormData): Observable<HttpResponse<UploadResponse>> {
    let requestObservable: Observable<HttpResponse<UploadResponse>> = null;
    const uri = `/gosfile/uploadvideo`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    requestObservable = this.http.post<UploadResponse>(this.domain + uri, formData, {
      observe: 'response'
    });
    return requestObservable;
  }

  /**
  * Method uploadAudioFile
  * @param file 
  * @return Full HTTP response as Observable
  */
  public uploadAudioFile(formData: FormData): Observable<HttpResponse<UploadResponse>> {
    let requestObservable: Observable<HttpResponse<UploadResponse>> = null;
    const uri = `/gosfile/uploadaudio`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    requestObservable = this.http.post<UploadResponse>(this.domain + uri, formData, {
      observe: 'response'
    });
    return requestObservable;
  }

  /**
  * Method uploadImageFile
  * @param file 
  * @return Full HTTP response as Observable
  */
  public uploadImageFile(formData: FormData): Observable<HttpResponse<UploadResponse>> {
      let requestObservable: Observable<HttpResponse<UploadResponse>> = null;
      const uri = `/gosfile/uploadimage`;
      const headers = new HttpHeaders();
      const params = new HttpParams();
      requestObservable = this.http.post<UploadResponse>(this.domain + uri, formData, {
        observe: 'response'
      });
      return requestObservable;
  }

  /**
  * Method uploadTextFile
  * @param file 
  * @return Full HTTP response as Observable
  */
 public uploadTextFile(formData: FormData): Observable<HttpResponse<UploadResponse>> {
  let requestObservable: Observable<HttpResponse<UploadResponse>> = null;
  const uri = `/gosfile/uploadtext`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  requestObservable = this.http.post<UploadResponse>(this.domain + uri, formData, {
    observe: 'response'
  });
  return requestObservable;
}

  /**
  * Method deleteFile
  * @param body 
  * @return Full HTTP response as Observable
  */
 /* public deleteFile(body: ): Observable<HttpResponse<any>> {
    const uri = `/gosfile/deletefile`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }*/

  /**
  * Method downloadFile
  * @param fileKey 
  * @return Full HTTP response as Observable
  */
  public downloadFile(fileKey: string, extension: string) {
    const uri = `/gosfile/download`;
    const headers = new HttpHeaders();
    let params = new HttpParams();
    if (fileKey !== undefined && fileKey !== null) {
      params = params.set('fileKey', fileKey + '');
    }
    if (extension == 'docx' || extension == 'doc' || extension == 'odt') {
      this.http.get(this.domain + uri,{
        params: params,
        responseType: 'arraybuffer',
        headers:headers
    } 
       ).subscribe(response => this.utilDownLoadFile(response, "application/msword"));
    }
    
    else if (extension == 'xls') {
      this.http.get(this.domain + uri,{
        params: params,
        responseType: 'arraybuffer',
        headers:headers} 
       ).subscribe(response => this.utilDownLoadFile(response, "application/ms-excel"));
    }
    else if (extension == 'pdf') {
      this.http.get(this.domain + uri,{
        params: params,
        responseType: 'arraybuffer',
        headers:headers} 
       ).subscribe(response => this.utilDownLoadFile(response, "application/pdf"));
    }
    else {
      this.http.get(this.domain + uri,{
        params: params,
        responseType: 'arraybuffer',
        headers:headers} 
       ).subscribe(response => this.utilDownLoadFile(response, "image/"+extension));
      }
  }

      /**
     * Method is use to download file.
     * @param data - Array Buffer data
     * @param type - type of the document.
     */
    utilDownLoadFile(data: any, type: string) {
      let blob = new Blob([data], { type: type});
      let url = window.URL.createObjectURL(blob);
      let pwa = window.open(url);
      if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
          alert( 'Veuillez autoriser votre navigateur à accepter les fenêtres pop-up.');
      }
    }

   
  /**
  * Method create AccessRight
  * @param body 
  * @return Full HTTP response as Observable
  */
 public createAccessRight(body: AccessRight): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/createAccessRight`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}
  /**
  * Method createEmployeeGroup
  * @param body 
  * @return Full HTTP response as Observable
  */
 public createEmployeeGroup(body: EmployeeGroup): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/createEmployeeGroup`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}
  
  /**
  * Method getAllEmployeeGroups
  * @return Full HTTP response as Observable
  */
 public getAllEmployeeGroups(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/getAllEmployeeGroups`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }
  /**
  * Method getAllAccessRights
  * @return Full HTTP response as Observable
  */
 public getAllAccessRights(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/getAllAccessRights`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }
  /**
  * Method updateAccessRight
  * @return Full HTTP response as Observable
  */
 public updateAccessRight(body: AccessRight): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/updateAccessRight`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  /**
  * Method updateEmployeeGroup
  * @return Full HTTP response as Observable
  */
 public updateEmployeeGroup(body: EmployeeGroup): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/updateEmployeeGroup`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method updateEmployeeGroupEmployee
  * @return Full HTTP response as Observable
  */
 public updateEmployeeGroupEmployee(body: EmployeeGroupEmployee, empGroupemp: EmployeeGroupEmployee): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/updateEmployeeGroupEmployee`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
  let params = new HttpParams();
  params = params.set('incommingEmployeeGroupEmp', JSON.stringify(empGroupemp) + '');
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method updateEmployeeGroupEmployee
  * @return Full HTTP response as Observable
  */
 public updateAccessRightEmployeeGroup(body: AccessRightEmployeeGroup, arempGroup: AccessRightEmployeeGroup): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/employees/updateAccessRightEmployeeGroup`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
  let params = new HttpParams();
  params = params.set('incommingarempGroup', JSON.stringify(arempGroup) + '');
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

  /**
  * Method affectAccessRightToGroup
  * @return Full HTTP response as Observable
  */
  public affectAccessRightToGroup(body: AccessRightEmployeeGroup): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/accessRightEmployeeGroup/affectAccessRightToGroup`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  /**
  * Method affectEmployeeToGroup
  * @return Full HTTP response as Observable
  */
  public affectEmployeeToGroup(body: EmployeeGroupEmployee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/employee/affectEmployeeToGroup`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
   /**
  * Method createEmployeeGroupEmployee
  * @return Full HTTP response as Observable
  */
  public createEmployeeGroupEmployee(body: EmployeeGroupEmployee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/employees/createEmployeeGroupEmployee`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
  /**
  * Method updateEmployeeGroupEmployeeWhereEndDate
  * @return Full HTTP response as Observable
  */
  public updateEmployeeGroupEmployeeWhereEndDate(body: EmployeeGroupEmployee): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/employees/updateEmployeeGroupEmployeeWhereEndDate`;
    const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
    }
  public updateAgentDeviceWhereEndDate(body: AgentDevice): Observable<HttpResponse<any>> {
      const uri = `/maincontroller/agent/updateAgentDeviceEndDate`;
      const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
      const params = new HttpParams();
      return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
      }

      public updateAgentSectorWhereEndDate(body: AgentSector): Observable<HttpResponse<any>> {
        const uri = `/maincontroller/agent/updateAgentSectorEndDate`;
        const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
    const params = new HttpParams();
        return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
      }
      public updateAccessRightEmployeeGroupEndDate(body: AccessRightEmployeeGroup): Observable<HttpResponse<any>> {
        const uri = `/maincontroller/employees/updateAccessRightEmployeeGroupEndDate`;
        const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
        const params = new HttpParams();
        return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
      }


       /**
  * Method createAlerts
  * @param body 
  * @return Full HTTP response as Observable
  */
  public createAlerts(body: Alerts): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/alerts/createAlerts`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
    /**
  * Method getAllActivateAlerts
  * @return Full HTTP response as Observable
  */
 public getAllActivateAlerts(): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/alerts/getAllActivateAlerts`;
  const headers = new HttpHeaders();
  const params = new HttpParams();
  return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
}
  /**
  * Method getAllDocumentsforAlerts
  * @return Full HTTP response as Observable
  */
  public getAllDocumentsforAlerts(body: Alerts): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/alerts/getAllDocumentsforAlerts`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }

/**
  * Method updateAlerts
  * @return Full HTTP response as Observable
  */
public updateAlerts(body: Alerts, currentEmployee: GlobalEmployee): Observable<HttpResponse<any>> {
  const uri = `/maincontroller/alerts/updateAlerts`;
  const headers = new HttpHeaders()
                                    .append("Authorization",this.localDaoService.getAccessToken());
  const params = new HttpParams();
  return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
}

  /**
  * Method getAllSectors
  * @return Full HTTP response as Observable
  */
  public getAllAgentActivities(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agentActivities/getAllAgentActivities`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }

  public getAllSystemAlert(): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agentActivities/getAllAlerts`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domain + uri, headers, params, null);
  }
  
  public setSystemAlert(body: AlertsSystems): Observable<HttpResponse<any>> {
    const uri = `/maincontroller/agentActivities/setAllAlerts`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('post', this.domain + uri, headers, params, JSON.stringify(body));
  }
 

  /**
  * ELLA BELINGA Jean-Paul
  * Method Quartier
  * @return Full HTTP response as Observable
  */
  public getAllQuartier(): Observable<HttpResponse<any>> {
    const uri = `/v1/quartier`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }  

  public getAllPlanZonnage(): Observable<HttpResponse<any>> {
    const uri = `/v1/plan_de_zonage`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }

  public getQuartierByName(name: string): Observable<HttpResponse<any>> {
    const uri = `/v1/quartier/find/`+name;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }

  public getAllForet(): Observable<HttpResponse<any>> {
    const uri = `/v1/foreturbaine`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }
  public getAllMarecage(): Observable<HttpResponse<any>> {
    const uri = `/v1/marecages`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }
  public getAllZonesInnondables(): Observable<HttpResponse<any>> {
    const uri = `/v1/zones_inondables`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }
  public getAllJardin(): Observable<HttpResponse<any>> {
    const uri = `/v1/jardinpublic`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }
  public getAllPlanEau(): Observable<HttpResponse<any>> {
    const uri = `/v1/planeau`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }
  public getAllReseauAssainnissament(): Observable<HttpResponse<any>> {
    const uri = `/v1/reseauassainissement`;
    const headers = new HttpHeaders();
    const params = new HttpParams();
    return this.sendRequest<any>('get', this.domainGis + uri, headers, params, null);
  }
    
  private sendRequest<T>(method: string, url: string, headers: HttpHeaders,
                          params: HttpParams, body: any): Observable<HttpResponse<T>> {
        let requestObservable: Observable<HttpResponse<T>> = null;
    	if (method === 'get') {
      requestObservable = this.http.get<T>(
        url,
        {
          headers: headers.set('Accept', 'application/json'),
          params: params,
          observe: 'response'
        }
      );
    } else if (method === 'put') {
      requestObservable = this.http.put<T>(
        url, body,
        {
          headers: headers.set('Content-Type', 'application/json'),
          params: params,
          observe: 'response'
        }
      );
    } else if (method === 'post') {
      requestObservable = this.http.post<T>(
        url, body, 
        {
          headers: headers.set('Content-Type', 'application/json'),
          params: params,
          observe: 'response'
        }
      );
    } else if (method === 'delete') {
      requestObservable = this.http.delete<T>(
        url,
        {
          headers: headers,
          params: params,
          observe: 'response'
        }
      );
    } else {
      return Observable.throw('Unsupported request: ' + method);
    }
    /*requestObservable.subscribe(
      resp => {
        const errorObject: any = resp.body;
        this.checkErrorService.checkRemoteData(errorObject);
      }
    );*/
    return requestObservable;
  }
  
}
