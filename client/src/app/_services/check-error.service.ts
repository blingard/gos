import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService } from './alert.service';
import { ErrorObject } from './../_generated/exports';

@Injectable()
export class CheckErrorService {

  constructor(
    private router: Router,
    private alertService: AlertService
  ) { }

  checkRemoteData (data: ErrorObject): boolean {
    if (data == null || (!data.errorCode && !data.errorText)) {
      return true;
    }
    if (data.errorCode === 404) {
      this.router.navigate(['/error-404']);
      return false;
    }
    if (data.errorCode === 500) {
      this.router.navigate(['/error-500']);
      return false;
    }
    this.alertService.error(data.errorText);
    return false;
  }
}
