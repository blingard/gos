import { TestBed, inject } from '@angular/core/testing';

import { ActionOnModelsService } from './action-on-models.service';

describe('ActionOnModelsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActionOnModelsService]
    });
  });

  it('should be created', inject([ActionOnModelsService], (service: ActionOnModelsService) => {
    expect(service).toBeTruthy();
  }));
});
