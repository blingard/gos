import { AccessRight } from './../_generated/models/accessRight.model';
import { Injectable } from '@angular/core';
import {Agent, AgentDevice, Alerts, AgentSector, AccessRightEmployeeGroup, Employee, Sector, Device, Site, GlobalEmployee, EmployeeGroup, EmployeeGroupEmployee,District } from '../_generated/exports';

@Injectable()
export class ActionOnModelsService {

  defaultValues: any;

  constructor() {
    this.defaultValues = {
      statusActivated : 0,
      statusDeactivated : 1,
      statusDeleted : 2,
      endDateDeleted :null
    };
  }

  isSiteActivated(site: Site) {
    return site.status === this.defaultValues.statusActivated;
  }

  isSiteDeactivated(site: Site) {
    return site.status === this.defaultValues.statusDeactivated;
  }

  isSiteDeleted(site: Site) {
    return site.status === this.defaultValues.statusDeleted;
  }
  isAgentDeleted(agent: Agent) {
    return agent.status === this.defaultValues.statusDeleted;
  }
 

  isDeviceDeleted(device: Device) {
    return device.status === this.defaultValues.statusDeleted;
  }
  isSectorDeleted(sector: Sector) {
    return sector.status === this.defaultValues.statusDeleted;
  }
  isDistrictDeleted(district: District) {
    return district.status === this.defaultValues.statusDeleted;
  }
  isEmployeeDeleted(employee: Employee) {
    return employee.status === this.defaultValues.statusDeleted;
  }

  isAccessRightDeleted(acces: AccessRight) {
    return acces.accessRightStatus === this.defaultValues.statusDeleted;
  }

  isAdminConnected(empl: GlobalEmployee) {
    for (let i = 0; i < empl.rightsList.length; i++) {
      const element = empl.rightsList[i].accessRightName;
      if (element == 'administrator_right' || element == 'All') {
        return true;
      }
    }
    return false;
  }

  
  isEmployeeGroupDeleted(employeeGroup: EmployeeGroup) {
    return employeeGroup.groupStatus === this.defaultValues.statusDeleted;
  }
  isEmployeeGroupEmployeeDeleted(empGemp: EmployeeGroupEmployee) {
    return empGemp.endDate != this.defaultValues.endDateDeleted;
  }
  isAgentDeviceDeleted(agDevice: AgentDevice) {
    return agDevice.endDate != this.defaultValues.endDateDeleted;
  }
  isAgentSectorDeleted(agSector: AgentSector) {
    return agSector.endDate != this.defaultValues.endDateDeleted;
  }
  isAlertsDeleted(alert: Alerts) {
    return alert.status != this.defaultValues.statusDeleted;
  }
  
  isAccessRightEmployeeGroupDeleted(arempGroup: AccessRightEmployeeGroup) {
    return arempGroup.endDate != this.defaultValues.endDateDeleted;
  }
  getTenFirstCharacter(chaine:string):string{
    {
      var sortie = '';
      if (chaine.length>15){
        for(var i=0; i<15; i++) 
            sortie = sortie + chaine.charAt(i);
        sortie = sortie+'...'
      }else{
        sortie = chaine;
      }
      return sortie;
    }

  }
  formatDate(chaine:string):string{
    var sortie = '';
	  if (chaine != null) {
		  if (chaine.length>10){
			for(var i=0; i<10; i++) 
				sortie = sortie + chaine.charAt(i);
			sortie = sortie
		  }else{
			sortie = chaine;
		  }
		  return sortie;
		}
		else {
			return sortie;
		}
  }

  formatDate17Characters(chaine:string):string{
    var sortie = '';
	  if (chaine != null) {
		  if (chaine.length>19){
			for(var i=0; i<19; i++) 
				sortie = sortie + chaine.charAt(i);
			sortie = sortie
		  }else{
			sortie = chaine;
		  }
		  return sortie;
		}
		else {
			return sortie;
		}
  }


}
