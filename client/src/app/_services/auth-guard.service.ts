import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable, Subscription } from "rxjs";
import { AuthenticationService } from "./authentication.service";
import { LocalDaoService } from "./local-dao.service";

@Injectable()
export class AuthGuard implements CanActivate  {

    isEmployeeConnected : Subscription;
    isConnected : boolean;


    constructor(private authService: AuthenticationService, private authenticationService: AuthenticationService,
                private router: Router,private localDaoService: LocalDaoService
        ){}
    canActivate(
        route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot
    ): boolean | Observable<boolean > | Promise<boolean > {
        this.authService.logSubject.subscribe(
            (isConnected: boolean)=>{
                this.isConnected = isConnected;              
            }
        );
        this.authService.emitLog();
        /*if(this.localDaoService.existToken()){ 
            alert("existToken");     
            if(this.localDaoService.checkExpiration()==false){ 
              alert("checkExpiration");  
              if(this.localDaoService.existSession()){ 
                alert("existSession");  
                this.router.navigate(['/dashboard']);
              }else{
          
              }
            }
        }
          }*/
        if(this.isConnected){
            return true;
        }else{
            this.authenticationService.logout();
            this.router.navigate(['/authentication']);

        }

    }

}