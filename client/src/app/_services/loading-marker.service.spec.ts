import { TestBed, inject } from '@angular/core/testing';

import { LoadingMarkerService } from './loading-marker.service';

describe('LoadingMarkerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadingMarkerService]
    });
  });

  it('should be created', inject([LoadingMarkerService], (service: LoadingMarkerService) => {
    expect(service).toBeTruthy();
  }));
});
