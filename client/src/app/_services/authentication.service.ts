import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Agent, Employee, GlobalEmployee } from '../_generated/exports';
import { LocalDaoService } from './local-dao.service';

@Injectable()
export class AuthenticationService {

    private isEmployeeConnected : boolean = false;
    private isCUMember : boolean = false;
    logSubject = new Subject<boolean>();
    isCUMemberSubject = new Subject<boolean>();

    constructor(private localDaoService: LocalDaoService) {
        this.isEmployeeConnected = false;
     }

    emitLog(){
        this.logSubject.next(this.isEmployeeConnected);
    }

    emitLogIsCUMember() {
        this.isCUMemberSubject.next(this.isCUMember);
    }

    logout() {
        this.localDaoService.removeData('currentEmployee');
       // this.localDaoService.clearMemory();
        this.isEmployeeConnected = false;
        this.emitLog();
    }
    

    enableHeaderBar() {
        this.isCUMember = true;
        this.emitLogIsCUMember();
    }

    disableHeaderBar() {
        this.isCUMember = false;
        this.emitLogIsCUMember();
    }

    isConnected(): boolean {
        this.isEmployeeConnected = this.localDaoService.exists('currentEmployee');
        this.emitLog();
        return this.isEmployeeConnected;
    }

    //Gestion des employés
    connectEmployee(empl: GlobalEmployee, remember: boolean) {
        this.localDaoService.save('currentEmployee', empl);
        this.isEmployeeConnected = true;
        this.emitLog();
    }

    isEmployeeRemembered(): boolean {
        return this.localDaoService.exists('rememberEmployee');
    }

    getcurrentGlobalEmployee(): GlobalEmployee {
        return this.localDaoService.getGlobalEmployee('currentEmployee');
    }

    isEmplConnected(): boolean {
        this.isEmployeeConnected = this.localDaoService.exists('currentEmployee');
        this.emitLog();
        return this.isEmployeeConnected;
    }
}
