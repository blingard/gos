import { TestBed, async, inject } from '@angular/core/testing';

import { ChildGuardGuard } from './child-guard.guard';

describe('ChildGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChildGuardGuard]
    });
  });

  it('should ...', inject([ChildGuardGuard], (guard: ChildGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
