import { TestBed, inject } from '@angular/core/testing';

import { CheckErrorService } from './check-error.service';

describe('CheckErrorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckErrorService]
    });
  });

  it('should be created', inject([CheckErrorService], (service: CheckErrorService) => {
    expect(service).toBeTruthy();
  }));
});
