import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthenticationService, AlertService } from './_services/exports';
import { BnNgIdleService } from 'bn-ng-idle'; // import it to your component

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'gos';
  notConnected = true;

  constructor(
    private router: Router,
    private location: Location,
    private authenticationService: AuthenticationService,
    private idleTimeOut : BnNgIdleService,
    private alertService: AlertService,
  ) {}

  ngOnInit(): void {
    this.idleTimeOut.startWatching(1800).subscribe((time)=>{
      if(this.authenticationService.isConnected()){
      console.log("Finish : " +time);
      this.authenticationService.logout();
      this.router.navigate(["/login"]);
      this.alertService.error("Votre Session a été désactivée: Reconnectez-vous");
    }
    })
    this.notConnected = !this.authenticationService.isConnected();
  }

  checkAuthState(): void {

  }
}
