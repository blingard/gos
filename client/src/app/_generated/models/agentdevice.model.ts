import { AgentDevicePK } from './agentdevicepk.model';
import { Agent } from './agent.model';
import { Device } from './device.model';

export interface AgentDevice {
  endDate: string;
  agentDevicePK: AgentDevicePK;
  agent: Agent;
  device: Device;
}
