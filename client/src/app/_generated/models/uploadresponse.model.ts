export interface UploadResponse {
  uploadLocation: string;
  uploadDate: string;
}
