import { AgentActivity } from "../exports";

export interface AlertsSystems {
    agentActivitiesConvocation: AgentActivity;
    days: number;
  }