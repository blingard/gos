
export interface AgentSectorPK {
  agentId: number;
  sectorId: number;
  beginDate: string;
}
