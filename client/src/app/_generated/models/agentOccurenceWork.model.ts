import { Agent } from './agent.model';

export interface AgentOccurenceWork {
    agent: Agent;
    numberOfActions: number;
  }