
export interface EmployeeGroupEmployeePK {
  employeeGroupId: number;
  employeeId: number;
  fromDate: string;
}
