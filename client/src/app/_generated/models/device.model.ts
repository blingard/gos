import { AgentDevice } from './agentdevice.model';

export interface Device {
  deviceId: number;
  deviceName: string;
  status: number;
  deviceImei: string;
  description: string;
  createddate: string;
  agentDeviceList: AgentDevice[];
}
