import { Employee } from './employee.model';
import { EmployeeAccessRights } from './employeeAccessRights.model';
export interface GlobalEmployee {
  employee: Employee;
  rightsList: EmployeeAccessRights [];
}
