export interface ReseauAssainissament{
    etat:String;
    hierarchie:String,
    longueur:Number,
    name:String,
    numero:number,
    coordonnee:Array<any>
  }