import { DocType } from './doctype.model';
import { Site } from './site.model';

export interface Documents {
  documentId: number;
  documentName: string;
  documentCode: string;
  documentNumber?:string;
  documentPath: string;
  secretKey: string;
  documentExtension: string;
  createdDate: string;
  status: number;
  docTypeId: DocType;
  siteId: Site
}
