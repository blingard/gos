export interface Rules {
    rulesId: number;
    rulesName: string;
    description: string;
    sousZone: string;
    code: string;
    localisation: string;
    superficieConstructible: string;
    hauteurMaximale: string;
    coefficientOccupationSol: string;
    coefficientEmpriseSol: string;
    reculRapportAuxVoies: string;
    implantationLimitesSeparatives: string;
    distanceEntreConstructions: string;
    espaceLibre: string;
    espacePlante: string;
    status: number;
  }
  