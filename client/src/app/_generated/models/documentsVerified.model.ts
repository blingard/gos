import { Apis } from './apis.model';
export interface DocumentsVerified {
    documentId: number;
    documentName: string;
    documentCode: string;
    documentNumber?:string;
    documentPath: string;
    secretKey: string;
    createdDate: string;
    status: number;
    apisSender: Apis;
    apisReceder: Apis;
    gosDocId: number;
    lastUpdate: string;
  }