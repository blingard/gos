import { AlertsDocuments } from './AlertsDocuments.model';
export interface Alerts {
  alertsId: number;
  alertsMessage: string;
  personName: string;
  alertsDescription: string;
  personPhone: string;
  alertsDistrict: string;
  alertsDate: string;
  status: number;
  alertsDocumentsList:AlertsDocuments[]
}
