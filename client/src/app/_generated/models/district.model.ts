import { Site } from './site.model';
import { Sector } from './sector.model';

export interface District {
  districtId: number;
  districtName: string;
  commune: string;
  standing: string;
  population: string;
  densite: string;
  description: string;
  status: number;
  siteList: Site[];
  sectorId: Sector;
}
