import { Coordonee } from './coordonate.model';

export interface Quartier{
    name:String;
    commune:String;
    standing:String;
    data:Array<Coordonee>;
    coordonnee:Array<any>
  }