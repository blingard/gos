import { AgentSectorPK } from './agentsectorpk.model';
import { Agent } from './agent.model';
import { Sector } from './sector.model';

export interface AgentSector {
  endDate: string;
  agentSectorPK: AgentSectorPK;
  agent: Agent;
  sector: Sector;
}
