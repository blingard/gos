import { NotificationPK } from './noticationPK.model';

export interface Notification {
    notifMessage: String;
    status: number;
    notificationPK: NotificationPK;
}