
export interface AccessRightEmployeeGroupPK {
  employeeGroupId: number;
  accessRightId: number;
  fromDate: string;
}
