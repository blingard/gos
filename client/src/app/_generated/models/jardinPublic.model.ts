export interface Jardin{
    name:String;
    leisure:String,
    tourism:String,
    localisation:String,
    coordonnee:Array<any>,
    sport:String
  }