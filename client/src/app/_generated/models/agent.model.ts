import { AgentDevice } from './agentdevice.model';
import { AgentSector } from './agentsector.model';

export interface Agent {
  agentId: number;
  agentName: string;
  agentLogin: string;
  agentPassword: string;
  isFirstConnection: boolean;
  status: number;
  agentPhoneNumber: string;
  lastConnectionDate?: string;
  token?: string;
  agentDeviceList: AgentDevice[];
  agentSectorList: AgentSector[];
}
