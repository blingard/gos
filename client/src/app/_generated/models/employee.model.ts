export interface Employee {
  employeeId: number;
  employeeName: string;
  employeeSurname: string;
  employeeEmail: string;
  employeePassword: string;
  employeePhoneNumber: string;
  employeeMatricule: string;
  employeeLogin: string;
  employeeBirthdate: string;
  employeePlaceOfBirth: string;
  employeeNationality: string;
  employeeAdress: string;
  employeeGender: string;
  token?: string;
  lastConnectionDate?: string;
  isFirstConnection?: boolean;
  status: number;
}
