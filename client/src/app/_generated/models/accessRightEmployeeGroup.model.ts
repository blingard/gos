import { AccessRightEmployeeGroupPK } from './accessRightEmployeeGroupPK.model';
import { AccessRight } from './accessRight.model';
import { EmployeeGroup } from './employeeGroup.model';

export interface AccessRightEmployeeGroup {
  endDate: string;
  accessRightEmployeeGroupPK: AccessRightEmployeeGroupPK;
  accessRight: AccessRight;
  employeeGroup: EmployeeGroup;
}
