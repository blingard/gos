
export interface AgentDevicePK {
  agentId: number;
  deviceId: number;
  beginDate: string;
}
