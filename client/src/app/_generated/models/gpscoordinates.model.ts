import { Site } from './site.model';

export interface GpsCoordinates {
  gpsCoordId: number;
  pointName: string;
  xcoord: number;
  ycoord: number;
  siteId: Site;
}
