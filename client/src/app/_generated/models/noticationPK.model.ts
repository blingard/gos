
export interface NotificationPK {
    agentId: number;
    employeeId: number;
    notifDate: String;
  }