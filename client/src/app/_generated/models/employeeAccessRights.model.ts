export interface EmployeeAccessRights {
  employeeId: number;
  groupId: number;
  accessRightId: number;
  accessRightName: string;
}
