import { Alerts } from './alerts.model';
import { AlertsDocuments } from './alertsDocuments.model';

export interface ViewsAlertsDocuments {
  alertsDocuments: AlertsDocuments;
  alerts: Alerts;
  
}
