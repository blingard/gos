import { Documents } from './documents.model';

export interface DocType {
  docTypeId: number;
  docTypeName: string;
  description: string;
  documentsList: Documents[];
}
