export interface ErrorObject {
  errorCode: number;
  errorText: string;
}
