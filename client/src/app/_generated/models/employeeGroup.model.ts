import { EmployeeGroup } from './employeeGroup.model';
import { Employee } from './employee.model';
import { EmployeeGroupEmployee } from './employeeGroupEmployee.model';
import { AccessRightEmployeeGroup } from './accessRightEmployeeGroup.model';

export interface EmployeeGroup {
  employeeGroupId: number;
  groupName: string;
  groupStatus: number;
  employeeGroupEmployeeList: EmployeeGroupEmployee[];
  accessRightEmployeeGroupList: AccessRightEmployeeGroup[];
  
}
