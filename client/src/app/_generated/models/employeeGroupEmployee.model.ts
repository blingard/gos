import { Employee } from './employee.model';
import { EmployeeGroup } from './employeeGroup.model';
import { EmployeeGroupEmployeePK } from './employeeGroupEmployeepk.model';

export interface EmployeeGroupEmployee {
  endDate: string;
  employee: Employee;
  employeeGroupEmployeePK: EmployeeGroupEmployeePK;
  employeeGroup: EmployeeGroup;
}
