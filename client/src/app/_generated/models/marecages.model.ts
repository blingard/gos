import { Coordonee } from './coordonate.model';

export interface Marecages {
    vocation:String;
    indication:String;
    data:Array<Coordonee>;
    coordonnee:Array<any>
  }