import { Coordonee } from './coordonate.model';

export interface Zonesinondables {
    idZone:String;
	bv:String;
	sbv:String;
    lieuDit:String;
    commune:String;
    hMax: Number;
	origine:String;
	sSbv:String;
	codeM2:String;
	actionsM2:String;
	notesM2:String;
	classeM1:String;
    data:Array<Coordonee>;
    coordonnee:Array<any>
  }