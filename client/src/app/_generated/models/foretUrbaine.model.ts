import { Coordonee } from './coordonate.model';

export interface Foret{
    name:String;
    superficie:Number;
    gid:number;
    data:Array<Coordonee>;
    coordonnee:Array<any>
  }