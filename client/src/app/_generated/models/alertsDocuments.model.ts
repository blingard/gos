import { Alerts } from './alerts.model';

export interface AlertsDocuments {
  documentId: number;
  documentName: string;
  documentPath: string;
  secretKey: string;
  documentExtension: string;
  createdDate: string;
  status: number;
  docType: string;
  alertsId: Alerts;
}
