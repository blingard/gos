
import { AgentSector } from './agentsector.model';

export interface Sector {
  sectorId: number;
  sectorName: string;
  description: string;
  status: number;
  agentSectorList: AgentSector[];
}
