
import { Agent } from './agent.model';
import { Documents } from './documents.model';

export interface AgentActivity {
    agentActivityId: number;
    agentAction: String;
    agentActivityDate : String;
    status: number;
    agentId : Agent;
    documentId: Documents
    documentslist: Documents[];
  }
  