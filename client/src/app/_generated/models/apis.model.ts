import {DocumentsVerified} from './documentsVerified.model'
export interface Apis{
    apisId:number;
    apisCode:number;
    apisName:String;
    apisDescription:String;
    createdDate: string;
    status:number;
    documentsList: DocumentsVerified[];
    documentsList1: DocumentsVerified[];
}