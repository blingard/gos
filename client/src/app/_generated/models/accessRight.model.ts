import { AccessRightEmployeeGroup } from './accessRightEmployeeGroup.model';

export interface AccessRight {
  accessRightId: number;
  accessRightName : string;
  accessRightStatus :  number;
  accessRightEmployeeGroupList: AccessRightEmployeeGroup [];
  }
