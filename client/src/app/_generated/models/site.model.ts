import { District } from './district.model';
import { Documents } from './documents.model';
import { GpsCoordinates } from './gpscoordinates.model';

export interface Site {
  siteId: number;
  siteName: string;
  refSite: string;
  ownerName: string;
  ownerPhone: string;
  ownerCni: string;
  description: string;
  status: number;
  districtId: District; 
  documentsList: Documents[];
  gpsCoordinatesList : GpsCoordinates[];
  dateToFinish? : String;
  dateToStop?: String;
}
