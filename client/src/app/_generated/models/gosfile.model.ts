export interface GosFile  {
  name: string;
  size: number;
  type: string;
  fileToUpload: File;
}
