import { Coordonee } from './coordonate.model';

export interface PlanZonnage{
    zone_habia:String;
    vocation:String;
    ces:String;
    cos:String;
    autoriser:String;
    interdicti:String;
    surfaceMa:String;
    restrictio:String;
    largeurVo:String;
    hauteurMa:String;
    reculImpo:String;
    reculImpo12:String;
    reculIm:String;
    profondeur:String;
    estEncour:String;
    stationHa:String;
    stationEt:String;
    stationAu:String;
    data:any;
    coordonnee:Array<any>
  }