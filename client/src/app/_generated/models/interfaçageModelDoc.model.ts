
import { Apis } from './apis.model';
import { DocumentsVerified } from './documentsVerified.model';

export interface InterfaçageModelDoc {
  chemin: string;
  acteName: string;
  acteCode:string;
  documentselect:DocumentsVerified
}